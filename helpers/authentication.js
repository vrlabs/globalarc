var bcrypt = require('bcrypt-nodejs');

module.exports.generateHash = function (password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
}

module.exports.validPassword = function (password) {
    return bcrypt.compareSync(password, this.local.password);
}

module.exports.isLoggedIn = function (req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    
    res.redirect('/auth');
}