var http = require('https');
var config = require('../config/config');
var language = require('../config/languages/' + config.config.language);

// create new account
module.exports.createNewAccount = function(email, callback) {
 
    return http.get({    
        host: config.andruav.url,
        port : config.andruav.port,
        method : 'GET',
        path: config.andruav.createNewAccount + email + '&nemail=true',
        rejectUnauthorized: false
    }, function(result) {
        var str = '';
        result.on('data', function(chunk) {
            str += chunk;
        });
        result.on('end', function() {
            if (result.statusCode == 200 && result.statusMessage == 'OK') {
                var obj = JSON.parse(str);
                if (obj.Err == 0) {
                    callback(1, obj.pwd);
                } else {
                    callback(0, obj.msg);
                }
            }
        });
        result.on('error', function (e) {
            console.log(e);
            callback(0, language.error);
        });
    }).on('error', function(err) {
        console.error(err.message);
    });
}

// new access code is generated and sent to an email
module.exports.regenerateNewAccessCode = function(email, callback) {
    return http.get({
        host: config.andruav.url,
        port : config.andruav.port,
        method : 'GET',
        path: config.andruav.regenerateAccessCode + email,
        rejectUnauthorized: false
        
    }, function(result) {
        var str = '';
        result.on('data', function(chunk) {
            str += chunk;
        });
        result.on('end', function() {
            if (result.statusCode == 200 && result.statusMessage == 'OK') {
                var obj = JSON.parse(str);
                if (obj.Err == 0) {
                    callback(1, obj.msg);
                } else {
                    callback(0, obj.msg);
                }
            }
        });
        result.on('error', function (e) {
            console.log(e);
            callback(0, language.error);
        });
    }).on('error', function(err) {
        console.error(err.message);
    });
}

// current access code is sent to an email
module.exports.retrieveAccessCode = function(email, callback) {
    return http.get({
        host: config.andruav.url,
        port : config.andruav.port,
        method : 'GET',
        path: config.andruav.retreiveAccessCode + email,
        rejectUnauthorized: false
        
    }, function(result) {
        var str = '';
        result.on('data', function(chunk) {
            str += chunk;
        });

        result.on('end', function () {
            if (result.statusCode == 200 && result.statusMessage == 'OK') {
                var obj = JSON.parse(str);
                if (obj.Err == 0) {
                    callback(1, obj.msg);
                } else {
                    callback(0, obj.msg);
                }
            }
        });
        
        result.on('error', function (e) {
            console.log(e);
            callback(0, language.error);
        });
    }).on('error', function(err) {
        console.error(err.message);
    });
}


// login to andruav app
module.exports.login = function (email, accessCode, callback) {
    return http.get({
        host: config.andruav.url,
        port : config.andruav.port,
        method : 'GET',
        path: config.andruav.loginCommand + email + config.andruav.loginPassword + accessCode + config.andruav.loginVersion,
        rejectUnauthorized: false
    }, function(result) {
        var str = '';
        result.on('data', function(chunk) {
            str += chunk;
        });
        
        result.on('end', function() {
            if (result.statusCode == 200 && result.statusMessage == 'OK') {
                var obj = JSON.parse(str);
                if (obj.Err == 0) {
                    callback(1, { loginCode: obj.sid, ip: obj.server.ip, port: obj.server.port});
                } else {
                    callback(0, obj.msg);
                }
            }
        });
        
        result.on('error', function (e) {
            console.log(e);
            callback(0, language.error);
        });
    }).on('error', function(err) {
        console.error(err.message);
    });
}
