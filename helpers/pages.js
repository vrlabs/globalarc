var config = require('../config/config').config;
var plans = require('../config/config').plans;
var language = require('../config/languages/' + config.language);
var url = require('url');
var User = require('../models/user');
var moment = require('moment');
var general = require('../helpers/general');

module.exports.renderFrontEndPage = function (res, page, title, params) {
    var fullTitle = config.title;

    if (title != '') {
        fullTitle += ' | ' + title;
    }

    res.render(page, {
        documentTitle: fullTitle,
        version: config.version,
        year: config.year,
        copyright: config.copyright,
        params: params
    });
}

module.exports.renderLoggedOutPage = function (res, page, title, message, color, params) {
    var fullTitle = config.title;

    if (title != '') {
        fullTitle += ' | ' + title;
    }

    res.render(page, {
        environment: config.environment,
        documentTitle: fullTitle,
        version: config.version,
        year: config.year,
        copyright: config.copyright,
        message: message,
        color: color,
        params: params,
        sample: false
    });
}


module.exports.renderLoggedInPage = function (req, res, page, title, message, color, params) {
    var user = req.user;

    // user name & photo
    var displayName = '';
    var photo = '';

    if (user.provider !== 'local') {
        displayName = eval('user.' + user.provider + '_display_name');
        photo = '';

        if (eval('user.' + user.provider + '_photo') != undefined && eval('user.' + user.provider + '_photo') != 'null' && eval('user.' + user.provider + '_photo') != '') {
            photo = eval('user.' + user.provider + '_photo');
        } else {
            photo = '/assets/images/avatar.png';
        }
    } else {
        displayName = user.name;
        if (user.photo != undefined && user.photo != 'null' && user.photo != '') {
            photo = user.photo;
        } else {
            photo = '/assets/images/avatar.png';
        }        
    }
    var groupName = '';
    var unitID = '';
    var vehicleID = '';
    
    if (page == 'flightplans/view') {
        groupName = params.groupName;
        unitID = params.unitID;
        vehicleID = params.vehicleID;
    }

    var accessCode = user.access_code;    
    if (page == 'user/profile' || page == 'user/settings' || page == 'help') {
        accessCode = '';
    }

    // page title
    var fullTitle = config.title;

    if (title != '') {
        fullTitle += ' | ' + title;
    }

    // alerts & warnings
    var warning = getWarnings(req.cookies, url.parse(req.url).pathname, user);

    var intercomData = {
        fullName: displayName,
        email: user.email,
        createdAt: moment(user.created_at).unix()
    }
    var data = {
        environment: config.environment,
        appName: config.title,
        documentTitle: fullTitle,
        version: config.version,
        year: config.year,
        copyright: config.copyright,
        userID: user.id,
        userDisplayName: displayName,
        userPhoto: photo,
        userRole: user.role,
        message: message,
        color: color, 
        warning: warning,
        params: params,
        accessCode: accessCode,
        groupName: groupName,
        unitID: unitID,
        vehicleID: vehicleID,
        companyName: user.company_name,
        companyEmail: user.company_email,
        permission: user.permission,
        companyPlan: user.plan,
        companyCreated: user.company_created,
        sample: true,
        intercomData: intercomData,
        deleted: user.deleted
    }

  
    var menu = getMenu(req, res, page, user.id, user.company_id, data);   
};

function getWarnings(cookies, currentPage, user) {
    var warnings = '';
    
    if (cookies.GlobalARCAlert === undefined || (cookies.GlobalARCAlert && cookies.GlobalARCAlert === '0')) {
        if (user.permission_id === 1 && user.company_set === 0 && currentPage !== '/settings') {
            warnings += language.companyInfoNotSet;
        }
    }
    
    return warnings;
};

function getMenu(req, res, page, userID, companyID, data) {
    User.getMenus(userID, companyID, function (err, projects, members) {
        
        if (err) {
            console.log(err);
        } else {
            var pastFlights = [];
            var currentFlights = [];
            var futureFlights = [];

            var now = moment();

            var sampleFlightplanID = 0;
            var sampleProjectID = 0;

            var sampleProjectSet = false;
            var sampleFlightplanSet = false;

           
            for (var i = 0; i < projects.length; i++) {
                if (!sampleProjectSet) {
                    if (projects[i].sample = 1) {
                        sampleProjectID = projects[i].id;
                        sampleProjectSet = true;
                    }
                }
                    
                for (var j = 0; j < projects[i].flightplans.length; j++){
                    if (!sampleFlightplanSet) {
                        if (projects[i].flightplans[j].sample = 1) {
                            sampleFlightplanID = projects[i].flightplans[j].id;
                            sampleFlightplanSet = true;
                        }
                    }
                    
                    var start = moment(projects[i].flightplans[j].start_time);
                    var end = moment(projects[i].flightplans[j].end_time);

                    if (end < now) {
                        if (pastFlights.length < 2) {
                            pastFlights.push(projects[i].flightplans[j]);
                        }    
                    } else if (start <= now && now <= end) {
                        currentFlights.push(projects[i].flightplans[j]);
                        
                    } else if (start > now) {
                        if (futureFlights.length < 2) {
                            futureFlights.push(projects[i].flightplans[j]);
                        }    
                    }
                }
            }  
            

            if (req.user.company_set == 0) {
                data.companyMenu = '<a href="/user/settings" class="text-info"><span>Set your company info</span></a>';
            } else {
                data.companyMenu = '<a href="/team"><span>' + req.user.company_name + '</span></a>';
            }   
            
            data.menuProjects = projects;
            data.menuMembers = members;
            
            data.pastFlights = pastFlights;
            data.currentFlights = currentFlights;
            data.futureFlights = futureFlights;

            data.sampleFlightplanID = sampleFlightplanID;
            data.sampleProjectID = sampleProjectID;

            var companyCreated = req.user.company_created;
            var planExpirationDate = moment(companyCreated).add(1, 'months').format(config.usTimeFormat);

            data.planExpirationDate = planExpirationDate;            

            User.getPlanNumbers(req.user.plan, req.user.company_id, function (err, result) {
                data.numOfUsers = result.numOfUsers;
                data.numOfProjects = result.numOfProjects;
                data.numOfFlights = result.numOfFlights;

                data.numOfUsersPercent = result.numOfUsersPercent + '%';
                data.numOfProjectsPercent = result.numOfProjectsPercent + '%';
                data.numOfFlightsPercent = result.numOfFlightsPercent + '%';

                data.storagePercent = result.storagePercent + '%';
                data.storageHumanSize = result.storageHumanSize;

                data.availableDocStorageSpaceHuman = general.humanFileSize(result.availableDocStorageSpace);
                data.availableDocStorageSpace = result.availableDocStorageSpace;

                res.render(page, data);

            });
        }    
    });  
}