var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var methodOverride = require('method-override');

var config = require('../config/config');
var language = require('../config/languages/' + config.config.language);
var authentication = require('../helpers/authentication');
var pages = require('../helpers/pages');
var User = require('../models/user');


router.use(bodyParser.urlencoded({ extended: true }));

router.use(methodOverride(function(req, res) {
    if (req.body && typeof req.body === 'object' && '_method' in req.body) {
        // look in urlencoded POST bodies and delete it
        var method = req.body._method
        delete req.body._method
        return method
    }
}));

router.post('/email-available', authentication.isLoggedIn, function(req, res) {
    User.isEmailAvailable(req.body.email, function(err, result) {
        if (err) {
            res.send(false);
        } else {
            if (result == 0) {
                res.send(true);
            } else if (result == 1) {
                res.send(false);
            }
        }
    });
});

router.post('/email-available-existing', authentication.isLoggedIn, function(req, res) {
    if (req.body.email != req.user.email) {
        User.isEmailAvailable(req.body.email, function(err, result) {
            if (err) {
                res.send(false);
            } else {
                // email is available
                if (result == 0) {
                    res.send(true);
                    // email is not available
                } else if (result == 1) {
                    res.send(false);
                }
            }
        });
    } else {
        res.send(true);
    }
});

router.post('/company-available', authentication.isLoggedIn, function(req, res) {
    User.isCompanyNameAvailable(req.body.name, function(err, result) {
        if (err) {
            res.send(false);
        } else {
            if (result == 0) {
                res.send(true);
            } else if (result == 1) {
                res.send(false);
            }
        }
    });
});

router.post('/company-available-existing', authentication.isLoggedIn, function(req, res) {
    if (req.body.name != req.user.company_name) {
        User.isCompanyNameAvailable(req.body.name, function(err, result) {
            if (err) {
                res.send(false);
            } else {
                if (result == 0) {
                    res.send(true);
                } else if (result == 1) {
                    res.send(false);
                }
            }
        });
    } else {
        res.send(true);
    }
});



module.exports = router;