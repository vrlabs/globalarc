var aws = require('aws-sdk');
var config = require('../config/config');
var rmdir = require('rmdir');
var mkdirp = require('mkdirp');
var db = require('../config/db').pool;
var connectionError = require('../config/db').connectionError;

module.exports.randomString = function (len) {
    var buf = [];
    var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charlen = chars.length;

    for (var i = 0; i < len; ++i) {
        buf.push(chars[getRandomInt(0, charlen - 1)]);
    }

    return buf.join('');
};

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}


module.exports.getExtension = function (filename) {
    var i = filename.lastIndexOf('.');
    return (i < 0) ? '' : filename.substr(i);
}

module.exports.humanFileSize = function (bytes) {
    var si = config.config.sizeCoefficient;

    var thresh = si ? 1000 : 1024;
    if (Math.abs(bytes) < thresh) {
        return bytes + ' B';
    }
    var units = si
        ? ['kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
        : ['KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB'];
    var u = -1;
    do {
        bytes /= thresh;
        ++u;
    } while (Math.abs(bytes) >= thresh && u < units.length - 1);
    return bytes.toFixed(1) + ' ' + units[u];
}

module.exports.getContentTypeByFileName = function (fileName) {
    var rc = 'application/octet-stream';
    var fileNameLowerCase = fileName.toLowerCase();

    if (fileNameLowerCase.indexOf('.html') >= 0) rc = 'text/html';
    else if (fileNameLowerCase.indexOf('.css') >= 0) rc = 'text/css';
    else if (fileNameLowerCase.indexOf('.json') >= 0) rc = 'application/json';
    else if (fileNameLowerCase.indexOf('.js') >= 0) rc = 'application/x-javascript';
    else if (fileNameLowerCase.indexOf('.png') >= 0) rc = 'image/png';
    else if (fileNameLowerCase.indexOf('.jpg') >= 0) rc = 'image/jpg';
    else if (fileNameLowerCase.indexOf('.jpeg') >= 0) rc = 'image/jpeg';

    return rc;
}

module.exports.getCoordinatesArray = function (coordinates) {
    var result = [];
    var splitCoordinates = coordinates.split('#');
    for (var i = 0; i < splitCoordinates.length; i++) {
        var splitCoordinate = splitCoordinates[i].split(',');
        result.push([splitCoordinate[0], splitCoordinate[1]]);
    }
    return result;
}

module.exports.decodeBase64Image = function (dataString) {
    var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);
    var response = {};

    if (matches.length !== 3) {
        return new Error('Invalid input string');
    }

    response.type = matches[1];
    response.data = new Buffer(matches[2], 'base64');

    return response;
}

module.exports.getImageContentType = function (fileName) {
    var rc = 'application/octet-stream';
    var fileNameLowerCase = fileName.toLowerCase();

    if (fileNameLowerCase.indexOf('.png') >= 0) rc = 'image/png';
    else if (fileNameLowerCase.indexOf('.jpg') >= 0 || fileNameLowerCase.indexOf('.jpeg') >= 0) rc = 'image/jpg';

    return rc;
}

module.exports.getMapCenter = function (array) {
    var x = array.map(function (a) { return a[0] });
    var y = array.map(function (a) { return a[1] });
    var minX = Math.min.apply(null, x);
    var maxX = Math.max.apply(null, x);
    var minY = Math.min.apply(null, y);
    var maxY = Math.max.apply(null, y);
    return [(minX + maxX) / 2, (minY + maxY) / 2];
}

module.exports.getRandomColor = function () {
    var rgb = [];
    for (var i = 0; i < 3; i++) {
        rgb.push(Math.floor(Math.random() * 255));
    }
    return 'rgb(' + rgb.join(',') + ')';
}

function getMinMaxOf2DIndex(arr, idx) {
    return {
        min: Math.min.apply(null, arr.map(function (e) { return e[idx] })),
        max: Math.max.apply(null, arr.map(function (e) { return e[idx] }))
    }
}

module.exports.findBoundingBox = function (array) {
    var x = getMinMaxOf2DIndex(array, 0);
    var y = getMinMaxOf2DIndex(array, 1);

    var result = {};

    var latBuffer = (x.max - x.min) * 0.05;
    var lonBuffer = (y.max - y.min) * 0.05;

    result.minX = x.min - latBuffer;
    result.minY = y.min - lonBuffer;
    result.maxX = x.max + latBuffer;
    result.maxY = y.max + lonBuffer;

    return result;
}

module.exports.deleteTmpFiles = function () {
    var path = __dirname + '/../public/tmp';

    rmdir(path, function (err, dirs, files) {
        mkdirp(path, function (err) {
            if (err) {
                console.error(err);
            }
        });
    });
}

module.exports.emptyS3Bucket = function (bucketName, companyID, callback) {
    var s3 = new aws.S3();
    var params = {
        Bucket: bucketName,
        Prefix: companyID + '/'
    };

    s3.listObjects(params, function (err, data) {
        if (err) {
            return callback(err);
        }

        if (data.Contents.length == 0) {
            callback();
        }

        params = { Bucket: bucketName };
        params.Delete = { Objects: [] };

        data.Contents.forEach(function (content) {
            params.Delete.Objects.push({ Key: content.Key });
        });

        s3.deleteObjects(params, function (err, data) {
            if (err) {
                return callback(err);
            } else if (data.Deleted.length == 1000) {
                emptyS3Bucket(bucketName, companyID, callback);
            } else {
                callback();
            }
        });
    });
}

module.exports.removeDeletedCompanies = function () {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sqlSubscriptions = "DELETE FROM subscriptions WHERE deleted = 1";

        connection.query(sqlSubscriptions, function (err, rowsSubscriptions) {
            if (err) {
                console.log(err);
            } else {
                var sqlCompanies = "DELETE FROM companies WHERE deleted = 1";

                connection.query(sqlCompanies, function (err, rowsCompanies) {
                    if (err) {
                        console.log(err);
                    }
                });
            }
        });
    });
}

// This function returns the coordinate
// conversion string in DD to DMS.
module.exports.ddToDms = function(lat, lng) {

   var lat = lat;
   var lng = lng;
   var latResult, lngResult, dmsResult;

   // Make sure that you are working with numbers.
   // This is important in case you are working with values
   // from input text in HTML.
   lat = parseFloat(lat);  
   lng = parseFloat(lng);

   // Call to getDms(lat) function for the coordinates of Latitude in DMS.
   // The result is stored in latResult variable.
   latResult = getDms(lat);

   // Check the correspondence of the coordinates for latitude: North or South.
   latResult += (lat >= 0)? 'N' : 'S';

   // Call to getDms(lng) function for the coordinates of Longitude in DMS.
   // The result is stored in lngResult variable.
   lngResult = getDms(lng);

   // Check the correspondence of the coordinates for longitude: East or West.
   lngResult += (lng >= 0)? 'E' : 'W';

   

   // Joining both variables and separate them with a space.
   dmsResult = latResult + lngResult;

   // Return the resultant string.
   return dmsResult;
}

// Function that converts DMS to DD.
// Taking as example the value -40.601203.
function getDms(val) {

   // Required variables
   var valDeg, valMin, valSec, result;

   // Here you'll convert the value received in the parameter to an absolute value.
   // Conversion of negative to positive.
   // In this step does not matter if it's North, South, East or West,
   // such verification was performed earlier.
   val = Math.abs(val); // -40.601203 = 40.601203

   // ---- Degrees ----
   // Stores the integer of DD for the Degrees value in DMS
   valDeg = Math.floor(val); // 40.601203 = 40

   // Add the degrees value to the result by adding the degrees symbol "º".
   result = valDeg + 'd'; // 40º

   // ---- Minutes ----
   // Removing the integer of the inicial value you get the decimal portion.
   // Multiply the decimal portion by 60.
   // Math.floor returns an integer discarding the decimal portion.
   // ((40.601203 - 40 = 0.601203) * 60 = 36.07218) = 36
   valMin = Math.floor((val - valDeg) * 60); // 36.07218 = 36

   // Add minutes to the result, adding the symbol minutes "'".
   result += valMin + "m"; // 40º36'

   // ---- Seconds ----
   // To get the value in seconds is required:
   // 1º - removing the degree value to the initial value: 40 - 40.601203 = 0.601203;
   // 2º - convert the value minutes (36') in decimal ( valMin/60 = 0.6) so
   // you can subtract the previous value: 0.601203 - 0.6 = 0.001203;
   // 3º - now that you have the seconds value in decimal,
   // you need to convert it into seconds of degree.
   // To do so multiply this value (0.001203) by 3600, which is
   // the number of seconds in a degree.
   // You get 0.001203 * 3600 = 4.3308
   // As you are using the function Math.round(),
   // which rounds a value to the next unit,
   // you can control the number of decimal places
   // by multiplying by 1000 before Math.round
   // and subsequent division by 1000 after Math.round function.
   // You get 4.3308 * 1000 = 4330.8 -> Math.round = 4331 -> 4331 / 1000 = 4.331
   // In this case the final value will have three decimal places.
   // If you only want two decimal places
   // just replace the value 1000 by 100.
   valSec = Math.round((val - valDeg - valMin / 60) * 3600 * 1000) / 1000; // 40.601203 = 4.331 

   // Add the seconds value to the result,
   // adding the seconds symbol " " ".
   result += valSec + 's'; // 40º36'4.331"

   // Returns the resulting string.
   return result;
 }
