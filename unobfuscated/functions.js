// GLOBAL VALIDATION RULES
var validationRules = {
    highlight: function(element, errorClass) {
        $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function(element, errorClass) {
        $(element).closest('.form-group').removeClass('has-error');
    }
};

// COOKIES
function writeCookie(name, value, days) {
    var date;
    var expires;
    if (days) {
        date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    } else {
        expires = "";
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
    var cookie;
    var cookieName = name + "=";
    var cookies = document.cookie.split(';');
    for (var i = 0; i < cookies.length; i++) {
        cookie = cookies[i];
        while (cookie.charAt(0) == ' ') {
            cookie = cookie.substring(1, cookie.length);
        }
        if (cookie.indexOf(cookieName) == 0) {
            return cookie.substring(cookieName.length, cookie.length);
        }
    }
    return '';
}

// PASSWORD VALIDATION
function validatePassword() {
    var password = document.getElementById('password');
    var confirmPassword = document.getElementById('confirm_password');

    if (password.value != confirmPassword.value) {
        confirmPassword.setCustomValidity('Passwords don\'t match!');
    } else {
        confirmPassword.setCustomValidity('');
    }
}


// REGISTER
function submitRegisterForm() {
    var rules = validationRules;

    rules.rules = {
        name: {
            required: true
        },
        company_name: {
            required: true,
            remote: {
                url: '/validate/company-available',
                type: 'POST'
            }
        },
        email: {
            required: true,
            email: true,
            remote: {
                url: '/validate/email-available',
                type: 'POST'
            }
        },
        password: {
            required: true,
            rangelength: [6, 20]
        },
        confirm_password: {
            required: true,
            equalTo: '#password'
        }
    }

    rules.messages = {
        name: 'Please enter your name.',
        company: {
            required: 'Please enter your company name.',
            remote: jQuery.validator.format('{0} is already taken.')
        },
        email: {
            required: 'Please enter your email address.',
            email: 'Please enter a valid email address.',
            remote: jQuery.validator.format('{0} is already taken.')
        },
        password: {
            required: 'Please enter password.',
            rangelength: 'The password entered must be between 6-20 characters long.'
        },
        confirm_password: 'Passwords must match.'
    }

    $("#form-register").validate(rules);

    if ($('#form-register').valid()) {
        $('#form-register').submit();
    }
}

// PROFILE
function submitProfileForm() {
    var rules = validationRules;

    rules.rules = {
        name: {
            required: true
        },
        email: {
            required: true,
            email: true,
            remote: {
                url: '/validate/email-available-existing',
                type: 'POST'
            }
        },
        password: {
            rangelength: [6, 20]
        },
        confirm_password: {
            equalTo: '#password'
        }
    }

    rules.messages = {
        name: 'Please enter your name.',
        email: {
            required: 'Please enter your email address.',
            email: 'Please enter a valid email address.',
            remote: jQuery.validator.format('{0} is already taken.')
        },
        password: 'The password entered must be between 6-20 characters long.',
        confirm_password: 'Passwords must match.'
    }

    $("#form-profile").validate(rules);


    if ($('#form-profile').valid()) {
        var name = $('#name').val();
        var email = $('#email').val();
        var password = $('#password').val();
        
        $.ajax({
            type: 'POST',
            contentType: 'application/json',
            cache: false,
            url: '/user/profile-local',
            data: JSON.stringify({
                name: name,
                email: email,
                password: password
            }),
            success: function (data) {
                location.reload();
            },
            error: function(xhr, status, error) {
                alert(error);
            }
        });
    }
}

function submitProfileProfessionalInfoForm() {
    var rules = validationRules;

    rules.rules = {
        mobile: {
            required: true
        }
    }

    rules.messages = {
        mobile: 'Please enter your mobile phone.'
    }

    $("#form-profile-professional-info").validate(rules);

    if ($('#form-profile-professional-info').valid()) {
        var mobile = $('#mobile').val();
        $.ajax({
            type: 'POST',
            contentType: 'application/json',
            cache: false,
            url: '/user/profile-professional',
            data: JSON.stringify({
                mobile: mobile,
            }),
            success: function (data) {
                location.reload();
            },
            error: function(xhr, status, error) {
                alert(error);
            }
        });
    }
}


// SETTINGS
function submitSettingsForm() {
    var rules = validationRules;

    rules.rules = {
        name: {
            required: true,
            remote: {
                url: '/validate/company-available-existing',
                type: 'POST'
            }
        }
    }

    rules.messages = {
        name: 'Please enter your company name.'
    }

    $("#form-settings").validate(rules);

    if ($('#form-settings').valid()) {
        $('#form-settings').submit();
    }
}

// HIDDEN FIELDS
function setHiddenFieldValue(fieldID, value) {
    $('#' + fieldID).attr('value', value);
}

// MESSAGES
function closeWarning() {
    $.ajax({
        url: '/general/set-alert-cookie',
        type: "POST",
        contentType: 'application/json',
        success: function(data) {
            $('#warning').remove();
        },
        error: function(xhr, status, error) {
            alert(error);
        }
    });
}

function closeMessage() {
    $('#message').remove();
}
function hideMessage() {
    $('#message').addClass('hide');
}

// NOTES
function setNotes(note) {
    $('#modal-notes .modal-body').html(note);
}

// PROJECTS
function setDeleteProjectButton(id) {
    $('#modal-delete-button').attr('onclick', 'deleteProject(' + id + ');');
}

function deleteProject(id) {
    location.href = '/projects/delete/' + id;
}

// VEHICLES
function setDeleteVehicleButton(id) {
    $('#modal-delete-button').attr('onclick', 'deleteVehicle(' + id + ');');
}

function deleteVehicle(id) {
    location.href = '/vehicles/delete/' + id;
}

// BATTERIES
function setDeleteBatteryButton(id) {
    $('#modal-delete-button').attr('onclick', 'deleteBattery(' + id + ');');
}

function deleteBattery(id) {
    location.href = '/batteries/delete/' + id;
}

// DOCUMENTS
function setDeleteDocumentButton(id, file) {
    $('#modal-delete-button').attr('onclick', 'deleteDocument(' + id + ', \'' + file + '\');');
}

function deleteDocument(id, fileName) {
    $('#loader-content').addClass('active');
    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        cache: false,
        url: '/documents/delete',
        data: JSON.stringify({
            id: id,
            fileName: fileName
        }),
        success: function(data) {
            $('#loader-content').removeClass('active');
            data = JSON.parse(data);

            if (data.result == 'ok') {
                location.reload();
            }
        },
        error: function() {
            alert('Ooops! Something went wrong. Please, try again.');
        }
    });
}
// TEAM
function setDeleteMemberButton(userID) {
    $('#modal-delete-button').attr('onclick', 'deleteUser(' + userID + ');');
}
function deleteUser(id) {
    location.href = '/team/delete/' + id;
}
// FLIGHTPLANS
function setDeleteFlightplanButton(projectID, flightplanID) {
    $('#modal-delete-button').attr('onclick', 'deleteFlightplan(' + projectID + ', ' + flightplanID + ');');
}

function deleteFlightplan(projectID, flightplanID) {
    location.href = '/flightplans/delete/' + projectID + '/' + flightplanID;
}

function removeTeamMember(button) {
    var select = $(button).parent().find('select');
    $(select).val($(select).find('option.empty-option').val());
    checkTeamMembers();
}

function changeSaveButton(type) {
    var temp = type.charAt(0).toUpperCase() + type.slice(1);
    var html = '<a href="#" onclick="addNewEquipment(\'' + type + '\');" class="btn btn-default btn-info save-link pull-left">Save</a>';
    html += '<div class="alert hide wrapper-sm m-b-none pull-right" id="' + type + '-message"><div><span></span></div></div>';
    $('#modal-add-' + type + ' .submit-container').html(html);
}

function changeDocumentButton(type) {
    var html = '<a href="#" onclick="uploadNewDocument();" class="btn btn-default btn-info save-link pull-left">Save</a>';
    html += '<div class="alert hide wrapper-sm m-b-none pull-right" id="document-message"><div><span></span></div></div>';
    $('#modal-add-document .submit-container').html(html);
}

// ajax call to add new vehicle or battery from 'Add New Flightplan' page
function addNewEquipment(type) {
    var data = {};
    var url = '';

    if (type == 'vehicle') {
        data = {
            name: $('#vehicle-name').val(),
            type: $('input[name="type"]').val(),
            manufacturer: $('input[name="manufacturer"]').val(),
            model: $('input[name="model"]').val(),
            serial_number: $('#vehicle-serial-number').val(),
            status: $('select[name="status"]').val(),
            notes: $('#vehicle-notes').val()
        };
        url = '/vehicles/add-new-vehicle';

        var vehicleRules = validationRules;
        vehicleRules.rules = {
            name: "required",
            type: "required"
        }
        vehicleRules.messages = {
            name: "Vehicle name is required.",
            type: "Vehicle type is required."
        }

        $("#vehicle-form").validate(vehicleRules);


    } else if (type == 'battery') {
        data = {
            name: $('#battery-name').val(),
            serial_number: $('#battery-serial-number').val(),
            charge: $('input[name="charge"]').val(),
            voltage: $('input[name="voltage"]').val(),
            status: $('select[name="status"]').val(),
            notes: $('#battery-notes').val()
        };
        url = '/batteries/add-new-battery';

        var batteryRules = validationRules;
        batteryRules.rules = {
            name: "required",
            serial_number: "required",
            charge: {
                digits: true
            },
            voltage: {
                number: true
            }
        }
        batteryRules.messages = {
            name: "Battery name is required.",
            type: "Serial number is required.",
            charge: "Enter battery charge as integer number.",
            voltage: "Enter battery voltage as decimal number."
        }

        $("#battery-form").validate(batteryRules);
    }

    $('#' + type + '-message').attr('class', 'alert hide');
    $('#' + type + '-message span').html('');

    if ($('#' + type + '-form').valid()) {
        $.ajax({
            type: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json',
            dataType: 'json',
            cache: false,
            url: url,
            success: function(data) {
                if (data.result == 'ok') {
                    $('#' + type + '-message').attr('class', 'alert alert-success wrapper-sm m-b-none pull-right');
                    $('#' + type + '-message span').html('You successfully added new ' + type + '.');

                    // add new vehicle/battery in select element (create select if it doesn't exist)                
                    if (type == 'vehicle') {
                        $('#vehicle-error').html('');
                        if ($('#select-vehicle').length > 0) {
                            $('#select-vehicle').append('<option value="' + data.id + '" selected="selected">' + $('#vehicle-name').val() + '</option>');
                        } else {
                            html =  '<select name="vehicle" class="form-control m-b w-auto-xs w-xxl m-r-sm" id="select-vehicle">' +
                                        '<option value="' + data.id + '" selected="selected">' + $('#vehicle-name').val() + '</option>' +
                                    '</select>';
                            $('#vehicles').html(html)
                        }

                    } else if (type == 'battery') {
                        if ($('#select-battery').length > 0) {
                            $('#select-battery').append('<option value="' + data.id + '">' + $('#battery-name').val() + '</option>');
                            $('#additional-batteries select').each(function(index) {
                                $(this).append('<option value="' + data.id + '">' + $('#battery-name').val() + '</option>');
                            });

                                                       
                        } else {
                            html = 
                                
                                '<div class="battery-container">' +
                                    '<div class="col-lg-11 col-md-11 col-sm-11 col-xs-11 p-l-none p-r-none">' + 
                                        '<select name="battery" class="form-control m-b w-full" id="select-battery">' +
                                            '<option value="' + data.id + '" selected="selected">' + $('#battery-name').val() + '</option>' +
                                        '</select>' + 
                                    '</div > ' +
                                    '<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 p-r-none text-right">' + 
                                        '<a class="btn btn-sm btn-info m-t-xxs" href="#/" onclick="removeBattery(this);"><i class="fa fa-minus"></i></a>' + 
                                    '</div>' +
                                '</div>' +
                                '<div id="additional-batteries"></div>' +
                                '<a class="btn btn-info btn-addon m-r-sm" onclick="addAdditionalBattery();"><i class="fa fa-plus"></i>Add Additional Battery</a>';
                            $('#batteries').html(html)
                        }
                    }

                    // close modal window after timeout and clear all the fields
                    setTimeout(function() {
                        $('#modal-add-' + type).modal('hide');
                        if (type == 'vehicle') {
                            $('#vehicle-name').val('');
                            $('input[name="type"]').val('');
                            $('input[name="manufacturer"]').val('');
                            $('input[name="model"]').val('');
                            $('#vehicle-serial-number').val('');
                            $('select[name="status"]').val(1);
                            $('#vehicle-notes').val('');
                        } else if (type == 'battery') {
                            $('#battery-name').val('');
                            $('#battery-serial-number').val('');
                            $('input[name="charge"]').val('');
                            $('input[name="voltage"]').val('');
                            $('select[name="status"]').val(1);
                            $('#battery-notes').val('');
                        }
                    }, 2000);

                } else if (data.result == 'error') {
                    $('#' + type + '-message').attr('class', 'alert alert-danger wrapper-sm m-b-none pull-right');
                    $('#' + type + '-message span').html(data.messages);
                }
            },
            error: function() {
                $('#' + type + '-message').attr('class', 'alert alert-danger wrapper-sm m-b-none pull-right');
                $('#' + type + '-message span').html('Ooops! Something went wrong. Please, try again.');
            }
        });
    }
}

function addAdditionalBattery() {
    if ($('#select-battery').length > 0) {
        $('#select-battery').parent().parent().clone().appendTo('#additional-batteries');
        $('#additional-batteries select').each(function(index) {
            $(this).removeAttr('id');
        });
        $('#additional-batteries a').each(function(index) {
            $(this).css('display', 'inline-block');
        });
    } else {
        $.ajax({
            type: 'POST',
            contentType: 'application/json',
            cache: false,
            url: '/batteries/get-all-batteries',
            success: function(data) {
                if (data.result == 'ok') {
                    var html = '<div class="battery-container">' +
                        '<div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">' + 
                        '<select name="battery" class="form-control m-b w-full m-r-sm" id="select-battery">';
                    for (var i = 0; i < data.batteries.length; i++) {
                        html += '<option value="' + data.batteries[i].id + '">' + data.batteries[i].name + '</option>';
                    }
                    html += '</select></div>' + 
                        '<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 p-r-none text-right">' + 
                        '< a class="btn btn-sm btn-info m-t-xxs" href= "#/" onclick= "removeBattery(this);" > <i class="fa fa-minus"></i></a ></div > </div>';

                    $('#additional-batteries').before(html);
                }
            },
            error: function() {
                alert('Ooops! Something went wrong. Please, try again.');
            }
        });
    }
}

function removeBattery(button) {
    $(button).parent().parent().remove();
}

var teamMembers = [];
// ADD/EDIT FLIGHTPLAN
function checkTeamMembers() {
    var selectedTeamMembers = [];

    if ($('#manager').val() != '' || $('#pilot').val() != '' || $('#observer').val() != '' || $('#payload_operator').val() != '' || $('#remote_observer').val() != '') {
        if ($('#manager').val() != '') {
            selectedTeamMembers.push($('#manager').val());
        }
        if ($('#pilot').val() != '') {
            selectedTeamMembers.push($('#pilot').val());
        }
        if ($('#observer').val() != '') {
            selectedTeamMembers.push($('#observer').val());
        }
        if ($('#payload_operator').val() != '') {
            selectedTeamMembers.push($('#payload_operator').val());
        }
        if ($('#remote_observer').val() != '') {
            selectedTeamMembers.push($('#remote_observer').val());
        }

        if (hasDuplicates(selectedTeamMembers)){
            $('#team-error').html('One team member can have only one role!');
            return false;
        } else {
            $('#team-error').html('');
            return true;
        }
     } else {
        selectedTeamMembers = [];
        $('#team-error').html('At least one team member must be selected!');
        return false;
    }
}

function hasDuplicates(array) {
    return (new Set(array)).size !== array.length;
}

function checkVehicle() {
    if($('select#select-vehicle').length) {
        $('#vehicle-error').html('');
        return true;
    } else {
        $('#vehicle-error').html('Create vehicle first!');
        return false;
    }
}

function checkFlightTime() {
    var start = new Date($('input[name=start_time]').val());
    var end = new Date($('input[name=end_time]').val());

    if (start >= end) {
        $('#time-error').html('Start time has to be before the end time!');
        return false;
    }else{
        $('#time-error').html('');
        return true;
    }
}

 
var getCenter = function(array){
    var x = array.map(function(a){ return a[0] });
    var y = array.map(function(a){ return a[1] });
    var minX = Math.min.apply(null, x);
    var maxX = Math.max.apply(null, x);
    var minY = Math.min.apply(null, y);
    var maxY = Math.max.apply(null, y);
    return [(minX + maxX)/2, (minY + maxY)/2];
}

function getMinMaxOf2DIndex (arr, idx) {
    return {
        min: Math.min.apply(null, arr.map(function (e) { return e[idx]})),
        max: Math.max.apply(null, arr.map(function (e) { return e[idx]}))
    }
} 

function findBoundingBox(array) {
    var x = getMinMaxOf2DIndex(array, 0);
    var y = getMinMaxOf2DIndex(array, 1);

    var result = {};

    var latBuffer = (x.max - x.min) * 0.05;
    var lonBuffer = (y.max - y.min) * 0.05;
    
    result.minX = x.min - latBuffer;
    result.minY = y.min - lonBuffer;
    result.maxX = x.max + latBuffer;
    result.maxY = y.max + lonBuffer;
    
    return result;
}

function fit(map, minX, minY, maxX, maxY) {
    var array = [];
    var minArray = [minX, maxY];
    var maxArray = [maxX, minY];
    array.push(minArray);
    array.push(maxArray);
    map.fitBounds(array);
}

 

function setPreviousLocationsDropdown(projectID) {
    Draw.deleteAll();
    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        cache: false,
        url: '/flightplans/get-locations/' + projectID,
        success: function (data) {
            if (data.result == 'ok') {
                var html = '<option value="">Choose Location</option>';
                for (var i = 0; i < data.locations.length; i++) {
                    html += '<option value="' + data.locations[i].id + '"><img src="' + data.locations[i].path + '" height="20"/>' + data.locations[i].name + '</option>';
                }

                $('#locationSelect').html(html);
                
                $('#previousLocationsButton').addClass('hidden');
                $('#newLocationButton').removeClass('hidden');
                
                $('#locationInputContainer').addClass('hidden');
                $('#locationSelectContainer').removeClass('hidden');

                $('#how_to_save').val('save');

                $('#map-error').css('display', 'none')
                
            } else {
                alert('Ooops! Something went wrong. Please, try again.');
            }
        },
        error: function() {
            alert('Ooops! Something went wrong. Please, try again.');
        }
    });
}

function setNewLocationInput() {
    $('#previousLocationsButton').removeClass('hidden');
    $('#newLocationButton').addClass('hidden');
    
    $('#locationInputContainer').removeClass('hidden');
    $('#locationSelectContainer').addClass('hidden');

    Draw.deleteAll();
}

function saveLocationAs(value) {
    $('#how_to_save').val(value);
    if (value == 'old' || value == 'save') {
        $('#locationInputContainer').addClass('hidden');
        $('#locationSelectContainer').removeClass('hidden');
    } else if (value == 'new') {
        $('#locationInputContainer').removeClass('hidden');
        $('#locationSelectContainer').addClass('hidden');
    }
}


function submitFlightplanForm() {   
    $('#loader-content').addClass('active');
    
    var flightplanRules = validationRules;
    
    flightplanRules.rules = {
        name: 'required',
        group_name: 'required',
        start_time:{
            required: true
        },
        end_time: {
            required: true
        }
    };
    flightplanRules.messages = {
        name: 'Flight plan name is required.',
        group_name: 'Flight plan group name is required.',
        start_time: {
            required: 'Flight start time is required.'
        },
        end_time: {
            required: 'Flight end time is required.'
        }
    };
    
    
    $("#flightplan-form").validate(flightplanRules);
    
    if ($('#flightplan-form').valid()) {
        if (checkTeamMembers() && checkVehicle() && checkFlightTime() && checkLocationName() && checkLocation()) {
            $('#location_name').val($('#location_name_input').val());
            
            unselectAllLayers();
            centerMap();

            setTimeout(function () { 
                
                var img = new Image();
                var mapCanvas = document.querySelector('.mapboxgl-canvas');
                $('#screenshot').val(mapCanvas.toDataURL());
                $("#flightplan-form").submit();
            }, 3000);            
        } else {
            $('#loader-content').removeClass('active');
        }      
    } else {
        $('#loader-content').removeClass('active');
        var a = checkTeamMembers();
        var b = checkVehicle();
        var c = checkFlightTime();
        var d = checkLocationName();
        var e = checkLocation();
        
    }
}


function checkLocationName() {
    if ($('#how_to_save').val() == 'new') {
        if ($('#location_name_input').val() == '') {
            $('#location_name_input').css('border', '1px solid #f05050');
            $('#location-name-error').css('display', 'block');
            return false;
        } else {
            $('#location_name_input').css('border', '1px solid #cfdadd');
            $('#location-name-error').css('display', 'none');
            return true;
        }
    } else {
        return true;
    }
}

function checkLocation() {
    if ($('input[name="coordinates"]').val() == '') {
        $('#map-error').css('display', 'block');
        return false;
    } else {
        $('#map-error').css('display', 'none');
        return true;
    }
}


function setLocationSnapshot(snapshot, name) {
    $('#modal-location .modal-title').html(name);
    $('#modal-location .modal-body').html('<img src="' + snapshot + '"/>');
}

function isChrome() {
    var isChromium = window.chrome;
    var winNav = window.navigator;
    var vendorName = winNav.vendor;
    var isOpera = winNav.userAgent.indexOf("OPR") > -1;
    var isIEedge = winNav.userAgent.indexOf("Edge") > -1;
    var isIOSChrome = winNav.userAgent.match("CriOS");

    if(isIOSChrome){
        return true;
    } else if(isChromium !== null && isChromium !== undefined && vendorName === "Google Inc." && isOpera == false && isIEedge == false) {
        return true;
    } else { 
        return false;
    }
}

function isMobile() {
    var check = false;
    (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
    return check;
}

function browserNotice(margin, flightplan) {
    if(!isChrome()){
        var link = '';
        if(isMobile()){
            link = 'https://www.google.com/chrome/browser/mobile/index.html';
        }else{
            link = 'https://www.google.com/chrome/browser/desktop/index.html';
        }
        
        var imageClass = '';
        if (margin) {
            imageClass = 'class="m-t-md m-b-md"';
        } else {
            imageClass = 'class="m-t-xs m-b-xs"';
        }
        
        var html = '';
        if (flightplan) {
            html = 'The browser you are using is not compatible with this application. For the best results please use &nbsp; &nbsp;<a href="' + link + '"><img src="/assets/images/chrome-logo.png" ' + imageClass + '/></a>';
        } else {
            html = 'The browser you are using is not compatible with this application. For the best results please use &nbsp; &nbsp;<a href="' + link + '"><img src="/assets/images/chrome-logo.png" ' + imageClass + '/></a>';
        }
        $('#browser-check').removeClass('hidden');
        $('#browser-check').html(html);
    } 
}

function browserModal() {
    var html = '';
    var link = '';
    
    if(isMobile()){
        link = 'https://www.google.com/chrome/browser/mobile/index.html';
    }else{
        link = 'https://www.google.com/chrome/browser/desktop/index.html';
    }

    $('#modal-browser-popup .modal-title').html('Incompatible Browser');
    $('#modal-browser-popup .modal-body').html('This function is not available in your browser. Please use <br><a href="' + link + '"><img src="/assets/images/chrome-logo.png" class="m-t-md m-b-md"/></a>');
}

function cameraNotice() {
    DetectRTC.load(function() {
        var hasWebcam = false;
        DetectRTC.MediaDevices.forEach(function(device) {
            if(device.kind.indexOf('video') !== -1) {
                hasWebcam = true;
            }
        });

        if (!hasWebcam) {
            var html = '<i class="fa  fa-video-camera text-danger"></i>&nbsp;&nbsp;&nbsp;To view images/video from your vehicle you have to have a webcam.';
            $('#camera-check').html(html);
            $('#camera-check').removeClass('hidden');
            }
    });
}

function dataURItoBlob(dataURI) {
        // convert base64/URLEncoded data component to raw binary data held in a string
        var binary = atob(dataURI.split(',')[1]);
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

        var array = [];
        for(var i = 0; i < binary.length; i++) {
            array.push(binary.charCodeAt(i));
        }
        return new Blob([new Uint8Array(array)], {type: mimeString});
    }

function closeWelcomePage() {
    if ($('#hideWelcomeForever').is(':checked')) {
        $.ajax({
            type: 'POST',
            contentType: 'application/json',
            cache: false,
            url: '/user/hide-welcome',
            error: function() {
                alert('Ooops! Something went wrong. Please, try again.');
            }
        });
    } 
    $('#welcome').css('display', 'none');
}


function startTour() {
    $('#welcome').css('display', 'none');
    hopscotch.startTour(tour);
}
function startNewProjectTour() {
    if (hopscotch.getState() === "tour:5") {
        hopscotch.startTour(tour);
    }
}
function startAllProjectsTour() {
    if (hopscotch.getState() === "tour:6") {
        hopscotch.startTour(tour);
    }
}
function startAddFlightplanTour() {
    if (hopscotch.getState() === "tour:7") {
        hopscotch.startTour(tour);
    }
}
function startViewFlightplanTour() {
    if (hopscotch.getState() === "tour:15") {
        hopscotch.startTour(tour);
    }
}

function checkPlanUsers(redirectURL) {
    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        cache: false,
        url: '/user/check-plan-users',
        success: function (data) {
            var modal = '<div id="modal-user-limit" class="modal fade" role="dialog">' +
                '<div class="modal-dialog">' +
                '<div class="modal-content">' +
                '<div class="modal-header">' +
                '<h4 class="modal-title">Users Limit</h4>' +
                '</div>' +
                '<div class="modal-body text-center">You have reached available number of users limit.<br>Go to your <a href="/user/settings" class="text-info">settings</a> page to upgrade your plan.</div>' +
                // '<div class="modal-body text-center">You have reached available number of users limit.</div>' +
                '<div class="modal-footer">' +
                '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>';
            if (data.result == 'error') {
                alert('Ooops! Something went wrong. Please, try again.');
            } else if (data.result == 'yes') {
                window.location.href = redirectURL;
            } else if (data.result == 'no') {
                if($("#modal-user-limit").length == 0) {
                    $('body').append(modal);
                }
                $('#modal-user-limit').modal('show');
            }
        },
        error: function() {
            alert('Ooops! Something went wrong. Please, try again.');
        }
    });
}

function checkPlanProjects(redirectURL) {
    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        cache: false,
        url: '/user/check-plan-projects',
        success: function (data) {
            var modal = '<div id="modal-project-limit" class="modal fade" role="dialog">' +
                '<div class="modal-dialog">' +
                '<div class="modal-content">' +
                '<div class="modal-header">' +
                '<h4 class="modal-title">Projects Limit</h4>' +
                '</div>' +
                '<div class="modal-body text-center">You have reached available number of projects limit.<br>Go to your <a href="/user/settings" class="text-info">settings</a> page to upgrade your plan or edit an existing project.</div>' +
                // '<div class="modal-body text-center">You have reached available number of projects limit.</div>' +
                '<div class="modal-footer">' +
                '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>';

            
            if (data.result == 'error') {
                alert('Ooops! Something went wrong. Please, try again.');
            } else if (data.result == 'yes') {
                window.location.href = redirectURL;
            } else if (data.result == 'no') {
                if($("#modal-project-limit").length == 0) {
                    $('body').append(modal);
                }
                $('#modal-project-limit').modal('show');
            }
        },
        error: function() {
            alert('Ooops! Something went wrong. Please, try again.');
        }
    });
}

function checkPlanFlightplans(redirectURL) {
    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        cache: false,
        url: '/user/check-plan-flightplans',
        success: function (data) {
            var modal = '<div id="modal-flightplan-limit" class="modal fade" role="dialog">' +
                '<div class="modal-dialog">' +
                '<div class="modal-content">' +
                '<div class="modal-header">' +
                '<h4 class="modal-title">Flight Plans Limit</h4>' +
                '</div>' +
                '<div class="modal-body text-center">You have reached available number of flight plans limit for this month.<br>Go to your <a href="/user/settings" class="text-info">settings</a> page to upgrade your plan or edit an existing flight plan.</div>' +
                // '<div class="modal-body text-center">You have reached available number of flight plans limit for this month.</div>' +
                '<div class="modal-footer">' +
                '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>';

            
            if (data.result == 'error') {
                alert('Ooops! Something went wrong. Please, try again.');
            } else if (data.result == 'yes') {
                window.location.href = redirectURL;
            } else if (data.result == 'no') {
                if($("#modal-flightplan-limit").length == 0) {
                    $('body').append(modal);
                }
                $('#modal-flightplan-limit').modal('show');
            }
        },
        error: function() {
            alert('Ooops! Something went wrong. Please, try again.');
        }
    });
}


function generateNewAccessCode() {
    $('#loader').removeClass('hidden');
    $.ajax({
            type: 'POST',
            contentType: 'application/json',
            cache: false,
            url: '/api/generate-new-access-code'
    });
    
    setTimeout(function () {
        window.location.href = '/user/settings';
    }, 3000);
}

function changeVehicleUnitID() {
    $('#change-vehicle-id-popup-link').trigger('click');
}
function changeMemberUnitID() {
    $('#change-member-id-popup-link').trigger('click');
}
function saveNewID(type, flightplanID) {
    $('#new-' + type + '-id-loader').removeClass('hidden');
    $('#new-' + type + '-id-msg').removeClass('text-danger');
    $('#new-' + type + '-id-msg').removeClass('text-success');
    $('#new-' + type + '-id-msg').html('');

    var id = $('#new-' + type + '-id').val();
    if (id != '') {
        if ($('#button-connect').html().indexOf('Disconnect') != -1) {
            $('#button-connect').trigger('click');
        }
        $('#new-' + type + '-id-loader').removeClass('hidden');


        $.ajax({
            type: 'POST',
            contentType: 'application/json',
            cache: false,
            url: '/flightplans/change-' + type + '-id/' + flightplanID + '/' + id,
            success: function (data) {
                if (data.result == 'ok') {
                    $('#new-' + type + '-id-loader').addClass('hidden');
                    $('#new-' + type + '-id-msg').addClass('text-success');
                    $('#new-' + type + '-id-msg').html('Success!');
                    $('#span' + type + 'ID1').html(id);
                    $('#span' + type + 'ID2').html(id);
                    setTimeout(function () {
                        $('#modal-change-' + type + '-id-popup').modal('hide');
                        $('#new-' + type + '-id').val('');
                        $('#new-' + type + '-id-loader').removeClass('hidden');
                        $('#new-' + type + '-id-msg').removeClass('text-danger');
                        $('#new-' + type + '-id-msg').removeClass('text-success');
                        $('#new-' + type + '-id-msg').html('');
                    }, 2000);
                }
            },
            error: function () {
                $('#new-' + type + '-id-loader').addClass('hidden');
                $('#new-' + type + '-id-msg').addClass('text-danger');
                $('#new-' + type + '-id-msg').html('Ooops! Something went wrong. Please, try again.');
            }
        });
    } else {
        $('#new-' + type + '-id-loader').addClass('hidden');
        $('#new-' + type + '-id-msg').addClass('text-danger');
        $('#new-' + type + '-id-msg').html('Please, enter new id.');
    }
}