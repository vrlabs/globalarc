//http://chawi3.com/2015/03/03/arraybuffer-to-base64-base64-to-arraybuffer-javascript/

function arrayBufferToBase64( buffer ) {
    var binary = '';
    var bytes = new Uint8Array( buffer );
    var len = bytes.byteLength;
    for (var i = 0; i < len; i++) {
        binary += String.fromCharCode( bytes[ i ] );
    }
    return window.btoa( binary );
}

function base64ToArrayBuffer(base64) {
    var binary_string =  window.atob(base64);
    var len = binary_string.length;
    var bytes = new Uint8Array( len );
    for (var i = 0; i < len; i++)        {
        bytes[i] = binary_string.charCodeAt(i);
    }
    return bytes.buffer;
}


// http://stackoverflow.com/questions/6965107/converting-between-strings-and-arraybuffers


function Str2BinaryArray (str)
{
	var buf = new ArrayBuffer(str.length); // 2 bytes for each char
	var	bufView = new Uint8Array(buf);
	
	for (var i=0, strLen=str.length; i<strLen; i++) 
	{
		bufView[i] = str.charCodeAt(i);
	}
	
	return bufView;
}
