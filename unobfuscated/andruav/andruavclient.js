/*************************************************************************************
 * 
 *   A N D R U A V - C L I E N T       JAVASCRIPT  LIB
 * 
 *   Author: Mohammad S. Hefny
 * 
 *   Date:   06 December 2015
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 *************************************************************************************/





// Command Types either System or Communication
const CMD_TYPE_SYS 			= 's';			// system command
const CMD_TYPE_COMM 		= 'c';   		// communication command

// Communication Commands
const CMD_COMM_GROUP 		='g';   		// group broadcast
const CMD_COMM_INDIVIDUAL 	='i';   		// individual broadcast

// System Commands:
const CMD_SYS_PING 			='ping';   		// group broadcast
const CMD_SYS_ADD 			='add';   		// group broadcast
const CMD_SYS_ADD_ENFORCE 	='addd';  		// group broadcast
const CMD_SYS_DEL 			='del';   		// group broadcast
const CMD_SYS_DEL_ENFORCE 	='dell';   		// group broadcast



// SOCKET STATUS
const SOCKET_STATUS_FREASH 			=		1;   // socket is new
const SOCKET_STATUS_CONNECTING    	=		2;	 // connecting to WS
const SOCKET_STATUS_DISCONNECTING 	=		3;   // disconnecting from WS
const SOCKET_STATUS_DISCONNECTED 	=		4;   // disconnected  from WS
const SOCKET_STATUS_CONNECTED 		=		5;   // connected to WS
const SOCKET_STATUS_REGISTERED 		=		6;   // connected and executed AddMe
const SOCKET_STATUS_UNREGISTERED 	=		7;   // connected but not registred
const SOCKET_STATUS_ERROR 		    =		8;   // Error

SOCKET_STATUS = ['Fresh','Connecting','Disconnecting','Disconnected','Connected','Registered','Error'];


// AndruaveMessageID
const TYPE_AndruavMessage_IMUStatistics 	= 1016;
const TYPE_AndruavMessage_ID 				= 1004;
const TYPE_AndruavMessage_RemoteExecute 	= 1005;
const TYPE_AndruavMessage_Error 			= 1008;
const TYPE_AndruavMessage_VideoFrame		= 1014;
const TYPE_AndruavMessage_IMG				= 1006;
const TYPE_AndruavMessage_GPS 				= 1002;
const TYPE_AndruavMessage_POW 				= 1003;
const TYPE_AndruavCMD_ExternalVideaStream 	= 1012;  //RX: {"tg":"GCS1","sd":"zxcv","ty":"c","gr":"1","cm":"i","mt":1012,"ms":"{\"E\":2,\"P\":0,\"I\":\"zxcv\"}"}

// AndruavMessage_RemoteExecute Commands
const RemoteCommand_MAKETILT 		= 100;
const RemoteCommand_TAKEIMAGE 		= 102;
const RemoteCommand_MAKEBEEP 		= 103;
const RemoteCommand_SENDSMS 		= 104;
const RemoteCommand_ROTATECAM 		= 105;
const RemoteCommand_IMUCTRL 		= 106;
const RemoteCommand_TELEMETRYCTRL 	= 108;
const RemoteCommand_NOTIFICATION 	= 109;
const RemoteCommand_STREAMVIDEO 	= 110;
const RemoteCommand_RECORDVIDEO 	= 111;
const RemoteCommand_STREAMVIDEORESUME 	= 112;
const RemoteCommand_GET_WAY_POINTS 	= 500;
const RemoteCommand_PUT_WAY_POINTS 	= 501;


// TYPE_AndruavCMD_ExternalVideaStream
const EXTERNAL_CAMERA_TYPE_RTCWEBCAM = 2


// Andruav Constants
const Default_Video_FrameResumeSize = 20;


var Me;


function AndruavClientSocket ()
{
	

}





AndruavClientSocket.prototype.init = function ()
{
	Me = this;
	AndruavClientSocket.prototype.server_ip = 'andruav.com';
	if (location.protocol === 'https:') {
		AndruavClientSocket.prototype.server_port = 9211;
	}else{
		AndruavClientSocket.prototype.server_port = 9210;
	}
	//AndruavClientSocket.prototype.server_port = 9210; // 19210;
	AndruavClientSocket.prototype.server_accessCode=null;
	AndruavClientSocket.prototype.ws = null;
	AndruavClientSocket.prototype.pub_streamOnOff = 'none';
	AndruavClientSocket.prototype.videoFrameCount = 0;
	/*AndruavClientSocket.prototype.andruavPubNub = Object.create(PubNub.prototype);
	
		
		this.andruavPubNub.onConnection = function ()
		{
			
		}
		
		this.andruavPubNub.onSessionStarted = function (session)
		{
			
			EVT_msgFromUnit_RTCVIDEO (session);
			
		}


		this.andruavPubNub.onSessionEnded = function (session)
		{
			EVT_msgFromUnit_RTCVIDEO(session);
		}*/
};


AndruavClientSocket.prototype.socketStatus = SOCKET_STATUS_FREASH;
AndruavClientSocket.prototype.onSocketStatus      	= function (status,statusName) 		{};  // ovveride to read socket status
AndruavClientSocket.prototype.onError      			= function (err) 	{};
AndruavClientSocket.prototype.onOpen       			= function () 		{};
AndruavClientSocket.prototype.onClose      			= function () 		{};
AndruavClientSocket.prototype.onMessage    			= function (evt) 	{};
AndruavClientSocket.prototype.onSend 	   			= function (msg) 	{};
AndruavClientSocket.prototype.onLog 	   			= function (msg) 	{};
AndruavClientSocket.prototype.andruavUnitList 	    = new AndruavUnitList();

AndruavClientSocket.prototype.ws					= null;

AndruavClientSocket.prototype.setSocketStatus = function (status)
{
	socketStatus = status;
	if (status == SOCKET_STATUS_REGISTERED)
	{
		console.log ('create ID function');
		AndruavClientSocket.prototype.timerID = setInterval(function () {
			Me.API_sendID();
			
		 }, 5000);
		 
		 // request IDfrom all units
		 Me.API_requestID(); 
		 
	}
	else
	{
		console.log ('kill ID function timer');
		clearInterval (this.timerID);
	}
	onSocketStatus(status,SOCKET_STATUS[status-1]);
}

AndruavClientSocket.prototype.parseJSONMessage = function (JsonMessage)
{
	
	var jmsg = JSON.parse(JsonMessage); //PHP sends Json data
			
	var message =
	{
		type    : jmsg.ty,                 //command type
		command : jmsg.cm,                 //main-command
		group	: jmsg.gr,                 //group name
		unitName: jmsg.sd,                 //sender name
		message : jmsg.ms
	};
	
	if (jmsg.hasOwnProperty('mt'))
	{
		message.messageType = jmsg .mt;
	}
	
	return message;
}


AndruavClientSocket.prototype.generateJSONMessage = function (senderGroup, senderName, targetName, cmdType, cmd, msgType, msg)
{
			
	var ms=null;
	if (msg != null)
	{
		ms = JSON.stringify(msg);
	}	
	//prepare json data
	var jmsg = {
		ty: cmdType,
		cm: cmd,
		sd: senderName,
		tg: targetName,
		gr: senderGroup,
		mt: msgType,
		ms: ms
		
		};
	
	//convert and send data to server
	return JSON.stringify(jmsg);
}

/**
 Called when message [TYPE_AndruavMessage_GPS] is received from a UNIT or GCS holding IMU Statistics
 * GPS3DFix		: 0 , 1 ,2 , 3 DFix
 * SatCount		: no. of satellites
 * YAW			: double
 * CurrentLocation: maybe null
 * 		Latitude
 * 		Longitude
 * 		Altitude
 * 		Time
 * 		Speed m/s
 * 		Bearing		...  note tha there is YAW as well.
 * 		Accuracy in meters.
*/
AndruavClientSocket.prototype.EVT_msgFromUnit_GPS = function (andruavUnit,GPS3DFix,satCount,yaw,latitude,logitude,gpsProvider,time,altitude,spead,bearing,accuracy)
{
}


AndruavClientSocket.prototype.EVT_msgFromUnit_IMUStatistics = function (groupname,unitID,msgID,msg)
{
}

/**
 * Called when recieving and image. GPS Information is available with image.
 * GPS is combined with image on Drone side when it is saved as JPG file., but here it is available as extra parameters.
 */
AndruavClientSocket.prototype.EVT_msgFromUnit_IMG = function (groupName,unitName,img,description,latitude,logitude,gpsProvider,time,altitude,spead,bearing,accuracy)
{
}


AndruavClientSocket.prototype.EVT_videoStateChanged = function (andruavFullName,OnOff)
{
}

AndruavClientSocket.prototype.EVT_msgFromUnit_RTCVIDEO = function (session)
{
	
			
}


AndruavClientSocket.prototype.EVT_msgFromUnit_VIDEO = function (groupName,unitName,img)
{
}


/**
 *			Called when a new unit joins the system.
*/
AndruavClientSocket.prototype.EVT_andruavUnitAdded = function (andruavUnit)
{
};

/**
 *		    Called as ID message is received from a known unit.
*/
AndruavClientSocket.prototype.EVT_andruavUnitUpdated = function (andruavUnit)
{
};



/**
 * Battery Information of Andruav Mobile & FCB Board.
 * Check Battery object andruavUnit.battery
 * 				andruavUnit.battery.BatteryLevel
				andruavUnit.battery.Voltage 	
				andruavUnit.battery.BatteryTemperature 	
				andruavUnit.battery.Health 				
				andruavUnit.battery.PlugStatus 			
				if (andruavUnit.battery.hasFCBPowerInfo)
				* 
					andruavUnit.battery.FCB_BatteryVoltage 
					andruavUnit.battery.FCB_CurrentConsumed 
					andruavUnit.battery.FCB_BatteryRemaining
				* 
*				
*/ 
AndruavClientSocket.prototype.EVT_batteryStatusChanged = function(andruavUnit)
{
};

/**
 * 	Received when a notification sent by remote UNIT.
 * 	It could be error, warning or notification.
 * 
 
			Received when a notification sent by remote UNIT.
			It could be error, warning or notification.
			*******************
			errorNo 			: 
									NOTIFICATION_TYPE_ERROR 	= 1
									NOTIFICATION_TYPE_WARNING	= 2
									NOTIFICATION_TYPE_NORMAL	= 3
									NOTIFICATION_TYPE_GENERIC	= 4
			infoType			:
									ERROR_CAMERA 	= 1
									ERROR_BLUETOOTH	= 2
									ERROR_USBERROR	= 3
									ERROR_KMLERROR	= 4
			notification_Type	:
									NOTIFICATION_TYPE_ERROR = 1;
									NOTIFICATION_TYPE_WARNING = 2;
									NOTIFICATION_TYPE_NORMAL = 3;
									NOTIFICATION_TYPE_GENERIC = 0;
			Description			: 
									Message
*/
AndruavClientSocket.prototype.EVT_andruavUnitError = function (andruavUnit,error)
{
};



AndruavClientSocket.prototype.prv_recordVideo  = function (andruavUnit,OnOff)
{

	var msg = 
		{
			C:RemoteCommand_RECORDVIDEO,
			Act:OnOff
		}
		this.API_sendCMD (andruavUnit.unitName,TYPE_AndruavMessage_RemoteExecute,msg);
		this.EVT_videoStateChanged(andruavUnit,OnOff);
		
		console.log ('video: prv_recordVideo:' + OnOff); 
}



AndruavClientSocket.prototype.prv_streamVideo  = function (andruavUnit,OnOff)
{

	var msg = 
		{
			C:RemoteCommand_STREAMVIDEO,
			Act:OnOff
		}
		this.API_sendCMD (andruavUnit.unitName,TYPE_AndruavMessage_RemoteExecute,msg);
		this.EVT_videoStateChanged(andruavUnit,OnOff);
		
		console.log ('video: this.pub_streamOnOff:' + this.pub_streamOnOff); 
}



AndruavClientSocket.prototype.prv_streamVideoResume = function (fullName)
{
	
	console.log ('Called prv_streamVideoResume : ' + fullName);
	var unit = Me.andruavUnitList.getUnit(fullName);
	if (unit==null) 
	{
		// you may declare an error message or send for ID Request
		console.log ('video: Unit ' + fullName + ' Not Found');
		return ; 
	}
	
	
	var msg = 
		{
			C:RemoteCommand_STREAMVIDEORESUME
			
		}
		this.API_sendCMD (unit.unitName,TYPE_AndruavMessage_RemoteExecute,msg);
		
		console.log ('prv_streamVideoResume'); 
}

/**
 * 
 */
AndruavClientSocket.prototype.API_addMe = function (groupname,unitID)
{
	AndruavClientSocket.prototype.unitID = unitID;
	//this.andruavPubNub.unitName = unitID;
	//this.andruavPubNub.login(unitID);
	
	AndruavClientSocket.prototype.groupName = groupname;
	
	var unit = new AndruavUnit();
	unit.IsMe 					= true;
	unit.IsGCS 					= true;
	unit.unitName				= unitID;
	unit.groupName				= groupname;
	unit.telemetry_protocol		= Unknown_Telemetry;
	unit.VehicleType			= VEHICLE_GCS;
	
	this.andruavUnitList.Add(unit.fullName(),unit);
	AndruavClientSocket.prototype.andruavUnit = unit;
	
	this.API_sendSYSCMD (CMD_SYS_ADD_ENFORCE,null,null);
	
}




AndruavClientSocket.prototype.API_delMe = function ()
{
	this.API_sendSYSCMD (CMD_SYS_DEL_ENFORCE,null,null);
}


//AndruavClientSocket.prototype.API_sendCMDToIndividual = function (targetName,msgType,msg)
AndruavClientSocket.prototype.API_sendCMD = function (targetName,msgType,msg)
{
 
	var commType;
	if (targetName != null)
	{
		commType = CMD_COMM_INDIVIDUAL;
	}
	else
	{
		commType = CMD_COMM_GROUP;
	}
	
	AndruavClientSocket.ws.sendex (this.generateJSONMessage(this.groupName,this.unitID,targetName,CMD_TYPE_COMM, commType ,msgType,msg));
    console.log (this.generateJSONMessage(this.groupName,this.unitID,targetName,CMD_TYPE_COMM, commType ,msgType,msg));	
}

/*
AndruavClientSocket.prototype.API_sendCMDToGroup = function (msgType,msg)
{
 
	AndruavClientSocket.ws.sendex (this.generateJSONMessage(this.groupName,this.unitID,null,CMD_TYPE_COMM, CMD_COMM_GROUP,msgType,msg));
	
}
*/

AndruavClientSocket.prototype.API_sendSYSCMD = function (cmdType,msgType,msg)
{
 
	AndruavClientSocket.ws.sendex (this.generateJSONMessage(this.groupName,this.unitID,null,CMD_TYPE_SYS, cmdType,msgType,msg));
	
}

AndruavClientSocket.prototype.API_sendID  = function (target)
{
	var msg = 
	{
		VT:VEHICLE_GCS, 							// VehicleType
		GS:this.andruavUnit.IsGCS,					// IsCGS
		VR:this.andruavUnit.VideoRecording,			// VideoRecording
		FI:this.andruavUnit.useFCBIMU,				// useFCBIMU
		SD:this.andruavUnit.IsShutdown,
		TP:this.andruavUnit.telemetry_protocol,
		DS:this.andruavUnit.Description				
	}
	this.API_sendCMD (target,TYPE_AndruavMessage_ID,msg);
}



AndruavClientSocket.prototype.API_requestID  = function (target)
{
	var msg = 
	{
		C:TYPE_AndruavMessage_ID
	}
	this.API_sendCMD (target,TYPE_AndruavMessage_RemoteExecute,msg);
}


AndruavClientSocket.prototype.API_remoteCommand_takeImage  = function (target)
{
	var msg = 
	{
		C:RemoteCommand_TAKEIMAGE,
		NumberofImages:1,
		TimeBetweenShots:0,
		SendBackImages:1
	}
	this.API_sendCMD (target,TYPE_AndruavMessage_RemoteExecute,msg);
}



AndruavClientSocket.prototype.API_webrtcConnect  = function (target)
{
	this.andruavPubNub.makeCall(target);
}




AndruavClientSocket.prototype.API_remoteCommand_streamVideo  = function (fullName)
{
	
	var unit = Me.andruavUnitList.getUnit(fullName);
	if (unit==null) 
	{
		// you may declare an error message or send for ID Request
		console.log ('video: Unit ' + fullName + ' Not Found');
		return ; 
	}
	
	if (this.pub_streamOnOff == fullName)
	{
		this.pub_streamOnOff = 'none';
		this.prv_streamVideo (unit,false);
		console.log ('video: Stop Video');
	}
	else
	{
		if (this.pub_streamOnOff!='none')
		{
			// we need to stop video from the other
			this.prv_streamVideo (Me.andruavUnitList.getUnit(this.pub_streamOnOff),false);
			console.log ('video: Disconnect Video from ' + this.pub_streamOnOff);
		}	
		
		//Activate Video
		this.pub_streamOnOff = fullName;
		this.prv_streamVideo (unit,true);
		console.log ('video: StreamVideo from ' + this.pub_streamOnOff);
		
	}
	
}



AndruavClientSocket.prototype.API_remoteCommand_recordVideo  = function (fullName)
{
	var unit = Me.andruavUnitList.getUnit(fullName);
	if (unit==null) 
	{
		// you may declare an error message or send for ID Request
		console.log ('video: Unit ' + fullName + ' Not Found');
		return ; 
	}
	
	this.prv_recordVideo (unit,!unit.VideoRecording);
}
	

	
AndruavClientSocket.prototype.connect = function (accesscode)
{
	var Me = this;
	
	
	
	
	
	function parseCommunicationMessage  (msg)
	{
		console.log ('parseCommunicationMessage: ' + msg.messageType ); //+ ' Contents:' + msg.message)
		
		
				
		
		switch (msg.messageType)
		{
			case TYPE_AndruavMessage_GPS:
				var jmsg = JSON.parse(msg.message);
				var unit = Me.andruavUnitList.getUnit(getFullName(msg.group,msg.unitName));
				if (unit == null) 
				{
					Me.API_requestID(msg.unitName);
					return ;
				}
				console.log (jmsg);
				Me.EVT_msgFromUnit_GPS (unit,jmsg['3D'],jmsg.Y,jmsg.SC,jmsg.la,jmsg.ln,jmsg.p,jmsg.t,jmsg.a,jmsg.g,jmsg.s,jmsg.b,jmsg.c);
			break; 
			
			case TYPE_AndruavCMD_ExternalVideaStream:
			//RX: {"tg":"GCS1","sd":"zxcv","ty":"c","gr":"1","cm":"i","mt":1012,"ms":"{\"E\":2,\"P\":0,\"I\":\"zxcv\"}"}
				var jmsg = JSON.parse(msg.message);
				var unit = Me.andruavUnitList.getUnit(getFullName(msg.group,msg.unitName));
				if (unit == null) 
				{
					Me.API_requestID(msg.unitName);
					return ;
				}
				
				
				switch (jmsg.E) //externalType
				{
					case EXTERNAL_CAMERA_TYPE_RTCWEBCAM:
						var targetId = jmsg.I;
						var urlVideo = "https://andruav.com:29001/viewer.html?drone=" + targetId + "&gcs=" + this.unitID ;
						window.open(urlVideo);
						//var session = {status:'connected',
						//				url : urlVideo};
						Me.EVT_msgFromUnit_RTCVIDEO(session);
					break;
				}
				
				
			break;
			
			case TYPE_AndruavMessage_ID:
				var jmsg = JSON.parse(msg.message);
				var unit = Me.andruavUnitList.getUnit(getFullName(msg.group,msg.unitName));
				if (unit != null)
				{
					unit.IsMe 					= false;
					unit.IsGCS 					= jmsg.GS;
					unit.unitName				= msg.unitName;
					unit.groupName				= msg.group;
					unit.Description			= jmsg.Description;
					unit.telemetry_protocol		= jmsg.TP;
					unit.VehicleType			= jmsg.VT;
					unit.VideoRecording			= jmsg.VR;
					unit.IsShutdown				= jmsg.SD;
					unit.useFCBIMU				= jmsg.FI;
					//console.log (JSON.stringify(unit));
					Me.andruavUnitList.putUnit(unit.fullName(),unit);
					Me.EVT_andruavUnitUpdated (unit);
				}
				else
				{
					var unit = new AndruavUnit();
					unit.IsMe 					= false;
					unit.IsGCS 					= jmsg.GS;
					unit.unitName				= msg.unitName;
					unit.groupName				= msg.group;
					unit.Description			= jmsg.Description;
					unit.telemetry_protocol		= jmsg.TP;
					unit.VehicleType			= jmsg.VT;
					unit.VideoRecording			= jmsg.VR;
					unit.IsShutdown				= jmsg.SD;
					unit.useFCBIMU				= jmsg.FI;
					
					Me.andruavUnitList.Add(unit.fullName(),unit);
					//console.log (JSON.stringify(unit));
					Me.EVT_andruavUnitAdded (unit);
				}
			break;
			
			case TYPE_AndruavMessage_RemoteExecute:
			
				var andruavUnit = Me.andruavUnitList.getUnit(getFullName(msg.group,msg.unitName));
				if (andruavUnit==null)
				{
					// unit not defined here ... send a request for ID
					Me.API_requestID(msg.unitName);
					return ;
				}
		
				var jmsg = JSON.parse(msg.message);
				switch (jmsg.C)
				{
					case TYPE_AndruavMessage_ID:
						// request send ID
						Me.API_sendID();
					break;
				}
			break;
			
			case TYPE_AndruavMessage_POW:
				var andruavUnit = Me.andruavUnitList.getUnit(getFullName(msg.group,msg.unitName));
				if (andruavUnit==null)
				{
					// unit not defined here ... send a request for ID
					Me.API_requestID(msg.unitName);
					return ;
				}
				
				var jmsg = JSON.parse(msg.message);
				andruavUnit.battery.BatteryLevel 		= jmsg.BL;
				andruavUnit.battery.Voltage 			= jmsg.V;
				andruavUnit.battery.BatteryTemperature 	= jmsg.BT;
				andruavUnit.battery.Health 				= jmsg.H;
				andruavUnit.battery.PlugStatus 			= jmsg.PS;
				if (jmsg.hasOwnProperty('FV'))
				{
					andruavUnit.battery.hasFCBPowerInfo 			= true;
					andruavUnit.battery.FCB_BatteryVoltage 			= jmsg.FV;
					andruavUnit.battery.FCB_CurrentConsumed 		= jmsg.FI;
					andruavUnit.battery.FCB_BatteryRemaining 		= jmsg.FR;
				}
			
				Me.EVT_batteryStatusChanged (andruavUnit);
			break;
				
			
			case TYPE_AndruavMessage_Error:
				var error;
				var jmsg = JSON.parse(msg.message);
				error.errorNo 			= jmsg.EN;
				error.infoType 			= jmsg.IT;
				error.notification_Type = jmsg.NT;
				error.Description 		= jmsg.DS;
				Me.EVT_andruavUnitError (andruavUnit,error);
			break;
		}
	}
	
	
	function parseSystemMessage (msg)
	{
		if (msg.command == 'connected')
		{
			if (msg.message.indexOf('OK:connected')!=-1)
			{
				Me.setSocketStatus(SOCKET_STATUS_CONNECTED);
			}
			else
			{
				Me.onLog ("connection refused");
			}
		
			return ;
		}
		
		if ((msg.command == 'add') || (msg.command == 'addd'))
		{
			if (msg.message.indexOf('OK:add')!=-1)
			{
				Me.setSocketStatus(SOCKET_STATUS_REGISTERED);
			}
			else
			{
				Me.onLog ("refused to add, maybe already added. pls use addd instead of add to enforce addition.");
			}
			return 
		}
		
		if ((msg.command == 'del') || (msg.command == 'dell'))
		{
			if (msg.message.indexOf('OK:del')!=-1)
			{
				Me.setSocketStatus(SOCKET_STATUS_FREASH);
			}
			else
			{
				Me.onLog ("refused to delete, maybe not existed. pls use dell instead of del to enforce addition.");
			}
			return 
		}
	}
	
	
	
	if (accesscode== null)
	{
		this.onError ('accesscode cannot be null');
		return ;
	}
	this.server_accessCode = accesscode;
	
	if (location.protocol === 'https:') {
		
		url = 'wss://' + this.server_ip + ':9211?KEY=9244ddc4-b89b-4111-9934-65ce596198f4&SID=' + this.server_accessCode;
	}else{
		url = 'ws://' + this.server_ip + ':9210?KEY=9244ddc4-b89b-4111-9934-65ce596198f4&SID=' + this.server_accessCode;
	 
	}
	 //console.log (url);
	 AndruavClientSocket.ws = new WebSocket(url);
	 AndruavClientSocket.ws.parent = this;
	 AndruavClientSocket.ws.sendex = function (msg)
	 {
		 Me.onSend(msg);
		 AndruavClientSocket.ws.send(msg);
	 };
	 
     if ("WebSocket" in window)
     {		
		// OnOpen callback of Websocket 
		AndruavClientSocket.ws.onopen = function()
        {
			Me.onOpen();
				
            
        };
				
		// OnMessage callback of websocket
        AndruavClientSocket.ws.onmessage = function (evt) 
        { 
			if (typeof evt.data === "string")
			{
				// This is a text message
				//console.log ('This is a text message');
				jmsg = Me.parseJSONMessage(evt.data);
				switch (jmsg.type)
				{
					case CMD_TYPE_SYS: 
						parseSystemMessage(jmsg);
						break;
						
					case CMD_TYPE_COMM:
						parseCommunicationMessage(jmsg);
						break;
				}
				Me.onMessage(evt);
			}
			else
			{
				//console.log ('This is a binary message');
						
				function extractBinary (evt)
				{
					//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/DataView
					//https://millermedeiros.github.io/mdoc/examples/node_api/doc/buffers.html
					//http://blog.tojicode.com/2011/08/jsstruct-c-style-struct-reading-in.html [*****]
					
					var andruavCMD;
					var andruavMessage;	 
					var internalCommandIndex;
					var jmsg;
					var data;
					
					// Parse message after extract it from the binary part
					function parseBinaryAndruavMessage ()
					{
						console.log ('parseBinaryAndruavMessage:' + andruavCMD.mt);
						
						
						switch (andruavCMD.mt)
						{
							case TYPE_AndruavMessage_IMUStatistics:
								console.log ('TYPE_AndruavMessage_IMUStatistics');
								 var andruavMessage = {
								'GroundAltitude_max': data.getUint32(data.buffer,internalCommandIndex) & data.getUint32(data.buffer,internalCommandIndex+4) << 32,
								'GroundSpeed_max'	: data.getFloat32(internalCommandIndex+8),
								'GroundSpeed_avg'	: data.getFloat32(internalCommandIndex+12),
								'IdleDuration'		: data.getFloat32(internalCommandIndex+16),
								'IdleTotalDuration'	: data.getFloat32(internalCommandIndex+24),
								'useFCBIMU'			: data.getUint8(internalCommandIndex+32)
								};
								
								Me.EVT_msgFromUnit_IMUStatistics (andruavCMD.gr,andruavCMD.sd,andruavCMD.mt,andruavMessage);
								//console.log (andruavMessage);
							break;
							
							case TYPE_AndruavMessage_VideoFrame:
								var intArr = [];
							    var c;
								for (i=internalCommandIndex,end = data.byteLength;i<end;++i)
								{
							    	c = data.getInt8(i);
									intArr.push(String.fromCharCode(data.getInt8(i)));
								}
								var img  = intArr.join("");
            			
								Me.EVT_msgFromUnit_VIDEO (andruavCMD.gr,andruavCMD.sd,img);
								Me.videoFrameCount = Me.videoFrameCount + 1;
								console.log ('prv_streamVideoResume: ' + Me.videoFrameCount);
								if (Me.videoFrameCount >= Default_Video_FrameResumeSize)
								{
									Me.videoFrameCount =0;
									console.log ('calling prv_streamVideoResume:' + getFullName(andruavCMD.gr,andruavCMD.sd));
								    Me.prv_streamVideoResume (getFullName(andruavCMD.gr,andruavCMD.sd));
								}
										
							break;			
							
							case TYPE_AndruavMessage_IMG:
							
							    var bytes = [];
							    var c;
								for (i=internalCommandIndex,end=data.byteLength;i<end;++i)
								{
							    	c = data.getInt8(i);
									if (c!=0)
									{
										// end of string has been reached
										// extract JSON parameters Text part of the command
										bytes.push(String.fromCharCode(c));
										
									}
									else
									{
										internalCommandIndex=i+1;
										console.log (bytes);
										
										var andruavMessage;
										try {
											andruavMessage = JSON.parse(bytes.join(""));
										}
										catch (err)
										{
											console.log (err);
											andruavMessage = new Object();
										}
										
										var intArr= []; 
										for (j=internalCommandIndex; j<data.byteLength ;++j)
										{
											intArr.push(String.fromCharCode(data.getInt8(j)));
										}
										//console.log (intArr);
										andruavMessage.img  = intArr.join("");
            			
										//console.log ('intArr type:' + intArr.constructor );
										
										//console.log ('andruavMessage.img type:' + andruavMessage.img.constructor );
										
										Me.EVT_msgFromUnit_IMG (andruavCMD.gr,andruavCMD.sd,andruavMessage.img
																,andruavMessage.des
																,andruavMessage.lat
																,andruavMessage.lng
																,andruavMessage.prv
																,andruavMessage.tim
																,andruavMessage.alt
																,andruavMessage.spd
																,andruavMessage.ber
																,andruavMessage.acc
																
																);
								
								
										break;
									}
								};
      
							break;
						}
					} 
						
					var reader = new FileReader();
					reader.onload = function(event) {
					var contents = event.target.result;  // this is an arrayBuffer http://blog.teamtreehouse.com/reading-files-using-the-html5-filereader-api
					
						// extract the text part of the message		
						
						data = new DataView(contents);
						
						//extract command:
						var bytes = [];
						var c;
						for (i=0;i<data.byteLength;++i)
						{
							c = data.getInt8(i);
							if (c!=0)
							{
								// end of string has been reached...after that it is the binary msg contents that could include string as well based on the internal command.
								bytes.push(String.fromCharCode(c));
							}
							else
							{
								andruavCMD = JSON.parse(bytes.join(""));
							
								internalCommandIndex=i+1;
								
								break;
							}
						}
						
						//console.log (andruavCMD);
						//console.log (internalCommandIndex + " of: " + data.byteLength);
						//console.log (andruavCMD);
						
						parseBinaryAndruavMessage ();
						//console.log (data);
					};

					reader.onerror = function(event) {
						console.error("File could not be read! Code " + event.target.error.code);
					};

					reader.readAsArrayBuffer(evt.data );
				}
				
				extractBinary (evt);
			}
			
			
			
        };
	
		// OnClose callback of websocket
        AndruavClientSocket.ws.onclose = function()
        { 
			Me.setSocketStatus(SOCKET_STATUS_DISCONNECTED);
            Me.onClose();
        };
        
        AndruavClientSocket.ws.onerror = function (err)
        {
			Me.setSocketStatus(SOCKET_STATUS_ERROR);
			Me.onError (err);
		};
	 }
     else
     {
          // The browser doesn't support WebSocket
          alert("WebSocket NOT supported by your Browser!");
     }
}




