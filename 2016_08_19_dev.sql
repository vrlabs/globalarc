# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.10)
# Database: globalarcdb
# Generation Time: 2016-08-19 11:59:25 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table batteries
# ------------------------------------------------------------

DROP TABLE IF EXISTS `batteries`;

CREATE TABLE `batteries` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(11) unsigned NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `serial_number` varchar(255) NOT NULL,
  `charge` int(10) DEFAULT NULL,
  `voltage` float DEFAULT NULL,
  `status` int(1) unsigned NOT NULL DEFAULT '1' COMMENT '1 = active, 2 = grounded, 3 = retired',
  `notes` text,
  PRIMARY KEY (`id`),
  KEY `fk_batteries` (`company_id`),
  CONSTRAINT `fk_batteries` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `batteries` WRITE;
/*!40000 ALTER TABLE `batteries` DISABLE KEYS */;

INSERT INTO `batteries` (`id`, `company_id`, `name`, `serial_number`, `charge`, `voltage`, `status`, `notes`)
VALUES
	(2,2,'Sample Battery','SN1234567890',90,12.5,1,'Sample Battery Notes');

/*!40000 ALTER TABLE `batteries` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table companies
# ------------------------------------------------------------

DROP TABLE IF EXISTS `companies`;

CREATE TABLE `companies` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `access_code` varchar(40) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `data_set` tinyint(1) unsigned DEFAULT '0',
  `plan` enum('free','professional','enterprise') NOT NULL DEFAULT 'free',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `subscription_id` int(11) unsigned DEFAULT NULL,
  `subscription_end` timestamp NULL DEFAULT NULL,
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `access_code` (`access_code`),
  KEY `fk_companies` (`subscription_id`),
  CONSTRAINT `fk_companies` FOREIGN KEY (`subscription_id`) REFERENCES `subscriptions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `companies` WRITE;
/*!40000 ALTER TABLE `companies` DISABLE KEYS */;

INSERT INTO `companies` (`id`, `name`, `access_code`, `email`, `data_set`, `plan`, `created`, `subscription_id`, `subscription_end`, `deleted`)
VALUES
	(2,'VRlabs','jP1cUUYZhhcJ','jelena@vrhsolutions.com',1,'professional','2016-08-16 13:05:54',5,'2016-09-15 13:13:19',0);

/*!40000 ALTER TABLE `companies` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table document_types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `document_types`;

CREATE TABLE `document_types` (
  `id` int(3) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `document_types` WRITE;
/*!40000 ALTER TABLE `document_types` DISABLE KEYS */;

INSERT INTO `document_types` (`id`, `name`)
VALUES
	(1,'Regulatory Certificate'),
	(2,'Insurance Certificate'),
	(3,'Checklist'),
	(4,'Manual'),
	(5,'Pilot Certificate');

/*!40000 ALTER TABLE `document_types` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table documents
# ------------------------------------------------------------

DROP TABLE IF EXISTS `documents`;

CREATE TABLE `documents` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned DEFAULT NULL COMMENT 'uploaded by',
  `user_name` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `file_type` varchar(255) DEFAULT NULL COMMENT 'pdf, image',
  `file_path` varchar(255) DEFAULT NULL,
  `file_ext` varchar(5) DEFAULT NULL COMMENT 'pdf, jpg, png',
  `size` int(50) unsigned DEFAULT NULL,
  `human_size` varchar(255) DEFAULT NULL,
  `document_type` tinyint(3) unsigned DEFAULT NULL COMMENT '1= Regulatory Certificate, 2 = Insurance Certificate, 3 = Checklist, 4 = Manual, 5 = Pilot Certificate',
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_documents` (`company_id`),
  CONSTRAINT `fk_documents` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `documents` WRITE;
/*!40000 ALTER TABLE `documents` DISABLE KEYS */;

INSERT INTO `documents` (`id`, `company_id`, `user_id`, `user_name`, `name`, `file_name`, `file_type`, `file_path`, `file_ext`, `size`, `human_size`, `document_type`, `modified`)
VALUES
	(6,2,2,'Jelena Kovacic Bozic','Preflight Checklist','2/documents/ab045402-b2ad-4569-b31d-c1ece88f8433.pdf','application/pdf','https://s3.amazonaws.com/globalarc.upload.local/2/documents/ab045402-b2ad-4569-b31d-c1ece88f8433.pdf','.pdf',375433,'107 kB',3,'2016-08-16 13:05:57');

/*!40000 ALTER TABLE `documents` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table flightplan_batteries
# ------------------------------------------------------------

DROP TABLE IF EXISTS `flightplan_batteries`;

CREATE TABLE `flightplan_batteries` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `flightplan_id` int(11) unsigned NOT NULL,
  `battery_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_flightplan_batteries` (`flightplan_id`),
  CONSTRAINT `fk_flightplan_batteries` FOREIGN KEY (`flightplan_id`) REFERENCES `flightplans` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `flightplan_batteries` WRITE;
/*!40000 ALTER TABLE `flightplan_batteries` DISABLE KEYS */;

INSERT INTO `flightplan_batteries` (`id`, `flightplan_id`, `battery_id`)
VALUES
	(13,13,2),
	(15,12,2);

/*!40000 ALTER TABLE `flightplan_batteries` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table flightplan_comments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `flightplan_comments`;

CREATE TABLE `flightplan_comments` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `flightplan_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `content` text NOT NULL,
  `created` datetime NOT NULL,
  `role_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `comments_fk1` (`user_id`),
  KEY `fk_flightplan_comments` (`flightplan_id`),
  CONSTRAINT `fk_flightplan_comments` FOREIGN KEY (`flightplan_id`) REFERENCES `flightplans` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table flightplan_documents
# ------------------------------------------------------------

DROP TABLE IF EXISTS `flightplan_documents`;

CREATE TABLE `flightplan_documents` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `flightplan_id` int(11) unsigned DEFAULT NULL,
  `document_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `flightplan_documents_fk1` (`document_id`),
  KEY `fk_flightplan_documents` (`flightplan_id`),
  CONSTRAINT `fk_flightplan_documents` FOREIGN KEY (`flightplan_id`) REFERENCES `flightplans` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `flightplan_documents` WRITE;
/*!40000 ALTER TABLE `flightplan_documents` DISABLE KEYS */;

INSERT INTO `flightplan_documents` (`id`, `flightplan_id`, `document_id`)
VALUES
	(11,12,6);

/*!40000 ALTER TABLE `flightplan_documents` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table flightplan_team_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `flightplan_team_users`;

CREATE TABLE `flightplan_team_users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `team_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `role_id` int(11) unsigned NOT NULL,
  `unit_id` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `team_users_fk1` (`user_id`),
  KEY `fk_flightplan_team_users` (`team_id`),
  CONSTRAINT `fk_flightplan_team_users` FOREIGN KEY (`team_id`) REFERENCES `flightplan_teams` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `flightplan_team_users` WRITE;
/*!40000 ALTER TABLE `flightplan_team_users` DISABLE KEYS */;

INSERT INTO `flightplan_team_users` (`id`, `team_id`, `user_id`, `role_id`, `unit_id`)
VALUES
	(13,13,2,2,'t_phi'),
	(15,15,2,1,'t_930');

/*!40000 ALTER TABLE `flightplan_team_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table flightplan_teams
# ------------------------------------------------------------

DROP TABLE IF EXISTS `flightplan_teams`;

CREATE TABLE `flightplan_teams` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `flightplan_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_flightplan_teams` (`flightplan_id`),
  CONSTRAINT `fk_flightplan_teams` FOREIGN KEY (`flightplan_id`) REFERENCES `flightplans` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `flightplan_teams` WRITE;
/*!40000 ALTER TABLE `flightplan_teams` DISABLE KEYS */;

INSERT INTO `flightplan_teams` (`id`, `flightplan_id`)
VALUES
	(15,12),
	(13,13);

/*!40000 ALTER TABLE `flightplan_teams` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table flightplan_vehicles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `flightplan_vehicles`;

CREATE TABLE `flightplan_vehicles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `flightplan_id` int(11) unsigned NOT NULL,
  `vehicle_id` int(11) unsigned NOT NULL,
  `unit_id` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `flightplan_vehicles_fk1` (`vehicle_id`),
  KEY `fk_flightplan_vehicles` (`flightplan_id`),
  CONSTRAINT `fk_flightplan_vehicles` FOREIGN KEY (`flightplan_id`) REFERENCES `flightplans` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `flightplan_vehicles` WRITE;
/*!40000 ALTER TABLE `flightplan_vehicles` DISABLE KEYS */;

INSERT INTO `flightplan_vehicles` (`id`, `flightplan_id`, `vehicle_id`, `unit_id`)
VALUES
	(13,13,2,'d_7pv'),
	(15,12,2,'d_p5t');

/*!40000 ALTER TABLE `flightplan_vehicles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table flightplans
# ------------------------------------------------------------

DROP TABLE IF EXISTS `flightplans`;

CREATE TABLE `flightplans` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(11) unsigned NOT NULL,
  `location_id` int(11) unsigned NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `group_name` varchar(255) NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `notes` text,
  `sample` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `created_by` int(11) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_flightplans` (`project_id`),
  CONSTRAINT `fk_flightplans` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `flightplans` WRITE;
/*!40000 ALTER TABLE `flightplans` DISABLE KEYS */;

INSERT INTO `flightplans` (`id`, `project_id`, `location_id`, `name`, `group_name`, `start_time`, `end_time`, `notes`, `sample`, `created_by`, `created_at`)
VALUES
	(12,6,12,'Sample Flight Plan','1','2016-08-16 13:05:57','2016-08-16 15:05:57','Sample Flight Plan Notes',1,2,'2016-08-16 13:05:57'),
	(13,7,13,'test','1','2016-08-16 00:00:00','2016-08-16 00:55:00','',0,2,'2016-08-16 13:14:03');

/*!40000 ALTER TABLE `flightplans` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table images
# ------------------------------------------------------------

DROP TABLE IF EXISTS `images`;

CREATE TABLE `images` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `flightplan_id` int(11) unsigned NOT NULL,
  `vehicle_id` int(11) unsigned NOT NULL,
  `created` datetime DEFAULT NULL,
  `latitude` varchar(255) DEFAULT NULL,
  `longitude` varchar(255) DEFAULT NULL,
  `path` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `images_fk1` (`vehicle_id`),
  KEY `fk_images` (`flightplan_id`),
  CONSTRAINT `fk_images` FOREIGN KEY (`flightplan_id`) REFERENCES `flightplans` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table location_coordinates
# ------------------------------------------------------------

DROP TABLE IF EXISTS `location_coordinates`;

CREATE TABLE `location_coordinates` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `location_id` int(11) unsigned NOT NULL,
  `latitude` float DEFAULT NULL,
  `longitude` float DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_location_coordinates` (`location_id`),
  CONSTRAINT `fk_location_coordinates` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `location_coordinates` WRITE;
/*!40000 ALTER TABLE `location_coordinates` DISABLE KEYS */;

INSERT INTO `location_coordinates` (`id`, `location_id`, `latitude`, `longitude`)
VALUES
	(50,12,-73.9758,40.7668),
	(51,12,-73.9739,40.7675),
	(52,12,-73.9728,40.7659),
	(53,12,-73.9741,40.7649),
	(54,12,-73.976,40.7659),
	(55,12,-73.9758,40.7668),
	(56,13,19.8147,45.2365),
	(57,13,19.8151,45.2419),
	(58,13,19.8258,45.2421),
	(59,13,19.8293,45.2372),
	(60,13,19.8224,45.2346),
	(61,13,19.8147,45.2365);

/*!40000 ALTER TABLE `location_coordinates` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table locations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `locations`;

CREATE TABLE `locations` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(11) unsigned NOT NULL,
  `type` enum('POLYGON','CIRCLE','RECTANGLE') DEFAULT 'POLYGON',
  `name` varchar(255) NOT NULL DEFAULT '',
  `image` varchar(255) NOT NULL DEFAULT '',
  `thumb` varchar(255) DEFAULT NULL,
  `path` varchar(255) NOT NULL DEFAULT '',
  `path_thumb` varchar(255) DEFAULT NULL,
  `center_latitude` float NOT NULL,
  `center_longitude` float NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_locations` (`project_id`),
  CONSTRAINT `fk_locations` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `locations` WRITE;
/*!40000 ALTER TABLE `locations` DISABLE KEYS */;

INSERT INTO `locations` (`id`, `project_id`, `type`, `name`, `image`, `thumb`, `path`, `path_thumb`, `center_latitude`, `center_longitude`)
VALUES
	(12,6,'POLYGON','Sample Location','sample.png',NULL,'https://s3.amazonaws.com/globalarc.upload.local/2/locations/snapshot_97fff7a736041efcb5e31467700fff5f7399fa48.png','https://s3.amazonaws.com/globalarc.upload.local/2/locations/snapshot_97fff7a736041efcb5e31467700fff5f7399fa48_thumb.png',-73.9744,40.7662),
	(13,7,'POLYGON','test','snapshot_f6bd6594715ac1573ccd55d7a27250f3f3b944ac.png','snapshot_f6bd6594715ac1573ccd55d7a27250f3f3b944ac_thumb.png','https://s3.amazonaws.com/globalarc.upload.local/2/locations/snapshot_f6bd6594715ac1573ccd55d7a27250f3f3b944ac.png','https://s3.amazonaws.com/globalarc.upload.local/2/locations/snapshot_f6bd6594715ac1573ccd55d7a27250f3f3b944ac_thumb.png',19.822,45.2383);

/*!40000 ALTER TABLE `locations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table logs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `logs`;

CREATE TABLE `logs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `flightplan_id` int(11) unsigned NOT NULL,
  `vehicle_id` int(11) unsigned NOT NULL,
  `path` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `logs_fk1` (`vehicle_id`),
  KEY `fk_logs` (`flightplan_id`),
  CONSTRAINT `fk_logs` FOREIGN KEY (`flightplan_id`) REFERENCES `flightplans` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table projects
# ------------------------------------------------------------

DROP TABLE IF EXISTS `projects`;

CREATE TABLE `projects` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(11) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `notes` text,
  `color` varchar(100) NOT NULL DEFAULT '',
  `sample` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `created_by` int(11) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_projects` (`company_id`),
  CONSTRAINT `fk_projects` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `projects` WRITE;
/*!40000 ALTER TABLE `projects` DISABLE KEYS */;

INSERT INTO `projects` (`id`, `company_id`, `name`, `notes`, `color`, `sample`, `created_by`, `created_at`)
VALUES
	(6,2,'Sample Project','Sample Project Notes','rgb(25,133,216)',1,2,'2016-08-16 13:05:54'),
	(7,2,'test','test','rgb(147,206,107)',0,2,'2016-08-16 13:13:28');

/*!40000 ALTER TABLE `projects` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table reports
# ------------------------------------------------------------

DROP TABLE IF EXISTS `reports`;

CREATE TABLE `reports` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(11) unsigned NOT NULL,
  `parent_id` int(11) unsigned DEFAULT NULL,
  `parent_name` varchar(255) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `type` enum('faa_flight_report') NOT NULL DEFAULT 'faa_flight_report',
  `path` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table statuses
# ------------------------------------------------------------

DROP TABLE IF EXISTS `statuses`;

CREATE TABLE `statuses` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `class` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `statuses` WRITE;
/*!40000 ALTER TABLE `statuses` DISABLE KEYS */;

INSERT INTO `statuses` (`id`, `name`, `class`)
VALUES
	(1,'Active','bg-success'),
	(2,'Grounded','bg-danger'),
	(3,'Retired','bg-warning');

/*!40000 ALTER TABLE `statuses` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table subscriptions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `subscriptions`;

CREATE TABLE `subscriptions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `archived` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `customer_id` varchar(255) NOT NULL DEFAULT '',
  `customer_email` varchar(255) NOT NULL DEFAULT '',
  `subscription_id` varchar(255) NOT NULL DEFAULT '',
  `subscription_plan` varchar(255) NOT NULL,
  `subscription_start` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `subscription_end` timestamp NULL DEFAULT NULL,
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `subscriptions` WRITE;
/*!40000 ALTER TABLE `subscriptions` DISABLE KEYS */;

INSERT INTO `subscriptions` (`id`, `archived`, `customer_id`, `customer_email`, `subscription_id`, `subscription_plan`, `subscription_start`, `subscription_end`, `deleted`)
VALUES
	(5,0,'cus_917kuyAhodJH40','jelena@vrhsolutions.com','sub_917kEjVDCC8lhq','professional','2016-08-16 13:13:19','2016-09-15 13:13:19',0);

/*!40000 ALTER TABLE `subscriptions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_permissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_permissions`;

CREATE TABLE `user_permissions` (
  `id` int(1) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `class` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `user_permissions` WRITE;
/*!40000 ALTER TABLE `user_permissions` DISABLE KEYS */;

INSERT INTO `user_permissions` (`id`, `name`, `class`)
VALUES
	(1,'Administrator','bg-danger'),
	(2,'Editor','bg-success'),
	(3,'Viewer','bg-warning');

/*!40000 ALTER TABLE `user_permissions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_roles`;

CREATE TABLE `user_roles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `class` varchar(100) DEFAULT '',
  `form_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `user_roles` WRITE;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;

INSERT INTO `user_roles` (`id`, `name`, `class`, `form_name`)
VALUES
	(1,'Manager','bg-info','manager'),
	(2,'Pilot','bg-success','pilot'),
	(3,'Observer','bg-warning','observer'),
	(4,'Payload Operator','bg-danger','payload_operator'),
	(5,'Remote Observer','bg-primary','remote_observer');

/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `archived` tinyint(1) NOT NULL DEFAULT '0',
  `help` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `sample` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `company_id` int(11) unsigned NOT NULL,
  `permission_id` tinyint(1) unsigned NOT NULL DEFAULT '2',
  `provider` varchar(45) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `photo` varchar(255) DEFAULT '/assets/images/avatar.png',
  `active` tinyint(1) unsigned DEFAULT '0',
  `activation_key` varchar(40) DEFAULT NULL,
  `reset_key` varchar(40) DEFAULT NULL,
  `invited` tinyint(1) unsigned DEFAULT '0',
  `invited_by` int(11) NOT NULL DEFAULT '0',
  `invitation_key` varchar(40) DEFAULT NULL,
  `google_id` varchar(45) DEFAULT NULL,
  `google_token` varchar(255) DEFAULT NULL,
  `google_display_name` varchar(255) DEFAULT NULL,
  `google_family_name` varchar(45) DEFAULT NULL,
  `google_given_name` varchar(45) DEFAULT NULL,
  `google_middle_name` varchar(45) DEFAULT NULL,
  `google_email` varchar(255) DEFAULT NULL,
  `google_email_type` varchar(45) DEFAULT NULL,
  `google_photo` varchar(255) DEFAULT '/assets/images/avatar.png',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_users` (`company_id`),
  CONSTRAINT `fk_users` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `archived`, `help`, `sample`, `company_id`, `permission_id`, `provider`, `name`, `email`, `password`, `mobile`, `photo`, `active`, `activation_key`, `reset_key`, `invited`, `invited_by`, `invitation_key`, `google_id`, `google_token`, `google_display_name`, `google_family_name`, `google_given_name`, `google_middle_name`, `google_email`, `google_email_type`, `google_photo`, `created_at`)
VALUES
	(2,0,0,1,2,1,'local','Jelena Kovacic Bozic','jelena@vrhsolutions.com','$2a$08$FKtBrCIJNiwiiFWHvGq/Tu/EUDsOf8KuOVYleNeTHOlvxeIQShgWW',NULL,'/assets/images/avatar.png',1,'Aoj26yHWCXtN1aTh17Bgequ6lxABXxDf9ww0jnSY',NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/assets/images/avatar.png','2016-08-16 13:05:54');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table vehicles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `vehicles`;

CREATE TABLE `vehicles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(11) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `manufacturer` varchar(255) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `serial_number` varchar(255) DEFAULT NULL,
  `status` int(10) NOT NULL DEFAULT '1' COMMENT '1 = active, 2 = grounded, 3 = retired',
  `notes` text,
  `faa_aircraft_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_vehicles` (`company_id`),
  CONSTRAINT `fk_vehicles` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `vehicles` WRITE;
/*!40000 ALTER TABLE `vehicles` DISABLE KEYS */;

INSERT INTO `vehicles` (`id`, `company_id`, `name`, `type`, `manufacturer`, `model`, `serial_number`, `status`, `notes`, `faa_aircraft_id`)
VALUES
	(2,2,'Sample Drone','drone','Easy Aerial Inc.','Easy Drone XL Pro','SN1234567890',1,'Sample Vehicle Notes','123'),
	(3,2,'aaa','bbb','ccc','ddd','eee',1,'ggg','fffaaa'),
	(4,2,'dsd','dsd','dsd','sd','dsd',1,'','undefined'),
	(5,2,'yty','yty','tyty','tyty','tyty',2,'','undefined'),
	(6,2,'jj','y','y','y','y',1,'yyy','y'),
	(7,2,'555','5','5','5','5',1,'5','undefined'),
	(8,2,'88','88','88','8','8',1,'888','8888'),
	(9,2,'io','iop','iop','iop','iop',2,'iop','iop');

/*!40000 ALTER TABLE `vehicles` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
