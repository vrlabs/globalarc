// GLOBAL VALIDATION RULES
var validationRules = {
    highlight: function (element, errorClass) {
        $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function (element, errorClass) {
        $(element).closest('.form-group').removeClass('has-error');
    }
};

// COOKIES
function writeCookie(name, value, days) {
    var date;
    var expires;
    if (days) {
        date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    } else {
        expires = "";
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
    var cookie;
    var cookieName = name + "=";
    var cookies = document.cookie.split(';');
    for (var i = 0; i < cookies.length; i++) {
        cookie = cookies[i];
        while (cookie.charAt(0) == ' ') {
            cookie = cookie.substring(1, cookie.length);
        }
        if (cookie.indexOf(cookieName) == 0) {
            return cookie.substring(cookieName.length, cookie.length);
        }
    }
    return '';
}

// PASSWORD VALIDATION
function validatePassword() {
    var password = document.getElementById('password');
    var confirmPassword = document.getElementById('confirm_password');

    if (password.value != confirmPassword.value) {
        confirmPassword.setCustomValidity('Passwords don\'t match!');
    } else {
        confirmPassword.setCustomValidity('');
    }
}


// REGISTER
function submitRegisterForm() {
    var rules = validationRules;

    rules.rules = {
        name: {
            required: true
        },
        company_name: {
            required: true,
            remote: {
                url: '/validate/company-available',
                type: 'POST'
            }
        },
        email: {
            required: true,
            email: true,
            remote: {
                url: '/validate/email-available',
                type: 'POST'
            }
        },
        password: {
            required: true,
            rangelength: [6, 20]
        },
        confirm_password: {
            required: true,
            equalTo: '#password'
        }
    }

    rules.messages = {
        name: 'Please enter your name.',
        company: {
            required: 'Please enter your company name.',
            remote: jQuery.validator.format('{0} is already taken.')
        },
        email: {
            required: 'Please enter your email address.',
            email: 'Please enter a valid email address.',
            remote: jQuery.validator.format('{0} is already taken.')
        },
        password: {
            required: 'Please enter password.',
            rangelength: 'The password entered must be between 6-20 characters long.'
        },
        confirm_password: 'Passwords must match.'
    }

    $("#form-register").validate(rules);

    if ($('#form-register').valid()) {
        $('#form-register').submit();
    }
}

// PROFILE
function submitProfileForm() {
    var rules = validationRules;

    rules.rules = {
        name: {
            required: true
        },
        email: {
            required: true,
            email: true,
            remote: {
                url: '/validate/email-available-existing',
                type: 'POST'
            }
        },
        password: {
            rangelength: [6, 20]
        },
        confirm_password: {
            equalTo: '#password'
        }
    }

    rules.messages = {
        name: 'Please enter your name.',
        email: {
            required: 'Please enter your email address.',
            email: 'Please enter a valid email address.',
            remote: jQuery.validator.format('{0} is already taken.')
        },
        password: 'The password entered must be between 6-20 characters long.',
        confirm_password: 'Passwords must match.'
    }

    $("#form-profile").validate(rules);


    if ($('#form-profile').valid()) {
        var name = $('#name').val();
        var email = $('#email').val();
        var password = $('#password').val();

        $.ajax({
            type: 'POST',
            contentType: 'application/json',
            cache: false,
            url: '/user/profile-local',
            data: JSON.stringify({
                name: name,
                email: email,
                password: password
            }),
            success: function (data) {
                location.reload();
            },
            error: function (xhr, status, error) {
                alert(error);
            }
        });
    }
}

function submitProfileProfessionalInfoForm() {
    var rules = validationRules;

    rules.rules = {
        mobile: {
            required: true
        }
    }

    rules.messages = {
        mobile: 'Please enter your mobile phone.'
    }

    $("#form-profile-professional-info").validate(rules);

    if ($('#form-profile-professional-info').valid()) {
        var mobile = $('#mobile').val();
        $.ajax({
            type: 'POST',
            contentType: 'application/json',
            cache: false,
            url: '/user/profile-professional',
            data: JSON.stringify({
                mobile: mobile,
            }),
            success: function (data) {
                location.reload();
            },
            error: function (xhr, status, error) {
                alert(error);
            }
        });
    }
}


// SETTINGS
function submitSettingsForm() {
    var rules = validationRules;

    rules.rules = {
        name: {
            required: true,
            remote: {
                url: '/validate/company-available-existing',
                type: 'POST'
            }
        }
    }

    rules.messages = {
        name: 'Please enter your company name.'
    }

    $("#form-settings").validate(rules);

    if ($('#form-settings').valid()) {
        $('#form-settings').submit();
    }
}

// HIDDEN FIELDS
function setHiddenFieldValue(fieldID, value) {
    $('#' + fieldID).attr('value', value);
}

// MESSAGES
function closeWarning() {
    $.ajax({
        url: '/general/set-alert-cookie',
        type: "POST",
        contentType: 'application/json',
        success: function (data) {
            $('#warning').remove();
        },
        error: function (xhr, status, error) {
            alert(error);
        }
    });
}

function closeMessage() {
    $('#message').remove();
}
function hideMessage() {
    $('#message').addClass('hide');
}

// NOTES
function setNotes(note) {
    $('#modal-notes .modal-body').html(note);
}

// PROJECTS
function setDeleteProjectButton(id) {
    $('#modal-delete-button').attr('onclick', 'deleteProject(' + id + ');');
}

function deleteProject(id) {
    location.href = '/projects/delete/' + id;
}

// VEHICLES
function setDeleteVehicleButton(id) {
    $('#modal-delete-button').attr('onclick', 'deleteVehicle(' + id + ');');
}

function deleteVehicle(id) {
    location.href = '/vehicles/delete/' + id;
}

// BATTERIES
function setDeleteBatteryButton(id) {
    $('#modal-delete-button').attr('onclick', 'deleteBattery(' + id + ');');
}

function deleteBattery(id) {
    location.href = '/batteries/delete/' + id;
}

// DOCUMENTS
function setDeleteDocumentButton(id, file) {
    $('#modal-delete-button').attr('onclick', 'deleteDocument(' + id + ', \'' + file + '\');');
}

function deleteDocument(id, fileName) {
    $('#loader-content').addClass('active');
    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        cache: false,
        url: '/documents/delete',
        data: JSON.stringify({
            id: id,
            fileName: fileName
        }),
        success: function (data) {
            $('#loader-content').removeClass('active');
            data = JSON.parse(data);

            if (data.result == 'ok') {
                location.reload();
            }
        },
        error: function () {
            alert('Ooops! Something went wrong. Please, try again.');
        }
    });
}
// TEAM
function setDeleteMemberButton(userID) {
    $('#modal-delete-button').attr('onclick', 'deleteUser(' + userID + ');');
}
function deleteUser(id) {
    location.href = '/team/delete/' + id;
}
// FLIGHTPLANS
function setDeleteFlightplanButton(projectID, flightplanID) {
    $('#modal-delete-button').attr('onclick', 'deleteFlightplan(' + projectID + ', ' + flightplanID + ');');
}

function deleteFlightplan(projectID, flightplanID) {
    location.href = '/flightplans/delete/' + projectID + '/' + flightplanID;
}

function removeTeamMember(button) {
    var select = $(button).parent().find('select');
    $(select).val($(select).find('option.empty-option').val());
    checkTeamMembers();
}

function changeSaveButton(type) {
    var temp = type.charAt(0).toUpperCase() + type.slice(1);
    var html = '<a href="#" onclick="addNewEquipment(\'' + type + '\');" class="btn btn-default btn-info save-link pull-left">Save</a>';
    html += '<div class="alert hide wrapper-sm m-b-none pull-right" id="' + type + '-message"><div><span></span></div></div>';
    $('#modal-add-' + type + ' .submit-container').html(html);
}

function changeDocumentButton(type) {
    var html = '<a href="#" onclick="uploadNewDocument();" class="btn btn-default btn-info save-link pull-left">Save</a>';
    html += '<div class="alert hide wrapper-sm m-b-none pull-right" id="document-message"><div><span></span></div></div>';
    $('#modal-add-document .submit-container').html(html);
}

// ajax call to add new vehicle or battery from 'Add New Flightplan' page
function addNewEquipment(type) {
    var data = {};
    var url = '';

    if (type == 'vehicle') {
        data = {
            name: $('#vehicle-name').val(),
            type: $('input[name="type"]').val(),
            manufacturer: $('input[name="manufacturer"]').val(),
            model: $('input[name="model"]').val(),
            serial_number: $('#vehicle-serial-number').val(),
            status: $('select[name="status"]').val(),
            faa_aircraft_id: $('input[name="faa_aircraft_id"]').val(),
            notes: $('#vehicle-notes').val()
        };
        url = '/vehicles/add-new-vehicle';

        var vehicleRules = validationRules;
        vehicleRules.rules = {
            name: "required",
            type: "required"
        }
        vehicleRules.messages = {
            name: "Vehicle name is required.",
            type: "Vehicle type is required."
        }

        $("#vehicle-form").validate(vehicleRules);


    } else if (type == 'battery') {
        data = {
            name: $('#battery-name').val(),
            serial_number: $('#battery-serial-number').val(),
            charge: $('input[name="charge"]').val(),
            voltage: $('input[name="voltage"]').val(),
            status: $('select[name="status"]').val(),
            notes: $('#battery-notes').val()
        };
        url = '/batteries/add-new-battery';

        var batteryRules = validationRules;
        batteryRules.rules = {
            name: "required",
            serial_number: "required",
            charge: {
                digits: true
            },
            voltage: {
                number: true
            }
        }
        batteryRules.messages = {
            name: "Battery name is required.",
            type: "Serial number is required.",
            charge: "Enter battery charge as integer number.",
            voltage: "Enter battery voltage as decimal number."
        }

        $("#battery-form").validate(batteryRules);
    }

    $('#' + type + '-message').attr('class', 'alert hide');
    $('#' + type + '-message span').html('');

    if ($('#' + type + '-form').valid()) {
        $.ajax({
            type: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json',
            dataType: 'json',
            cache: false,
            url: url,
            success: function (data) {
                if (data.result == 'ok') {
                    $('#' + type + '-message').attr('class', 'alert alert-success wrapper-sm m-b-none pull-right');
                    $('#' + type + '-message span').html('You successfully added new ' + type + '.');

                    // add new vehicle/battery in select element (create select if it doesn't exist)                
                    if (type == 'vehicle') {
                        $('#vehicle-error').html('');
                        if ($('#select-vehicle').length > 0) {
                            $('#select-vehicle').append('<option value="' + data.id + '" selected="selected">' + $('#vehicle-name').val() + '</option>');
                        } else {
                            html = '<select name="vehicle" class="form-control m-b w-auto-xs w-xxl m-r-sm" id="select-vehicle">' +
                                '<option value="' + data.id + '" selected="selected">' + $('#vehicle-name').val() + '</option>' +
                                '</select>';
                            $('#vehicles').html(html)
                        }

                    } else if (type == 'battery') {
                        if ($('#select-battery').length > 0) {
                            $('#select-battery').append('<option value="' + data.id + '">' + $('#battery-name').val() + '</option>');
                            $('#additional-batteries select').each(function (index) {
                                $(this).append('<option value="' + data.id + '">' + $('#battery-name').val() + '</option>');
                            });


                        } else {
                            html =

                                '<div class="battery-container">' +
                                '<div class="col-lg-11 col-md-11 col-sm-11 col-xs-11 p-l-none p-r-none">' +
                                '<select name="battery" class="form-control m-b w-full" id="select-battery">' +
                                '<option value="' + data.id + '" selected="selected">' + $('#battery-name').val() + '</option>' +
                                '</select>' +
                                '</div > ' +
                                '<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 p-r-none text-right">' +
                                '<a class="btn btn-sm btn-info m-t-xxs" href="#/" onclick="removeBattery(this);"><i class="fa fa-minus"></i></a>' +
                                '</div>' +
                                '</div>' +
                                '<div id="additional-batteries"></div>' +
                                '<a class="btn btn-info btn-addon m-r-sm" onclick="addAdditionalBattery();"><i class="fa fa-plus"></i>Add Additional Battery</a>';
                            $('#batteries').html(html)
                        }
                    }

                    // close modal window after timeout and clear all the fields
                    setTimeout(function () {
                        $('#modal-add-' + type).modal('hide');
                        if (type == 'vehicle') {
                            $('#vehicle-name').val('');
                            $('input[name="type"]').val('');
                            $('input[name="manufacturer"]').val('');
                            $('input[name="model"]').val('');
                            $('#vehicle-serial-number').val('');
                            $('input[name="faa_aircraft_id"]').val('');
                            $('select[name="status"]').val(1);
                            $('#vehicle-notes').val('');
                        } else if (type == 'battery') {
                            $('#battery-name').val('');
                            $('#battery-serial-number').val('');
                            $('input[name="charge"]').val('');
                            $('input[name="voltage"]').val('');
                            $('select[name="status"]').val(1);
                            $('#battery-notes').val('');
                        }
                    }, 2000);

                } else if (data.result == 'error') {
                    $('#' + type + '-message').attr('class', 'alert alert-danger wrapper-sm m-b-none pull-right');
                    $('#' + type + '-message span').html(data.messages);
                }
            },
            error: function () {
                $('#' + type + '-message').attr('class', 'alert alert-danger wrapper-sm m-b-none pull-right');
                $('#' + type + '-message span').html('Ooops! Something went wrong. Please, try again.');
            }
        });
    }
}

function addAdditionalBattery() {
    if ($('#select-battery').length > 0) {
        $('#select-battery').parent().parent().clone().appendTo('#additional-batteries');
        $('#additional-batteries select').each(function (index) {
            $(this).removeAttr('id');
        });
        $('#additional-batteries a').each(function (index) {
            $(this).css('display', 'inline-block');
        });
    } else {
        $.ajax({
            type: 'POST',
            contentType: 'application/json',
            cache: false,
            url: '/batteries/get-all-batteries',
            success: function (data) {
                if (data.result == 'ok') {
                    var html = '<div class="battery-container">' +
                        '<div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">' +
                        '<select name="battery" class="form-control m-b w-full m-r-sm" id="select-battery">';
                    for (var i = 0; i < data.batteries.length; i++) {
                        html += '<option value="' + data.batteries[i].id + '">' + data.batteries[i].name + '</option>';
                    }
                    html += '</select></div>' +
                        '<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 p-r-none text-right">' +
                        '< a class="btn btn-sm btn-info m-t-xxs" href= "#/" onclick= "removeBattery(this);" > <i class="fa fa-minus"></i></a ></div > </div>';

                    $('#additional-batteries').before(html);
                }
            },
            error: function () {
                alert('Ooops! Something went wrong. Please, try again.');
            }
        });
    }
}

function removeBattery(button) {
    $(button).parent().parent().remove();
}

var teamMembers = [];
// ADD/EDIT FLIGHTPLAN
function checkTeamMembers() {
    var selectedTeamMembers = [];

    if ($('#manager').val() != '' || $('#pilot').val() != '' || $('#observer').val() != '' || $('#payload_operator').val() != '' || $('#remote_observer').val() != '') {
        if ($('#manager').val() != '') {
            selectedTeamMembers.push($('#manager').val());
        }
        if ($('#pilot').val() != '') {
            selectedTeamMembers.push($('#pilot').val());
        }
        if ($('#observer').val() != '') {
            selectedTeamMembers.push($('#observer').val());
        }
        if ($('#payload_operator').val() != '') {
            selectedTeamMembers.push($('#payload_operator').val());
        }
        if ($('#remote_observer').val() != '') {
            selectedTeamMembers.push($('#remote_observer').val());
        }

        if (hasDuplicates(selectedTeamMembers)) {
            $('#team-error').html('One team member can have only one role!');
            return false;
        } else {
            $('#team-error').html('');
            return true;
        }
    } else {
        selectedTeamMembers = [];
        $('#team-error').html('At least one team member must be selected!');
        return false;
    }
}

function hasDuplicates(array) {
    return (new Set(array)).size !== array.length;
}

function checkVehicle() {
    if ($('select#select-vehicle').length) {
        $('#vehicle-error').html('');
        return true;
    } else {
        $('#vehicle-error').html('Create vehicle first!');
        return false;
    }
}

function checkFlightTime() {
    var start = new Date($('input[name=start_time]').val());
    var end = new Date($('input[name=end_time]').val());

    if (start >= end) {
        $('#time-error').html('Start time has to be before the end time!');
        return false;
    } else {
        $('#time-error').html('');
        return true;
    }
}


var getCenter = function (array) {
    var x = array.map(function (a) { return a[0] });
    var y = array.map(function (a) { return a[1] });
    var minX = Math.min.apply(null, x);
    var maxX = Math.max.apply(null, x);
    var minY = Math.min.apply(null, y);
    var maxY = Math.max.apply(null, y);
    return [(minX + maxX) / 2, (minY + maxY) / 2];
}

function getMinMaxOf2DIndex(arr, idx) {
    return {
        min: Math.min.apply(null, arr.map(function (e) { return e[idx] })),
        max: Math.max.apply(null, arr.map(function (e) { return e[idx] }))
    }
}

function findBoundingBox(array) {
    var x = getMinMaxOf2DIndex(array, 0);
    var y = getMinMaxOf2DIndex(array, 1);

    var result = {};

    var latBuffer = (x.max - x.min) * 0.05;
    var lonBuffer = (y.max - y.min) * 0.05;

    result.minX = x.min - latBuffer;
    result.minY = y.min - lonBuffer;
    result.maxX = x.max + latBuffer;
    result.maxY = y.max + lonBuffer;

    return result;
}

function fit(map, minX, minY, maxX, maxY) {
    var array = [];
    var minArray = [minX, maxY];
    var maxArray = [maxX, minY];
    array.push(minArray);
    array.push(maxArray);
    map.fitBounds(array);
}



function setPreviousLocationsDropdown(projectID) {
    if (Draw != undefined) {
        Draw.deleteAll();
    }
    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        cache: false,
        url: '/flightplans/get-locations/' + projectID,
        success: function (data) {
            if (data.result == 'ok') {
                var html = '<option value="">Choose Location</option>';
                for (var i = 0; i < data.locations.length; i++) {
                    html += '<option value="' + data.locations[i].id + '"><img src="' + data.locations[i].path + '" height="20"/>' + data.locations[i].name + '</option>';
                }

                $('#locationSelect').html(html);

                $('#previousLocationsButton').addClass('hidden');
                $('#newLocationButton').removeClass('hidden');

                $('#locationInputContainer').addClass('hidden');
                $('#locationSelectContainer').removeClass('hidden');

                $('#how_to_save').val('save');

                $('#map-error').css('display', 'none')

            } else {
                alert('Ooops! Something went wrong. Please, try again.');
            }
        },
        error: function () {
            alert('Ooops! Something went wrong. Please, try again.');
        }
    });
}

function setNewLocationInput() {
    $('#previousLocationsButton').removeClass('hidden');
    $('#newLocationButton').addClass('hidden');

    $('#locationInputContainer').removeClass('hidden');
    $('#locationSelectContainer').addClass('hidden');

    Draw.deleteAll();
}

function saveLocationAs(value) {
    $('#how_to_save').val(value);
    if (value == 'old' || value == 'save') {
        $('#locationInputContainer').addClass('hidden');
        $('#locationSelectContainer').removeClass('hidden');
    } else if (value == 'new') {
        $('#locationInputContainer').removeClass('hidden');
        $('#locationSelectContainer').addClass('hidden');
    }
}


function submitFlightplanForm() {
    $('#loader-content').addClass('active');

    var flightplanRules = validationRules;

    flightplanRules.rules = {
        name: 'required',
        group_name: 'required',
        start_time: {
            required: true
        },
        end_time: {
            required: true
        }
    };
    flightplanRules.messages = {
        name: 'Flight plan name is required.',
        group_name: 'Flight plan group name is required.',
        start_time: {
            required: 'Flight start time is required.'
        },
        end_time: {
            required: 'Flight end time is required.'
        }
    };


    $("#flightplan-form").validate(flightplanRules);

    if ($('#flightplan-form').valid()) {
        if (checkTeamMembers() && checkVehicle() && checkFlightTime() && checkLocationName() && checkLocation()) {
            $('#location_name').val($('#location_name_input').val());

            unselectAllLayers();
            centerMap();

            setTimeout(function () {

                var img = new Image();
                var mapCanvas = document.querySelector('.mapboxgl-canvas');
                $('#screenshot').val(mapCanvas.toDataURL());
                $("#flightplan-form").submit();
            }, 3000);
        } else {
            $('#loader-content').removeClass('active');
        }
    } else {
        $('#loader-content').removeClass('active');
        var a = checkTeamMembers();
        var b = checkVehicle();
        var c = checkFlightTime();
        var d = checkLocationName();
        var e = checkLocation();

    }
}


function checkLocationName() {
    if ($('#how_to_save').val() == 'new') {
        if ($('#location_name_input').val() == '') {
            $('#location_name_input').css('border', '1px solid #f05050');
            $('#location-name-error').css('display', 'block');
            return false;
        } else {
            $('#location_name_input').css('border', '1px solid #cfdadd');
            $('#location-name-error').css('display', 'none');
            return true;
        }
    } else {
        return true;
    }
}

function checkLocation() {
    if ($('input[name="coordinates"]').val() == '') {
        $('#map-error').css('display', 'block');
        return false;
    } else {
        $('#map-error').css('display', 'none');
        return true;
    }
}


function setLocationSnapshot(snapshot, name) {
    $('#modal-location .modal-title').html(name);
    $('#modal-location .modal-body').html('<img src="' + snapshot + '" width="100%"/>');
}

function isChrome() {
    var isChromium = window.chrome;
    var winNav = window.navigator;
    var vendorName = winNav.vendor;
    var isOpera = winNav.userAgent.indexOf("OPR") > -1;
    var isIEedge = winNav.userAgent.indexOf("Edge") > -1;
    var isIOSChrome = winNav.userAgent.match("CriOS");

    if (isIOSChrome) {
        return true;
    } else if (isChromium !== null && isChromium !== undefined && vendorName === "Google Inc." && isOpera == false && isIEedge == false) {
        return true;
    } else {
        return false;
    }
}

function isMobile() {
    var check = false;
    (function (a) { if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true })(navigator.userAgent || navigator.vendor || window.opera);
    return check;
}

function browserNotice(margin, flightplan) {
    if (!isChrome()) {
        var link = '';
        if (isMobile()) {
            link = 'https://www.google.com/chrome/browser/mobile/index.html';
        } else {
            link = 'https://www.google.com/chrome/browser/desktop/index.html';
        }

        var imageClass = '';
        if (margin) {
            imageClass = 'class="m-t-md m-b-md"';
        } else {
            imageClass = 'class="m-t-xs m-b-xs"';
        }

        var html = '';
        if (flightplan) {
            html = 'The browser you are using is not compatible with this application. For the best results please use &nbsp; &nbsp;<a href="' + link + '"><img src="/assets/images/chrome-logo.png" ' + imageClass + '/></a>';
        } else {
            html = 'The browser you are using is not compatible with this application. For the best results please use &nbsp; &nbsp;<a href="' + link + '"><img src="/assets/images/chrome-logo.png" ' + imageClass + '/></a>';
        }
        $('#browser-check').removeClass('hidden');
        $('#browser-check').html(html);
    }
}

function browserModal() {
    var html = '';
    var link = '';

    if (isMobile()) {
        link = 'https://www.google.com/chrome/browser/mobile/index.html';
    } else {
        link = 'https://www.google.com/chrome/browser/desktop/index.html';
    }

    $('#modal-browser-popup .modal-title').html('Incompatible Browser');
    $('#modal-browser-popup .modal-body').html('This function is not available in your browser. Please use <br><a href="' + link + '"><img src="/assets/images/chrome-logo.png" class="m-t-md m-b-md"/></a>');
}

function cameraNotice() {
    DetectRTC.load(function () {
        var hasWebcam = false;
        DetectRTC.MediaDevices.forEach(function (device) {
            if (device.kind.indexOf('video') !== -1) {
                hasWebcam = true;
            }
        });

        if (!hasWebcam) {
            var html = '<i class="fa  fa-video-camera text-danger"></i>&nbsp;&nbsp;&nbsp;To view images/video from your vehicle you have to have a webcam.';
            $('#camera-check').html(html);
            $('#camera-check').removeClass('hidden');
        }
    });
}

function dataURItoBlob(dataURI) {
    // convert base64/URLEncoded data component to raw binary data held in a string
    var binary = atob(dataURI.split(',')[1]);
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    var array = [];
    for (var i = 0; i < binary.length; i++) {
        array.push(binary.charCodeAt(i));
    }
    return new Blob([new Uint8Array(array)], { type: mimeString });
}

function closeWelcomePage() {
    if ($('#hideWelcomeForever').is(':checked')) {
        $.ajax({
            type: 'POST',
            contentType: 'application/json',
            cache: false,
            url: '/user/hide-welcome',
            error: function () {
                alert('Ooops! Something went wrong. Please, try again.');
            }
        });
    }
    $('#welcome').css('display', 'none');
}


function startTour() {
    $('#welcome').css('display', 'none');
    hopscotch.startTour(tour);
}
function startNewProjectTour() {
    if (hopscotch.getState() === "tour:5") {
        hopscotch.startTour(tour);
    }
}
function startAllProjectsTour() {
    if (hopscotch.getState() === "tour:6") {
        hopscotch.startTour(tour);
    }
}
function startAddFlightplanTour() {
    if (hopscotch.getState() === "tour:7") {
        hopscotch.startTour(tour);
    }
}
function startViewFlightplanTour() {
    if (hopscotch.getState() === "tour:15") {
        hopscotch.startTour(tour);
    }
}

function checkPlanUsers(redirectURL) {
    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        cache: false,
        url: '/user/check-plan-users',
        success: function (data) {
            var modal = '<div id="modal-user-limit" class="modal fade" role="dialog">' +
                '<div class="modal-dialog">' +
                '<div class="modal-content">' +
                '<div class="modal-header">' +
                '<h4 class="modal-title">Users Limit</h4>' +
                '</div>' +
                '<div class="modal-body text-center">You have reached available number of users limit.<br>Go to your <a href="/user/settings" class="text-info">settings</a> page to upgrade your plan.</div>' +
                // '<div class="modal-body text-center">You have reached available number of users limit.</div>' +
                '<div class="modal-footer">' +
                '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>';
            if (data.result == 'error') {
                alert('Ooops! Something went wrong. Please, try again.');
            } else if (data.result == 'yes') {
                window.location.href = redirectURL;
            } else if (data.result == 'no') {
                if ($("#modal-user-limit").length == 0) {
                    $('body').append(modal);
                }
                $('#modal-user-limit').modal('show');
            }
        },
        error: function () {
            alert('Ooops! Something went wrong. Please, try again.');
        }
    });
}

function checkPlanStorageSpace(redirectURL) {
    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        cache: false,
        url: '/user/check-plan-storage-space',
        success: function (data) {
            var modal = '<div id="modal-storage-limit" class="modal fade" role="dialog">' +
                '<div class="modal-dialog">' +
                '<div class="modal-content">' +
                '<div class="modal-header">' +
                '<h4 class="modal-title">Storage Space Limit</h4>' +
                '</div>' +
                '<div class="modal-body text-center">You have reached available storage space limit.<br>Go to your <a href="/user/settings" class="text-info">settings</a> page to upgrade your plan.</div>' +
                '<div class="modal-footer">' +
                '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>';
            if (data.result == 'error') {
                alert('Ooops! Something went wrong. Please, try again.');
            } else if (data.result == 'yes') {
                window.location.href = redirectURL;
            } else if (data.result == 'no') {
                if ($("#modal-storage-limit").length == 0) {
                    $('body').append(modal);
                }
                $('#modal-storage-limit').modal('show');
            }
        },
        error: function () {
            alert('Ooops! Something went wrong. Please, try again.');
        }
    });
}

function checkPlanProjects(redirectURL) {
    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        cache: false,
        url: '/user/check-plan-projects',
        success: function (data) {
            var modal = '<div id="modal-project-limit" class="modal fade" role="dialog">' +
                '<div class="modal-dialog">' +
                '<div class="modal-content">' +
                '<div class="modal-header">' +
                '<h4 class="modal-title">Projects Limit</h4>' +
                '</div>' +
                '<div class="modal-body text-center">You have reached available number of projects limit.<br>Go to your <a href="/user/settings" class="text-info">settings</a> page to upgrade your plan or edit an existing project.</div>' +
                // '<div class="modal-body text-center">You have reached available number of projects limit.</div>' +
                '<div class="modal-footer">' +
                '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>';


            if (data.result == 'error') {
                alert('Ooops! Something went wrong. Please, try again.');
            } else if (data.result == 'yes') {
                window.location.href = redirectURL;
            } else if (data.result == 'no') {
                if ($("#modal-project-limit").length == 0) {
                    $('body').append(modal);
                }
                $('#modal-project-limit').modal('show');
            }
        },
        error: function () {
            alert('Ooops! Something went wrong. Please, try again.');
        }
    });
}

function checkPlanFlightplans(redirectURL) {
    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        cache: false,
        url: '/user/check-plan-flightplans',
        success: function (data) {
            var modal = '<div id="modal-flightplan-limit" class="modal fade" role="dialog">' +
                '<div class="modal-dialog">' +
                '<div class="modal-content">' +
                '<div class="modal-header">' +
                '<h4 class="modal-title">Flight Plans Limit</h4>' +
                '</div>' +
                '<div class="modal-body text-center">You have reached available number of flight plans limit for this month.<br>Go to your <a href="/user/settings" class="text-info">settings</a> page to upgrade your plan or edit an existing flight plan.</div>' +
                // '<div class="modal-body text-center">You have reached available number of flight plans limit for this month.</div>' +
                '<div class="modal-footer">' +
                '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>';


            if (data.result == 'error') {
                alert('Ooops! Something went wrong. Please, try again.');
            } else if (data.result == 'yes') {
                window.location.href = redirectURL;
            } else if (data.result == 'no') {
                if ($("#modal-flightplan-limit").length == 0) {
                    $('body').append(modal);
                }
                $('#modal-flightplan-limit').modal('show');
            }
        },
        error: function () {
            alert('Ooops! Something went wrong. Please, try again.');
        }
    });
}


function generateNewAccessCode() {
    $('#loader').removeClass('hidden');
    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        cache: false,
        url: '/api/generate-new-access-code'
    });

    setTimeout(function () {
        window.location.href = '/user/settings';
    }, 3000);
}

function changeVehicleUnitID() {
    $('#change-vehicle-id-popup-link').trigger('click');
}
function changeMemberUnitID() {
    $('#change-member-id-popup-link').trigger('click');
}
function saveNewID(type, flightplanID) {
    $('#new-' + type + '-id-loader').removeClass('hidden');
    $('#new-' + type + '-id-msg').removeClass('text-danger');
    $('#new-' + type + '-id-msg').removeClass('text-success');
    $('#new-' + type + '-id-msg').html('');

    var id = $('#new-' + type + '-id').val();
    if (id != '') {
        if ($('#button-connect').html().indexOf('Disconnect') != -1) {
            $('#button-connect').trigger('click');
        }
        $('#new-' + type + '-id-loader').removeClass('hidden');


        $.ajax({
            type: 'POST',
            contentType: 'application/json',
            cache: false,
            url: '/flightplans/change-' + type + '-id/' + flightplanID + '/' + id,
            success: function (data) {
                if (data.result == 'ok') {
                    $('#new-' + type + '-id-loader').addClass('hidden');
                    $('#new-' + type + '-id-msg').addClass('text-success');
                    $('#new-' + type + '-id-msg').html('Success!');
                    $('#span' + type + 'ID1').html(id);
                    $('#span' + type + 'ID2').html(id);
                    setTimeout(function () {
                        $('#modal-change-' + type + '-id-popup').modal('hide');
                        $('#new-' + type + '-id').val('');
                        $('#new-' + type + '-id-loader').removeClass('hidden');
                        $('#new-' + type + '-id-msg').removeClass('text-danger');
                        $('#new-' + type + '-id-msg').removeClass('text-success');
                        $('#new-' + type + '-id-msg').html('');
                    }, 2000);
                }
            },
            error: function () {
                $('#new-' + type + '-id-loader').addClass('hidden');
                $('#new-' + type + '-id-msg').addClass('text-danger');
                $('#new-' + type + '-id-msg').html('Ooops! Something went wrong. Please, try again.');
            }
        });
    } else {
        $('#new-' + type + '-id-loader').addClass('hidden');
        $('#new-' + type + '-id-msg').addClass('text-danger');
        $('#new-' + type + '-id-msg').html('Please, enter new id.');
    }
}

function setAirmapInfo(type) {
    if (type == 'recreational') {
        $('#modal-airmap .modal-title').html('Recreational - 5 Mile Radius');
        $('#modal-airmap .modal-body p').html('Community-based guidelines require recreational operators to give notice for flights within 5 statute miles of an airport. Notice must be given to the airport operator and air traffic control tower, if the airport has a tower.');
    } else if (type == 'commercial') {
        $('#modal-airmap .modal-title').html('Commercial - Blanket COA');
        $('#modal-airmap .modal-body p').html('Commercial operators with a Section 333 exemption from the FAA may operate under specified circumstances without a specific Certificate of Waiver or Authorization. For more information, click here (<a href="https://www.faa.gov/news/updates/?newsId=82245" target="_blank" class="text-info">https://www.faa.gov/news/updates/?newsId=82245</a>). Touching this button displays circles around facilities with appropriate radii according to FAA guidance:<ul><li>5 nautical miles (NM) from an airport having an operational control tower; or</li><li>3 NM from an airport with a published instrument flight procedure, but not an operational tower; or</li><li>2 NM from an airport without a published instrument flight procedure or an operational tower; or</li><li>2 NM from a heliport with a published instrument flight procedure</li></ul>');
    } else if (type == 'controlled') {
        $('#modal-airmap .modal-title').html('Controlled Airspace');
        $('#modal-airmap .modal-body p').html('Areas of Class B, C, D, and E controlled airspace below 500 feet are displayed. To simplify the map for small UAS operators, upper level airspace areas are not displayed. Under the FAA Notice of Proposed Rulemaking, small UAS operations would require air traffic control authorization prior to flight in these areas. We anticipate that this will become a final rule in 2016.');
    } else if (type == 'prohibited') {
        $('#modal-airmap .modal-title').html('Prohibited Special Use Airspace');
        $('#modal-airmap .modal-body p').html('Prohibited areas protect the most sensitive areas in the United States, such as the White House and Camp David. Permission from the using agency (such as the Secret Service) is required to enter a Prohibited Area and is almost never available.');
    } else if (type == 'restricted') {
        $('#modal-airmap .modal-title').html('Restricted Special Use Airspace');
        $('#modal-airmap .modal-body p').html('Restricted areas are typically located around military installations or other areas where flight could be hazardous. Permission from the controlling agency (air traffic control) is required to enter these areas and is often not available.');
    } else if (type == 'parks') {
        $('#modal-airmap .modal-title').html('National Parks');
        $('#modal-airmap .modal-body p').html('This layer depicts areas within the boundaries of units of the National Park System. Launching, landing, or operating unmanned aircraft is prohibited on lands and waters administered by the National Park Service within these areas. More information about the location of units of the National Park System and the National Park Service drone ban is available on the website of each park area which can be found on <a href="https://www.nps.gov" class="text-info" target="_blank">www.nps.gov</a>.');
    } else if (type == 'marine') {
        $('#modal-airmap .modal-title').html('NOAA Marine Protection Areas');
        $('#modal-airmap .modal-body p').html('National Oceanic and Atmospheric Administration regulations prohibit certain flights of powered aircraft (including drones) in these areas. More information available at <a target="_blank" href="http://sanctuaries.noaa.gov/flight/welcome.html" class="text-info">http://sanctuaries.noaa.gov/flight/welcome.html</a>');
    } else if (type == 'hospitals') {
        $('#modal-airmap .modal-title').html('Hospitals');
        $('#modal-airmap .modal-body p').html('This layer displays a 250ft radius around the address of properties designated as a hospital or medical facility. In some instances multiple radii may be layered over each building associated with a hospital or medical facility. This layer is valuable to operators because some municipalities have determined that hospitals are sensitive sites with unique safety and privacy concerns. Avoiding these facilities may allow operators to minimize hassles and decrease the likelihood of a dispute.');
    } else if (type == 'schools') {
        $('#modal-airmap .modal-title').html('Schools');
        $('#modal-airmap .modal-body p').html('This layer displays a 250ft radius around the address of properties designated as a school, or at which regularly scheduled school events are conducted (boathouses on lakes, research sites within parks). This layer adds a lot of detail to the map because it includes public and private schools, universities, and other related educational facilities. While this layer seems overwhelming, it is valuable to operators because some municipalities have determined that schools are sensitive areas, or are areas where model aircraft flights are not permitted in, or are only permitted during certain hours or with permission. This layer raises awareness about these properties, empowering operators to minimize hassles and decrease the likelihood of a dispute.');
    } else if (type == 'heliports') {
        $('#modal-airmap .modal-title').html('Heliports');
        $('#modal-airmap .modal-body p').html('This layer displays a 500ft radius around areas designated as heliports. These heliports may not be active, and in some instances may be an empty field where helicopters can land in emergencies. The layer adds a lot of detail in cities, where many buildings have heliports. We include this as an advisory area to help alert UAS operators that they should be particularly alert to helicopter traffic in the area.');
    } else if (type == 'powerplants') {
        $('#modal-airmap .modal-title').html('Power Plants');
        $('#modal-airmap .modal-body p').html('This layer displays a 250ft radius around power generation facilities. Some locales have specific rules regarding flights near critical infrastructure like powerplants. This information is provided for advisory purposes only.');
    }
}

$('.main-logo').hover(
    function () {
        $('.hover-logo').css('width', '105px');
    }, function () {
        $('.hover-logo').css('width', '0px');
    }
);


function downloadDocuments(settingsPage) {
    $('.download-loader').removeClass('hidden');
    $('.download-message').html('');
    $('.download-message').removeClass('text-success');
    $('.download-message').removeClass('text-danger');

    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        cache: false,
        url: '/documents/download-documents',
        success: function (data) {
            $('.download-loader').addClass('hidden');
            if (data.result == 'error') {
                $('.download-message').removeClass('text-success');
                $('.download-message').html('Ooops! Something went wrong. Please, try again.');
                $('.download-message').addClass('text-danger');
            } else if (data.result == 'ok') {
                if (settingsPage) {
                    $('.download-message').removeClass('text-danger');
                    $('.download-message').addClass('text-success');
                    $('.download-message').html('You successfully downloaded your documents.<br>All your documents will be deleted from our servers after you downgrade your account.');
                }
                $('#fileLink a').attr('href', data.zip);
                $('#fileLink a')[0].click();
            }
        },
        error: function () {
            $('.download-message').removeClass('text-success');
            $('.download-loader').addClass('hidden');
            $('.download-message').html('Ooops! Something went wrong. Please, try again.');
            $('.download-message').addClass('text-danger');
        }
    });
}

function downloadDocument(file) {
    $('.download-message').html('');
    $('.download-message').removeClass('text-success');
    $('.download-message').removeClass('text-danger');

    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        cache: false,
        url: '/documents/download-document/' + file,
        success: function (data) {
            if (data.result == 'error') {
                $('.download-message').removeClass('text-success');
                $('.download-message').html('Ooops! Something went wrong. Please, try again.');
                $('.download-message').addClass('text-danger');
            } else if (data.result == 'ok') {
                $('#fileLink a').attr('href', data.file);
                $('#fileLink a')[0].click();
            }
        },
        error: function () {
            $('.download-message').removeClass('text-success');
            $('.download-message').html('Ooops! Something went wrong. Please, try again.');
            $('.download-message').addClass('text-danger');
        }
    });
}

function destroyTree() {
    $("#professionalProjects").jstree("destroy");
    $('#modal-professional').modal('hide');
}

var selectedProjectsAndFlightplans = [];

function openDowngradeModal(modal) {
    selectedProjectsAndFlightplans = [];

    $('#freeProjects').html('');
    $('#professionalProjects').html('');
    $('#freeFlightplans').html('');

    $("#professionalProjects").jstree("destroy");

    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        cache: false,
        url: '/user/get-downgrade-data',
        success: function (data) {
            console.log(data);
            if (data.result == 'error') {
                alert('Ooops! Something went wrong. Please, try again.');
            } else if (data.result == 'ok') {
                var html = '';
                if (modal == 'free') {
                    if (data.content.projects.length > 0) {
                        html = '<p>Your current projects:</p><div class="tree"><ul>';
                        for (var i = 0; i < data.content.projects.length; i++) {
                            html += '<li><div class="radio" style="margin:0; margin-bottom:2px;"><label class="i-checks i-checks-sm"><input type="radio" name="selectedProject" value="' + data.content.projects[i].id + '"';
                            if (data.content.projects[i].flightplans != undefined && data.content.projects[i].flightplans.length > 0) {
                                var temp = '';
                                for (var j = 0; j < data.content.projects[i].flightplans.length; j++) {
                                    temp += data.content.projects[i].flightplans[j].id + '!#!' + data.content.projects[i].flightplans[j].name + '!##!';
                                }
                                temp = temp.substr(0, temp.length - 4);
                                html += 'onchange = "showFlightplans(\'' + temp + '\');"';
                            }
                            html += '><i></i> ' + data.content.projects[i].name + '</label></div></li > ';
                        }
                        html += '</ul></div>';
                        $('#freeProjects').html(html);
                        $('#free-has-projects').css('display', 'block');
                    } else {
                        $('#free-has-projects').css('display', 'none');
                    }
                    $('#modal-' + modal).modal('show');

                } else if (modal == 'professional') {
                    usersHtml = '<ul style="list-style:none; padding:0; margin:0">';
                    for (var i = 0; i < data.content.users.length; i++) {
                        usersHtml += '<li style="list-style:none; padding:0; margin:0"><div class="checkbox" style="margin:0; margin-bottom:2px;"><label class="i-checks i-checks-sm"><input type= "checkbox" name="selectedUsers[]" class="user" value= "' + data.content.users[i].id + '"';

                        if (data.content.users[i].permission_id == 1) {
                            usersHtml += ' disabled="disabled" checked="checked" ';
                        }
                        if (data.content.users[i].name != '') {
                            usersHtml += '><i></i>' + data.content.users[i].name;
                        } else if (data.content.users[i].google_display_name != '') {
                            usersHtml += '><i></i>' + data.content.users[i].google_display_name;
                        }

                        if (data.content.users[i].permission_id == 1) {
                            usersHtml += ' <span class="font-bold" style="margin-left:0px;">(Admin)</span>';
                        }

                        usersHtml += '</label></div></li> ';
                    }
                    usersHtml += '</ul>';


                    $('#professionalUsers').html(usersHtml);

                    $('#professionalUsers input.user').on('change', function (e) {
                        if ($('input.user:checked').length > 5) {
                            $(this).prop('checked', false);
                            alert("Allowed only 2 users (including Admin)");
                        }
                    });

                    if (data.content.projects.length > 0) {
                        html = '<ul>';
                        for (var i = 0; i < data.content.projects.length; i++) {
                            html += '<li id="project_' + data.content.projects[i].id + '">' + data.content.projects[i].name;
                            if (data.content.projects[i].flightplans != undefined && data.content.projects[i].flightplans.length > 0) {
                                html += '<ul>';
                                for (var j = 0; j < data.content.projects[i].flightplans.length; j++) {
                                    html += '<li id="flightplan_' + data.content.projects[i].flightplans[j].id + '">' + data.content.projects[i].flightplans[j].name + '</li>';
                                }
                                html += '</ul>';
                                html += '</li>';
                            }
                        }
                        html += '</ul>';

                        $('#professionalProjects').html(html);

                        var selectedProjects = [];
                        var selectedFlightplans = [];

                        var maxProjects = 5;
                        var maxFlightplans = 15;

                        $(function () {
                            $("#professionalProjects").jstree({
                                "checkbox": {
                                    keep_selected_style: false,
                                    three_state: false
                                },
                                "plugins": ["checkbox"]
                            });

                            $("#professionalProjects").bind("changed.jstree",
                                function (e, data) {



                                    if (data.instance.is_leaf(data.node)) {
                                        data.instance.select_node(data.instance.get_node(data.node.parent).id, true, false);
                                        if (data.instance.is_selected(data.node)) {
                                            selectedProjects.push(data.instance.get_node(data.node.parent).id);
                                            selectedFlightplans.push(data.node.id);
                                        } else {
                                            var index1 = selectedProjects.indexOf(data.instance.get_node(data.node.parent).id);
                                            selectedProjects.splice(index1, 1);
                                            var index2 = selectedFlightplans.indexOf(data.instance.id);
                                            selectedFlightplans.splice(index2, 1);
                                        }

                                    } else if (data.instance.is_parent(data.node)) {
                                        if (data.instance.is_selected(data.node)) {
                                            selectedProjects.push(data.node.id);
                                        } else {
                                            var index3 = selectedProjects.indexOf(data.node.id);
                                            selectedProjects.splice(index3, 1);
                                        }
                                    }

                                    var uniqueProjects = selectedProjects.filter(onlyUnique);
                                    var uniqueFlightplans = selectedFlightplans.filter(onlyUnique);

                                    if (uniqueProjects.length > maxProjects) {
                                        data.instance.deselect_node(data.node, false);
                                        alert('Allowed only  ' + maxProjects + ' projects.');
                                    }

                                    if (uniqueFlightplans.length > maxFlightplans) {
                                        if (data.instance.get_node(data.node.parent).children.length == 1) {
                                            data.instance.deselect_node(data.node.parent, true);
                                        }
                                        data.instance.deselect_node(data.node, false);
                                        alert('Allowed only ' + maxFlightplans + ' flight plans.');
                                    }


                                    var i, j;
                                    var array = [];
                                    for (i = 0, j = data.selected.length; i < j; i++) {
                                        array.push(data.instance.get_node(data.selected[i]).id);
                                    }

                                    selectedProjectsAndFlightplans = array.filter(onlyUnique);
                                });
                        });

                        $('#professional-has-projects').css('display', 'block');

                    } else {
                        $('#professional-has-projects').css('display', 'none');
                    }

                    $('#modal-' + modal).modal('show');
                }
            }
        },
        error: function () {
            alert('Ooops! Something went wrong. Please, try again.');
        }
    });
}

function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
}


function showFlightplans(flightplans) {
    var splitFlightplans = flightplans.split('!##!');
    var html = '<p>Selected project flight plans:</p><div class="tree"><ul>';
    for (var i = 0; i < splitFlightplans.length; i++) {
        var splitOneFlightplan = splitFlightplans[i].split('!#!');
        html += '<li><div class="checkbox" style="margin:0; margin-bottom:2px;"><label class="i-checks i-checks-sm"><input type="checkbox" class="selectedFlightplans" name="selectedFlightplans[]" value="' + splitOneFlightplan[0] + '"/><i></i> ' + splitOneFlightplan[1] + '</label></div></li > ';
    }
    html += '</ul></div>';
    html += '<button class="btn btn-sm btn-default" onclick="showProjects();"><i class="fa fa-chevron-left text"></i><span class="text"></span></button>';
    $('#freeFlightplans').html(html);
    $('#freeProjects').css('display', 'none');
    $('#freeFlightplans').css('display', 'block');

    $('input.selectedFlightplans').on('change', function (e) {
        if ($('input.selectedFlightplans:checked').length > 5) {
            $(this).prop('checked', false);
            alert("Allowed only 5");
        }
    });
}

function showProjects() {
    $('#freeProjects').css('display', 'block');
    $('#freeFlightplans').css('display', 'none');
}



function downgrade(plan) {
    var data = {};
    var selectedProjects = [];
    var selectedFlightplans = [];
    var selectedUsers = [];

    if (plan == 'free') {
        $("input[name='selectedFlightplans[]']:checked").each(function () {
            selectedFlightplans.push('flightplan_' + $(this).val());
        });

        var projectID = $("input:radio[name ='selectedProject']:checked").val();
        var pf = [];
        if (projectID == undefined) {
            pf = [];
        } else {
            pf = ['project_' + $("input:radio[name ='selectedProject']:checked").val(), selectedFlightplans.join(',')];
        }

        data = {
            plan: 'free',
            projectsAndFlightplans: pf,
            users: []
        };
    } else if (plan == 'professional') {
        $("input[name='selectedUsers[]']:checked").each(function () {
            selectedUsers.push($(this).val());
        });

        data = {
            plan: 'professional',
            projectsAndFlightplans: selectedProjectsAndFlightplans,
            users: selectedUsers
        };
    }

    $('#downgrade-' + plan + '-submit-button').removeClass('hidden');
    $('#downgrade-' + plan + '-close-button').html('Cancel');
    $('#downgrade-' + plan + '-loader').removeClass('hidden');
    $('#downgrade-' + plan + '-message').html('');

    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        cache: false,
        url: '/user/downgrade',
        data: JSON.stringify(data),
        success: function (data) {
            $('#downgrade-' + plan + '-loader').addClass('hidden');
            if (data.result == 'error') {
                $('#downgrade-' + plan + '-message').html('Ooops! Something went wrong, please try again.');
            } else if (data.result == 'ok') {
                $('#downgrade-' + plan + '-message').html('Your account has been successfully downgraded.');
                $('#downgrade-' + plan + '-submit-button').addClass('hidden');
                $('#downgrade-' + plan + '-close-button').html('Close');
                $('#downgrade-' + plan + '-close-button').attr('onclick', 'window.location.href=\'/user/settings\'');
            }
        },
        error: function (xhr, status, error) {
            $('#downgrade-' + plan + '-loader').addClass('hidden');
            $('#downgrade-' + plan + '-message').html('Ooops! Something went wrong, please try again.<br>' + error);
        }
    });
}

function deleteAccount() {
    $('#delete-loader').removeClass('hidden');
    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        cache: false,
        url: '/user/delete-account',
        success: function (data) {
            console.log(data);
            $('#delete-loader').addClass('hidden');
            if (data.result == 'error') {
                $('#delete-message').html('Ooops! Something went wrong, please try again, or contact us.');
            } else if (data.result == 'ok') {
                $('#delete-message').html('We\'re sorry you\'re leaving. <br>Your account has been successfully deleted. Redirecting now...');
                setTimeout(function () {
                    window.location.href = '/';
                }, 3000);
            }
        },
        error: function (xhr, status, error) {
            $('#delete-message').html('Ooops! Something went wrong, please try again, or contact us.');
            $('#delete-loader').addClass('hidden');
        }
    });
}

function activateUsers() {
    var users = [];

    $('#users-loader').removeClass('hidden');
    $('#users-message').html('');

    $("input[name='selectedUsers[]']:checked").each(function () {
        users.push($(this).val());
    });

    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        cache: false,
        url: '/user/activate-users',
        data: JSON.stringify(users),
        success: function (data) {
            $('#users-loader').addClass('hidden');
            if (data.result == 'error') {
                $('#users-message').html('Ooops! Something went wrong. Please, try again.');
            } else if (data.result == 'ok') {
                $('#users-message').html('You successfully activated your personnel members.');
                setTimeout(function () {
                    window.location.href = '/user/settings';
                }, 3000);
            }
        },
        error: function (xhr, status, error) {
            console.log('ajax error');
            $('#delete-loader').addClass('hidden');
        }
    });
}

function activateMemberButton(id) {
    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        cache: false,
        url: '/user/check-active-users',
        success: function (data) {
            if (data.result == 'error') {
                $('#users-message').html('Ooops! Something went wrong. Please, try again.');
            } else if (data.result == 'ok') {
                $('#modal-activate-button').attr('onclick', 'activateUser(' + id + ');');
            } else if (data.result == 'limit') {
                $('.modal-footer a').hide();
                $('.modal-body').css('text-align', 'center');
                $('.modal-body').html('You have reached available number of active users limit.<br>Go to your <a href="/user/settings" class="text-info">settings</a> page to upgrade your plan.');
            }
        },
        error: function (xhr, status, error) {
            console.log('ajax error');
        }
    });
}

function activateUser(id) {
    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        cache: false,
        url: '/user/activate-user',
        data: JSON.stringify({id: id}),
        success: function (data) {
            if (data.result == 'error') {
                $('#users-message').html('Ooops! Something went wrong. Please, try again.');
            } else if (data.result == 'ok') {
                $('#users-message').html('You successfully activated selected member.');
                setTimeout(function () {
                    window.location.href = '/team';
                }, 3000);
            }
        },
        error: function (xhr, status, error) {
            console.log('ajax error');
        }
    });
}

function setSecondReportDropdown(value) {
    $('#pdf-generator-message').html('');
    if (value == 'faa_flight_report') {
        $.ajax({
            type: 'POST',
            contentType: 'application/json',
            cache: false,
            url: '/reports/get-flightplans-for-company',
            success: function (data) {
                if (data.result == 'error') {
                    $('#pdf-generator-message').html('Ooops! Something went wrong. Please, try again.');
                } else{
                    var html = '<option value="">Select</option>';
                    for (var i = 0; i < data.result.length; i++){
                        html += '<option value="'+data.result[i].id+'">'+data.result[i].name+'</option>'
                    }
                    $('#secondDropDown').html(html);
                    $('#secondDropDownLabel').html('Flight Plans');
                    $('#secondDropDownContainer').removeClass('hidden');
                }
            },
            error: function (xhr, status, error) {
                console.log('ajax error');
            }
        });
    } else if (value == '') {
        $('#generator-form a').addClass('hidden');
        $('#secondDropDown').html('');
        $('#secondDropDownLabel').html('');
        $('#secondDropDownContainer').addClass('hidden');
    }
}

function checkReportValues(id) {
    if(id != ''){
        $('#pdf-generator-message').html('');
        $('#generator-form a').addClass('hidden');
        if ($('#firstDropDown').val() == 'faa_flight_report') {
            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                cache: false,
                url: '/reports/check-flightplan-for-report',
                data: JSON.stringify({ id: id }),
                success: function (data) {
                    console.log(data);
                    if (data.result == 'error') {
                        $('#pdf-generator-message').html('Ooops! Something went wrong. Please, try again.');
                    } else if (data.result.message == 'ok') {
                        $('#generator-form a').removeClass('hidden');
                    } else if (data.result.message == 'vehicle') {
                        $('#pdf-generator-message').html('Vehicle has no FAA Aircraft ID. Click <a class="text-danger" href="/vehicles/edit/' + data.result.id + '">here</a> to fix this.');
                    } else if (data.result.message == 'pilot') {
                        $('#pdf-generator-message').html('Pilot is not selected for this flight plan. Click <a class="text-danger" href="/flightplans/edit/' + id + '">here</a> to fix this.');
                    } else if (data.result.message == 'all') {
                        $('#pdf-generator-message').html('Vehicle has no FAA Aircraft ID and pilot is not selected for this flight plan.<br/> Click <a  class="text-danger" href="/vehicles/edit/' + data.result.id + '">here</a> to add FAA Aircraft ID and click <a class="text-danger" href="/flightplans/edit/' + id + '">here</a> to select a pilot for this flight plan.');
                    }
                },
                error: function (xhr, status, error) {
                    console.log('ajax error');
                }
            });
        }
    } else {
        $('#pdf-generator-message').html('');
        $('#generator-form a').addClass('hidden');
    }
}


function generatePDF() {
    $('#pdf-generator-loader').removeClass('hidden');
    $('#pdf-generator-message').html('');
    var pdfType = $('#firstDropDown').val();
    var id = $('#secondDropDown').val();

    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        cache: false,
        url: '/reports/generate-pdf',
        data: JSON.stringify({ type: pdfType, id: id }),
        success: function (data) {
            if (data.result == 'ok'){
                $('#pdf-generator-message').html('You successfully generated the PDF Report. Refreshing the page...');
                setTimeout(function () {
                    window.location = '/reports';
                }, 3000);
            } else {
                $('#pdf-generator-loader').addClass('hidden');
                $('#pdf-generator-message').html('Ooops! Something went wrong. Please, try again.');
            }
        },
        error: function (xhr, status, error) {
            $('#pdf-generator-loader').addClass('hidden');
            $('#pdf-generator-message').html('Ooops! Something went wrong. Please, try again.');
            console.log('ajax error');
        }
    });
}


function downloadReport(id, button) {
    $(button).html('<i class="fa fa-spin fa-circle-o-notch"></i>');

    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        cache: false,
        url: '/reports/download-report/' + id,
        success: function (data) {
            $(button).html('<i class="glyphicon glyphicon-cloud-download"></i>');
            if (data.result == 'ok') {
                $('#fileLink a').attr('href', data.file);
                $('#fileLink a')[0].click();
            } else {
                alert('Ooops! Something went wrong. Please, try again.');
            }
        },
        error: function () {
            $(button).html('<i class="glyphicon glyphicon-cloud-download"></i>');
            alert('Ooops! Something went wrong. Please, try again.');
        }
    });
}

function setDeleteReportButton(id) {
    $('#modal-delete-button').attr('onclick', 'deleteReport(' + id + ');');
}


function deleteReport(id) {
    $('#loader-content').addClass('active');
    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        cache: false,
        url: '/reports/delete',
        data: JSON.stringify({
            id: id
        }),
        success: function (data) {
            $('#loader-content').removeClass('active');
            data = JSON.parse(data);

            if (data.result == 'ok') {
                location.reload();
            }
        },
        error: function () {
            alert('Ooops! Something went wrong. Please, try again.');
        }
    });
}