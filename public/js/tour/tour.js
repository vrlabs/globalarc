var tour = {
    id: "tour",
    steps: [
        {
            title: "Dashboard",
            content: "Your starter dashboard will show a sample project with a sample flight plan to help you get familiar with the interface.",
            target: document.querySelector("#tour-dashboard-projects-list"),
            placement: "bottom"
        },
        {
            title: "Flights Map",
            content: "Here you can see all your past, live and future flights on a map.",
            target: document.querySelector("#tour-dashboard-map"),
            placement: "left"
        },
        {
            title: "Access Code",
            content: "It will also provide your remote UAV access code on the left,",
            target: document.querySelector(".accesskey"),
            placement: "right"
        },
        {
            title: "Recent Flights",
            content: "and recent flights on the right.",
            target: document.querySelector("#right-sidebar"),
            placement: "left"
        },
        {
            title: "Relevant Data",
            content: "Here you will find all your team, vehicle, battery and documentation details.",
            target: document.querySelector("#tour-dashboard-data"),
            placement: "top",
            multipage: true,
            onNext: function() {
                window.location = "/projects/edit/" + pID
            }
        },
        {
            title: "Creating a new project",
            content: "Name your project and add your notes.",
            target: document.querySelector("#tour-new-project"),
            placement: "top",
            multipage: true,
            onNext: function() {
                window.location = "/projects"
            }
        },
        {
            title: "Viewing all projects",
            content: "Each project can have multiple flight plans. Click on a project name to access all projects flight plans.",
            target: document.querySelector("#tour-projects"),
            placement: "top",
            multipage: true,
            onNext: function() {
                window.location = "/flightplans/add/" + pID
            }
        },
        {
            title: "Creating a new flight",
            content: "Enter flight location name",
            target: document.querySelector("#location_name_input"),
            placement: "top"
        },
        {
            title: "Creating a new flight",
            content: "Enter flight plan name",
            target: document.querySelector("#tour-flight-name"),
            placement: "top"
        },
        {
            title: "Creating a new flight",
            content: "Set start and end time",
            target: document.querySelector("#time-container"),
            placement: "top"
        },
        {
            title: "Creating a new flight",
            content: "Select your team",
            target: document.querySelector("#tour-flight-team"),
            placement: "top"
        },
        {
            title: "Creating a new flight",
            content: "Select vehicle",
            target: document.querySelector("#tour-flight-vehicle"),
            placement: "top"
        },
        {
            title: "Creating a new flight",
            content: "Select batteries",
            target: document.querySelector("#tour-flight-batteries"),
            placement: "top"
        },
        {
            title: "Creating a new flight",
            content: "Upload documents",
            target: document.querySelector("#tour-flight-documents"),
            placement: "top"
        },
        {
            title: "Creating a new flight",
            content: "Enter notes",
            target: document.querySelector("#tour-flight-notes"),
            placement: "top",
            multipage: true,
            onNext: function() {
                window.location = "/flightplans/view/" + fID
            }
        },
        {
            title: "Flight Monitoring",
            content: "Connect to your drone",
            target: document.querySelector("#button-connect"),
            placement: "left"
        },
        {
            title: "Flight Monitoring",
            content: "Follow vehicles on the map",
            target: document.querySelector("#tour-flight-map"),
            placement: "top"
        },
        {
            title: "Flight Monitoring",
            content: "Watch live video from UAV",
            target: document.querySelector("#tour-flight-video"),
            placement: "top"
        },
        {
            title: "Flight Monitoring",
            content: "Communicate via chat",
            target: document.querySelector("#tour-flight-chat"),
            placement: "top"
        }
    ],
    onEnd: function () {
        window.location = '/dashboard'
    }
};
