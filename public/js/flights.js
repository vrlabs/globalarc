var map = null;

var firstTime = true;

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 2,
        center: centerCoordinates,
        disableDefaultUI: true,
        rotateControl: true,
        panControl: true, //enable pan Control
        zoomControl: true, //enable zoom control
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.DEFAULT,
            position: google.maps.ControlPosition.RIGHT_TOP
        },
        mapTypeControl: true,
        mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.DEFAULT,
            position: google.maps.ControlPosition.LEFT_TOP
        },
        scaleControl: true, // enable scale control
    });

    var polygon = new google.maps.Polygon({
        paths: polygonCoordinates,
        strokeColor: '#03A9F4',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#03A9F4',
        fillOpacity: 0.1,
        zIndex: 1
    });

    polygon.setMap(map);

    if (firstTime) {
        centerMap();
        firstTime = false;
    }


};

function centerMap() {
    var boundsCoordinates = findBoundingBox(coordinates);
    var bounds = new google.maps.LatLngBounds();
    var NW = new google.maps.LatLng(boundsCoordinates.maxY, boundsCoordinates.minX);
    var SE = new google.maps.LatLng(boundsCoordinates.minY, boundsCoordinates.maxX);
    bounds.extend(NW);
    bounds.extend(SE);
    map.fitBounds(bounds);
}

//  COMMENTS --------------------------------------------------   

browserNotice(false, true);
cameraNotice();
startViewFlightplanTour();

var socket = io();

socket.on('commentAdded', function (result) {
    if (result == 'error') {
        alert('Something went wrong. Please, try again.');
    } else {

        $('.streamline').append('<div>' +
            '<a class="pull-left thumb-sm avatar m-l-n-md">' +
            '<img src="' + result.userPhoto + '" class="img-circle">' +
            '</a>' +
            '<div class="m-l-lg panel b-a">' +
            '<div class="panel-heading pos-rlt b-b b-light">' +
            '<span class="arrow left"></span>' +
            '<a href="#">' + result.userDisplayName + '</a>' +
            '<label class="label ' + result.roleClass + ' m-l-xs">' + result.roleName + '</label>' +
            '<span class="text-muted m-l-sm pull-right"><i class="fa fa-clock-o m-r-xs"></i>' + result.created + '</span>' +
            '</div>' +
            '<div class="panel-body"><div>' + result.comment + '</div></div>' +
            '</div>' +
            '</div>');

        var scrollingPanel = $(".slimScrollDiv");
        scrollingPanel.slimScroll({ destroy: true });
        scrollingPanel.slimScroll({ height: '500px', size: '8px', start: 'bottom' });
        $('#new-comment').focus();
    }
});

$(document).ready(function () {
    // comments
    $('#post-comment-btn').click(function () {
        var comment = $('#new-comment').val();
        var userDisplayName = $('#userDisplayName').html();
        var userPhoto = $('#userPhoto').attr('src');

        $('#new-comment').val('');
        var data = {
            comment: comment,
            userID: userID, 
            flightplanID: flightplanID,
            roleID: roleID,
            userDisplayName: userDisplayName,
            userPhoto: userPhoto
        }
        if (comment != '') {
            socket.emit('addComment', data);
        } else {
            $('#new-comment').focus();
        }
    });

    $('#new-comment').keypress(function (e) {
        if (e.which == 13) {
            $(this).blur();
            $('#post-comment-btn').focus().click();
        }
    });
});

// ANDRUAV --------------------------------------------------   

var saveData = function (fileURL, fileName) {
    // for non-IE
    if (!window.ActiveXObject) {
        var save = document.createElement('a');
        save.href = fileURL;
        save.target = '_blank';
        save.download = fileName || 'unknown';

        var event = document.createEvent('Event');
        event.initEvent('click', true, true);
        save.dispatchEvent(event);
        (window.URL || window.webkitURL).revokeObjectURL(save.href);
    }

    // for IE
    else if (!!window.ActiveXObject && document.execCommand) {
        var _window = window.open(fileURL, '_blank');
        _window.document.close();
        _window.document.execCommand('SaveAs', true, fileName || fileURL)
        _window.close();
    }
}


var andruavClient = null;
var counter = 0;



function getImage(target) {
    var res = target.toLowerCase();
    andruavClient.API_remoteCommand_takeImage(vehicleUnitID);
}

// Switch Video OnOff
function toggleVideo(unitFullName) {
    andruavClient.API_remoteCommand_streamVideo(unitFullName, andruavClient.pub_streamOnOff);
}

// Switch Video OnOff
function toggleRecrodingVideo(unitFullName) {
    andruavClient.API_remoteCommand_recordVideo(unitFullName);
}

function hlp_getBatteryCSSClass(battery) {
    if (battery == null) return null;
    var bat = battery.PlugStatus + " ";

    if (bat == 'Charging') return bat;
    if (parseInt(battery.BatteryLevel, 0) > 80) {
        bat += 'bat_4 ';
    } else if (parseInt(battery.BatteryLevel, 0) > 50) {
        bat += 'bat_3 ';
    } else if (parseInt(battery.BatteryLevel, 0) > 25) {
        bat += 'bat_2 ';
    } else {
        bat += 'bat_1 ';
    }

    return bat;
}

// Generates HTML for Andruav UNIT ROW DIV

function hlp_generateHTMLforUnit(andruavUnit) {
    var html = "<tr id='" + andruavUnit.fullName() + "' class='IsShutdown_" + andruavUnit.IsShutdown + "'><td class='gcs " + "IsGCS_" + andruavUnit.IsGCS + "'>";

    if (andruavUnit.IsGCS == true) {
        html += "<img  alt='GCS'/></td>";
        html += "<td class='camera IsGCS_true'><img  alt='Drone'/></td>";
        html += "<td class='video IsGCS_true'><img  alt='Drone'/></td>";
    } else {
        $('#imageLink').attr('onclick', 'openImage("'+ andruavUnit.unitName + '");');
        html += "<img src='/assets/images/plane_b_32x32.png' alt='Vehicle'/></td>";
        html += "<td class='camera  IsGCS_false'><img  alt='Camera' onclick='getImage(\"" + andruavUnit.unitName + "\");'/></td>";
        html += "<td class='video  IsGCS_false ready' ><img  alt='Video' onclick='toggleVideo(\"" + andruavUnit.fullName() + "\");'/></td>";
        if (andruavUnit.VideoRecording == VIDEORECORDING_ON) {
            html += "<td class='recvideo  IsGCS_false run' ><img  alt='Video' onclick='toggleRecrodingVideo(\"" + andruavUnit.fullName() + "\");'/></td>";
            $('#videoLink').attr('onclick', 'openVideo("' + andruavUnit.unitName + '");');
        } else {
            html += "<td class='recvideo  IsGCS_false ready' ><img  alt='Video' onclick='toggleRecrodingVideo(\"" + andruavUnit.fullName() + "\");'/></td>";
            $('#videoLink').attr('onclick', 'openVideo("' + andruavUnit.unitName + '");');
        }
    }

    if (andruavUnit.battery != null) {
        html += "<td class='battery  IsGCS_false " + hlp_getBatteryCSSClass(andruavUnit.battery) + "' ><img alt='Batt'/></td>";
    }
    html += "<td class='unitname  IsShutdown_" + andruavUnit.IsShutdown + "'>" + andruavUnit.unitName + "</td></tr>";

    

    return html;
}

/////////////////////////////////////////////////////////////////////////////// Events from AndruavClient

// Websocket Connection established
var onOpen = function () {
    andruavClient.API_addMe(group, unitID);
}

// Generic on Message Received
var onMessage = function (evt) {
    counter += 1;
    $('#message_log').append("<div class='log_rx_msg'>" + counter + "- RX: " + evt.data + "</div>");
}

// called when sending any message
var onSend = function (message) {
    counter += 1;
    $('#message_log').append("<div class='log_tx_msg'>" + counter + "- TX: " + message + "</div>");
}

// called when Websocket Error	
var onError = function (error) {
    counter += 1;
    $('#message_log').append("<div class='log_error'>E" + counter + "- error: " + error + "</div>");
}

// called when Websocket Closed
var onClose = function () {
    counter += 1;
    $('#message_log').append("<div class='log_ctrl'>" + counter + "- Connection Closed</div>");
}

// Generic verbos log function
var onLog = function (msg) {
    counter += 1;
    $('#message_log').append("<div class='log_log_ctrl'>" + counter + "- Log:" + msg + "</div>");
}

/**
 * called when Connection status Changed
    const SOCKET_STATUS_FREASH 			=		1;   // socket is new
    const SOCKET_STATUS_CONNECTING    	=		2;	 // connecting to WS
    const SOCKET_STATUS_DISCONNECTING 	=		3;   // disconnecting from WS
    const SOCKET_STATUS_DISCONNECTED 	=		4;   // disconnected  from WS
    const SOCKET_STATUS_CONNECTED 		=		5;   // connected to WS
    const SOCKET_STATUS_REGISTERED 		=		6;   // connected and executed AddMe
    const SOCKET_STATUS_UNREGISTERED 	=		7;   // connected but not registred
    const SOCKET_STATUS_ERROR 		    =		8;   // Error

    SOCKET_STATUS = ['Fresh','Connecting','Disconnecting','Disconnected','Connected','Registered','Error'];
    
    param status: index of status in SOCKET_STATUS
    param name: string name of status
*/

var onSocketStatus = function (status, name) {
    $('#loader-content').removeClass('active');
    counter += 1;
    $('#message_log').append("<div class=\"log_ctrl\">" + counter + "- Socket Status:" + name + "</div>");

    if (status == SOCKET_STATUS_REGISTERED) {
        $('#button-connect').html('<i class="fa fa-unlink"></i>Disconnect');
        $('#button-connect').removeClass('btn-success');
        $('#button-connect').addClass('btn-danger');
        $('#communication').show();
        $('#andruav-unit-list').show();
        //$('#log').show();

    } else {
        $('#button-connect').html('<i class="fa fa-link"></i>Connect');
        $('#button-connect').removeClass('btn-danger');
        $('#button-connect').addClass('btn-success');
        $('#communication').hide();
        $('#andruav-unit-list').hide();
        //$('#log').hide();

    }
}


/**
Called when message [TYPE_AndruavMessage_GPS] is received from a UNIT or GCS holding IMU Statistics
*/
var EVT_msgFromUnit_GPS = function (andruavUnit, GPS3DFix, satCount, yaw, latitude, logitude, gpsProvider, time, altitude, spead, bearing, accuracy) {
    function getLabel() {
        return andruavUnit.unitName;
    }

    function getVehicleIcon() {
        if (andruavUnit.IsGCS == true) {
            return '/assets/images/map_gcs_3_32x32.png';
        } else {
            switch (andruavUnit.VehicleType) {
                case VEHICLE_TRI:
                case VEHICLE_QUAD:
                    return '/assets/images/drone_q1_32x32.png';
                case VEHICLE_PLANE:
                    return '/assets/images/map_gcs_3_32x32.png';
                case VEHICLE_HELI:
                    return '/assets/images/heli_1_32x32.png';
                case VEHICLE_ROVER:
                    return '/assets/images/car_1_32x32.png';
                default:
                    return '/assets/images/drone_3_32x32.png';
            }
        }
    }
    //console.log ('EVT_msgFromUnit_GPS gr:' + groupname + ', unitID:' + unitID);
    if (latitude != null) {
        var marker = andruavUnit.marker;
        var latlng = new google.maps.LatLng(latitude, logitude);

        if (marker == null) {
            var image = {
                url: getVehicleIcon(),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(16, 16),
                scaledSize: new google.maps.Size(32, 32)
            };

            andruavUnit.marker = new google.maps.Marker({
                map: map,
                label: getLabel(),
                anchor: new google.maps.Point(16, 16),
                icon: image,
                zIndex: 100
            });


            var infowindow = new google.maps.InfoWindow({
                position: latlng,
                title: andruavUnit.unitName,
                disableAutoPan: true,
                content: andruavUnit.groupName + " -- " + andruavUnit.unitName
            });

            andruavUnit.marker.addListener('click', function () {
                if (map.getZoom() < 16) {
                    map.setZoom(17);
                }
                map.panTo(andruavUnit.marker.position);
                infowindow.open(map, marker);
            });
        }
        andruavUnit.marker.setPosition(latlng);

    } else {
        console.log(' no location info for ' + andruavUnit.unitName);
    }
}

/**
Called when message [TYPE_AndruavMessage_IMUStatistics] is received from a UNIT or GCS holding IMU Statistics
*/
var EVT_msgFromUnit_IMUStatistics = function (groupname, unitID, msgID, msg) {
    console.log ('EVT_msgFromUnit_IMUStatistics gr:' + groupname + ', unitID:' + unitID + ', msgID:' + msgID + ',msg:' + msg);
}


/**
Called when message [TYPE_AndruavMessage_IMG] is received from a UNIT or GCS 
*/
var EVT_msgFromUnit_IMG = function (groupName, unitName, img, description, latitude, logitude, gpsProvider, time, altitude, spead, bearing, accuracy) {
    console.log ('EVT_msgFromUnit_IMG');
    var bin = Str2BinaryArray(img);
    console.log ('o is :'  + bin.constructor);
    var blob = new Blob([bin], { type: 'image/jpeg' });
    var reader = new FileReader();
    reader.onload = function (e) {
        var contents = event.target.result;
        saveData(contents, 'andruav-image.jpg');
    };

    reader.onerror = function (event) {
        console.error("File could not be read! Code " + event.target.error.code);
    };

    reader.readAsDataURL(blob);
    $('#unitImg').attr('src', 'data:image/jpeg;base64,' + arrayBufferToBase64(bin));

}

/**
Called when a video request is sent to a Unit
*/
var EVT_videoStateChanged = function (andruavUnit, OnOff) {
    var id = 'tr#' + andruavUnit.fullName() + ' td.video';
    if (OnOff == true) {
        $(id).removeClass('ready');
        $(id).addClass('run');
    } else {
        $(id).removeClass('run');
        $(id).addClass('ready');
    }
    console.log (id);
}

var EVT_msgFromUnit_VIDEO = function (groupName, unitName, img) {
    $('video-rtc-div').hide();
    $('#video-rtc-div').html('');
    $('#unitVideo').show();
    var bin = Str2BinaryArray(img);
    $('#unitVideo').attr('src', 'data:image/jpeg;base64,' + arrayBufferToBase64(bin));
}

var EVT_msgFromUnit_RTCVIDEO = function (session) {
    if ((session != null) && (session.status == 'connected')) {
        $('#unitVideo').hide();
        $('#video-rtc-div').show();
        $('#video-rtc-div').html('<video id="videoHolder" class="videoHolder" src="' + session.video.src + '" autoplay id="' + session.video.id + '" </video>'); //event.mediaElement ; //|| event;
        $('#video-rtc-div').focus();
    } else {   // hide control
        $('video-rtc-div').hide();
        $('#video-rtc-div').html('');
        $('#unitVideo').show();
    }
}



/**
Called when a new unit joins the system.
*/
var EVT_andruavUnitAdded = function (andruavUnit) {
    var html = hlp_generateHTMLforUnit(andruavUnit);
    $("table#andruavUnitList").append(html);
}

/**
Called as ID message is received from a known unit.
*/
var EVT_andruavUnitUpdated = function (andruavUnit) {
    console.log ('EVT_andruavUnitUpdated');
    var html = hlp_generateHTMLforUnit(andruavUnit);
    $('tr#' + andruavUnit.fullName()).replaceWith(html);
};



var EVT_batteryStatusChanged = function (andruavUnit) {
    var html = hlp_generateHTMLforUnit(andruavUnit);
    $('tr#' + andruavUnit.fullName()).replaceWith(html);
};


/**
Received when a notification sent by remote UNIT.
It could be error, warning or notification.
*******************
errorNo 			: 
                        NOTIFICATION_TYPE_ERROR 	= 1
                        NOTIFICATION_TYPE_WARNING	= 2
                        NOTIFICATION_TYPE_NORMAL	= 3
                        NOTIFICATION_TYPE_GENERIC	= 4
infoType			:
                        ERROR_CAMERA 	= 1
                        ERROR_BLUETOOTH	= 2
                        ERROR_USBERROR	= 3
                        ERROR_KMLERROR	= 4
notification_Type	:
                        NOTIFICATION_TYPE_ERROR = 1;
                        NOTIFICATION_TYPE_WARNING = 2;
                        NOTIFICATION_TYPE_NORMAL = 3;
                        NOTIFICATION_TYPE_GENERIC = 0;
Description	'DS		: 
                        Messag
*/
var EVT_andruavUnitError = function (andruavUnit, error) {
    console.log ('EVT_andruavUnitError unit:' + andruavUnit.unitName + ' sent  msg:' + error.Description);
};

//////////////////////////////////////////////////////////////////////

$(document).ready(function () {
    $('#button-connect').on('click', function () {
        $('#loader-content').addClass('active');
        $.ajax({
            type: 'POST',
            contentType: 'application/json',
            cache: false,
            url: '/flightplans/andruav-login',
            success: function (data) {
                $('#message-text').html('');
                $('#message').addClass('hide');

                if (data.result == 'ok') {
                    // create a group object
                    if (andruavClient == null) {
                        andruavClient = Object.create(AndruavClientSocket.prototype);
                        andruavClient.init();
                        andruavClient.onOpen = onOpen;
                        andruavClient.onError = onError;
                        andruavClient.onSend = onSend;
                        andruavClient.onMessage = onMessage;
                        andruavClient.onClose = onClose;
                        andruavClient.onLog = onLog;
                        andruavClient.EVT_msgFromUnit_GPS = EVT_msgFromUnit_GPS;
                        andruavClient.EVT_msgFromUnit_IMUStatistics = EVT_msgFromUnit_IMUStatistics;
                        andruavClient.EVT_msgFromUnit_IMG = EVT_msgFromUnit_IMG;
                        andruavClient.EVT_msgFromUnit_VIDEO = EVT_msgFromUnit_VIDEO;
                        andruavClient.EVT_msgFromUnit_RTCVIDEO = EVT_msgFromUnit_RTCVIDEO;
                        andruavClient.EVT_videoStateChanged = EVT_videoStateChanged;
                        andruavClient.EVT_andruavUnitAdded = EVT_andruavUnitAdded;
                        andruavClient.EVT_andruavUnitUpdated = EVT_andruavUnitUpdated;
                        andruavClient.EVT_batteryStatusChanged = EVT_batteryStatusChanged;
                        andruavClient.EVT_andruavUnitError = EVT_andruavUnitError;
                        console.log (SOCKET_STATUS);

                        andruavClient.server_ip = 'andruav.com';
                        andruavClient.server_port = data.params.port;
                        andruavClient.connect(data.params.loginCode);
                    } else {
                        andruavClient.API_delMe();
                        andruavClient = null;
                    }
                } else if (data.result == 'error') {
                    $('#loader-content').removeClass('active');
                    $('#message-text').html(data.message);
                    $('#message').removeClass('hide');
                }
            },
            error: function (e) {
                console.log(e);
                $('#loader-content').removeClass('active');
                $('#message-text').html('Ooops! Something went wrong. Please, try again.');
                $('#message').removeClass('hide');
            }
        });


    });

    $('#button-send-message').click(function (event) {
        event.preventDefault();
        switch ($('#comm_method').val()) {
            case CMD_COMM_GROUP:
                //console.log ('sendCMDToGroup');
                andruavClient.API_sendCMD(null, 0, $('#txtMsg').val());
                break;
            case CMD_COMM_INDIVIDUAL:
                //console.log ('sendCMDToIndividual');
                andruavClient.API_sendCMD($('#txtTargetUnitID').val(), 0, $('#txtMsg').val());
                break;
        }
    });

    $('#button-clear-log').click(function (event) {
        event.preventDefault();
        $('#message_log').empty();
    });
});

function openImage(id) {
    $('#video-container').addClass('hidden');
    $('#image-container').removeClass('hidden');
    andruavClient.API_remoteCommand_takeImage(id);
    $('#unitImg').css('width', '100%');
}

function openVideo(id) {
    if (!isChrome()) {
        browserModal();
        $('#browser-popup-link').trigger('click');
    } else {
        $('#image-container').addClass('hidden');
        $('#video-container').removeClass('hidden');
        
        $('#video-container').html('<iframe src="https://andruav.com:29001/viewer.html?drone=' + id + '-stream&gcs=' + unitID + '" style="width:100%; height:470px;"></iframe>')
    }
}