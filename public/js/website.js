// home page
function setVideoDimensions(){
    if($(window).width() > 768){
        $('#mac').width(695);
        $('#mac').height(420);
        $('#video').width(528); 
        $('#video').height(332);                    
        
        $('#mac').css('background-size', '695px 420px');
        $('#mac').css('margin', '0 auto');
        
        $('#video').css('margin-top', '22px');
        $('#video').css('margin-left', '83px');
        
        $('#video').css('display', 'inline');
    }else{
        var macW = $('.video-container').width();
        var macH = macW * 689 / 1138;
        var videoW = macW * 865 / 1138;
        var videoH = videoW * 544 / 865;
        
        var marginT = 0.065 * videoH;
        var marginL = marginT / 0.26;
        
        $('#mac').width(macW);
        $('#mac').height(macH);
        $('#video').width(videoW); 
        $('#video').height(videoH);                    
        
        $('#mac').css('background-size', (macW + 'px' + ' ' + macH + 'px'));
        
        $('#video').css('margin-top', (marginT + 'px'));
        $('#video').css('margin-left', (marginL + 'px'));
        
        $('#video').css('display', 'inline');
    }            
}

function setIFrameDimensions() {
    if ($(window).width() < 595) {
        $('iframe').width($(window).width() - 20);
        $('iframe').height($('iframe').width() / 1.77);
    } else {
        $('iframe').width(560);
        $('iframe').height(315);
    }
}