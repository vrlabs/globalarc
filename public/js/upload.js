var dropzone;

$(function () {
    var dropzoneObject = {};
    dropzoneObject.S3RequestURL = S3RequestURL;
    dropzoneObject.S3Bucket = S3Bucket;
    
    dropzoneObject.dropzoneAcceptCallback = function _dropzoneAcceptCallback(file, done) {
        file.postData = [];
        $.ajax({
            url: dropzoneObject.S3RequestURL,
            data: {
                name: file.name,
                type: file.type,
                size: file.size
                },
            type: 'POST',
            success: function jQAjaxSuccess(response) {
                response = JSON.parse(response);
                file.custom_status = 'ready';
                file.postData = response;
                file.s3 = response.key;   
                $(file.previewTemplate).find('td.status').html('<span title="Ready for Upload"><i class="text-lg text-info glyphicon glyphicon-thumbs-up"></i></span>');
                done();
            },
            error: function(response) {
                file.custom_status = 'rejected';
                
                if (response.responseText) {
                    response = JSON.parse(response.responseText);
                }
                if (response.message) {
                    $(file.previewTemplate).find('td.status').html('<span class="text-danger"><i class="glyphicon glyphicon-ban-circle"></i>' + response.message + '</span>');
                    done(response.message);
                    return;
                }
                $(file.previewTemplate).find('td.status').html('<span class="text-danger"><i class="glyphicon glyphicon-ban-circle"></i>Error preparing the upload</span>');
                
                done('error preparing the upload');
            }
        });
    };


    dropzoneObject.dropzoneSendingCallback = function(file, xhr, formData) {
        $.each(file.postData, function(k, v) {
            formData.append(k, v);
        });
        formData.append('Content-type', '');
        formData.append('Content-length', '');
        formData.append('acl', 'public-read');
    };
    

    dropzoneObject.dropzoneCompleteCallback = function(file) {
        $(file.previewTemplate).find('.delete-file').show();
        $(file.previewTemplate).find('.upload-file').hide();
        $(file.previewTemplate).find('.cancel-file').hide();
        $(file.previewTemplate).find('.remove-file').hide();
        
        if(file.status == 'error'){
            $(file.previewTemplate).find('td.status').html('<span class="text-danger"><i class="glyphicon glyphicon-ban-circle"></i> Error</span>');
        }else{   
            var fileData = {
                    url: dropzoneObject.S3Bucket + file.postData.key,
                    originalFilename: file.name, 
                    modified: file.lastModified,
                    s3Name: file.postData.key,
                    size: file.size,
                    fileType: file.type,
                    documentType: $(file.previewTemplate).find('select').val()                     
            }       

            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                cache: false,
                data: JSON.stringify(fileData),
                url: '/documents/add',
                success: function (data) {
                    if (data.result == 'ok') {
                        if (inline) {
                            var html = '';
                            html += '<tr>';
                                html += '<td  class="text-center"><label class="i-checks m-b-none"><input type="checkbox" name="documents" value="' + data.params.id + '"><i></i></label></td>';
                                html += '<td>' + data.params.name + '</td>';
                                html += '<td>' + data.params.size + '</td>';
                                html += '<td>' + data.params.type + '</td>';
                                html += '<td>' + data.params.user + '</td>';
                                html += '<td class="text-center">' + data.params.modified + '</td>';
                                html += '<td class="text-center"><a href="' + data.params.path + '" target="_blank"><i class="icon-magnifier"></i></a></td>';
                                html += '</tr>';
                            $('#all-documents').append(html);
                        }
                        $(file.previewTemplate).find('td.status').html('<span class="text-success"><i class="glyphicon glyphicon-ok"></i></span>');
                        $(file.previewTemplate).find('.delete-file').attr('data-id', data.id);
                        $(file.previewTemplate).find('.delete-file').attr('data-file', fileData.s3Name);
                    } else if (data.result == 'storage') {
                        if (inline) {
                            var html = '';
                            html += '<tr>';
                                html += '<td  class="text-center"><label class="i-checks m-b-none"><input type="checkbox" name="documents" value="' + data.params.id + '"><i></i></label></td>';
                                html += '<td>' + data.params.name + '</td>';
                                html += '<td>' + data.params.size + '</td>';
                                html += '<td>' + data.params.type + '</td>';
                                html += '<td>' + data.params.user + '</td>';
                                html += '<td class="text-center">' + data.params.modified + '</td>';
                                html += '<td class="text-center"><a href="' + data.params.path + '" target="_blank"><i class="icon-magnifier"></i></a></td>';
                                html += '</tr>';
                            $('#all-documents').append(html);
                        }
                        $(file.previewTemplate).find('td.status').html('<span class="text-danger"><i class="glyphicon glyphicon-ban-circle"></i>&nbsp;&nbsp;Not enough storage space.</span>');
                        $(file.previewTemplate).find('.delete-file').attr('data-id', data.id);
                        $(file.previewTemplate).find('.delete-file').attr('data-file', fileData.s3Name);
                    }
                },
                error: function() {
                    $(file.previewTemplate).find('td.status').html('<span class="text-danger"><i class="glyphicon glyphicon-ban-circle"></i> Error</span>');
                }
            });
        }
    }

    dropzone = new Dropzone(document.body, {        
        url: dropzoneObject.S3Bucket,
        method: 'POST',
        createImageThumbnails: false,
        autoProcessQueue: false,
        previewTemplate: rowTemplate,
        previewsContainer: '#previews',
        clickable: '#dropzone',
        acceptedFiles: 'image/*, application/pdf',
        parallelUploads: 3,
        accept: dropzoneObject.dropzoneAcceptCallback,
        sending: dropzoneObject.dropzoneSendingCallback,
        complete: dropzoneObject.dropzoneCompleteCallback
    });
        
        
    dropzone.on('addedfile', function (file) {        
        $('#previews a.dz-remove').each(function( index ) {
            $(this).wrap('<td class="text-center"> </td>');
            $(this).attr('href', '#');
            $(this).addClass('btn btn-default btn-xs');
        });
        
        $('#upload-all').removeAttr('disabled');
        $('#remove-all').removeAttr('disabled');
        
        // action buttons
        var deleteButton = Dropzone.createElement('<a href="#" class="btn btn-rounded btn btn-icon btn-default delete-file" style="display:none;" title="Delete File from Server"><i class="glyphicon glyphicon-trash"></i></a>');
        var uploadButton = Dropzone.createElement('<a href="#" class="btn btn-rounded btn btn-icon btn-default m-r-sm upload-file" title="Upload File"><i class="glyphicon glyphicon-upload text-lg"></i></a>');
        var cancelButton = Dropzone.createElement('<a href="#" class="btn btn-rounded btn btn-icon btn-default cancel-file" style="display:none;" title="Cancel File Upload"><i class="glyphicon glyphicon-ban-circle text-lg"></i></a>');
        var removeButton = Dropzone.createElement('<a href="#" class="btn btn-rounded btn btn-icon btn-default remove-file" title="Remove File from Queue"><i class="glyphicon glyphicon-remove"></i></a>');
                
        $(file.previewTemplate).find('td.action').append(uploadButton);
        $(file.previewTemplate).find('td.action').append(deleteButton);
        $(file.previewTemplate).find('td.action').append(cancelButton);
        $(file.previewTemplate).find('td.action').append(removeButton);
                       
        var _this = this;

        deleteButton.addEventListener("click", function(e) {
            $('#loader-content').addClass('active');
            e.preventDefault();
            e.stopPropagation();
                        
            var id = $(this).attr('data-id');
            var fileName = $(this).attr('data-file');
            
            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                cache: false,
                data: JSON.stringify({id: id, fileName: fileName}),
                url: '/documents/delete',
                success: function(data) {
                    $('#loader-content').removeClass('active');
                    data = JSON.parse(data);
                    if (data.result == 'ok') {
                        _this.removeFile(file);
                    }else if(data.result == 'error'){
                        $(file.previewTemplate).find('td.status').html('<span class="text-danger"><i class="glyphicon glyphicon-ban-circle"></i> ' + data.error + '</span>');
                    }
                },
                error: function() {
                    $('#loader-content').removeClass('active');
                    $(file.previewTemplate).find('td.status').html('<span class="text-danger"><i class="glyphicon glyphicon-ban-circle"></i> Error. Please, try again.</span>');
                }
            });
        });
        
        cancelButton.addEventListener("click", function(e) {
            e.preventDefault();
            e.stopPropagation();
            _this.cancelUpload(file);
            $(file.previewTemplate).find('.progress-bar').css('width', '0%');
            $(file.previewTemplate).find('td.status').html('<span title="Ready for Upload"><i class="text-lg text-success glyphicon glyphicon-thumbs-up"></i></span>');
            
            $(file.previewTemplate).find('.delete-file').hide();
            $(file.previewTemplate).find('.cancel-file').hide();
            $(file.previewTemplate).find('.upload-file').show();
            $(file.previewTemplate).find('.remove-file').show();
        });
        
        uploadButton.addEventListener("click", function(e) {
            e.preventDefault();
            e.stopPropagation();
            _this.processFile(file);
            
            $(file.previewTemplate).find('.delete-file').hide();
            $(file.previewTemplate).find('.upload-file').hide();
            $(file.previewTemplate).find('.cancel-file').show();
            $(file.previewTemplate).find('.remove-file').hide();
        });  
        
        removeButton.addEventListener("click", function(e) {
            e.preventDefault();
            e.stopPropagation();
            _this.removeFile(file);
        });     

        
    });
    
    dropzone.on('processing', function(file) {
        $(file.previewTemplate).find('td.status').html('<span title="Uploading"><i class="text-md text-info glyphicon glyphicon-cloud-upload"></i></span>');
        $(file.previewTemplate).find('.delete-file').hide();
        $(file.previewTemplate).find('.upload-file').hide();
        $(file.previewTemplate).find('.cancel-file').show();
        $(file.previewTemplate).find('.remove-file').hide();
    });

    dropzone.on('queuecomplete', function(progress) {
        dropzone.options.autoProcessQueue = false;
    });
            
    dropzone.on('reset', function(totalPercentage, totalBytesToBeSent, totalBytesSent){
        $('#upload-all').attr('disabled', 'disabled');
        $('#remove-all').attr('disabled', 'disabled');
    });
    
});

function uploadAll(){
    dropzone.options.autoProcessQueue = true;
    dropzone.processQueue();
}
function removeAll(){
    dropzone.removeAllFiles(true);
}
