var express = require('express');
var router = express.Router();
var http = require('https');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var authentication = require('../helpers/authentication');
var User = require('../models/user');
var config = require('../config/config');
var stripe = require('stripe')(config.stripe.secretKey);
var moment = require('moment');

var emails = require('../config/emails');

var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
var transporter = nodemailer.createTransport(smtpTransport({
        host: config.email.host,
        port: config.email.port,
        auth: {
            user: config.email.auth.user,
            pass: config.email.auth.pass
        }
    })
);

router.use(bodyParser.urlencoded({ extended: true }));

router.use(methodOverride(function (req, res) {
    if (req.body && typeof req.body === 'object' && '_method' in req.body) {
        // look in urlencoded POST bodies and delete it
        var method = req.body._method
        delete req.body._method
        return method
    }
}));

router.get('/create-sample-data/:companyID/:userID/:userName', authentication.isLoggedIn, function (req, res) {
    var companyID = req.params.companyID;
    var userID = req.params.userID;
    var userName = req.params.userName;

    res.contentType('json');

    User.createSampleDataExport(companyID, userID, userName, function (err, sampleCreated) {
        if (err) {
            res.send({ result: 'error' });
        } else {
            if (sampleCreated) {
                res.send('sample created ok');
            } else {
                res.send('sample created error');
            }
        }
    });
});

router.get('/plan/:email', function (req, res) {
    var email = req.params.email;

    res.contentType('json');

    User.getUserPlan(email, function (err, plan) {
        if (err) {
            res.send({ result: 'error' });
        } else {
            res.send({ plan: plan });
        }
    });
});

router.post('/generate-new-access-code', authentication.isLoggedIn, function (req, res) {
    var companyID = req.user.company_id;

    res.contentType('json');

    User.getCompanyEmailAndAccessCode(companyID, function (err, data) {
        if (err) {
            console.log(err);
            res.send({ result: 'error' });
        } else {

            return http.get({
                host: config.andruav.url,
                port: config.andruav.port,
                method: 'GET',
                path: config.andruav.regenerateAccessCode + data.email + '&nemail=true&cb=' + config.mainURL + '/api/save-new-access-code/$USR/$PWD',
                rejectUnauthorized: false
            }, function (result) {
                result.on('end', function () {
                });

                result.on('error', function (e) {
                    console.log(e);
                });
            }).on('error', function (err) {
                console.error(err.message);
            });

            /*http.get(config.andruav.url + ':' + config.andruav.port + config.andruav.regenerateAccessCode + data.email + '&nemail=true&cb=' + config.mainURL + '/api/save-new-access-code/$USR/$PWD', function (result) {
                
                result.on('end', function () {
                });
                                            
                result.on('error', function (e) {
                    console.log(e);
                });
            });*/
        }
    });
});


router.get('/save-new-access-code/:email/:code', function (req, res) {
    var email = req.params.email;
    var accessCode = req.params.code;

    User.saveNewAccessCode(email, accessCode, function (err, result) {
        if (err) {
            console.log(err);
        } else {
            console.log(result);
        }
    });
});

// PAYMENT


router.post('/paymentok', function (req, res) {
    res.contentType('json');
       
    if (result.object.lines.data[0].period.end != undefined) {
        var newDate = moment(result.object.lines.data[0].period.end).format(config.config.mysqlTimeFormat);
        var customer = result.object.customer;

        User.updateSubscriptionDate(customer, newDate, function (err, result) {
            if (err) {
                console.log(err);
            } else {
                if (result == 'ok') {
                    res.sendStatus(200);
                }    
            }
        });
    }
});

router.post('/paymentfailed', function (req, res) {
    res.contentType('json');
    var result = req.body;

    if (result.data.object.lines.data[0].id!= undefined) {
        var subscriptionID = result.data.object.lines.data[0].id;
        if (result.data.object.customer != undefined) {
            var customerID = result.data.object.customer;

            User.getCompanyFromSubscriptionID(subscriptionID, customerID, function (err, customer) {
                if (err) {
                    console.log(err);
                } else {
                    // send activation email
                    var mailOptions = {
                        from: config.email.fromName + '<' + config.email.fromEmail + '>',
                        to: customer.name + '<' + customer.email + '>',
                        subject: emails.paymentFailed(customer.name).subject,
                        text: emails.paymentFailed(customer.name).text,
                        html: emails.paymentFailed(customer.name).html
                    };

                    transporter.sendMail(mailOptions, function (error, info) {
                        if (error) {
                            console.log(error);
                        } else {
                            User.failedSubscription(customer.id, function (err, result) {
                                if (err) {
                                    console.log(err);
                                } else {
                                    res.sendStatus(200);
                                }
                            });
                        }
                    });    

                }
            });
        }
    }
});

module.exports = router;