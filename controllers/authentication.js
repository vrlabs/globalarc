var express = require('express');
var http = require('http');
var router = express.Router();
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var expressValidator = require('express-validator');
var passport = require('passport');


var config = require('../config/config');
var language = require('../config/languages/' + config.config.language);
var pages = require('../helpers/pages');
var authentication = require('../helpers/authentication');
var emails = require('../config/emails');

var User = require('../models/user');

var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
var transporter = nodemailer.createTransport(smtpTransport({
        host: config.email.host,
        port: config.email.port,
        auth: {
            user: config.email.auth.user,
            pass: config.email.auth.pass
        }
    })
);

router.use(bodyParser.urlencoded({ extended: true }));

router.use(methodOverride(function(req, res) {
    if (req.body && typeof req.body === 'object' && '_method' in req.body) {
        // look in urlencoded POST bodies and delete it
        var method = req.body._method
        delete req.body._method
        return method
    }
}));

// LOGIN
router.get('/', function(req, res) {
    pages.renderLoggedOutPage(res, 'authentication/login', 'Sign In', req.flash('message'), req.flash('color'), req.body);
});

router.post('/', function(req, res) {
    req.checkBody('email', language.emailRequired).notEmpty();
    req.checkBody('email', language.validEmail).isEmail();
    req.checkBody('password', language.passwordRequired).notEmpty();

    var validationErrors = req.validationErrors();

    if (validationErrors) {
        console.log(validationErrors);
        var messages = '';
        for (var i = validationErrors.length - 1; i >= 0; i--) {
            messages += validationErrors[i].msg + '<br/>';
        }
        pages.renderLoggedOutPage(res, 'authentication/login', 'Sign In', messages, 'danger', req.body);

    } else {
        User.checkSubscription(req.body.email, function (err, result) {
            if (err) {
                console.log(err);
            } else {
                console.log(result);
                if (result == 'ok') {
                    passport.authenticate('local-login', {
                        successRedirect: '/dashboard',
                        failureRedirect: '/auth',
                        failureFlash: true
                    })(req, res);
                } else {
                    pages.renderLoggedOutPage(res, 'authentication/login', 'Sign In', language.subscriptionExpired, 'danger', {});
                }
            }
        });        
    }
});

// REGISTER
router.get('/register/:plan/:customerID/:subscriptionID', function (req, res) {
    req.session.plan = req.params.plan;
    req.session.customerID = req.params.customerID;
    req.session.subscriptionID = req.params.subscriptionID;
    
    pages.renderLoggedOutPage(res, 'authentication/register', 'Sign Up', req.flash('message'), req.flash('color'), req.body);
});

router.post('/register/:plan/:customerID/:subscriptionID', function (req, res) {
    delete req.session.plan;
    delete req.session.customerID;
    delete req.session.subscriptionID;
    
    var plan = req.params.plan;
    var customerID = req.params.customerID;
    var subscriptionID = req.params.subscriptionID;
    
    var params = req.body;
    params.plan = plan;
    params.customerID = customerID;
    params.subscriptionID = subscriptionID;
       
    params.checkboxMessage = language.agreeToTerms;

    User.isEmailAvailable(req.body.email, function(err, result) {
        if (err) {
            console.log(err);
        } else {
            if (result == 0) {
                User.isCompanyNameAvailable(req.body.company, function(err, result) {
                    if (err) {
                        console.log(err);
                    } else {
                        if (result == 0) {
                            req.checkBody('name', language.nameRequired).notEmpty();
                            req.checkBody('company', language.companyRequired).notEmpty();
                            req.checkBody('email', language.emailRequired).notEmpty();
                            req.checkBody('email', language.validEmail).isEmail();
                            req.checkBody('password', language.passwordRequired).notEmpty();
                            req.checkBody('password', language.passwordLength).len(6, 20);
                            req.checkBody('confirm_password', language.confirmPasswordRequired).notEmpty();
                            req.checkBody('confirm_password', language.passwordsDontMatch).equals(req.body.password);
                            
                            var validationErrors = req.validationErrors();

                            if (validationErrors) {
                                console.log(validationErrors);
                                var messages = '';
                                for (var i = 0; i < validationErrors.length; i++) {
                                    messages += validationErrors[i].msg + '<br/>';
                                }                                
                                
                                pages.renderLoggedOutPage(res, 'authentication/register', 'Sign Up', messages, 'danger', params);
                            } else {                            
                                passport.authenticate('local-register', {
                                    successRedirect: '/auth/register/' + plan + '/' + customerID + '/' + subscriptionID,
                                    failureRedirect: '/auth/register/' + plan + '/' + customerID + '/' + subscriptionID,
                                    failureFlash: true
                                })(req, res);                            
                            }
                        } else if (result == 1) {
                            pages.renderLoggedOutPage(res, 'authentication/register', 'Sign Up', language.companyNotAvailable, 'danger', params);
                        }
                    }
                });
            } else if (result == 1) {
                pages.renderLoggedOutPage(res, 'authentication/register', 'Sign Up', language.emailNotAvailable, 'danger', params);
            }
        }
    });    
});

// ACCOUNT ACTIVATION
router.get('/activation/:key', function(req, res) {
    var key = req.params.key;
    var message = '';
    var color = '';

    // results: error, 'active' or 'ok'
    User.activateUser(key, function(err, msg) {
        if (err) {
            console.log(err);
            message = err;
            color = 'danger';
            pages.renderLoggedOutPage(res, 'authentication/message', 'Activation', message, color, req.body);
        } else {
            if (msg == 'active') {
                message = language.accountAlreadyActivated;
                color = 'danger';
                pages.renderLoggedOutPage(res, 'authentication/message', 'Activation', message, color, req.body);
            } else {
                message = language.accountActivated;
                color = 'success';
                pages.renderLoggedOutPage(res, 'authentication/message', 'Activation', message, color, {});
            }
        }
    });
});

// INVITED ACCOUNT ACTIVATION
router.get('/invitation/:key', function(req, res) {
    pages.renderLoggedOutPage(res, 'authentication/set-password', 'Set Password', '', '', {});
});

router.post('/invitation/:key', function(req, res) {
    var key = req.params.key;
    var message = '';
    var color = '';

    req.checkBody('password', language.passwordRequired).notEmpty();
    req.checkBody('password', language.passwordLength).len(6, 20);
    req.checkBody('confirm_password', language.confirmPasswordRequired).notEmpty();
    req.checkBody('confirm_password', language.passwordsDontMatch).equals(req.body.password);

    var validationErrors = req.validationErrors();

    if (validationErrors) {
        console.log(validationErrors);
        var messages = '';
        for (var i = validationErrors.length - 1; i >= 0; i--) {
            messages += validationErrors[i].msg + '<br/>';
        }
        pages.renderLoggedOutPage(res, 'authentication/set-password', 'Set Password', messages, 'danger', req.body);

    } else {
        var message = '';
        var color = '';

        var password = req.body.password;

        User.activateInvitedUser(key, authentication.generateHash(password), function(err, msg) {
            if (err) {
                console.log(err);
                message = err;
                color = 'danger';
                pages.renderLoggedOutPage(res, 'authentication/set-password', 'Set Password', message, color, req.body);
            } else {
                if (msg == 'active') {
                    message = language.invitedAccountAlreadyActivated;
                    color = 'danger';
                    pages.renderLoggedOutPage(res, 'authentication/message', 'Activation', message, color, req.body);
                } else {
                    message = language.invitedAccountActivated;
                    color = 'success';
                    pages.renderLoggedOutPage(res, 'authentication/message', 'Activation', message, color, {});
                }
            }
        });
    }
});


// RESENDING ACTIVATION EMAIL
router.get('/resend-activation', function(req, res) {
    
    pages.renderLoggedOutPage(res, 'authentication/resend-activation', 'Resend Activation', req.flash('message'), req.flash('color'), req.body);
});

router.post('/resend-activation', function(req, res) {
    req.checkBody('email', language.emailRequired).notEmpty();
    req.checkBody('email', language.validEmail).isEmail();

    var validationErrors = req.validationErrors();

    if (validationErrors) {
        console.log(validationErrors);
        var messages = '';
        for (var i = validationErrors.length - 1; i >= 0; i--) {
            messages += validationErrors[i].msg + '<br/>';
        }
        pages.renderLoggedOutPage(res, 'authentication/resend-activation', 'Resend Activation', messages, 'danger', req.body);

    } else {
        var message = '';
        var color = '';

        var email = req.body.email;

        User.getUserByEmail(email, function(err, user) {
            if (err) {
                console.log(err);
                message = err;
                color = 'danger';
                pages.renderLoggedOutPage(res, 'authentication/resend-activation', 'Resend Activation', message, color, req.body);
            } else {
                if (!user) {
                    message = language.userNotFound;
                    color = 'danger';
                    pages.renderLoggedOutPage(res, 'authentication/resend-activation', 'Resend Activation', message, color, req.body);
                } else {
                    if (user.active == '1') {
                        message = language.accountAlreadyActivated;
                        color = 'warning';
                        pages.renderLoggedOutPage(res, 'authentication/resend-activation', 'Resend Activation', message, color, req.body);
                    } else {
                        if (user.invited == '1') {
                            // send invitation email
                            var url = req.protocol + '://' + req.get('host') + '/auth/invitation/' + user.invitation_key;

                            var mailOptions = {
                                from: config.email.fromName + '<' + config.email.fromEmail + '>',
                                to: user.name + '<' + user.email + '>',
                                subject: emails.invitation(req.user.name, url, user.name).subject,
                                text: emails.invitation(req.user.name, url, user.name).text,
                                html: emails.invitation(req.user.name, url, user.name).html
                            };

                            transporter.sendMail(mailOptions, function(error, info) {
                                if (error) {
                                    console.log(error);
                                    message = language.emailError;
                                    color = 'danger';
                                    pages.renderLoggedOutPage(res, 'authentication/resend-activation', 'Resend Activation', message, color, req.body);
                                } else {
                                    message = language.activationEmailSent;
                                    color = 'success';
                                    pages.renderLoggedOutPage(res, 'authentication/message', 'Resend Activation', message, color, {});
                                }
                            });
                        } else {
                            User.reActivateUser(email, function(err, activationKey) {
                                if (err) {
                                    console.log(err);
                                    message = err;
                                    color = 'danger';
                                    pages.renderLoggedOutPage(res, 'authentication/resend-activation', 'Resend Activation', message, color, req.body);
                                } else {

                                    // send activation email
                                    var url = req.protocol + '://' + req.get('host') + '/auth/activation/' + activationKey;
                                    var mailOptions = {
                                        from: config.email.fromName + '<' + config.email.fromEmail + '>',
                                        to: user.name + '<' + email + '>',
                                        subject: emails.activation(url, user.name).subject,
                                        text: emails.activation(url, user.name).text,
                                        html: emails.activation(url, user.name).html
                                    };

                                    transporter.sendMail(mailOptions, function(error, info) {
                                        if (error) {
                                            console.log(error);
                                            message = language.emailError;
                                            color = 'danger';
                                            pages.renderLoggedOutPage(res, 'authentication/resend-activation', 'Resend Activation', message, color, req.body);
                                        } else {
                                            message = language.activationEmailSent;
                                            color = 'success';
                                            pages.renderLoggedOutPage(res, 'authentication/message', 'Resend Activation', message, color, {});
                                        }
                                    });
                                }
                            });
                        }
                    }
                }
            }
        });
    }
});



// FORGOT PASSWORD
router.get('/forgot-password', function(req, res) {
    pages.renderLoggedOutPage(res, 'authentication/forgot-password', 'Forgot Password', req.flash('message'), req.flash('color'), req.body);
});

router.post('/forgot-password', function(req, res) {
    req.checkBody('email', language.emailRequired).notEmpty();
    req.checkBody('email', language.validEmail).isEmail();

    var validationErrors = req.validationErrors();

    if (validationErrors) {
        console.log(validationErrors);
        var messages = '';
        for (var i = validationErrors.length - 1; i >= 0; i--) {
            messages += validationErrors[i].msg + '<br/>';
        }
        pages.renderLoggedOutPage(res, 'authentication/forgot-password', 'Forgot Password', messages, 'danger', req.body);

    } else {
        var message = '';
        var color = '';

        User.getUserByEmail(req.body.email, function(err, user) {
            if (err) {
                console.log(err);
                message = err;
                color = 'danger';
                pages.renderLoggedOutPage(res, 'authentication/forgot-password', 'Reset Password', message, color, req.body);
            } else {
                if (!user) {
                    message = language.userNotFound;
                    color = 'danger';
                    pages.renderLoggedOutPage(res, 'authentication/forgot-password', 'Reset Password', message, color, req.body);
                } else {
                    User.setResetKey(req.body.email, function(err, key) {
                        if (err) {
                            console.log(err);
                            message = err;
                            color = 'danger';
                            pages.renderLoggedOutPage(res, 'authentication/forgot-password', 'Reset Password', message, color, req.body);
                        } else {

                            // send reset password email
                            var url = req.protocol + '://' + req.get('host') + '/auth/reset/' + key;

                            var mailOptions = {
                                from: config.email.fromName + '<' + config.email.fromEmail + '>',
                                to: user.name + '<' + req.body.email + '>',
                                subject: emails.reset(url, user.name).subject,
                                text: emails.reset(url, user.name).text,
                                html: emails.reset(url, user.name).html
                            };

                            // send mail with defined transport object
                            transporter.sendMail(mailOptions, function(error, info) {
                                if (error) {
                                    console.log(error);
                                    message = error;
                                    color = 'danger';
                                    pages.renderLoggedOutPage(res, 'authentication/forgot-password', 'Reset Password', message, color, req.body);
                                } else {
                                    message = language.resetEmailSent;
                                    color = 'success';
                                    pages.renderLoggedOutPage(res, 'authentication/message', 'Reset Password', message, color, req.body);
                                }
                            });
                        }
                    });
                }
            }
        });
    }
});


router.get('/reset/:key', function(req, res) {
    pages.renderLoggedOutPage(res, 'authentication/reset-password', 'Reset Password', req.flash('message'), req.flash('color'), req.body);
});

router.post('/reset/:key', function(req, res) {
    var key = req.params.key;

    req.checkBody('password', language.passwordRequired).notEmpty();
    req.checkBody('password', language.passwordLength).len(6, 20);
    req.checkBody('confirm_password', language.confirmPasswordRequired).notEmpty();
    req.checkBody('confirm_password', language.passwordsDontMatch).equals(req.body.password);

    var validationErrors = req.validationErrors();

    if (validationErrors) {
        console.log(validationErrors);
        var messages = '';
        for (var i = validationErrors.length - 1; i >= 0; i--) {
            messages += validationErrors[i].msg + '<br/>';
        }
        pages.renderLoggedOutPage(res, 'authentication/reset-password', 'Reset Password', messages, 'danger', req.body);

    } else {
        var message = '';
        var color = '';

        var password = req.body.password;

        User.resetPassword(key, authentication.generateHash(password), function(err, msg) {
            if (err) {
                console.log(err);
                message = err;
                color = 'danger';
                pages.renderLoggedOutPage(res, 'authentication/reset-password', 'Reset Password', message, color, req.body);
            } else {
                if (msg == 'ok') {
                    message = language.newPasswordSet;
                    color = 'success';
                    pages.renderLoggedOutPage(res, 'authentication/message', 'Reset Password', message, color, {});
                }
            }
        });
    }
});


// GOOGLE
router.get('/google/', passport.authenticate('google', { scope: ['profile', 'email'] }));

router.get('/google/callback', passport.authenticate('google', {
    successRedirect: '/dashboard',
    failureRedirect: '/auth',
    failureFlash: true
}));



// LOGOUT
router.get('/logout', function(req, res) {
    req.logout();
    res.redirect('/auth');
});

module.exports = router;