var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var config = require('../config/config');
var language = require('../config/languages/' + config.config.language);
var authentication = require('../helpers/authentication');
var pages = require('../helpers/pages');
var Project = require('../models/project');
var moment = require('moment');
var general = require('../helpers/general');
var User = require('../models/user');

router.use(bodyParser.urlencoded({ extended: true }));

router.use(methodOverride(function(req, res) {
    if (req.body && typeof req.body === 'object' && '_method' in req.body) {
        // look in urlencoded POST bodies and delete it
        var method = req.body._method
        delete req.body._method
        return method
    }
}));

// ALL PROJECTS
router.get('/', authentication.isLoggedIn, function (req, res) {
    Project.getAllProjectsForUser(req.user.id, function(err, projects) {
        if (err) {
            console.log(err);
        } else {
            for (var i = 0; i < projects.length; i++) {
                if (projects[i].notes != undefined && projects[i].notes != 'null' && projects[i].notes != '') {
                    projects[i].notes = '<a href="#" data-toggle="modal" data-target="#modal-notes" onclick="setNotes(\'' + projects[i].notes + '\');"><i class="fa fa-file-text-o"></i></a>';
                }
                if (projects[i].numOfFlightplans == 0) {
                    var link = ('/flightplans/add/' + projects[i].id).toString();
                    projects[i].flightplans = '<a class="btn btn-sm btn-info" href="#" onclick="checkPlanFlightplans(\'' + link + '\');"><i class="fa fa-plus"></i></a>';
                } else {
                    projects[i].name = '<a class="text-info" href="/flightplans/all/' + projects[i].id + '">' + projects[i].name + '</a>';
                    projects[i].flightplans = '<a href="/flightplans/all/' + projects[i].id + '"><i class="icon-paper-plane"></i></a>';
                }

                projects[i].edit = '<a href="/projects/edit/' + projects[i].id + '"><i class="glyphicon glyphicon-edit"></i></a>';
                projects[i].delete = '<a href="#" data-toggle="modal" data-target="#modal-delete" onclick="setDeleteProjectButton(' + projects[i].id + ');"><i class="glyphicon glyphicon-trash"></i></a>';
            }
            pages.renderLoggedInPage(req, res, 'projects/projects', 'Projects', req.flash('message'), req.flash('color'), { 'data': JSON.stringify(projects), 'header': 'Projects' });
        }
    });
});

// add
router.get('/add', authentication.isLoggedIn, function (req, res) {
    User.checkPlanProjects(req.user.company_id, req.user.plan, function (err, result) {
        if (err) {
            console.log(err);
        } else {
            if (result == 'yes') {
                var breadcrumbs = '<a href="/projects" class="text-info">Projects</a> <i class="fa fa-angle-right m-l-sm m-r-sm light"></i> Create New Project';
                pages.renderLoggedInPage(req, res, 'projects/modify', 'Projects | Add New', '', '', {
                    header: breadcrumbs
                });
            } else if (result == 'no') {
                res.sendStatus(403);
            }
        }
    });
});
router.post('/add', authentication.isLoggedIn, function(req, res) {
    req.checkBody('name', language.projectNameRequired).notEmpty();

    req.asyncValidationErrors().then(function() {
        var data = {
            company_id: req.user.company_id,
            name: req.body.name,
            notes: req.body.notes
        }

        Project.add(req.user.id, data, function(err, id) {
            if (err) {
                console.log(err);
                pages.renderLoggedInPage(req, res, 'projects/modify', 'Projects | Add New', language.dbError, 'danger', { header: 'Create New Project' });
            } else {
                req.flash('message', language.projectAdded);
                req.flash('color', 'success');
                res.redirect('/flightplans/all/' + id);
            }
        });

    }).catch(function(errors) {
        if (errors) {
            console.log(errors);
            var messages = '';
            for (var i = errors.length - 1; i >= 0; i--) {
                messages += errors[i].msg + '<br/>';
            }
            pages.renderLoggedInPage(req, res, 'projects/modify', 'Projects | Add New', messages, 'danger', {
                header: 'Create New Project',
                name: req.body.name,
                notes: req.body.notes
            });
        };
    });
});


// edit
router.get('/edit/:id', authentication.isLoggedIn, function(req, res) {
    Project.getProjectByID(req.params.id, function(err, project) {
        if (err) {
            console.log(err);
        } else {
            var breadcrumbs = '<a href="/projects" class="text-info">Projects</a> <i class="fa fa-angle-right m-l-sm m-r-sm light"></i> <a href="/flightplans/all/' + project.id + '" class="text-info">' + project.name + '</a> <i class="fa fa-angle-right m-l-sm m-r-sm light"></i> Edit';

            pages.renderLoggedInPage(req, res, 'projects/modify', 'Projects | Edit', '', '', {
                name: project.name,
                notes: project.notes,
                projectID: project.id,
                header: breadcrumbs
            });
        }
    });

});
router.post('/edit/:id', authentication.isLoggedIn, function(req, res) {
    var id = req.params.id;

    req.checkBody('name', language.projectNameRequired).notEmpty();

    req.asyncValidationErrors().then(function() {
        var data = {
            name: req.body.name,
            notes: req.body.notes
        };

        Project.edit(id, data, function(err, affectedRows) {
            if (err) {
                console.log(err);
                pages.renderLoggedInPage(req, res, 'projects/modify', 'Projects | Edit', language.dbError, 'danger', {
                    header: 'Edit Project',
                    name: req.body.name,
                    notes: req.body.notes
                });
            } else {
                if (affectedRows === 1) {
                    req.flash('message', language.projectModified);
                    req.flash('color', 'success');
                    res.redirect('/projects');
                }
            }
        });

    }).catch(function(errors) {
        if (errors) {
            console.log(errors);
            var messages = '';
            for (var i = errors.length - 1; i >= 0; i--) {
                messages += errors[i].msg + '<br/>';
            }
            pages.renderLoggedInPage(req, res, 'projects/modify', 'Projects | Edit', messages, 'danger', { header: 'Edit Project' });
        };
    });
});

// delete
router.get('/delete/:id', authentication.isLoggedIn, function(req, res) {
    var id = req.params.id;

    Project.delete(id, function(err, affectedRows) {
        if (err) {
            console.log(err);
            req.flash('message', language.dbError);
            req.flash('color', 'danger');
            res.redirect('/projects');
        } else {
            if (affectedRows === 1) {
                req.flash('message', language.projectDeleted);
                req.flash('color', 'success');
                res.redirect('/projects');
            }
        }
    });
});

router.post('/get-locations/:id', authentication.isLoggedIn, function(req, res) {
    var id = req.params.id;
    res.contentType('json');

    Project.getLocations(id, function(err, locations) {
        if (err) {
            res.send({ result: 'error' });
        } else {
            var coordinates = [];
            for (var i = 0; i < locations.length; i++){
                coordinates.push([locations[i].center_longitude, locations[i].center_latitude]);
            }
            var bounds = general.findBoundingBox(coordinates);
            res.send({result: 'ok', bounds:bounds});
        }
    });
});




module.exports = router;