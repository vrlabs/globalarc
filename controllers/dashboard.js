var express = require('express');
var router = express.Router();
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var config = require('../config/config');
var passport = require('passport');
var authentication = require('../helpers/authentication');
var pages = require('../helpers/pages');
var moment = require('moment');

var Flightplan = require('../models/flightplan');
var Project = require('../models/project');
var Team = require('../models/team');
var Battery = require('../models/battery');
var Vehicle = require('../models/vehicle');
var Document = require('../models/document');

router.use(bodyParser.urlencoded({ extended: true }));

router.use(methodOverride(function(req, res) {
    if (req.body && typeof req.body === 'object' && '_method' in req.body) {
        // look in urlencoded POST bodies and delete it
        var method = req.body._method
        delete req.body._method
        return method
    }
}));

router.get('/', authentication.isLoggedIn, function (req, res) {
    var companyID = req.user.company_id;
    var userID = req.user.id;

    Vehicle.getAllVehiclesForCompany(companyID, function(err, vehicles) {
        if (err) {
            console.log(err);
        } else {
            Battery.getAllBatteriesForCompany(companyID, function(err, batteries) {
                if (err) {
                    console.log(err);
                } else {
                    Document.getAllDocumentsForCompany(companyID, function(err, documents) {
                        if (err) {
                            console.log(err);
                        } else {
                            for (var i = 0; i < documents.length; i++){
                                if (documents[i].provider == 'google') {
                                    documents[i].created_by = documents[i].google_user_name;
                                } else {
                                    documents[i].created_by = documents[i].user_name;
                                }

                                documents[i].modified = moment(documents[i].modified).format(config.config.timeFormat);
                            }
                            Team.getAllTeamMembersForCompany(companyID, function(err, members) {
                                if (err) {
                                    console.log(err);
                                } else {
                                    for (var j = 0; j < members.length; j++){
                                        if (members[j].provider == 'google') {
                                            members[j].user_photo = members[j].google_photo;
                                        } else {
                                            members[j].user_photo = members[j].photo;
                                        }
                                    }
                                    Project.getAllProjectsForUser(userID, function(err, projects) {
                                        if (err) {
                                            console.log(err);
                                        } else {
                                            var markers = [];
                                            for (var i = 0; i < projects.length; i++) {
                                                for (var j = 0; j < projects[i].flightplans.length; j++){
                                                    var coordinates = [];
                                                    coordinates[0] = projects[i].flightplans[j].center_latitude;
                                                    coordinates[1] = projects[i].flightplans[j].center_longitude;

                                                    var now = moment();
                                                    var start = moment(projects[i].flightplans[j].start_time);
                                                    var end = moment(projects[i].flightplans[j].end_time);

                                                    var markerClass = '';
                                                    var type = '';
                                                    
                                                    if (end < now) {
                                                        markerClass = 'fa  fa-arrow-left';
                                                        type = 'past';
                                                    } else if (start <= now && now <= end) {
                                                        markerClass = 'fa  fa-clock-o';
                                                        type = 'current';
                                                    } else if (start > now) {
                                                        markerClass = 'fa  fa-arrow-right';  
                                                        type = 'future';
                                                    }
                                                    
                                                    markers.push({
                                                        type: 'Feature',
                                                        geometry: {
                                                            type: 'Point',
                                                            coordinates: coordinates
                                                        },


                                                        properties: {
                                                            title: projects[i].flightplans[j].name,
                                                            description: '<img width="' + config.config.thumbW + '" height="' + config.config.thumbH + '" src="' + projects[i].flightplans[j].snapshot + '"/><div class="description"><strong>Start time:</strong> ' + moment(projects[i].flightplans[j].start_time).format(config.config.timeFormat) + '<br/><strong>End time:</strong> ' + moment(projects[i].flightplans[j].end_time).format(config.config.timeFormat) + '</div>',
                                                            icon: {
                                                                class: 'marker ',
                                                                html: '<div class="outer-marker" style="background-color:' + projects[i].color + '"><div class="inner-marker"><i class="' + markerClass + '"></i></div></div>',
                                                                iconSize: null

                                                            }
                                                        }
                                                    })
                                                }
                                            }

                                            var hasMarkers = 0;
                                            if (markers.length > 0) {
                                                hasMarkers = 1;
                                            }

                                            pages.renderLoggedInPage(req, res, 'dashboard', 'Dashboard', '', '', {
                                                header: 'Dashboard',
                                                vehicles: vehicles,
                                                batteries: batteries,
                                                documents: documents,
                                                members: members,
                                                projects: projects,
                                                hasMarkers: hasMarkers,
                                                markers: JSON.stringify(markers),
                                                airmapApiKey: config.airmapApiKey,
                                                mapboxApiKey: config.mapboxApiKey,
                                                showWelcomeScreen: req.user.help
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
    });
});

module.exports = router;