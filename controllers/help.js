var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var config = require('../config/config');
var language = require('../config/languages/' + config.config.language);
var authentication = require('../helpers/authentication');
var pages = require('../helpers/pages');

router.use(bodyParser.urlencoded({ extended: true }));

router.use(methodOverride(function(req, res) {
    if (req.body && typeof req.body === 'object' && '_method' in req.body) {
        // look in urlencoded POST bodies and delete it
        var method = req.body._method
        delete req.body._method
        return method
    }
}));

router.get('/', authentication.isLoggedIn, function(req, res) {
    pages.renderLoggedInPage(req, res, 'help', 'Help', '', '', {header: 'Help'});
});

module.exports = router;