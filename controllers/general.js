var express = require('express');
var router = express.Router();
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var authentication = require('../helpers/authentication');

router.use(bodyParser.urlencoded({ extended: true }));

router.use(methodOverride(function (req, res) {
    if (req.body && typeof req.body === 'object' && '_method' in req.body) {
        // look in urlencoded POST bodies and delete it
        var method = req.body._method
        delete req.body._method
        return method
    }
}));

router.post('/set-alert-cookie', authentication.isLoggedIn, function (req, res) {           
    res.cookie('GlobalARCAlert', '1', { maxAge: 86400000, httpOnly: true });
    res.send('');
});


module.exports = router;