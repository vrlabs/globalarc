var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var moment = require('moment');
var http = require('http');
var fs = require('fs');

var config = require('../config/config');
var language = require('../config/languages/' + config.config.language);
var authentication = require('../helpers/authentication');
var general = require('../helpers/general');
var pages = require('../helpers/pages');
var Andruav = require('../helpers/andruav');

var Flightplan = require('../models/flightplan');
var Project = require('../models/project');
var Team = require('../models/team');
var Battery = require('../models/battery');
var Vehicle = require('../models/vehicle');
var Document = require('../models/document');
var User = require('../models/user');

router.use(bodyParser.urlencoded({ extended: true }));

router.use(methodOverride(function (req, res) {
    if (req.body && typeof req.body === 'object' && '_method' in req.body) {
        // look in urlencoded POST bodies and delete it
        var method = req.body._method
        delete req.body._method
        return method
    }
}));

// ALL FLIGHTPLANS
router.get('/all/:id', authentication.isLoggedIn, function (req, res) {
    var id = req.params.id;
    var userID = req.user.id;

    Flightplan.getAllFlightplanInfo(userID, id, function (err, result) {
        if (err) {
            console.log(err);
        } else {
            for (var i = 0; i < result.flightplans.length; i++) {
                result.flightplans[i].name = '<a href="/flightplans/view/' + result.flightplans[i].id + '" class="text-info">' + result.flightplans[i].name + '</a>';
                result.flightplans[i].start_time = moment(result.flightplans[i].start_time).format(config.config.timeFormat);
                result.flightplans[i].end_time = moment(result.flightplans[i].end_time).format(config.config.timeFormat);

                if (result.flightplans[i].notes != undefined && result.flightplans[i].notes != 'null' && result.flightplans[i].notes != '') {
                    result.flightplans[i].notes = '<a href="#" data-toggle="modal" data-target="#modal-notes" onclick="setNotes(\'' + result.flightplans[i].notes + '\');"><i class="fa fa-file-text-o"></i></a>';
                }
                result.flightplans[i].location = '<a href="#" data-toggle="modal" data-target="#modal-location" onclick="setLocationSnapshot(\'' + result.flightplans[i].snapshot + '\', \'' + result.flightplans[i].location_name + '\')" class="text-info">' + result.flightplans[i].location_name + '</a>';
                result.flightplans[i].view = '<a href="/flightplans/view/' + result.flightplans[i].id + '"><i class="icon-magnifier"></i></a>';
                result.flightplans[i].edit = '<a href="/flightplans/edit/' + result.flightplans[i].id + '"><i class="glyphicon glyphicon-edit"></i></a>';
                result.flightplans[i].delete = '<a href="#" data-toggle="modal" data-target="#modal-delete" onclick="setDeleteFlightplanButton(' + id + ', ' + result.flightplans[i].id + ');"><i class="glyphicon glyphicon-trash"></i></a>';
            }

            var breadcrumbs = '<a href="/projects" class="text-info">Projects</a> <i class="fa fa-angle-right m-l-sm m-r-sm light"></i> ' + result.projectName;



            pages.renderLoggedInPage(req, res, 'flightplans/flightplans', 'Flightplans', req.flash('message'), req.flash('color'), {
                'flightplans': JSON.stringify(result.flightplans),
                'documentTypes': result.documentTypes,
                'header': breadcrumbs,
                'projectID': id
            });
        }
    });
});


// add
router.get('/add/:id', authentication.isLoggedIn, function (req, res) {
    User.checkPlanFlightplans(req.user.id, req.user.company_id, req.user.plan, function (err, result) {
        if (err) {
            console.log(err);
        } else {
            if (result == 'yes') {
                //var groupName = general.randomString(config.lengths.groupName);

                //Flightplan.getAvailableGroupName(companyID)    
                var groupName = 1;
                var projectID = req.params.id;
                var companyID = req.user.company_id;
                var userID = req.user.id;

                Flightplan.getAllInfoAdd(projectID, companyID, function (err, result) {
                    if (err) {
                        console.log(err);
                    } else {
                        var breadcrumbs = '<a href="/projects" class="text-info">Projects</a> <i class="fa fa-angle-right m-l-sm m-r-sm light"></i> <a href="/flightplans/all/' + projectID + '" class="text-info">' + result.projectName + '</a> <i class="fa fa-angle-right m-l-sm m-r-sm light"></i> Create New Flightplan';

                        var hasPreviousLocations = false;            
                        if (result.locations > 0) {
                            hasPreviousLocations = true;
                        }

                        pages.renderLoggedInPage(req, res, 'flightplans/add', 'Flightplans | Add New', req.flash('message'), req.flash('color'), {
                            header: breadcrumbs,
                            members: result.members,
                            batteries: result.batteries,
                            vehicles: result.vehicles,
                            roles: result.roles,
                            documentTypes: result.documentTypes,
                            documents: result.documents,
                            projectID: projectID,
                            flightplanDocuments: [],
                            S3RequestURL: '/documents/sign/documents',
                            S3Bucket: 'http://' + config.aws.bucketName + '.s3.amazonaws.com/',
                            currentStartTime: moment().format(config.config.usTimeFormat),
                            currentEndTime: moment().add(1, 'm').format(config.config.usTimeFormat),
                            groupName: groupName,
                            airmapApiKey: config.airmapApiKey,
                            mapboxApiKey: config.mapboxApiKey,
                            hasPreviousLocations: hasPreviousLocations
                        });
                    }
                });
            } else if (result == 'no') {
                res.sendStatus(403);
            }
        }
    });
});

router.post('/add/:id', authentication.isLoggedIn, function (req, res) {   
    var projectID = req.params.id;
    var coordinates = general.getCoordinatesArray(req.body.coordinates);
    var userID = req.user.id;

    var saveAs = req.body.how_to_save;
    if (req.body.how_to_save == '' || req.body.how_to_save == undefined) {
        saveAs = 'new';
    }

    if (saveAs == 'new') {
        Flightplan.addLocation(req.user.company_id, projectID, req.body.location_name, coordinates, req.body.screenshot, function (err, locationID) {
            if (err) {
                console.log(err);
                req.flash('message', err);
                req.flash('color', 'danger');
                res.redirect('/flightplans/all/' + projectID);
            } else {
                
                Flightplan.add(projectID, locationID, userID, req.body, function (err, result) {
                    if (err) {
                        console.log(err);
                        req.flash('message', err);
                        req.flash('color', 'danger');
                        res.redirect('/flightplans/all/' + projectID);
                    } else {
                        if (result == 'ok') {
                            req.flash('message', language.flightplanAdded);
                            req.flash('color', 'success');
                            res.redirect('/flightplans/all/' + projectID);
                        }
                    }
                });
            }
        })
    } else if (saveAs == 'old') {
        Flightplan.editLocation(req.user.company_id, req.body.selected_location_id, coordinates, req.body.screenshot, function (err, locationID) {
            if (err) {
                console.log(err);
                req.flash('message', err);
                req.flash('color', 'danger');
                res.redirect('/flightplans/all/' + projectID);
            } else {
                Flightplan.add(projectID, req.body.selected_location_id, userID, req.body, function (err, result) {
                    if (err) {
                        console.log(err);
                        req.flash('message', err);
                        req.flash('color', 'danger');
                        res.redirect('/flightplans/all/' + projectID);
                    } else {
                        if (result == 'ok') {
                            req.flash('message', language.flightplanAdded);
                            req.flash('color', 'success');
                            res.redirect('/flightplans/all/' + projectID);
                        }
                    }
                });
            }
        });
    } else if (saveAs == 'save') {
        Flightplan.add(projectID, req.body.selected_location_id, userID, req.body, function (err, result) {
            if (err) {
                console.log(err);
                req.flash('message', err);
                req.flash('color', 'danger');
                res.redirect('/flightplans/all/' + projectID);
            } else {
                if (result == 'ok') {
                    req.flash('message', language.flightplanAdded);
                    req.flash('color', 'success');
                    res.redirect('/flightplans/all/' + projectID);
                }
            }
        });
    }
});

//edit
router.get('/edit/:id', authentication.isLoggedIn, function (req, res) {
    var flightplanID = req.params.id;
    var companyID = req.user.company_id;
    var userID = req.user.id;

    Flightplan.getAllInfoEdit(flightplanID, companyID, function (err, result) {
        if (err) {
            console.log(err);
        } else {
            var breadcrumbs = '<a href="/projects" class="text-info">Projects</a> <i class="fa fa-angle-right m-l-sm m-r-sm light"></i> <a href="/flightplans/all/' + result.info.project_id + '" class="text-info">' + result.projectName + '</a> <i class="fa fa-angle-right m-l-sm m-r-sm light"></i> <a href="/flightplans/view/' + flightplanID + '" class="text-info">' + result.info.name + '</a> <i class="fa fa-angle-right m-l-sm m-r-sm light"></i> Edit';

            var selectedLocationCoordinatesString = '';
            for (var i = 0; i < result.selectedLocationCoordinates.length; i++){
                selectedLocationCoordinatesString += result.selectedLocationCoordinates[i].latitude + ',' + result.selectedLocationCoordinates[i].longitude + '#';
            }
            selectedLocationCoordinatesString = selectedLocationCoordinatesString.substr(0, selectedLocationCoordinatesString.length - 1);


            pages.renderLoggedInPage(req, res, 'flightplans/edit', 'Flightplans | Edit', req.flash('message'), req.flash('color'), {
                header: breadcrumbs,
                members: result.members,
                batteries: result.batteries,
                vehicles: result.vehicles,
                roles: result.roles,
                documentTypes: result.documentTypes,
                documents: result.documents,
                flightplan: result.info,
                flightplanMembers: result.flightplanMembers,
                flightplanBatteries: result.flightplanBatteries,
                flightplanVehicles: result.flightplanVehicles,
                flightplanDocuments: result.flightplanDocuments,
                S3RequestURL: '/documents/sign/documents',
                S3Bucket: 'http://' + config.aws.bucketName + '.s3.amazonaws.com/',
                startTime: moment(new Date(result.info.start_time)).format(config.config.usTimeFormat),
                endTime: moment(new Date(result.info.end_time)).format(config.config.usTimeFormat),
                projectID: result.info.project_id,
                airmapApiKey: config.airmapApiKey,
                mapboxApiKey: config.mapboxApiKey,
                locations: result.locations,
                selectedLocationCoordinates: result.selectedLocationCoordinates,
                selectedLocationCoordinatesString: selectedLocationCoordinatesString
            });
        }
    });
});

router.post('/edit/:id', authentication.isLoggedIn, function (req, res) {
    var projectID = req.body.hidden_project_id;
    var flightplanID = req.params.id;
    var coordinates = general.getCoordinatesArray(req.body.coordinates);
    var userID = req.user.id;

    var saveAs = req.body.how_to_save;
    if (req.body.how_to_save == '' || req.body.how_to_save == undefined) {
        saveAs = 'save';
    }

    if (saveAs == 'new') {
        Flightplan.addLocation(req.user.company_id, projectID, req.body.location_name, coordinates, req.body.screenshot, function (err, locationID) {
            if (err) {
                console.log(err);
                req.flash('message', err);
                req.flash('color', 'danger');
                res.redirect('/flightplans/all/' + projectID);
            } else {
                Flightplan.edit(projectID, flightplanID, locationID, userID, req.body, function (err, result) {
                    if (err) {
                        console.log(err);
                        req.flash('message', err);
                        req.flash('color', 'danger');
                        res.redirect('/flightplans/all/' + projectID);
                    } else {
                        if (result == 'ok') {
                            req.flash('message', language.flightplanEdited);
                            req.flash('color', 'success');
                            res.redirect('/flightplans/all/' + projectID);
                        }
                    }
                });
            }
        })
    } else if (saveAs == 'old') {
        Flightplan.editLocation(req.user.company_id, req.body.selected_location_id, coordinates, req.body.screenshot, function (err, locationID) {
            if (err) {
                console.log(err);
                req.flash('message', err);
                req.flash('color', 'danger');
                res.redirect('/flightplans/all/' + projectID);
            } else {
                Flightplan.edit(projectID, flightplanID, req.body.selected_location_id, userID, req.body, function (err, result) {
                    if (err) {
                        console.log(err);
                        req.flash('message', err);
                        req.flash('color', 'danger');
                        res.redirect('/flightplans/all/' + projectID);
                    } else {
                        if (result == 'ok') {
                            req.flash('message', language.flightplanEdited);
                            req.flash('color', 'success');
                            res.redirect('/flightplans/all/' + projectID);
                        }
                    }
                });
            }
        });
    } else if (saveAs == 'save') {
        Flightplan.edit(projectID, flightplanID, req.body.selected_location_id, userID, req.body, function (err, result) {
            if (err) {
                console.log(err);
                req.flash('message', err);
                req.flash('color', 'danger');
                res.redirect('/flightplans/all/' + projectID);
            } else {
                if (result == 'ok') {
                    req.flash('message', language.flightplanEdited);
                    req.flash('color', 'success');
                    res.redirect('/flightplans/all/' + projectID);
                }
            }
        });
    }
});  

// delete
router.get('/delete/:projectID/:flightplanID', authentication.isLoggedIn, function (req, res) {
    var projectID = req.params.projectID;
    var flightplanID = req.params.flightplanID;

    Flightplan.delete(flightplanID, function (err, affectedRows) {
        if (err) {
            console.log(err);
            req.flash('message', language.dbError);
            req.flash('color', 'danger');
            res.redirect('/flightplans/all/' + projectID);
        } else {
            if (affectedRows === 1) {
                req.flash('message', language.flightplanDeleted);
                req.flash('color', 'success');
                res.redirect('/flightplans/all/' + projectID);
            }
        }
    });
});


// view
router.get('/view/:id', authentication.isLoggedIn, function (req, res) {
    var flightplanID = req.params.id;

    Flightplan.getFlightplanByID(flightplanID, function (err, flightplan) {
        if (err) {
            console.log(err);
        } else {
            Flightplan.getFlightplanComments(flightplanID, function (err, comments) {
                if (err) {
                    console.log(err);
                } else {
                    var parsedComments = JSON.parse(comments);

                    for (var i = 0; i < parsedComments.length; i++) {
                        var timestamp = parsedComments[i].timestamp;
                        var formattedTimestamp = moment(timestamp).format(config.config.timeFormat);
                        parsedComments[i].timestamp = formattedTimestamp;
                    }

                    Flightplan.getMemberUnitID(flightplanID, req.user.id, function (err, ids) {
                        if (err) {
                            console.log(err);
                        } else {

                            Flightplan.getVehicleUnitID(flightplanID, function (err, vehicleIds) {
                                if (err) {
                                    console.log(err)
                                } else {
                                    Flightplan.getLocationCoordinates(flightplan.location_id, function (err, coord) {
                                        if (err) {
                                            console.log(err);
                                        } else {
                                            var breadcrumbs = '<a href="/projects" class="text-info">Projects</a> <i class="fa fa-angle-right m-l-sm m-r-sm light"></i> <a href="/flightplans/all/' + flightplan.project_id + '" class="text-info">' + flightplan.project_name + '</a> <i class="fa fa-angle-right m-l-sm m-r-sm light"></i> ' + flightplan.name;
                                            var coordinates = [];
                                            var coordinatesGoogle = [];
                                            for (var i = 0; i < coord.length; i++){
                                                coordinates.push([coord[i].latitude, coord[i].longitude]);
                                                coordinatesGoogle.push({ lat: coord[i].longitude, lng: coord[i].latitude });
                                            }
                                            
                                            var coordinatesCenter = general.getMapCenter(coordinates);
                                            var coordinatesCenterGoogle = {lat: coordinatesCenter[1], lng: coordinatesCenter[0]}
                                            pages.renderLoggedInPage(req, res, 'flightplans/view', 'Flightplans | View', '', '', {
                                                header: breadcrumbs,
                                                comments: parsedComments,
                                                flightplanID: req.params.id,
                                                projectID: flightplan.project_id,
                                                accessCode: req.user.access_code,
                                                unitID: ids.unit_id,
                                                roleID: ids.role_id,
                                                userID: req.user.id,
                                                groupName: flightplan.group_name, 
                                                vehicleID: vehicleIds.unit_id,
                                                //mapboxApiKey: config.mapboxApiKey,
                                                googleMapsApiKey: config.googleMapsApiKey.key,
                                                coordinates: JSON.stringify(coordinates),
                                                coordinatesGoogle: JSON.stringify(coordinatesGoogle),
                                                coordinatesCenter: JSON.stringify(coordinatesCenterGoogle)
                                            });
                                        }
                                    });
                                }
                            })
                        }
                    })
                }
            });
        }
    });
});

router.post('/andruav-login', authentication.isLoggedIn, function (req, res) {
    res.contentType('json');

    var email = req.user.company_email;
    var accessCode = req.user.access_code;

    Andruav.login(email, accessCode, function (result, data) {
        var message = '';
        var color = '';
        var loginCode = '';
        var ip = '';
        var port = '';

        if (result == 1) {
            res.send(JSON.stringify({ result: 'ok', params: { loginCode: data.loginCode, ip: data.ip, port: data.port } }));
        } else {
            res.send(JSON.stringify({ result: 'error', message: data }));
        }
    })
});

router.post('/get-locations/:id', authentication.isLoggedIn, function (req, res) {
    res.contentType('json');

    Flightplan.getAllLocations(req.params.id, function (err, locations) {
        if (err) {
            console.log(err);
            res.send({ result: 'error' });
        } else {
            res.send({ result: 'ok', locations: locations });
        }
    })
});

router.post('/get-location-coordinates/:id', authentication.isLoggedIn, function (req, res) {
    res.contentType('json');

    Flightplan.getLocationCoordinates(req.params.id, function (err, coordinates) {
        if (err) {
            console.log(err);
            res.send({ result: 'error' });
        } else {
            res.send({ result: 'ok', coordinates: coordinates });
        }
    })
});

router.post('/change-vehicle-id/:flightplanID/:vehicleID', authentication.isLoggedIn, function (req, res) {
    var flightplanID = req.params.flightplanID;
    var newVehicleID = req.params.vehicleID;
   
    res.contentType('json');

    Flightplan.changeVehicleID(flightplanID, newVehicleID, function (err, affectedRows) {
        if (err) {
            console.log(err);
            res.send({ result: 'error' });
        } else {
            if (affectedRows == 1) {
                res.send({ result: 'ok' });
            } else {
                res.send({ result: 'error' });                
            }    
        }
    })
});

router.post('/change-member-id/:flightplanID/:memberID', authentication.isLoggedIn, function (req, res) {
    var flightplanID = req.params.flightplanID;
    var newMemberID = req.params.memberID;

    res.contentType('json');

    Flightplan.changeMemberID(flightplanID, newMemberID, req.user.id, function (err, affectedRows) {
        if (err) {
            console.log(err);
            res.send({ result: 'error' });
        } else {
            if (affectedRows == 1) {
                res.send({ result: 'ok' });
            } else {
                res.send({ result: 'error' });                
            }    
        }
    })
});

module.exports = router;