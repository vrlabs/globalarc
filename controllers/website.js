var http = require('http');
var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var config = require('../config/config');
var pages = require('../helpers/pages');
var language = require('../config/languages/' + config.config.language);
var expressValidator = require('express-validator');


router.use(bodyParser.urlencoded({ extended: true }));
router.use(expressValidator());

router.use(methodOverride(function (req, res) {
    if (req.body && typeof req.body === 'object' && '_method' in req.body) {
        // look in urlencoded POST bodies and delete it
        var method = req.body._method
        delete req.body._method
        return method
    }
}));

router.get('/', function (req, res) {
    pages.renderLoggedOutPage(res, 'website/home', '', '', '', {signup: true, environment:config.environment});
});

router.get('/about', function (req, res) {
    pages.renderFrontEndPage(res, 'website/about', 'About', {signup: true, environment:config.environment});
});

router.get('/privacy', function (req, res) {
    pages.renderFrontEndPage(res, 'website/privacy', 'Privacy Policy', {signup: true, environment:config.environment});
});

router.get('/pricing', function (req, res) {
    pages.renderFrontEndPage(res, 'website/pricing', 'Pricing', {
        plans: config.plans,
        stripe: config.stripe,
        signup: false,
        environment: config.environment
    });
});

router.get('/terms', function (req, res) {
    pages.renderFrontEndPage(res, 'website/terms', 'Terms of Service', {signup: true, environment:config.environment});
});

router.get('/call', function (req, res) {
    pages.renderLoggedOutPage(res, 'website/call', '', '', '', {signup: true, environment:config.environment});
});

router.post('/call', function (req, res) {
    res.contentType('json');

    req.checkBody('email', 'Valid email address is required').notEmpty().isEmail();

    var validationErrors = req.validationErrors();
    var message = '';
    var color = '';

    if (!validationErrors) {
        var customer = {
            name: req.body.name,
            email: req.body.email,
            phone: req.body.phone
        };

        // send request call email
        var mailOptions = {
            from: config.email.fromName + '<' + config.email.fromEmail + '>',
            to: config.email.toName + '<' + config.email.toEmail + '>',
            subject: emails.requestCall().subject,
            text: emails.requestCall(customer.name, customer.email, customer.phone).text,
            html: emails.requestCall(customer.name, customer.email, customer.phone).html
        };

        transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                console.log(error);
                res.send({ result: 'error', message: 'Something went wrong while sending your request. Please, try again'});
            } else {
                res.send({ result: 'ok' });
            }
        });

        // validation errors    
    } else {
        message = validationErrors[0].msg;
        res.send({ result: 'error', message: message });
    }
});

module.exports = router;