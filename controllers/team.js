var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var moment = require('moment');
var expressValidator = require('express-validator');

var config = require('../config/config');
var language = require('../config/languages/' + config.config.language);
var authentication = require('../helpers/authentication');
var pages = require('../helpers/pages');

var Team = require('../models/team');
var User = require('../models/user');

var emails = require('../config/emails');
var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
var transporter = nodemailer.createTransport(smtpTransport({
        host: config.email.host,
        port: config.email.port,
        auth: {
            user: config.email.auth.user,
            pass: config.email.auth.pass
        }
    })
);

router.use(bodyParser.urlencoded({ extended: true }));

router.use(methodOverride(function(req, res) {
    if (req.body && typeof req.body === 'object' && '_method' in req.body) {
        // look in urlencoded POST bodies and delete it
        var method = req.body._method
        delete req.body._method
        return method
    }
}));

// ALL TEAM MEMBERS
router.get('/', authentication.isLoggedIn, function(req, res) {
    Team.getAllTeamMembersForCompany(req.user.company_id, function(err, members) {
        if (err) {
            console.log(err);
        } else {
            var result = [];
            for (var i = 0; i < members.length; i++) {
                result[i] = {};
                result[i].id = members[i].id;

                if (members[i].archived == 0) {
                    if (members[i].provider == 'local') {
                        result[i].photo = '<a href="/team/get/' + members[i].id + '"><span class="thumb-sm avatar"><img src="' + members[i].photo + '"></span></a>';
                        result[i].name = '<a href="/team/get/' + members[i].id + '" class="text-info">' + members[i].name + '</a>';
                        result[i].email = '<a href="mailto:' + members[i].email + '">' + members[i].email + '</a>';
                        result[i].mobile = members[i].mobile || '';
                    } else if (members[i].provider == 'google') {
                        result[i].photo = '<a href="/team/get/' + members[i].id + '"><span class="thumb-sm avatar"><img src="' + members[i].google_photo + '"></span></a>';
                        result[i].name = '<a href="/team/get/' + members[i].id + '" class="text-info">' + members[i].google_display_name + '</a>';
                        result[i].email = '<a href="mailto:' + members[i].google_email + '">' + members[i].google_email + '</a>';
                        result[i].mobile = '';
                    }
                } else {
                    if (members[i].provider == 'local') {
                        result[i].photo = '<span class="thumb-sm avatar"><img src="' + members[i].photo + '"></span>';
                        result[i].name = '<i>' + members[i].name + '</i>';
                        result[i].email = '<a href="mailto:' + members[i].email + '">' + members[i].email + '</a>';
                        result[i].mobile = members[i].mobile || '';
                    } else if (members[i].provider == 'google') {
                        result[i].photo = '<span class="thumb-sm avatar"><img src="' + members[i].google_photo + '"></span>';
                        result[i].name = '<i>' + members[i].google_display_name + '</i>';
                        result[i].email = '<a href="mailto:' + members[i].google_email + '">' + members[i].google_email + '</a>';
                        result[i].mobile = '';
                    }
                }    

                result[i].permission = '<span class="label ' + members[i].permission_class + '">' + members[i].permission + '</span>';                

                if (members[i].archived == 0) {
                    if (members[i].active == 0) {
                        //not active, invited
                        if (members[i].invited == 1) {
                            result[i].status = '<span class="label bg-info">Invited</span>';
                            //not active, not invited
                        } else {
                            result[i].status = '<span class="label bg-danger">Not Active</span>';
                        }
                    } else {
                        result[i].status = '<span class="label bg-success">Active</span>';
                    }
                // archived
                } else {
                    result[i].status = '<span class="label bg-warning">Archived</span>';
                }

                if ((req.user.permission_id == 1)) {
                    if (members[i].id == req.user.id) {
                        result[i].delete = '';
                        result[i].activate = '';
                    } else {
                        if (members[i].archived == 1) {
                            result[i].activate = '<a href="#" data-toggle="modal" data-target="#modal-activate" onclick="activateMemberButton(' + members[i].id + ');"><i class="icon icon-rocket"></i></a>';
                        } else {
                            result[i].activate = '';
                        }
                        result[i].delete = '<a href="#" data-toggle="modal" data-target="#modal-delete" onclick="setDeleteMemberButton(' + members[i].id + ');"><i class="glyphicon glyphicon-trash"></i></a>';
                    }    
                } else {
                    result[i].delete = '';
                }                
            }

            var data = '';
            if (members.length > 0) {
                data = JSON.stringify(result);
            }

            pages.renderLoggedInPage(req, res, 'team/team', 'Company Members', req.flash('message'), req.flash('color'), { 
                'data': data, 
                'header': req.user.company_name,
                'permission': req.user.permission_id
            });
        }
    });
});

// get team member
router.get('/get/:id', authentication.isLoggedIn, function(req, res) {
    var id = req.params.id;
    Team.getTeamMember(id, function(err, member) {
        if(err){
            console.log(err);
        } else {
            var photo = '';
            if (member.user.photo == '' || member.user.photo == undefined || member.user.photo == 'null') {
                if (member.user.google_photo == '' || member.user.google_photo == undefined || member.user.google_photo == 'null') {
                    photo = '/assets/images/avatar.png';
                } else {
                    photo = member.user.google_photo;
                }
            } else {
                photo = member.user.photo;
            }

            var name = '';

            if (member.user.name == '' || member.user.name == undefined || member.user.name == 'null') {
                if (member.user.google_display_name == '' || member.user.google_display_name == undefined || member.user.google_display_name == 'null') {
                    name = '';
                } else {
                    name = member.user.google_display_name;
                }
            } else {
                name = member.user.name;
            }

            var breadcrumbs = '<a href="/team" class="text-info">' + req.user.company_name + '</a> <i class="fa fa-angle-right m-l-sm m-r-sm light"></i> ' + name;

            for (var i = 0; i < member.flightplans.length; i++) {
                member.flightplans[i].role = '<span class="label '+member.flightplans[i].role_class+'">'+member.flightplans[i].role+'</span>';
                member.flightplans[i].name = '<a href="/flightplans/view/' + member.flightplans[i].flightplan_id + '" class="text-info">' + member.flightplans[i].name + '</a>';
                member.flightplans[i].start_time = moment(member.flightplans[i].start_time).format(config.config.timeFormat);
                member.flightplans[i].end_time = moment(member.flightplans[i].end_time).format(config.config.timeFormat);
            }

            
            pages.renderLoggedInPage(req, res, 'team/view', 'Team | View', '', '', {
                header: breadcrumbs,
                photo: photo,
                name: name, 
                mobile: member.user.mobile,
                permission: member.user.permission,
                permission_class: member.user.permission_class,
                flightplans: JSON.stringify(member.flightplans)
            });
        }
    })
});

// add team member
router.get('/add', authentication.isLoggedIn, function (req, res) {
    User.checkPlanUsers(req.user.company_id, req.user.plan, function (err, result) {
        if (err) {
            console.log(err);
        } else {
            if (result == 'yes') {
                var breadcrumbs = '<a href="/team" class="text-info">' + req.user.company_name + '</a> <i class="fa fa-angle-right m-l-sm m-r-sm light"></i> Invite New Member';

                pages.renderLoggedInPage(req, res, 'team/modify', 'Team | Add New', '', '', {
                    header: breadcrumbs
                });
            } else if (result == 'no') {
                res.sendStatus(403);
            }
        }
    });
});

router.post('/add', authentication.isLoggedIn, function(req, res) {
    User.isEmailAvailable(req.body.email, function(err, result) {
        if (err) {
            console.log(err);
            pages.renderLoggedInPage(req, res, 'team/modify', 'Team | Add New', language.dbError, 'danger', req.body);
        } else {
            if (result == 0) {
                req.checkBody('name', language.nameRequired).notEmpty();
                req.checkBody('email', language.emailRequired).notEmpty();
                req.checkBody('email', language.validEmail).isEmail();

                var validationErrors = req.validationErrors();

                if (validationErrors) {
                    console.log(validationErrors);
                    var messages = '';
                    for (var i = validationErrors.length - 1; i >= 0; i--) {
                        messages += validationErrors[i].msg + '<br/>';
                    }
                    pages.renderLoggedInPage(req, res, 'team/modify', 'Team | Add New', messages, 'danger', req.body);
                } else {
                    var data = {
                        company_id: req.user.company_id,
                        provider: 'local',
                        name: req.body.name,
                        email: req.body.email,
                        photo: '/assets/images/avatar.png',
                        active: 0,
                        invited: 1,
                        help: 0,
                        sample: 0,
                        permission_id: req.body.permission_id,
                        invited_by: req.user.id
                    }


                    User.inviteUser(data, function(err, invitationKey) {
                        if (err) {
                            console.log(err);
                        } else {
                            // send invitation email
                            var url = req.protocol + '://' + req.get('host') + '/auth/invitation/' + invitationKey;

                            var mailOptions = {
                                from: config.email.fromName + '<' + config.email.fromEmail + '>',
                                to: req.body.name + '<' + req.body.email + '>',
                                subject: emails.invitation(req.user.name, url, req.body.name).subject,
                                text: emails.invitation(req.user.name, url, req.body.name).text,
                                html: emails.invitation(req.user.name, url, req.body.name).html
                            };

                            transporter.sendMail(mailOptions, function(error, info) {
                                if (error) {
                                    console.log(error);
                                    pages.renderLoggedInPage(req, res, 'team/modify', 'Team | Add New', language.emailError, 'danger', req.body);
                                } else {
                                    req.flash('message', language.userInvited);
                                    req.flash('color', 'success');
                                    res.redirect('/team');
                                }
                            });
                        }
                    });                    
                }                    

            } else if (result == 1) {
                pages.renderLoggedInPage(req, res, 'team/modify', 'Team | Add New', language.emailNotAvailable, 'danger', req.body);
            }
        }
    });
})

// delete
router.get('/delete/:id', authentication.isLoggedIn, function(req, res) {
    var id = req.params.id;

    Team.delete(id, function(err, affectedRows) {
        if (err) {
            req.flash('message', language.dbError);
            req.flash('color', 'danger');
            res.redirect('/team');
        } else {
            if (affectedRows === 1) {
                req.flash('message', language.userDeleted);
                req.flash('color', 'success');
                res.redirect('/team');
            }
        }
    });
});


module.exports = router;