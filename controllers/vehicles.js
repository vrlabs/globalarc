var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var config = require('../config/config');
var language = require('../config/languages/' + config.config.language);
var authentication = require('../helpers/authentication');
var pages = require('../helpers/pages');
var Vehicle = require('../models/vehicle');
var moment = require('moment');

router.use(bodyParser.urlencoded({ extended: true }));

router.use(methodOverride(function (req, res) {
    if (req.body && typeof req.body === 'object' && '_method' in req.body) {
        // look in urlencoded POST bodies and delete it
        var method = req.body._method
        delete req.body._method
        return method
    }
}));


// ALL VEHICLES
router.get('/', authentication.isLoggedIn, function (req, res) {
    Vehicle.getAllVehiclesForCompany(req.user.company_id, function (err, vehicles) {
        if (err) {
            console.log(err);
        } else {      
            for (var i = 0; i < vehicles.length; i++){
                vehicles[i].status = '<span class="label ' + vehicles[i].status_class + '">' + vehicles[i].status_name + '</span>';
                if (vehicles[i].notes != undefined && vehicles[i].notes != 'null' && vehicles[i].notes != '') {
                    vehicles[i].notes = '<a href="#" data-toggle="modal" data-target="#modal-notes" onclick="setNotes(\'' + vehicles[i].notes + '\');"><i class="fa fa-file-text-o"></i></a>';
                }
                vehicles[i].edit = '<a href="/vehicles/edit/' + vehicles[i].id + '"><i class="glyphicon glyphicon-edit"></i></a>';
                vehicles[i].delete = '<a href="#" data-toggle="modal" data-target="#modal-delete" onclick="setDeleteVehicleButton(' + vehicles[i].id + ');"><i class="glyphicon glyphicon-trash"></i></a>';
            }
            
            var data = '[]';
            if (vehicles.length > 0) {
                data = JSON.stringify(vehicles);
            }
                                    
            pages.renderLoggedInPage(req, res, 'vehicles/vehicles', 'Vehicles', req.flash('message'), req.flash('color'), {'data': data, 'header' : 'Vehicles'});   
        }    
    });  
});

// SINGLE VEHICLE
// add
router.get('/add', authentication.isLoggedIn, function(req, res) {
    var breadcrumbs = '<a href="/vehicles" class="text-info">Vehicles</a> <i class="fa fa-angle-right m-l-sm m-r-sm light"></i> Create New Vehicle';
    pages.renderLoggedInPage(req, res, 'vehicles/modify', 'Vehicles | Add New', '', '', {
        header: breadcrumbs
    });
});
router.post('/add', authentication.isLoggedIn, function (req, res) {
    req.checkBody('name', language.vehicleNameRequired).notEmpty();
    req.checkBody('type', language.vehicleTypeRequired).notEmpty();
            
    req.asyncValidationErrors().then(function () {             
        var data = {
            company_id: req.user.company_id,
            name: req.body.name,
            type: req.body.type,
            manufacturer: req.body.manufacturer || '',
            model: req.body.model || '',
            serial_number: req.body.serial_number || '',
            faa_aircraft_id: req.body.faa_aircraft_id || '',
            status: req.body.status,
            notes: req.body.notes || '',
        }

        Vehicle.add(data, function (err, id) {
            if (err) {
                console.log(err);
                pages.renderLoggedInPage(req, res, 'vehicles/modify', 'Vehicles | Add New', language.dbError, 'danger', {
                    header: 'Add New Vehicle'
                });
            } else {
                req.flash('message', language.vehicleAdded);
	            req.flash('color', 'success');
                res.redirect('/vehicles');
            }
        });
        
    }).catch(function (errors) {
        if (errors) {
            console.log(errors);
            var messages = '';
            for (var i = errors.length - 1; i >= 0; i--) {
                messages += errors[i].msg + '<br/>';
            }
            pages.renderLoggedInPage(req, res, 'vehicles/modify', 'Vehicles | Add New', messages, 'danger', {
                header: 'Add New Vehicle'
            });
        };
    });
});


// edit
router.get('/edit/:id', authentication.isLoggedIn, function (req, res) {
    Vehicle.getVehicleByID(req.params.id, function (err, vehicle) {
        if (err) {
            console.log(err);
        } else {
            var breadcrumbs = '<a href="/vehicles" class="text-info">Vehicles</a> <i class="fa fa-angle-right m-l-sm m-r-sm light"></i> ' + vehicle.name + ' <i class="fa fa-angle-right m-l-sm m-r-sm light"></i> Edit';
                
            pages.renderLoggedInPage(req, res, 'vehicles/modify', 'Vehicles | Edit', '', '', {
                name: vehicle.name,
                type: vehicle.type,
                manufacturer: vehicle.manufacturer,
                model: vehicle.model,
                serial_number: vehicle.serial_number,
                faa_aircraft_id: vehicle.faa_aircraft_id,
                header: breadcrumbs,
                status: vehicle.status,
                notes: vehicle.notes
            });         
        }    
    });
    
});
router.post('/edit/:id', authentication.isLoggedIn, function (req, res) {
    var id = req.params.id;
    req.checkBody('name', language.vehicleNameRequired).notEmpty();
    req.checkBody('type', language.vehicleTypeRequired).notEmpty();
            
    req.asyncValidationErrors().then(function () {             
        var data = {
            name: req.body.name,
            type: req.body.type,
            manufacturer: req.body.manufacturer || '',
            model: req.body.model || '',
            serial_number: req.body.serial_number || '',
            faa_aircraft_id: req.body.faa_aircraft_id || '',
            status: req.body.status,
            notes: req.body.notes || ''
        };
        
        Vehicle.edit(id, data, function (err, affectedRows) {
            if (err) {
                console.log(err);
                pages.renderLoggedInPage(req, res, 'vehicles/modify', 'Vehicles | Edit', language.dbError, 'danger', {
                    header: 'Edit Vehicle'
                });
            } else {
                if (affectedRows === 1) {
                    req.flash('message', language.vehicleModified);
                    req.flash('color', 'success');
                    res.redirect('/vehicles');
                }
            }
        });
        
    }).catch(function (errors) {
        if (errors) {
            console.log(errors);
            var messages = '';
            for (var i = errors.length - 1; i >= 0; i--) {
                messages += errors[i].msg + '<br/>';
            }
            pages.renderLoggedInPage(req, res, 'vehicles/modify', 'Vehicles | Edit', messages, 'danger', {
                header: 'Edit Vehicle'
            });
        };
    });
});

// delete
router.get('/delete/:id', authentication.isLoggedIn, function (req, res) {
    var id = req.params.id;
    
    Vehicle.delete(id, function (err, affectedRows) {
        if (err) {
            console.log(err);
            req.flash('message', language.dbError);
            req.flash('color', 'danger');
            res.redirect('/vehicles');
        } else {
            if (affectedRows === 1) {
                req.flash('message', language.vehicleDeleted);
                req.flash('color', 'success');
                res.redirect('/vehicles');
            }
        }
    });
});

// ajax
router.post('/add-new-vehicle', authentication.isLoggedIn, function(req, res) {
    res.contentType('json');
    req.checkBody('name', language.vehicleNameRequired).notEmpty();
    req.checkBody('type', language.vehicleTypeRequired).notEmpty();
            
    req.asyncValidationErrors().then(function () {             
        var data = {
            company_id: req.user.company_id,
            name: req.body.name,
            type: req.body.type,
            manufacturer: req.body.manufacturer,
            model: req.body.model,
            serial_number: req.body.serial_number,
            faa_aircraft_id: req.body.faa_aircraft_id,
            status: req.body.status,
            notes: req.body.notes
        }

        Vehicle.add(data, function (err, id) {
            if (err) {
                console.log(err);
                res.send(JSON.stringify({result:'error'}));
            } else {
                res.send(JSON.stringify({result:'ok', id: id}));
            }
        });
        
    }).catch(function (errors) {
        if (errors) {
            console.log(errors);
            var messages = '';
            for (var i = errors.length - 1; i >= 0; i--) {
                messages += errors[i].msg + '<br/>';
            }
            res.send(JSON.stringify({result:'error', messages: messages}));
        };
    });
});


module.exports = router;