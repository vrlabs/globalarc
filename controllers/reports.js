var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var config = require('../config/config');
var language = require('../config/languages/' + config.config.language);
var authentication = require('../helpers/authentication');
var pages = require('../helpers/pages');
var moment = require('moment');
var general = require('../helpers/general');
var fs = require('fs');
var pdf = require('html-pdf');
var Report = require('../models/report');
var Flightplan = require('../models/flightplan');
var pdfTemplates = require('../config/pdf_templates');
var aws = require('aws-sdk');
var configAWS = require('../config/config').aws;

router.use(bodyParser.urlencoded({ extended: true }));

router.use(methodOverride(function (req, res) {
    if (req.body && typeof req.body === 'object' && '_method' in req.body) {
        // look in urlencoded POST bodies and delete it
        var method = req.body._method
        delete req.body._method
        return method
    }
}));

router.get('/', authentication.isLoggedIn, function (req, res) {
    Report.getAllReportsForCompany(req.user.company_id, function (err, reports) {
        if (err) {
            console.log(err);
        } else {
            for (var i = 0; i < reports.length; i++) {
                if (reports[i].type == 'faa_flight_report') {
                    reports[i].type = 'FAA Flight Report';
                    reports[i].parent_name = 'Flight Plan: ' + reports[i].parent_name;
                }
                reports[i].date = moment(reports[i].date).format(config.config.usTimeFormat);
                reports[i].download = '<a href="#" onclick="downloadReport(' + reports[i].id + ', this);"><i class="glyphicon glyphicon-cloud-download"></i></a>';
                reports[i].delete = '<a href="#" data-toggle="modal" data-target="#modal-delete" onclick="setDeleteReportButton(' + reports[i].id + ');"><i class="glyphicon glyphicon-trash"></i></a>';
            }

            pages.renderLoggedInPage(req, res, 'reports/reports', 'Reports', req.flash('message'), req.flash('color'), {
                data: JSON.stringify(reports),
                header: 'Reports'
            });
        }
    });
});


router.post('/get-flightplans-for-company', authentication.isLoggedIn, function (req, res) {
    Flightplan.getAllFlightplansForCompany(req.user.company_id, function (err, flightplans) {
        if (err) {
            console.log(err);
            res.send({ result: 'error' });
        } else {
            res.send({ result: flightplans });
        }
    })
});

router.post('/check-flightplan-for-report', authentication.isLoggedIn, function (req, res) {
    Flightplan.checkFlightplanForReport(req.body.id, function (err, result) {
        if (err) {
            console.log(err);
            res.send({ result: 'error' });
        } else {
            res.send({ result: result });
        }
    })
});

router.post('/generate-pdf', authentication.isLoggedIn, function (req, res) {
    if (req.body.type == 'faa_flight_report') {
        Flightplan.getFlightplanInfoForReport(req.body.id, function (err, result) {
            if (err) {
                console.log(err);
                res.send({ result: 'error' });
            } else {
                var crypto = require('crypto');
                var seed = crypto.randomBytes(10);
                var randomString = crypto.createHash('sha1').update(seed).digest('hex');

                var html = pdfTemplates.faa_flight_report(result).html;
                var fileName = 'ffa_flight_report_' + randomString + '.pdf';
                var tmpFile = __dirname + '/../public/tmp/' + fileName;

                pdf.create(html, config.pdfOptions).toFile(tmpFile, function (err, pdfCreateResult) {
                    if (err) {
                        console.log(err);
                    } else {
                        var s3 = new aws.S3();
                        var fileBuffer = fs.readFileSync(tmpFile);
                        var key = req.user.company_id + '/reports/' + fileName;

                        s3.putObject({
                            ACL: 'public-read',
                            Bucket: configAWS.bucketName,
                            Key: key,
                            Body: fileBuffer,
                            ContentType: 'application/pdf'
                        }, function (err, response) {
                            if (err) {
                                console.log(err);
                                res.send({ result: 's3error' });
                            } else {
                                var data = {
                                    company_id: req.user.company_id,
                                    parent_id: req.body.id,
                                    parent_name: result.flightplan_name,
                                    date: moment().format(config.config.mysqlTimeFormat),
                                    type: 'faa_flight_report',
                                    path: key
                                }

                                Report.add(data, function (err, result) {
                                    if (err) {
                                        console.log(err);
                                        res.send({ result: 'error' });
                                    } else {
                                        console.log(result);

                                       if (result == 1) {
                                            fs.exists(tmpFile, function (exists) {
                                                if (exists) {
                                                    fs.unlink(tmpFile);
                                                }
                                            });
                                            res.send({ result: 'ok' });
                                        } else {
                                            res.send({ result: 'dberror' });
                                        }
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }
});

router.post('/download-report/:id', function (req, res) {
    res.contentType('json');

    var s3 = new aws.S3();

    Report.getById(req.params.id, function (err, result) {
        if (err) {
            console.log(err);
            res.send({ result: 'error' });
        } else {
            var path = result.path;
            var pathSplit = path.split(req.user.company_id + '/reports/')
            var fileLink = __dirname + '/../public/tmp/' + pathSplit[1];

            var s3Params = {
                Bucket: config.aws.bucketName,
                Key: path,
            };

            s3.getObject(s3Params, function (err, data) {
                if (err) {
                    console.log(err);
                    res.send({ result: 'error' });
                } else {
                    fs.writeFile(__dirname + '/../public/tmp/' + pathSplit[1], data.Body, { flag: 'w' }, function (err) {
                        if (err) {
                            console.log(err);
                            res.send({ result: 'error' });
                        } else {
                            res.send({ result: 'ok', file: '/assets/tmp/'+pathSplit[1]});
                        }
                    });
                }
            });
        }
    });
});

router.post('/delete', authentication.isLoggedIn, function (req, res) {
    var id = req.body.id;

    var s3 = new aws.S3();

    Report.getById(id, function (err, result) {
        if (err) {
            console.log(err);
            res.send({ result: 'error' });
        } else {
            var path = result.path;
            var pathSplit = path.split(req.user.company_id + '/reports/')
            var fileLink = __dirname + '/../public/tmp/' + pathSplit[1];

            var deleteParams = {
                Bucket: configAWS.bucketName,
                Delete: {
                    Objects: [
                        { Key: path }
                    ]
                }
            };
            
            
            s3.deleteObjects(deleteParams, function (err, data) {
                if (err) {
                    console.log(err);
                    res.send(JSON.stringify({ result: 'error' }));
                } else {
                    Report.delete(id, function (err, id) {
                        if (err) {
                            console.log(err);
                            res.send(JSON.stringify({ result: 'error' }));
                        } else {
                            res.send(JSON.stringify({ result: 'ok' }));
                        }
                    });
                }
            });
        }
    });
});


module.exports = router;