var LocalStrategy = require('passport-local').Strategy;
var RememberMeStrategy = require('passport-remember-me').Strategy;
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
var http = require('https');

var authentication = require('../helpers/authentication');
var general = require('../helpers/general');
var User = require('../models/user');
var config = require('../config/config');
var language = require('../config/languages/' + config.config.language);
var bcrypt = require('bcrypt-nodejs');
var Andruav = require('../helpers/andruav');

var emails = require('../config/emails');
var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
var transporter = nodemailer.createTransport(smtpTransport({
    host: config.email.host,
    port: config.email.port,
    auth: {
        user: config.email.auth.user,
        pass: config.email.auth.pass
    }
})
);

// REMEMBER ME TOKENS
var tokens = {};

function consumeRememberMeToken(token, fn) {
    var uid = tokens[token];
    // invalidate the single-use token
    delete tokens[token];
    return fn(null, uid);
}

function saveRememberMeToken(token, uid, fn) {
    tokens[token] = uid;
    return fn();
}

function issueToken(user, done) {
    var token = general.randomString(64);
    saveRememberMeToken(token, user.id, function (err) {
        if (err) { return done(err); }
        return done(null, token);
    });
}


// PASSPORT
module.exports = function (passport) {
    passport.serializeUser(function (id, callback) {
        callback(null, id);
    });

    passport.deserializeUser(function (id, callback) {
        if (typeof (id) == 'object') {
            User.getUserFromSession(id.id, function (err, user) {
                if (err) {
                    console.log(err);
                    callback(err, null);
                } else {
                    callback(null, user);
                }

            });
        } else {
            User.getUserFromSession(id, function (err, user) {
                if (err) {
                    console.log(err);
                    callback(err, null);
                } else {
                    callback(null, user);
                }
            });
        }

    });

    // LOGIN
    passport.use('local-login', new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true
    },
        function (req, email, password, callback) {
            process.nextTick(function () {
                // searching for 'local' user
                User.getUserByEmail(email, function (err, user) {
                    if (err) {
                        console.log(err);
                        return callback(err);
                    }
                    if (!user) {
                        // searching for 'provider' user
                        User.getUserByUnknownProviderEmail(email, function (err, providerUser) {
                            if (err) {
                                console.log(err);
                                return callback(err);
                            }
                            if (providerUser) {
                                return callback(null, false, req.flash('message', language.emailNotAvailableProviderLogin), req.flash('color', 'danger'));
                            } else {
                                return callback(null, false, req.flash('message', language.userNotFound));
                            }
                        });
                    } else {
                        if (user.deleted == 1) {
                            return callback(null, false, req.flash('message', language.userNotFound));
                        } else {
                            if (user.active == '0') {
                                if (user.invited == '1') {
                                    return callback(null, false, req.flash('message', language.userInvitedNotActive));
                                } else {
                                    return callback(null, false, req.flash('message', language.userNotActive));
                                }

                            } else {
                                if (user.archived == '1') {
                                    return callback(null, false, req.flash('message', language.archivedUser));
                                } else {
                                    
                                    if (!bcrypt.compareSync(password, user.password)) {
                                        return callback(null, false, req.flash('message', language.wrongPassword));
                                    } else {
                                        User.updateProvider(user.id, 'local', function (err, userID) {
                                            if (err) {
                                                console.log(err);
                                                return callback(err, null);
                                            } else {
                                                return callback(null, userID);
                                            }
                                        })
                                    }
                                }
                            }
                        }

                    }
                });
            });
        }));


    // REGISTER
    passport.use('local-register', new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true
    },
        function (req, email, password, callback) {

            process.nextTick(function () {
                //  user logged in
                if (req.user) {
                    var user = req.user;
                    user.email = email;
                    user.name = req.body.name;
                    user.mobile = req.body.mobile;
                    user.password = authentication.generateHash(password);

                    User.updateUser(user, function (err, affectedRows) {
                        if (err) {
                            console.log(err);
                            return callback(err);
                        }
                        if (affectedRows === 1) {
                            return callback(null, user.id);
                        }
                    });
                    // user not logged in 
                } else {
                    User.getUserByUnknownProviderEmail(email, function (err, user) {
                        if (err) {
                            console.log(err);
                            return callback(err, null);
                        }
                        if (user) {
                            return callback(null, false, req.flash('message', language.emailNotAvailableProviderRegister), req.flash('color', 'danger'));
                            // user is logged in, we're connecting a new local account
                        } else if (req.user) {
                            var user = req.user;
                            user.email = email;
                            user.password = authentication.generateHash(password);
                            User.updateUser(user, function (err, affectedRows) {
                                if (err) {
                                    console.log(err);
                                    return callback(err, null);
                                }
                                if (affectedRows === 1) {
                                    return callback(null, user.id);
                                }
                            });
                        } else {
                            // message = access code
                            Andruav.createNewAccount(email, function (result, message) {
                                if (result == 1) {
                                    User.createUser(message, email, authentication.generateHash(password), req.body.name, req.body.company, req.body.plan, req.body.customerID, req.body.subscriptionID, function (err, id, activationKey) {

                                        if (err) {
                                            console.log(err);
                                            return callback(err, null);
                                        }
                                        // send activation email
                                        var url = req.protocol + '://' + req.get('host') + '/auth/activation/' + activationKey;
                                        var mailOptions = {
                                            from: config.email.fromName + '<' + config.email.fromEmail + '>',
                                            to: req.body.name + '<' + email + '>',
                                            subject: emails.activation(url, req.body.name).subject,
                                            text: emails.activation(url, req.body.name).text,
                                            html: emails.activation(url, req.body.name).html
                                        };

                                        transporter.verify(function (error, success) {
                                            if (error) {
                                                console.log(error);
                                            } else {
                                                console.log('Server is ready to take our messages');
                                            }
                                        });

                                        transporter.sendMail(mailOptions, function (error, info) {
                                            if (error) {
                                                console.log(error);
                                                return callback(error, null, null, null);
                                            } else {
                                                return callback(null, id, req.flash('message', language.activationEmailSent), req.flash('color', 'info'));
                                            }
                                        });
                                    });
                                } else {
                                    var randomAccessCode = general.randomString(12);

                                    User.createUser(randomAccessCode, email, authentication.generateHash(password), req.body.name, req.body.company, req.body.plan, req.body.customerID, req.body.subscriptionID, function (err, id, activationKey) {

                                        if (err) {
                                            console.log(err);
                                            return callback(err, null);
                                        } else {
                                            return http.get({
                                                host: config.andruav.url,
                                                port: config.andruav.port,
                                                method: 'GET',
                                                path: config.andruav.retreiveAccessCode + email + '&nemail=true&cb=' + config.mainURL + '/api/save-new-access-code/$USR/$PWD',
                                                rejectUnauthorized: false
                                            }, function (result) {
                                                result.on('end', function () {
                                                    // send activation email
                                                    var url = req.protocol + '://' + req.get('host') + '/auth/activation/' + activationKey;
                                                    var mailOptions = {
                                                        from: config.email.fromName + '<' + config.email.fromEmail + '>',
                                                        to: req.body.name + '<' + email + '>',
                                                        subject: emails.activation(url, req.body.name).subject,
                                                        text: emails.activation(url, req.body.name).text,
                                                        html: emails.activation(url, req.body.name).html
                                                    };

                                                    transporter.sendMail(mailOptions, function (error, info) {
                                                        if (error) {
                                                            console.log(error);
                                                            return callback(error);
                                                        } else {
                                                            return callback(null, id, req.flash('message', language.activationEmailSent), req.flash('color', 'info'));
                                                        }
                                                    });
                                                });
                                                result.on('error', function (e) {
                                                    console.log(e);
                                                });
                                            }).on('error', function (err) {
                                                console.error(err.message);
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }));

    // GOOGLE  
    passport.use(new GoogleStrategy({
        clientID: config.auth.google.clientID,
        clientSecret: config.auth.google.clientSecret,
        callbackURL: config.auth.google.callbackURL,
        passReqToCallback: true
    },

        function (req, token, refreshToken, profile, callback) {
            process.nextTick(function () {
                // user not logged in            
                if (!req.user) {
                    User.getUserByEmail(profile.emails[0].value, function (err, user) {
                        if (err) {
                            console.log(err);
                            return callback(err, null);
                        }

                        // LOCAL 1
                        if (user) {
                            User.getUserByProviderID('google', profile.id, function (err, googleUser) {
                                if (err) {
                                    console.log(err);
                                    return callback(err, null);
                                }

                                // GOOGLE 1 -> login with google credentials
                                if (googleUser) {
                                    return callback(null, googleUser.id);
                                    // GOOGLE 0 -> update local account with google data
                                } else {
                                    User.updateUserProviderData(user.id, 'google', profile, token, function (err, id) {
                                        if (err) {
                                            console.log(err);
                                            return callback(err, null);
                                        }
                                        return callback(null, id);
                                    });
                                }
                            });
                            // LOCAL 0
                        } else {
                            User.getUserByProviderID('google', profile.id, function (err, googleUser) {
                                if (err) {
                                    console.log(err);
                                    return callback(err, null);
                                }

                                // GOOGLE 1 -> login with google credentials
                                if (googleUser) {
                                    return callback(null, googleUser.id);
                                    // GOOGLE 0 -> create account with google data
                                } else {
                                    return http.get({
                                        host: config.andruav.url,
                                        port: config.andruav.port,
                                        method: 'GET',
                                        path: config.andruav.createNewAccount + profile.emails[0].value + '&nemail=true',
                                        rejectUnauthorized: false
                                    }, function (res) {
                                        var str = '';
                                        res.on('data', function (chunk) {
                                            str += chunk;
                                        });

                                        res.on('end', function () {
                                            if (res.statusCode == 200 && res.statusMessage == 'OK') {
                                                var obj = JSON.parse(str);

                                                var plan = 'free';
                                                var subscriptionID = 0;
                                                var customerID = 0;

                                                if (req.session.plan != undefined && req.session.plan == 'professional') {
                                                    plan = 'professional';
                                                }
                                                if (req.session.subscriptionID != undefined) {
                                                    subscriptionID = req.session.subscriptionID;
                                                }

                                                if (req.session.customerID != undefined) {
                                                    customerID = req.session.customerID;
                                                }

                                                if (obj.Err == 0) {
                                                    var accessCode = obj.pwd;

                                                    User.createUserByProvider('google', accessCode, profile, token, plan, customerID, subscriptionID, function (err, id) {
                                                        if (err) {
                                                            return callback(err);
                                                        } else {
                                                            delete req.session.plan;
                                                            delete req.session.subscriptionID;
                                                            delete req.session.customerID;

                                                            return callback(null, id);
                                                        }
                                                    });

                                                    // duplicate andruav account
                                                } else if (obj.Err == 3) {
                                                    var randomAccessCode = general.randomString(12);

                                                    User.createUserByProvider('google', randomAccessCode, profile, token, plan, customerID, subscriptionID, function (err, id) {
                                                        if (err) {
                                                            return callback(err);
                                                        } else {
                                                            delete req.session.plan;
                                                            delete req.session.subscriptionID;
                                                            delete req.session.customerID;

                                                            return http.get({
                                                                host: config.andruav.url,
                                                                port: config.andruav.port,
                                                                method: 'GET',
                                                                path: config.andruav.retreiveAccessCode + profile.emails[0].value + '&nemail=true&cb=' + config.mainURL + '/api/save-new-access-code/$USR/$PWD',
                                                                rejectUnauthorized: false
                                                            }, function (result) {
                                                                result.on('end', function () {
                                                                    return callback(null, id);
                                                                });
                                                                result.on('error', function (e) {
                                                                    console.log(e);
                                                                });
                                                            }).on('error', function (err) {
                                                                console.error(err.message);
                                                            });
                                                        }
                                                    });
                                                }
                                            }
                                        });

                                        res.on('error', function (e) {
                                            console.log(e);
                                            return callback(null, false, req.flash('message', language.error), req.flash('color', 'danger'));
                                        });
                                    }).on('error', function (err) {
                                        console.error(err.message);
                                    });
                                }
                            });
                        }
                    });
                    // user logged in     
                } else {
                    var user = req.user;

                    user.google_id = profile.id;
                    user.google_token = token;
                    user.google_display_name = profile.displayName;
                    user.google_family_name = profile.familyName;
                    user.google_given_name = profile.givenName;
                    user.google_middle_name = profile.middleName;
                    user.google_email = profile.emails[0].value;
                    user.google_email_type = profile.emails[0].type;

                    var photoFull = profile.photos[0].value || '';
                    var photo = photoFull.substring(0, photoFull.indexOf('?')) || '';

                    user.google_photo = photo;

                    User.updateUser(user, function (err, affectedRows) {
                        if (err) {
                            console.log(err);
                            return callback(err, null);
                        }
                        if (affectedRows === 1) {
                            return callback(null, user.id);
                        }
                    });

                }
            });
        }));

    // REMEMBER ME
    passport.use(new RememberMeStrategy(
        function (token, done) {
            consumeRememberMeToken(token, function (err, uid) {
                if (err) {
                    console.log(err);
                    return done(err);
                }
                if (!uid) { return done(null, false); }

                User.getUserByID(uid, function (err, user) {
                    if (err) {
                        console.log(err);
                        return done(err);
                    }
                    if (!user) {
                        return done(null, false);
                    }

                    return done(null, user);
                });
            });
        }, issueToken
    ));
}