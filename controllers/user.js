var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var passport = require('passport');
var moment = require('moment');
var config = require('../config/config');
var language = require('../config/languages/' + config.config.language);
var authentication = require('../helpers/authentication');
var pages = require('../helpers/pages');
var general = require('../helpers/general');
var fs = require('fs');
var path = require('path');
var uuid = require('node-uuid');
var expressValidator = require('express-validator');
var aws = require('aws-sdk');
var emails = require('../config/emails');
var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
var transporter = nodemailer.createTransport(smtpTransport({
    host: config.email.host,
    port: config.email.port,
    auth: {
        user: config.email.auth.user,
        pass: config.email.auth.pass
    }
})
);

router.use(bodyParser.urlencoded({ extended: true }));
router.use(expressValidator());

var User = require('../models/user');

var bucket = config.aws.bucketName;
var aws = require('aws-sdk');
aws.config.update({
    accessKeyId: config.aws.key,
    secretAccessKey: config.aws.secret
});

var s3 = new aws.S3();

router.use(bodyParser.urlencoded({ extended: true }));
router.use(methodOverride(function (req, res) {
    if (req.body && typeof req.body === 'object' && '_method' in req.body) {
        var method = req.body._method
        delete req.body._method
        return method
    }
}));

// PROFILE PAGE
router.get('/profile', authentication.isLoggedIn, function (req, res) {
    var user = req.user;

    var showDeleteAvatarButton = 1;

    if (!user.photo || user.photo == 'null' || user.photo == undefined) {
        user.photo = '/assets/images/avatar.png';
    }

    if (!user.google_photo || user.google_photo == 'null' || user.google_photo == undefined) {
        user.google_photo = '/assets/images/avatar.png';
    }

    if (user.photo == '/assets/images/avatar.png') {
        showDeleteAvatarButton = 0;
    }

    var params = {
        name: user.name,
        email: user.email,
        mobile: user.mobile,
        photo: user.photo,
        google_token: user.google_token,
        google_display_name: user.google_display_name,
        google_email: user.google_email,
        google_photo: user.google_photo,
        S3RequestURL: '/documents/sign/avatar',
        S3Bucket: 'http://' + config.aws.bucketName + '.s3.amazonaws.com/',
    };

    pages.renderLoggedInPage(req, res, 'user/profile', 'Profile', req.flash('message'), req.flash('color'), {
        args: params,
        showDeleteAvatarButton: showDeleteAvatarButton
    });
});

// connecting/editing local
router.post('/profile-professional', authentication.isLoggedIn, function (req, res) {
    var user = req.user;
    user.mobile = req.body.mobile;


    User.updateUser(user, function (err, affectedRows) {
        res.contentType('json');
        if (err) {
            req.flash('message', language.userProfileErrorModified);
            req.flash('color', 'danger');
            res.send(JSON.stringify({ result: 'error' }));

        }
        if (affectedRows === 1) {
            req.flash('message', language.userProfileSuccessfullyModified);
            req.flash('color', 'success');
            res.send(JSON.stringify({ result: 'ok' }));

        }
    });
});

router.post('/profile-local', authentication.isLoggedIn, function (req, res) {


    if (req.body.email != '') {
        var user = req.user;

        user.name = req.body.name;
        user.email = req.body.email;

        if (req.body.password != '') {
            user.password = authentication.generateHash(req.body.password);
        }

        User.updateUser(user, function (err, affectedRows) {
            res.contentType('json');

            if (err) {
                req.flash('message', language.userProfileErrorModified);
                req.flash('color', 'danger');
                res.send(JSON.stringify({ result: 'error' }));
            }
            if (affectedRows === 1) {
                req.flash('message', language.userProfileSuccessfullyModified);
                req.flash('color', 'success');
                res.send(JSON.stringify({ result: 'ok' }));
            }
        });
    } else {
        passport.authenticate('local-register', {
            successRedirect: '/user/profile',
            failureRedirect: '/user/profile',
            failureFlash: true
        })(req, res);
    }
});

// avatar
router.post('/avatar/add', authentication.isLoggedIn, function (req, res) {

    User.addUserAvatar(req.user.company_id, req.user.id, req.body.image, function (err, image) {
        res.contentType('json');

        if (err) {
            res.send(JSON.stringify({ result: 'error' }));
        } else {
            res.send(JSON.stringify({ result: 'ok', image: image }));
        }
    });
});


router.post('/avatar/delete', authentication.isLoggedIn, function (req, res) {
    User.deleteUserAvatar(req.user.company_id, req.user.id, function (err, result) {
        if (err) {
            res.contentType('json');
            res.send(JSON.stringify({ result: 'error' }));
        } else {
            res.send(JSON.stringify({ result: 'ok' }));
        }
    });
});

// connecting google
router.get('/connect/google', passport.authorize('google', { scope: ['profile', 'email'] }));
router.get('/connect/google/callback',
    passport.authorize('google', {
        successRedirect: '/user/profile',
        failureRedirect: '/user/profile'
    })
);

// unlinking local
router.get('/unlink/local', function (req, res) {
    var user = req.user;

    user.provider = 'google';
    user.name = '';
    user.email = '';
    user.password = '';

    User.updateUser(user, function (err, affectedRows) {
        if (err) {
            console.log(err);
        }
        if (affectedRows === 1) {
            res.redirect('/user/profile');
        }
    });
});


// unlinking google 
router.get('/unlink/google', function (req, res) {
    var user = req.user;

    user.google_token = '';
    user.provider = 'local';

    User.updateUser(user, function (err, affectedRows) {
        if (err) {
            console.log(err);
        }
        if (affectedRows === 1) {
            res.redirect('/user/profile');
        }
    });
});

// SETTINGS PAGE
router.get('/settings', authentication.isLoggedIn, function (req, res) {
    var user = req.user;
    var companyName = '';
    var accessCode = '';

    if (user.company_set === 1) {
        companyName = user.company_name;
    } else {
        companyName = '';
    }

    accessCode = user.access_code;

    pages.renderLoggedInPage(req, res, 'user/settings', 'Settings', '', '', {
        companyName: companyName,
        accessCode: accessCode,
        companyPlan: req.user.plan,
        plans: config.plans,
        stripe: config.stripe
    });
});

router.post('/settings', authentication.isLoggedIn, function (req, res) {
    var message = '';
    var color = '';

    var user = req.user;
    var companyName = '';
    var accessCode = '';

    if (user.company_set === 1) {
        companyName = user.company_name;
    } else {
        companyName = '';
    }

    var companyCreated = user.company_created;
    var planExpirationDate = moment(companyCreated).add(1, 'months').format(config.config.usTimeFormat);

    accessCode = user.access_code;

    User.changeCompanyInfo(user.company_id, req.body.name, req.body.access_code_hidden, function (err, msg) {
        if (err) {
            console.log(err);
            message = language.dbError;
            color = 'danger';
            pages.renderLoggedInPage(req, res, 'user/settings', 'Settings', message, color, {
                companyName: companyName,
                accessCode: accessCode,
                companyPlan: req.user.plan,
                planExpirationDate: planExpirationDate,
                plans: config.plans,
                stripe: config.stripe
            });
        } else {
            if (msg === 'ok') {
                message = language.companyInfoSet;
                color = 'success';
                pages.renderLoggedInPage(req, res, 'user/settings', 'Settings', message, color, {
                    companyName: companyName,
                    accessCode: accessCode,
                    companyPlan: req.user.plan,
                    planExpirationDate: planExpirationDate,
                    plans: config.plans,
                    stripe: config.stripe
                });
            }
        }
    });
});

router.post('/hide-welcome', authentication.isLoggedIn, function (req, res) {
    res.contentType('json');

    User.hideWelcome(req.user.id, function (err, result) {
        if (err) {
            console.log(err);
            res.send({ result: 'error' });
        } else {
            res.send({ result: 'ok' });
        }
    });
});

router.post('/check-plan-users', authentication.isLoggedIn, function (req, res) {
    res.contentType('json');

    User.checkPlanUsers(req.user.company_id, req.user.plan, function (err, result) {
        if (err) {
            console.log(err);
            res.send({ result: 'error' });
        } else {
            res.send({ result: result });
        }
    });
});

router.post('/check-plan-projects', authentication.isLoggedIn, function (req, res) {
    res.contentType('json');

    User.checkPlanProjects(req.user.company_id, req.user.plan, function (err, result) {
        if (err) {
            console.log(err);
            res.send({ result: 'error' });
        } else {
            res.send({ result: result });
        }
    });
});

router.post('/check-plan-flightplans', authentication.isLoggedIn, function (req, res) {
    res.contentType('json');

    User.checkPlanFlightplans(req.user.id, req.user.company_id, req.user.plan, function (err, result) {
        if (err) {
            console.log(err);
            res.send({ result: 'error' });
        } else {
            res.send({ result: result });
        }
    });
});

router.post('/check-plan-storage-space', authentication.isLoggedIn, function (req, res) {
    res.contentType('json');

    User.checkPlanStorageSpace(req.user.company_id, req.user.plan, function (err, result) {
        if (err) {
            console.log(err);
            res.send({ result: 'error' });
        } else {
            res.send({ result: result });
        }
    });
});


router.post('/call', authentication.isLoggedIn, function (req, res) {

    res.contentType('json');

    req.checkBody('email', 'Valid email address is required').notEmpty().isEmail();

    var validationErrors = req.validationErrors();
    var message = '';
    var color = '';

    if (!validationErrors) {
        var customer = {
            name: req.body.name,
            email: req.body.email,
            phone: req.body.phone
        };

        // send request call email
        var mailOptions = {
            from: config.email.fromName + '<' + config.email.fromEmail + '>',
            to: config.email.toName + '<' + config.email.toEmail + '>',
            //to: 'Jelena' + '<jelena@vrhsolutions.com>',
            subject: emails.requestCall().subject,
            text: emails.requestCall(customer.name, customer.email, customer.phone).text,
            html: emails.requestCall(customer.name, customer.email, customer.phone).html
        };

        transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                console.log(error);
                res.send({ result: 'error', message: 'Something went wrong while sending your request. Please, try again' });
            } else {
                res.send({ result: 'ok' });
            }
        });

        // validation errors    
    } else {
        message = validationErrors[0].msg;
        res.send({ result: 'error', message: message });
    }

});


router.post('/charge', authentication.isLoggedIn, function (req, res) {
    res.contentType('json');

    var stripe = require('stripe')(config.stripe.secretKey);

    stripe.customers.create({
        source: req.body.id,
        plan: config.plans.professional.id,
        email: req.body.email
    }, function (err, customer) {
        if (err) {
            console.log(err);
            res.send({ result: 'error', message: err });
        } else {
            var data = {};
            data.customer_id = customer.id;
            data.customer_email = customer.email;
            data.subscription_id = customer.subscriptions.data[0].id;
            data.subscription_start = moment.unix(customer.subscriptions.data[0].start).format(config.config.mysqlTimeFormat);
            data.subscription_end = moment.unix(customer.subscriptions.data[0].current_period_end).format(config.config.mysqlTimeFormat);
            data.subscription_plan = 'professional';

            User.subscribe(data, req.user.company_id, function (err, result) {
                if (err) {
                    console.log(err);
                    res.send({ result: 'error', message: err });
                } else {
                    res.send({ result: 'ok' });
                }
            })
        }
    });
});

router.post('/create-professional-account', function (req, res) {
    res.contentType('json');

    var stripe = require('stripe')(config.stripe.secretKey);

    stripe.customers.create({
        source: req.body.id,
        plan: config.plans.professional.id,
        email: req.body.email,
        coupon: config.plans.discount.id
    }, function (err, customer) {
        if (err) {
            console.log(err);
            res.send({ result: 'error', message: err });
        } else {
            
            var data = {};
            var customerID = customer.id;
            var subscriptionID = customer.subscriptions.data[0].id;
            data.customer_id = customerID;
            data.customer_email = customer.email;
            data.subscription_id = subscriptionID;
            data.subscription_start = moment.unix(customer.subscriptions.data[0].start).format(config.config.mysqlTimeFormat);
            data.subscription_end = moment.unix(customer.subscriptions.data[0].current_period_end).format(config.config.mysqlTimeFormat);
            data.subscription_plan = 'professional';

            var customerSplit = customerID.split('cus_');
            var subscriptionSplit = subscriptionID.split('sub_');

            User.subscribeNewCustomer(data, function (err, id) {
                if (err) {
                    console.log(err);
                    res.send({ result: 'error', message: err });
                } else {
                    res.send({ result: 'ok', customerID: customerSplit[1], subscriptionID: subscriptionSplit[1] });
                }
            })
        }
    });
});

router.post('/get-downgrade-data', function (req, res) {
    res.contentType('json');
    User.getDowngradeData(req.user.id, req.user.company_id, function (err, result) {
        if (err) {
            res.send({ result: 'error' });
        } else {
            res.send({ result: 'ok', content: result });
        }
    });
})

router.post('/downgrade', function (req, res) {
    res.contentType('json');

    User.downgrade(req.user.id, req.user.company_id, req.body, function (err, result) {
        if (err) {
            console.log(err);
            res.send({ result: 'error' });
        } else {
            res.send({ result: result });
        }
    });

});


router.post('/delete-account', function (req, res) {
    res.contentType('json');

    User.deleteAccount(req.user.plan, req.user.company_id, function (err, result) {
        if (err) {
            console.log(err);
            res.send({ result: 'error' });
        } else {
            res.send({ result: result});
        }
    });
});

router.post('/archived-users', function (req, res) {
    res.contentType('json');

    User.getArchivedUsers(req.user.company_id, function (err, users) {
        if (err) {
            console.log(err);
            res.send({ result: 'error' });
        } else {
            res.send({ result: users });
        }
    });
});

router.post('/activate-users', function (req, res) {
    res.contentType('json');
    User.activateUsers(req.body, function (err, users) {
        if (err) {
            console.log(err);
            res.send({ result: 'error' });
        } else {
            res.send({ result: 'ok' });
        }
    });
});

router.post('/activate-user', function (req, res) {
    res.contentType('json');
    User.activateUser(req.body.id, function (err, users) {
        if (err) {
            console.log(err);
            res.send({ result: 'error' });
        } else {
            res.send({ result: 'ok' });
        }
    });
});

router.post('/check-active-users', function (req, res) {
    res.contentType('json');

    User.checkActiveUsers(req.user.company_id, req.user.plan, function (err, result) {
        if (err) {
            console.log(err);
            res.send({ result: 'error' });
        } else {
            res.send({ result: result });
        }
    });
});



module.exports = router;