var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var aws = require('aws-sdk');
var moment = require('moment');
var policy = require('s3-policy');
var uuid = require('node-uuid');
var fs = require('fs');
var mkdirp = require('mkdirp');
var zipFolder = require('zip-folder');
const download = require('download');
var slug = require('slug');
slug.defaults.mode = 'rfc3986';

var config = require('../config/config');
var language = require('../config/languages/' + config.config.language);
var authentication = require('../helpers/authentication');
var pages = require('../helpers/pages');
var general = require('../helpers/general');
var bucket = config.aws.bucketName;

var Document = require('../models/document');
var User = require('../models/user');

aws.config.update({
    accessKeyId: config.aws.key,
    secretAccessKey: config.aws.secret
});


router.use(bodyParser.urlencoded({ extended: true }));

router.use(methodOverride(function (req, res) {
    if (req.body && typeof req.body === 'object' && '_method' in req.body) {
        // look in urlencoded POST bodies and delete it
        var method = req.body._method
        delete req.body._method
        return method
    }
}));

// ALL DOCUMENTS
router.get('/', authentication.isLoggedIn, function (req, res) {
    Document.getAllDocumentsForCompany(req.user.company_id, function (err, documents) {
        if (err) {
            console.log(err);
        } else {
            for (var i = 0; i < documents.length; i++) {
                documents[i].size = general.humanFileSize(documents[i].size);
                documents[i].modified = moment(documents[i].modified).format(config.config.timeFormat);
                documents[i].preview = '<a href="' + documents[i].file_path + '" target="_blank"><i class="icon-magnifier"></i></a>';
                
                documents[i].delete = '<a href="#" data-toggle="modal" data-target="#modal-delete" onclick="setDeleteDocumentButton(' + documents[i].id + ', \'' + documents[i].file_name + '\');"><i class="glyphicon glyphicon-trash"></i></a>';
            }

            var data = '[]';
            if (documents.length > 0) {
                data = JSON.stringify(documents);
            }

            pages.renderLoggedInPage(req, res, 'documents/documents', 'Documents', req.flash('message'), req.flash('color'), { 'data': data, 'header': 'Documents' });
        }
    });
});

// UPLOAD DOCUMENTS
router.get('/add', authentication.isLoggedIn, function (req, res) {
    User.checkPlanStorageSpace(req.user.company_id, req.user.plan, function (err, result) {
        if (err) {
            console.log(err);
        } else {
            if (result == 'yes') {
                Document.getDocumentTypes(function (err, documentTypes) {
                    if (err) {
                        console.log(err);
                    } else {
                        var breadcrumbs = '<a href="/documents" class="text-info">Documents</a> <i class="fa fa-angle-right m-l-sm m-r-sm light"></i> Upload New Document';
                        pages.renderLoggedInPage(req, res, 'documents/upload', 'Documents | Upload New Document', '', '', {
                            header: breadcrumbs,
                            S3RequestURL: '/documents/sign/documents',
                            S3Bucket: 'http://' + config.aws.bucketName + '.s3.amazonaws.com/',
                            documentTypes: documentTypes
                        });
                    }
                });
            } else if (result == 'no') {
                res.sendStatus(403);
            }
        }
    });
});

// AJAX CALLS
router.post('/sign/:type', authentication.isLoggedIn, function (req, res) {
    var filename = uuid.v4() + general.getExtension(req.body.name);
    var acl = 'public-read';

    var destination = '';
    if (req.params.type == 'documents') {
        destination = req.user.company_id + '/documents/' + filename;
    } else if (req.params.type == 'avatar') {
        destination = req.user.company_id + '/avatars/' + filename;
    }

    var p = policy({
        acl: 'public-read',
        secret: config.aws.secret,
        length: 5000000,
        bucket: config.aws.bucketName,
        key: destination,
        expires: new Date(Date.now() + 60000)
    });

    var result = {
        'AWSAccessKeyId': config.aws.key,
        'key': destination,
        'policy': p.policy,
        'signature': p.signature
    };

    res.write(JSON.stringify(result));
    res.end();
});

router.post('/add', authentication.isLoggedIn, function (req, res) {
    res.contentType('json');

    User.checkPlanStorageSpace(req.user.company_id, req.user.plan, function (err, result) {
        if (err) {
            console.log(err);
        } else {
            if (result == 'yes') {
                var params = {
                    company_id: req.user.company_id,
                    user_id: req.user.id,
                    name: req.body.originalFilename,
                    file_name: req.body.s3Name,
                    size: req.body.size,
                    file_type: req.body.fileType,
                    file_path: req.body.url,
                    file_ext: general.getExtension(req.body.s3Name),
                    document_type: req.body.documentType,
                    modified: moment().format("YYYY-MM-DD HH:mm:ss")
                }

                Document.add(params, function (err, params) {
                    if (err) {
                        console.log(err);
                        res.send(JSON.stringify({ result: 'error' }));
                    } else {
                        res.send(JSON.stringify({ result: 'ok', params: params }));
                    }
                });
            } else if (result == 'no') {
                res.send(JSON.stringify({ result: 'storage', params: params }));
            }
        }
    });


});


router.post('/delete', authentication.isLoggedIn, function (req, res) {
    var id = req.body.id;
    var fileName = req.body.fileName;

    var s3 = new aws.S3();

    var params = {
        Bucket: bucket,
        Delete: { Objects: [{ Key: fileName }] }
    };

    s3.deleteObjects(params, function (err, data) {
        if (err) {
            console.log(err);
            res.send(JSON.stringify({ result: 'error', error: err }));
        } else {
            Document.delete(id, function (err, id) {
                if (err) {
                    console.log(err);
                    res.send(JSON.stringify({ result: 'error', error: err }));
                } else {
                    res.send(JSON.stringify({ result: 'ok', id: id }));
                }
            });
        }
    });
});

router.post('/download-documents', function (req, res) {
    res.contentType('json');

    var s3 = new aws.S3();

    Document.getAllDocumentsForCompany(req.user.company_id, function (err, result) {
        if (err) {
            console.log(err);
            res.send({ result: 'error' });
        } else {
            if (result.length > 0) {
                var newDirName = slug(req.user.company_name, '_') + '_documents';
                var dir = __dirname + '/../public/tmp/' + newDirName + '/documents';
                var zipFile = __dirname + '/../public/tmp/' + newDirName + '.zip';
                var zipLink = '/assets/tmp/' + newDirName + '.zip';

                mkdirp(dir, function (err) {
                    if (err) {
                        console.error(err);
                        res.send({ result: 'error' });
                    } else {
                        for (var i = 0; i < result.length; i++) {
                            (function (i) {
                                var file = result[i].file_name;
                                var splitFile = file.split('documents/');
                                var fileName = splitFile[1];
                                var s3Params = {
                                    Bucket: config.aws.bucketName,
                                    Key: result[i].file_name,
                                };

                                s3.getObject(s3Params, function (err, data) {
                                    if (err) {
                                        console.log(err);
                                        res.send({ result: 'error' });
                                    } else {
                                        fs.writeFile(dir + '/' + fileName, data.Body, { flag: 'w' }, function (err) {
                                            if (err) {
                                                console.log(err);
                                                res.send({ result: 'error' });
                                            } else {
                                                if (i == result.length - 1) {
                                                    zipFolder(dir, zipFile, function (err) {
                                                        if (err) {
                                                            console.log(err);
                                                            res.send({ result: 'error' });
                                                        } else {
                                                            res.send({ result: 'ok', zip: config.mainURL + zipLink });
                                                        }
                                                    });
                                                }
                                            }
                                        });
                                    }
                                });
                            })(i);
                        }
                    }
                });
            }
        }
    });
});

module.exports = router;