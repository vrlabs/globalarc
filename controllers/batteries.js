var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var config = require('../config/config');
var language = require('../config/languages/' + config.config.language);
var authentication = require('../helpers/authentication');
var pages = require('../helpers/pages');
var Battery = require('../models/battery');
var moment = require('moment');

router.use(bodyParser.urlencoded({ extended: true }));

router.use(methodOverride(function(req, res) {
    if (req.body && typeof req.body === 'object' && '_method' in req.body) {
        // look in urlencoded POST bodies and delete it
        var method = req.body._method
        delete req.body._method
        return method
    }
}));


// ALL BATTERIES
router.get('/', authentication.isLoggedIn, function(req, res) {
    Battery.getAllBatteriesForCompany(req.user.company_id, function(err, batteries) {
        if (err) {
            console.log(err);
        } else {
            for (var i = 0; i < batteries.length; i++) {
                batteries[i].status = '<span class="label ' + batteries[i].status_class + '">' + batteries[i].status_name + '</span>';
                if (batteries[i].notes != undefined && batteries[i].notes != 'null' && batteries[i].notes != '') {
                    batteries[i].notes = '<a href="#" data-toggle="modal" data-target="#modal-notes" onclick="setNotes(\'' + batteries[i].notes + '\');"><i class="fa fa-file-text-o"></i></a>';
                }
                batteries[i].edit = '<a href="/batteries/edit/' + batteries[i].id + '"><i class="glyphicon glyphicon-edit"></i></a>';
                batteries[i].delete = '<a href="#" data-toggle="modal" data-target="#modal-delete" onclick="setDeleteBatteryButton(' + batteries[i].id + ');"><i class="glyphicon glyphicon-trash"></i></a>';
            }

            var data = '[]';
            if (batteries.length > 0) {
                data = JSON.stringify(batteries);
            }

            pages.renderLoggedInPage(req, res, 'batteries/batteries', 'Batteries', req.flash('message'), req.flash('color'), { 'data': data, 'header': 'Batteries' });
        }
    });
});

// SINGLE VEHICLE
// add
router.get('/add', authentication.isLoggedIn, function(req, res) {
    pages.renderLoggedInPage(req, res, 'batteries/modify', 'Batteries | Add New', '', '', { 
        header: 'Add New Battery'
    });
});
router.post('/add', authentication.isLoggedIn, function(req, res) {
    req.checkBody('name', language.batteryNameRequired).notEmpty();
    req.checkBody('serial_number', language.batterySerialNumberRequired).notEmpty();
    if (req.body.charge != '') {
        req.checkBody('charge', language.batteryChargeIntegerRequired).isInt();
    }
    if (req.body.voltage != '') {
        req.checkBody('voltage', language.batteryVoltageDecimalRequired).isDecimal();
    }

    req.asyncValidationErrors().then(function() {
        var data = {
            company_id: req.user.company_id,
            name: req.body.name,
            serial_number: req.body.serial_number,
            charge: req.body.charge || '',
            voltage: req.body.voltage || '',
            status: req.body.status,
            notes: req.body.notes || ''
        }

        Battery.add(data, function(err, id) {
            if (err) {
                console.log(err);
                pages.renderLoggedInPage(req, res, 'batteries/modify', 'Batteries | Add New', language.dbError, 'danger', { 
                    header: 'Add New Battery'
                });
            } else {
                req.flash('message', language.batteryAdded);
                req.flash('color', 'success');
                res.redirect('/batteries');
            }
        });

    }).catch(function(errors) {
        if (errors) {
            console.log(errors);
            var messages = '';
            for (var i = errors.length - 1; i >= 0; i--) {
                messages += errors[i].msg + '<br/>';
            }
            pages.renderLoggedInPage(req, res, 'batteries/modify', 'Batteries | Add New', messages, 'danger', { 
                header: 'Add New Battery'
            });
        };
    });
});


// edit
router.get('/edit/:id', authentication.isLoggedIn, function(req, res) {
    Battery.getBatteryByID(req.params.id, function(err, battery) {
        if (err) {
            console.log(err);
        } else {

            pages.renderLoggedInPage(req, res, 'batteries/modify', 'Batteries | Edit', '', '', {
                name: battery.name,
                serial_number: battery.serial_number,
                charge: battery.charge,
                voltage: battery.voltage,
                status: battery.status,
                notes: battery.notes,
                header: 'Edit Battery'
            });
        }
    });

});
router.post('/edit/:id', authentication.isLoggedIn, function(req, res) {
    var id = req.params.id;

    req.checkBody('name', language.batteryNameRequired).notEmpty();
    req.checkBody('serial_number', language.batterySerialNumberRequired).notEmpty();
    if (req.body.charge != '') {
        req.checkBody('charge', language.batteryChargeIntegerRequired).isInt();
    }
    if (req.body.voltage != '') {
        req.checkBody('voltage', language.batteryVoltageDecimalRequired).isDecimal();
    }

    req.asyncValidationErrors().then(function() {
        var data = {
            name: req.body.name,
            serial_number: req.body.serial_number,
            charge: req.body.charge || '',
            voltage: req.body.voltage || '',
            status: req.body.status,
            notes: req.body.notes || ''
        };

        Battery.edit(id, data, function(err, affectedRows) {
            if (err) {
                console.log(err);
                pages.renderLoggedInPage(req, res, 'batteries/modify', 'Batteries | Edit', language.dbError, 'danger', { 
                    header: 'Edit Battery'
                });
            } else {
                if (affectedRows === 1) {
                    req.flash('message', language.batteryModified);
                    req.flash('color', 'success');
                    res.redirect('/batteries');
                }
            }
        });

    }).catch(function(errors) {
        if (errors) {
            console.log(errors);
            var messages = '';
            for (var i = errors.length - 1; i >= 0; i--) {
                messages += errors[i].msg + '<br/>';
            }
            pages.renderLoggedInPage(req, res, 'batteries/modify', 'Batteries | Edit', messages, 'danger', { 
                header: 'Edit Battery'
            });
        };
    });
});

// delete
router.get('/delete/:id', authentication.isLoggedIn, function(req, res) {
    var id = req.params.id;

    Battery.delete(id, function(err, affectedRows) {
        if (err) {
            console.log(err);
            req.flash('message', language.dbError);
            req.flash('color', 'danger');
            res.redirect('/batteries');
        } else {
            if (affectedRows === 1) {
                req.flash('message', language.batteryDeleted);
                req.flash('color', 'success');
                res.redirect('/batteries');
            }
        }
    });
});

// ajax
router.post('/add-new-battery', authentication.isLoggedIn, function(req, res) {
    res.contentType('json');
    req.checkBody('name', language.batteryNameRequired).notEmpty();
    req.checkBody('serial_number', language.batterySerialNumberRequired).notEmpty();
    if (req.body.charge != '') {
        req.checkBody('charge', language.batteryChargeIntegerRequired).isInt();
    }
    if (req.body.voltage != '') {
        req.checkBody('voltage', language.batteryVoltageDecimalRequired).isDecimal();
    }

    req.asyncValidationErrors().then(function() {
        var data = {
            company_id: req.user.company_id,
            name: req.body.name,
            serial_number: req.body.serial_number,
            charge: req.body.charge,
            voltage: req.body.voltage,        
            status: req.body.status,
            notes: req.body.notes
        }

        Battery.add(data, function(err, id) {
            if (err) {
                console.log(err);
                res.send(JSON.stringify({result:'error'}));
            } else {
                res.send(JSON.stringify({result:'ok', id: id}));
            }
        });

    }).catch(function(errors) {
        if (errors) {
            console.log(errors);
            var messages = '';
            for (var i = errors.length - 1; i >= 0; i--) {
                messages += errors[i].msg + '<br/>';
            }
            res.send(JSON.stringify({result:'error', messages: messages}));
        };
    });
});

router.post('/get-all-batteries', authentication.isLoggedIn, function(req, res) {
    res.contentType('json');

    Battery.getAllBatteriesForCompany(req.user.company_id, function(err, batteries) {
        if (err) {
            console.log(err);
            res.send(JSON.stringify({result:'error'}));
        } else {
            res.send(JSON.stringify({result:'ok', batteries: batteries}));
        }
    });
});

module.exports = router;