var mysql = require('mysql');

var pool  = mysql.createPool({
    connectionLimit : 100,
    host: process.env.DB_HOST,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE
});

var connectionError = function (error, connection) {
     if (error) {
        connection.release();
        throw error;
    }
}

module.exports.pool = pool;
module.exports.connectionError = connectionError;