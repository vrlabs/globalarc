var moment = require('moment');

// GENERAL
var config = {
    title: 'GlobalARC',
    year: moment().format('YYYY'),
    copyright: '© GlobalARC 2016. All rights reserved',
    version: '1.0.0',
    language: 'english',
    timeFormat: 'MM/DD/YYYY h:mm:ss A',
    mysqlTimeFormat: 'YYYY-MM-DD HH:mm:ss',
    usTimeFormat: 'MM/DD/YYYY h:mm:ss A',
    thumbW: 210,
    thumbH: 136,
    environment: process.env.ENVIRONMENT,
    sizeCoefficient: true // true = 1000, false = 1024
}

// MAIN URL
var mainURL = '';
var googleMapsKey = '';
if (process.env.ENVIRONMENT == 'local') {
    mainURL = 'http://localhost:4000';
    googleMapsKey = 'AIzaSyCups7yDn5WYBm2v8YXVAArW7M6hbTcuIg';
} else if (process.env.ENVIRONMENT == 'development') {
    mainURL = 'http://dev.globalarc.us';
    googleMapsKey = 'AIzaSyAOTXzE4h9j8qk11dG89CL0j29QLsJ1akA';
} else if (process.env.ENVIRONMENT == 'production') {
    mainURL = 'https://globalarc.us';
    googleMapsKey = 'AIzaSyBP43jysC6AiTXfijaPib1jdphujp5J2Sk';
}

// GOOGLE MAPS
var googleMapsApiKey = {
    key: googleMapsKey
}
var airmapApiKey = 'E8D05ADD-DF71-3D14-3794-93FAF8ED8F59';
var mapboxApiKey = 'pk.eyJ1IjoiZ2xvYmFsYXJjIiwiYSI6ImNpbjVzZjZjMTAwdGN2cW0ydXBjdHQzNGwifQ.eFLqZogcPIkQgWC5C42ygQ';

// SESSION 
var session = {
    name: 'GlobalARCSession',
    secret: 'LQDVmn2MQ7S4BFwYdW4WKtXvUN62dguMyPK6efmF',
    cookieExpireTime: new Date(Date.now() + 1209600000) // two weeks
}

// EMAIL
var email = {
    fromEmail: 'no-reply@globalarc.us',
    fromName: 'GlobalARC',
    toEmail: 'ivan@easyaerial.com',
    toName: 'Ivan Stamatovski',
    host: 'lagonda.websitewelcome.com',
    port: 465,
    auth: {
        user: 'no-reply@globalarc.us',
        pass: 'sudija000'
    }
}

// MAILCHIMP
var mailchimp = {
    apiKey: '4884250e4fea1cbebc337cfb33c9a942-us10',
    subscriptionListID: '04079a684a'
}

// GOOGLE
var googleClientID = '';
var googleClientSecret = '';
var googleCallbackURL = mainURL + '/auth/google/callback';

if (process.env.ENVIRONMENT == 'local') {
    googleClientID = '316360659559-uu889lkp2s7klrvkbn3kpmqal6dte9l8.apps.googleusercontent.com';
    googleClientSecret = '9x-8Z6SvJ_3e8192HCxejasi';
} else if (process.env.ENVIRONMENT == 'development') {
    googleClientID = '497529897792-q6une2a3893jcr73oe86ouuhpka6jh41.apps.googleusercontent.com';
    googleClientSecret = 'MJSOjLH_jyqvfdbNudyBj8sX';
} else if (process.env.ENVIRONMENT == 'production') {
    googleClientID = '934846810285-oj6jbpnefa8jlt6ia4hov2qgk94ohrsk.apps.googleusercontent.com';
    googleClientSecret = 'Qwg18bkaKhn1R3xgjNE1qxgt';
}

// STRIPE
var stripe = {};
var stripeSecretKey = '';
var stripePublishableKey = '';

if (process.env.ENVIRONMENT == 'local' || process.env.ENVIRONMENT == 'development') {
    stripeSecretKey = 'sk_test_I3LFjZx3EKSA26KBuFSZ1YPv';
    stripePublishableKey = 'pk_test_KWUgGs1Ik0M8YkXbERwXxiSN';
} else if (process.env.ENVIRONMENT == 'production') {
    stripeSecretKey = 'sk_live_nAuUAaOo00pbSyg0FqBQlCYx';
    stripePublishableKey = 'pk_live_GzOBa58uvQDAcrNjbJsn1nAi';
}

stripe.secretKey = stripeSecretKey;
stripe.publishableKey = stripePublishableKey;

// PLANS
var plans = {
    discount: {
        active: true,
        id: 'flystro'
    },
    free: {
        id: 'free',
        name: 'Free',
        price: 0,
        users: 1,
        projects: 1,
        flights: 5,
        storage: 10,
        storageInBytes:10000000
    },
    professional: {
        id: 'professional',
        name: 'Professional',
        stripePrice: 995,
        price: 9.95,
        users: 5,
        projects: 5,
        flights: 15,
        storage: 100,
        coupon: 'flystro',
        storageInBytes: 100000000
    },
    enterprise: {
        id: 'enterprise',
        name: 'Enterprise',
        price: 0,
        users: 0,
        projects: 0,
        flights: 0,
        storage: 1000,
        storageInBytes: 1000000000

    }
}

// AUTHORIZATION
var auth = {
    google: {
        clientID: googleClientID,
        clientSecret: googleClientSecret,
        callbackURL: googleCallbackURL
    }
};

var providers = ['google'];

var lengths = {
    accessCode: 12,
    vehicleUnitID: 3, // 'D' + randomString(3) => D001
    teamUserUnitID: 3,    // 'T' + randomString(3) => T001
    groupName: 4
}

// AMAZON CREDENTIALS (FILE UPLOAD)
var bucket = '';


if (process.env.ENVIRONMENT == 'local') {
    bucket = 'globalarc.upload.local';
}else if(process.env.ENVIRONMENT == 'development') {
    bucket = 'globalarc.upload';
} else {
    bucket = 'globalarc.upload.live';
}
var aws = {
    key: 'AKIAIMWXCPTIP4YAPGGA',
    secret: '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',
    region: 'us-east-1',
    bucketName: bucket
}


// ANDRUAV
var andruav = {
    url: 'andruav.com',
    port: 19106,
    createNewAccount: '/?cmd=c&acc=',
    regenerateAccessCode: '/?cmd=u&acc=',
    retreiveAccessCode: '/?cmd=r&acc=',
    loginCommand: '/?cmd=v&acc=',
    loginPassword: '&pwd=',
    loginVersion: '&ver=9.0.0'
}

var sampleData = {
    s3Path: 'https://s3.amazonaws.com/' + bucket + '/',
    projectName: 'Sample Project',
    projectNotes: 'Sample Project Notes',
    locationName: 'Sample Location',
    locationImage: 'sample.png',
    locationThumb: 'sample.png',
    locationCenterLatitude: -73.9744,
    locationCenterLongitude: 40.7662,
    locationCoordinates: [[-73.9758, 40.7668], [-73.9739, 40.7675], [-73.9728, 40.7659], [-73.9741, 40.7649], [-73.976, 40.7659], [-73.9758, 40.7668]],
    batteryName: 'Sample Battery',
    batterySerialNumber: 'SN1234567890',
    batteryCharge: 90,
    batteryVoltage: 12.5,
    batteryStatus: 1,
    batteryNotes: 'Sample Battery Notes',
    vehicleName: 'Sample Drone',
    vehicleType: 'drone',
    vehicleManufacturer: 'Easy Aerial Inc.',
    vehicleModel: 'Easy Drone XL Pro',
    vehicleSerialNumber: 'SN1234567890',
    vehicleStatus: 1,
    vehicleNotes: 'Sample Vehicle Notes',
    flightplanName: 'Sample Flight Plan',
    flightplanGroupName: '1',
    flightplanNotes: 'Sample Flight Plan Notes',
    flightplanVehicleUnitID: 'v_abc',
    flightplanTeamUsersRoleID: 1,
    flightplanTeamUsersUnitID: 't_abc',
    documentName: 'Preflight Checklist',
    documentFileType: 'application/pdf',
    documentFileName: 'preflight_checklist.pdf',
    documentFileExtension: '.pdf',
    documentSize: 375433,
    documentHumanSize: '107 kB',
    documentType: 3,
    commentContent: 'Hello World!',
    commentRoleID: 1
}

var pdfOptions = {
    format: 'Letter',
    orientation: 'portrait',
    border: '1cm',
    zoomFactor: '1',
    type: 'pdf',
    header: {
        height: '0mm'
    },
    footer: {
        height: '10mm',
        contents: {
            default:'© GlobalARC'
        }
    }
}

module.exports = {
    config: config,
    session: session,
    email: email,
    mailchimp: mailchimp,
    auth: auth,
    providers: providers,
    lengths: lengths,
    aws: aws,
    andruav: andruav,
    airmapApiKey: airmapApiKey,
    mapboxApiKey: mapboxApiKey,
    sampleData: sampleData,
    stripe: stripe,
    plans: plans,
    mainURL: mainURL,
    googleMapsApiKey: googleMapsApiKey,
    pdfOptions:pdfOptions
};