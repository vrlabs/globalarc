module.exports = {
    // WEBSITE -------------------------------------------------------------------
    // beta subscription:
    'successBetaSubscription': 'You have successfully subscribed for our Beta.',
    'errorBetaSubscription': 'Something went wrong with your subscription. Please, try again later.',
    'memberExistsBetaSubscription': 'You are already subscribed for our Beta.',

    // ANDRUAV
    'andruavBadEmail': 'Bad email.',
    'andruavBadEmailFormat': 'Bad email format.',
    'andruavDuplicateEntry': 'Duplicate email entry.',
    'andruavDatabaseError': 'Database error',
    'andruavAccountNotFound': 'Account not found.',
    'andruavUpgradeApp': 'Please upgrade your mobile app to latest version.',
    'andruavOldAppVersion': 'Old version of Andruav App.',
    'andruavAccessCodeRegenerated': 'Check your email to get your regenerated access code.',
    'andruavAccessCodeRetrieved': 'Check your email to get your current access code.',
    
        
    // APPLICATION ---------------------------------------------------------------
    // general
    'badRequest': 'Your request could not be processed.<br/>Please, try again later.',
    'emailError': 'Error while sending an email.<br/>Please, try again later.',
    'agreeToTerms': 'Please, agree to our terms and policy to continue.',
    'dbError': 'Database Error.',
    'fieldRequired': 'This field is required.',
    'integerRequired': 'Please, enter integer number',
    'decimalRequired': 'Please enter decimal number',
    'error': 'Ooops! Something went wrong, please try again.',
    
    // AUTHENTICATION ------------------------------------------------------------
    // register  
    'nameRequired': 'Name is required.',
    'emailRequired': 'Email is required.',
    'validEmail': 'Please enter a valid email address.',
    'emailNotAvailable': 'Email is already taken.',
    'emailNotAvailableProviderRegister': 'Email is already taken.',
    'emailNotAvailableProviderLogin': 'Sign in with Google and<br/>set your password in profile page.',
    'passwordRequired': 'Password is required.',
    'passwordLength': 'Password has to be 6 to 20 characters long.',
    'confirmPasswordRequired': 'Confirmed password is required.',
    'passwordsDontMatch': 'Passwords don\'t match.', 
    'companyRequired': 'Company name is required.',
    'companyNotAvailable': 'Company name is already taken.',
    'companyError': 'Wrong company name.',
    'archivedUser': 'Your account has been archived.<br/>Contact your team admin.',
    'subscriptionExpired': 'Your subscription has expired.',
    
        
    // activation
    'activationEmailSent': 'An activation email has been sent.<br/>Please, check your email.',    
    'accountActivated': 'Your account has been activated.',
    'accountAlreadyActivated': 'Your account is already activated.',

    // invitation
    'invitedAccountActivated': 'Your account has been activated.',
    'invitedAccountAlreadyActivated': 'Your account is already activated.',    
    
    // login
    'userNotFound': 'User not found.',
    'wrongPassword': 'Oops! Wrong password.',  
    'userNotActive': 'Your account is not activated.',
    'userInvitedNotActive': 'Your account is not activated yet.',
    
    // forgot password
    'resetEmailSent': 'Password reset link sent.<br/>Please, check your email.',
    'newPasswordSet': 'Your new password is set.',
    
    // PROFILE
    'userProfileSuccessfullyModified': 'You successfully modified your profile info.',
    'userProfileErrorModified': 'Oops! Something went wrong, please try again.',
    
    // COMPANY
    'companyInfoNotSet': 'Your company info is not set. Go to <a class="text-info" href="/user/settings">settings page</a> to set it.',
    'companyInfoSet':'Your company info is successfully set.',
    'companyNameRequired': 'Company name is required.',
    
    
    
    // PROJECTS
    'projectAdded': 'You successfully added new project.',
    'projectModified': 'You successfully modified the project.',
    'projectDeleted': 'You successfully deleted the project.',
    'projectNameRequired': 'Project name is required.',
    
    
    // VEHICLES
    'vehicleNameRequired': 'Enter vehicle name.',
    'vehicleTypeRequired': 'Enter vehicle type.',
    'vehicleAdded': 'You successfully added new vehicle.',
    'vehicleModified': 'You successfully modified the vehicle.',
    'vehicleDeleted': 'You successfully deleted the vehicle.',
    
    // BATTERIES
    'batteryNameRequired': 'Enter battery name.',
    'batterySerialNumberRequired': 'Enter battery serial number.',
    'batteryChargeIntegerRequired': 'Enter battery charge as integer number.',
    'batteryVoltageDecimalRequired': 'Enter battery voltage as decimal number.',
    'batteryAdded': 'You successfully added new battery.',
    'batteryModified': 'You successfully modified the battery.',
    'batteryDeleted': 'You successfully deleted the battery.',

    // TEAM
    'userInvited': 'You successfully invited new team member. <br> Be sure to add him/her as a team member on a flight plan.',
    'userDeleted': 'You successfully deleted the user.',

    // FLIGHTPLANS
    'flightplanAdded': 'You successfully added new flightplan.',
    'flightplanDeleted': 'You successfully deleted the flightplan.',
    'flightplanEdited': 'You successfully modified the flightplan.',
    'flightplanGroupNameRequired': 'Flight plan group name is required.'
}
