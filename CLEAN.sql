# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.10)
# Database: globalarcdb
# Generation Time: 2016-06-06 09:00:04 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table batteries
# ------------------------------------------------------------

DROP TABLE IF EXISTS `batteries`;

CREATE TABLE `batteries` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(11) unsigned NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `serial_number` varchar(255) NOT NULL,
  `charge` int(10) DEFAULT NULL,
  `voltage` float DEFAULT NULL,
  `status` int(1) unsigned NOT NULL DEFAULT '1' COMMENT '1 = active, 2 = grounded, 3 = retired',
  `notes` text,
  PRIMARY KEY (`id`),
  KEY `fk_batteries` (`company_id`),
  CONSTRAINT `fk_batteries` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table companies
# ------------------------------------------------------------

DROP TABLE IF EXISTS `companies`;

CREATE TABLE `companies` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `access_code` varchar(40) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `data_set` tinyint(1) unsigned DEFAULT '0',
  `plan` enum('free','professional','enterprise') NOT NULL DEFAULT 'free',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `subscription_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `access_code` (`access_code`),
  KEY `fk_companies` (`subscription_id`),
  CONSTRAINT `fk_companies` FOREIGN KEY (`subscription_id`) REFERENCES `subscriptions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table document_types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `document_types`;

CREATE TABLE `document_types` (
  `id` int(3) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `document_types` WRITE;
/*!40000 ALTER TABLE `document_types` DISABLE KEYS */;

INSERT INTO `document_types` (`id`, `name`)
VALUES
	(1,'Regulatory Certificate'),
	(2,'Insurance Certificate'),
	(3,'Checklist'),
	(4,'Manual'),
	(5,'Pilot Certificate');

/*!40000 ALTER TABLE `document_types` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table documents
# ------------------------------------------------------------

DROP TABLE IF EXISTS `documents`;

CREATE TABLE `documents` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned DEFAULT NULL COMMENT 'uploaded by',
  `user_name` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `file_type` varchar(255) DEFAULT NULL COMMENT 'pdf, image',
  `file_path` varchar(255) DEFAULT NULL,
  `file_ext` varchar(5) DEFAULT NULL COMMENT 'pdf, jpg, png',
  `size` int(50) unsigned DEFAULT NULL,
  `human_size` varchar(255) DEFAULT NULL,
  `document_type` tinyint(3) unsigned DEFAULT NULL COMMENT '1= Regulatory Certificate, 2 = Insurance Certificate, 3 = Checklist, 4 = Manual, 5 = Pilot Certificate',
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_documents` (`company_id`),
  CONSTRAINT `fk_documents` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table flightplan_batteries
# ------------------------------------------------------------

DROP TABLE IF EXISTS `flightplan_batteries`;

CREATE TABLE `flightplan_batteries` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `flightplan_id` int(11) unsigned NOT NULL,
  `battery_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_flightplan_batteries` (`flightplan_id`),
  CONSTRAINT `fk_flightplan_batteries` FOREIGN KEY (`flightplan_id`) REFERENCES `flightplans` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table flightplan_comments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `flightplan_comments`;

CREATE TABLE `flightplan_comments` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `flightplan_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `content` text NOT NULL,
  `created` datetime NOT NULL,
  `role_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `comments_fk1` (`user_id`),
  KEY `fk_flightplan_comments` (`flightplan_id`),
  CONSTRAINT `fk_flightplan_comments` FOREIGN KEY (`flightplan_id`) REFERENCES `flightplans` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table flightplan_documents
# ------------------------------------------------------------

DROP TABLE IF EXISTS `flightplan_documents`;

CREATE TABLE `flightplan_documents` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `flightplan_id` int(11) unsigned DEFAULT NULL,
  `document_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `flightplan_documents_fk1` (`document_id`),
  KEY `fk_flightplan_documents` (`flightplan_id`),
  CONSTRAINT `fk_flightplan_documents` FOREIGN KEY (`flightplan_id`) REFERENCES `flightplans` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table flightplan_team_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `flightplan_team_users`;

CREATE TABLE `flightplan_team_users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `team_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `role_id` int(11) unsigned NOT NULL,
  `unit_id` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `team_users_fk1` (`user_id`),
  KEY `fk_flightplan_team_users` (`team_id`),
  CONSTRAINT `fk_flightplan_team_users` FOREIGN KEY (`team_id`) REFERENCES `flightplan_teams` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table flightplan_teams
# ------------------------------------------------------------

DROP TABLE IF EXISTS `flightplan_teams`;

CREATE TABLE `flightplan_teams` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `flightplan_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_flightplan_teams` (`flightplan_id`),
  CONSTRAINT `fk_flightplan_teams` FOREIGN KEY (`flightplan_id`) REFERENCES `flightplans` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table flightplan_vehicles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `flightplan_vehicles`;

CREATE TABLE `flightplan_vehicles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `flightplan_id` int(11) unsigned NOT NULL,
  `vehicle_id` int(11) unsigned NOT NULL,
  `unit_id` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `flightplan_vehicles_fk1` (`vehicle_id`),
  KEY `fk_flightplan_vehicles` (`flightplan_id`),
  CONSTRAINT `fk_flightplan_vehicles` FOREIGN KEY (`flightplan_id`) REFERENCES `flightplans` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table flightplans
# ------------------------------------------------------------

DROP TABLE IF EXISTS `flightplans`;

CREATE TABLE `flightplans` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(11) unsigned NOT NULL,
  `location_id` int(11) unsigned NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `group_name` varchar(255) NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `notes` text,
  `sample` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `created_by` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_flightplans` (`project_id`),
  CONSTRAINT `fk_flightplans` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table images
# ------------------------------------------------------------

DROP TABLE IF EXISTS `images`;

CREATE TABLE `images` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `flightplan_id` int(11) unsigned NOT NULL,
  `vehicle_id` int(11) unsigned NOT NULL,
  `created` datetime DEFAULT NULL,
  `latitude` varchar(255) DEFAULT NULL,
  `longitude` varchar(255) DEFAULT NULL,
  `path` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `images_fk1` (`vehicle_id`),
  KEY `fk_images` (`flightplan_id`),
  CONSTRAINT `fk_images` FOREIGN KEY (`flightplan_id`) REFERENCES `flightplans` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table location_coordinates
# ------------------------------------------------------------

DROP TABLE IF EXISTS `location_coordinates`;

CREATE TABLE `location_coordinates` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `location_id` int(11) unsigned NOT NULL,
  `latitude` float DEFAULT NULL,
  `longitude` float DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_location_coordinates` (`location_id`),
  CONSTRAINT `fk_location_coordinates` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table locations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `locations`;

CREATE TABLE `locations` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(11) unsigned NOT NULL,
  `type` enum('POLYGON','CIRCLE','RECTANGLE') DEFAULT 'POLYGON',
  `name` varchar(255) NOT NULL DEFAULT '',
  `image` varchar(255) NOT NULL DEFAULT '',
  `thumb` varchar(255) DEFAULT NULL,
  `path` varchar(255) NOT NULL DEFAULT '',
  `path_thumb` varchar(255) DEFAULT NULL,
  `center_latitude` float NOT NULL,
  `center_longitude` float NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_locations` (`project_id`),
  CONSTRAINT `fk_locations` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table logs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `logs`;

CREATE TABLE `logs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `flightplan_id` int(11) unsigned NOT NULL,
  `vehicle_id` int(11) unsigned NOT NULL,
  `path` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `logs_fk1` (`vehicle_id`),
  KEY `fk_logs` (`flightplan_id`),
  CONSTRAINT `fk_logs` FOREIGN KEY (`flightplan_id`) REFERENCES `flightplans` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table projects
# ------------------------------------------------------------

DROP TABLE IF EXISTS `projects`;

CREATE TABLE `projects` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(11) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `notes` text,
  `color` varchar(100) NOT NULL DEFAULT '',
  `sample` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `created_by` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_projects` (`company_id`),
  CONSTRAINT `fk_projects` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table statuses
# ------------------------------------------------------------

DROP TABLE IF EXISTS `statuses`;

CREATE TABLE `statuses` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `class` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `statuses` WRITE;
/*!40000 ALTER TABLE `statuses` DISABLE KEYS */;

INSERT INTO `statuses` (`id`, `name`, `class`)
VALUES
	(1,'Active','bg-success'),
	(2,'Grounded','bg-danger'),
	(3,'Retired','bg-warning');

/*!40000 ALTER TABLE `statuses` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table subscriptions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `subscriptions`;

CREATE TABLE `subscriptions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` varchar(255) NOT NULL DEFAULT '',
  `customer_email` varchar(255) NOT NULL DEFAULT '',
  `subscription_id` varchar(255) NOT NULL DEFAULT '',
  `subscription_start` datetime NOT NULL,
  `subscription_plan` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table user_permissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_permissions`;

CREATE TABLE `user_permissions` (
  `id` int(1) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `class` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `user_permissions` WRITE;
/*!40000 ALTER TABLE `user_permissions` DISABLE KEYS */;

INSERT INTO `user_permissions` (`id`, `name`, `class`)
VALUES
	(1,'Administrator','bg-danger'),
	(2,'Editor','bg-success'),
	(3,'Viewer','bg-warning');

/*!40000 ALTER TABLE `user_permissions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_roles`;

CREATE TABLE `user_roles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `class` varchar(100) DEFAULT '',
  `form_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `user_roles` WRITE;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;

INSERT INTO `user_roles` (`id`, `name`, `class`, `form_name`)
VALUES
	(1,'Manager','bg-info','manager'),
	(2,'Pilot','bg-success','pilot'),
	(3,'Observer','bg-warning','observer'),
	(4,'Payload Operator','bg-danger','payload_operator'),
	(5,'Remote Observer','bg-primary','remote_observer');

/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `help` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `sample` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `company_id` int(11) unsigned NOT NULL,
  `permission_id` tinyint(1) unsigned NOT NULL DEFAULT '2',
  `provider` varchar(45) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `photo` varchar(255) DEFAULT '/assets/images/avatar.png',
  `active` tinyint(1) unsigned DEFAULT '0',
  `activation_key` varchar(40) DEFAULT NULL,
  `reset_key` varchar(40) DEFAULT NULL,
  `invited` tinyint(1) unsigned DEFAULT '0',
  `invited_by` int(11) NOT NULL DEFAULT '0',
  `invitation_key` varchar(40) DEFAULT NULL,
  `google_id` varchar(45) DEFAULT NULL,
  `google_token` varchar(255) DEFAULT NULL,
  `google_display_name` varchar(255) DEFAULT NULL,
  `google_family_name` varchar(45) DEFAULT NULL,
  `google_given_name` varchar(45) DEFAULT NULL,
  `google_middle_name` varchar(45) DEFAULT NULL,
  `google_email` varchar(255) DEFAULT NULL,
  `google_email_type` varchar(45) DEFAULT NULL,
  `google_photo` varchar(255) DEFAULT '/assets/images/avatar.png',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_users` (`company_id`),
  CONSTRAINT `fk_users` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table vehicles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `vehicles`;

CREATE TABLE `vehicles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(11) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `manufacturer` varchar(255) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `serial_number` varchar(255) DEFAULT NULL,
  `status` int(10) NOT NULL DEFAULT '1' COMMENT '1 = active, 2 = grounded, 3 = retired',
  `notes` text,
  PRIMARY KEY (`id`),
  KEY `fk_vehicles` (`company_id`),
  CONSTRAINT `fk_vehicles` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
