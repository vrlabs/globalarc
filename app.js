var express = require('express');
var path = require('path');

// loading config file
require('dotenv').config({ path: path.join(__dirname, 'config', 'environment.env') });

var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var passport = require('passport');
var flash = require('connect-flash');
var session = require('express-session');
var config = require('./config/config');
var Flightplan = require('./models/flightplan');
var app = express();
var CronJob = require('cron').CronJob;
var general = require('./helpers/general');

app.io = require('socket.io')();

// passport configuration
require('./controllers/passport')(passport); 

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

//favicon
app.use(favicon(__dirname + '/public/favicon.ico'));


// static content (image, stylesheets, custom scripts)
app.use('/assets', express.static(path.join(__dirname, 'public')));
// app.use('/streamer', express.static(path.join(__dirname, 'streamer')));

// logger
if (process.env.ENVIRONMENT == 'production') {
    app.use(logger('common', { skip: function (req, res) { return res.statusCode < 400 }, stream: __dirname + '/../morgan.log' }));
} else {
    app.use(logger('dev'));
}

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

// cookies & sessions
app.use(cookieParser(config.session.secret));
var sessionData = {
    name: config.session.name,
    secret: config.session.secret,
    cookie: { maxAge: config.session.cookieExpireTime },
    resave: false,
    saveUninitialized: true
};
app.use(session(sessionData));


app.use(function (req, res, next) {
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', config.mainURL);

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

// passport
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(passport.authenticate('remember-me'));

// flash messages from session
app.use(flash());

// controllers
app.use('/', require('./controllers/website'));
app.use('/general', require('./controllers/general'));
app.use('/auth', require('./controllers/authentication'));
app.use('/user', require('./controllers/user'));
app.use('/dashboard', require('./controllers/dashboard'));
app.use('/projects', require('./controllers/projects'));
app.use('/vehicles', require('./controllers/vehicles'));
app.use('/batteries', require('./controllers/batteries'));
app.use('/flightplans', require('./controllers/flightplans'));
app.use('/documents', require('./controllers/documents'));
app.use('/team', require('./controllers/team'));
app.use('/validate', require('./helpers/validators'));
app.use('/help', require('./controllers/help'));
app.use('/api', require('./controllers/api'));
app.use('/reports', require('./controllers/reports'));

// socket.io
app.io.on('connection', function (socket) {
    socket.on('addComment', function(data) {
        Flightplan.addComment(data, function (err, result) {
            if (err) {
                app.io.emit('commentAdded', 'error');
            } else {
                app.io.emit('commentAdded', data);
            }
        });
    });
});

// cron jobs
var deleteTmpFiles = new CronJob({
    cronTime: '0 0 * * * *',
    onTick: function() {
        general.deleteTmpFiles();
    },
    start: true,
    timeZone: 'America/New_York'
});
deleteTmpFiles.start();

var removeDeletedCompanies = new CronJob({
    cronTime: '0 0 0 * * *',
    onTick: function() {
        general.removeDeletedCompanies();
    },
    start: true,
    timeZone: 'America/New_York'
});
removeDeletedCompanies.start();

module.exports = app;