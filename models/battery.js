var db = require('../config/db').pool;
var connectionError = require('../config/db').connectionError;

module.exports.getBatteryByID = function (id, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "SELECT * FROM batteries WHERE id = " + id + " LIMIT 1;";
                
        connection.query(sql, function (err, rows) {
            connection.release();
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(null, rows[0]);
            }
        });
    });
};

module.exports.getAllBatteriesForCompany = function (id, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "SELECT batteries.id, batteries.name, batteries.serial_number, batteries.charge, batteries.voltage, batteries.status, batteries.notes, statuses.name AS status_name, statuses.class AS status_class FROM batteries JOIN statuses ON statuses.id = batteries.status WHERE company_id = " + id + ";";
        
        connection.query(sql, function (err, rows) {
            connection.release();
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(null, rows);
            }
        });
    });
};

module.exports.getAllBatteriesForFlightplan = function (id, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "SELECT battery_id FROM flightplan_batteries WHERE flightplan_id = " + id + ";";
                
        connection.query(sql, function (err, rows) {
            connection.release();
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(null, rows);
            }
        });
    });
};


module.exports.add = function (data, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        if (data.charge == '') {
            data.charge = 'NULL';
        }        
        if (data.voltage == '') {
            data.voltage = 'NULL';
        }
        var sql = "INSERT INTO batteries(company_id, name, serial_number, charge, voltage, status, notes) VALUES (" + data.company_id + ",'" + data.name + "', '" + data.serial_number + "', " + data.charge + ", " + data.voltage + ", " + data.status + ", '" + data.notes + "');";
                
        connection.query(sql, function (err, rows) {
            connection.release();
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(null, rows.insertId);
            }
        });   
    });
}

module.exports.edit = function (id, data, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        if (data.charge == '') {
            data.charge = 'NULL';
        }        
        if (data.voltage == '') {
            data.voltage = 'NULL';
        }        
        
        var sql = "UPDATE batteries SET name = '" + data.name + "',serial_number = '" + data.serial_number + "', charge = " + data.charge + ", voltage = " + data.voltage + ", status = " + data.status + ", notes = '" + data.notes + "' WHERE id = " + id + ";";
                        
        connection.query(sql, function (err, rows) {
            connection.release();
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(null, rows.affectedRows);
            }
        });   
    });
}

module.exports.delete = function (id, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);
        
        var sql = "DELETE FROM batteries WHERE id = " + id + ";";
                        
        connection.query(sql, function (err, rows) {
            connection.release();
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(null, rows.affectedRows);
            }
        });   
    });
}
