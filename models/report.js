var db = require('../config/db').pool;
var connectionError = require('../config/db').connectionError;

module.exports.getAllReportsForCompany = function (companyID, callback) {
    var result = [];

    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "SELECT * FROM reports WHERE company_id = " + connection.escape(companyID) + " ORDER BY date DESC";

        connection.query(sql, function (err, rows) {
            if (err) {
                console.log(err);
                callback(err, null);
            } else {

                callback(err, rows);
            }
        });
    });
};

module.exports.getById = function (id, callback) {
    var result = [];

    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "SELECT * FROM reports WHERE id = " + connection.escape(id);

        connection.query(sql, function (err, rows) {
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(err, rows[0]);
            }
        });
    });
};

module.exports.add = function (data, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "INSERT INTO reports(company_id, parent_id, parent_name, date, type, path) VALUES(" + connection.escape(data.company_id) + "," + connection.escape(data.parent_id) + "," + connection.escape(data.parent_name) + "," + connection.escape(data.date) + "," + connection.escape(data.type) + "," + connection.escape(data.path) + ")";

        connection.query(sql, function (err, rows) {
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(err, rows.affectedRows);
            }
        });
    });
};


module.exports.delete = function (id, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "DELETE FROM reports WHERE id = " + connection.escape(id);

        connection.query(sql, function (err, rows) {
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(err, rows.affectedRows);
            }
        });
    });
};

