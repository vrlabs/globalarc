var db = require('../config/db').pool;
var connectionError = require('../config/db').connectionError;
var User = require('./user');
var config = require('../config/config');
var general = require('../helpers/general');
var moment = require('moment');

module.exports.getDocumentByID = function (id, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "SELECT * FROM documents WHERE id = " + id + " LIMIT 1;";

        connection.query(sql, function (err, rows) {
            connection.release();
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(null, rows[0]);
            }
        });
    });
};

module.exports.getAllDocumentsForCompany = function (id, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "SELECT documents.*, users.name AS user_name, users.google_display_name AS user_google_name, document_types.name AS document_type FROM documents JOIN users ON documents.user_id = users.id JOIN document_types ON document_types.id = documents.document_type WHERE documents.company_id = " + id + ";";

        connection.query(sql, function (err, rows) {
            connection.release();
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(null, rows);
            }
        });
    });
};

module.exports.getAllDocumentsForFlightplan = function (id, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "SELECT document_id FROM flightplan_documents WHERE flightplan_id = " + id + ";";

        connection.query(sql, function (err, rows) {
            connection.release();
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(null, rows);
            }
        });
    });
};


module.exports.getDocumentTypes = function (callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "SELECT * FROM document_types;";

        connection.query(sql, function (err, rows) {
            connection.release();
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(null, rows);
            }
        })
    })
}

module.exports.add = function (data, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        if (data.size == '') {
            data.size = 'NULL';
        }

        User.getUserNameByID(data.user_id, function (err, userName) {
            if (err) {
                callback(err, null);
            } else {
                getDocumentTypeName(data.document_type, function (err, typeName) {
                    if (err) {
                        callback(err, null);
                    } else {
                        var humanSize = general.humanFileSize(data.size);

                        var sql = "INSERT INTO documents(company_id, user_id, user_name, name, file_name, size, human_size, file_type, file_path, file_ext, document_type, modified) VALUES (" + data.company_id + "," + data.user_id + ", '" + userName + "', '" + data.name + "', '" + data.file_name + "', " + data.size + ", '" + humanSize + "', '" + data.file_type + "', '" + data.file_path + "', '" + data.file_ext + "', " + data.document_type + ", '" + data.modified + "');";

                        connection.query(sql, function (err, rows) {
                            connection.release();
                            if (err) {
                                console.log(err);
                                callback(err, null);
                            } else {
                                var params = {
                                    id: rows.insertId,
                                    name: data.name,
                                    size: humanSize,
                                    type: typeName,
                                    user: userName,
                                    modified: moment(data.modified).format(config.config.timeFormat),
                                    path: data.file_path
                                }
                                callback(null, params);
                            }
                        });
                    }
                })
            }
        })
    });
}

module.exports.delete = function (id, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sqlFlightplanDocs = "DELETE FROM flightplan_documents WHERE document_id = " + connection.escape(id);
        connection.query(sql, function (err, rowsFlightplanDocs) {
            connection.release();
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                var sql = "DELETE FROM documents WHERE id = " + id + ";";

                connection.query(sql, function (err, rows) {
                    connection.release();
                    if (err) {
                        console.log(err);
                        callback(err, null);
                    } else {
                        
                        callback(null, rows.affectedRows);
                    }
                });
            }
        });
    });
}

function getDocumentTypeName(type_id, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "SELECT name FROM document_types WHERE id = " + type_id + ";";

        connection.query(sql, function (err, rows) {
            connection.release();
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(null, rows[0].name);
            }
        });
    });
}