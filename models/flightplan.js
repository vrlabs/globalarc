var moment = require('moment');
var fs = require('fs');
var aws = require('aws-sdk');
var gm = require('gm').subClass({imageMagick: true});;

var db = require('../config/db').pool;
var connectionError = require('../config/db').connectionError;

var Project = require('../models/project');
var Battery = require('../models/battery');
var Vehicle = require('../models/vehicle');
var User = require('../models/user');
var Document = require('../models/document');

var config = require('../config/config').config;
var general = require('../helpers/general');
var lengths = require('../config/config').lengths;
var configAWS = require('../config/config').aws;
var sample = require('../config/config').sampleData;


module.exports.getFlightplanByID = function (id, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "SELECT flightplans.*, projects.name AS project_name FROM flightplans JOIN projects ON flightplans.project_id = projects.id  WHERE flightplans.id = '" + id + "' LIMIT 1;";

        connection.query(sql, function (err, rows) {
            connection.release();
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(null, rows[0]);
            }
        });
    });
};

module.exports.getAllFlightplansForProject = function (id, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "SELECT * FROM flightplans WHERE project_id = '" + id + "' ORDER BY start_time ASC";
        connection.query(sql, function (err, rows) {
            connection.release();
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(null, rows);
            }
        });
    });
}

module.exports.getAllFlightplansForCompany = function (companyID, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "SELECT id, name FROM flightplans WHERE project_id IN(SELECT id FROM projects WHERE company_id = " + connection.escape(companyID) + ")";

        connection.query(sql, function (err, rows) {
            connection.release();
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(null, rows);
            }
        });
    });
};

module.exports.getFlightplanProjectID = function (id, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "SELECT project_id FROM flightplans WHERE id = '" + id + "' LIMIT 1;";

        connection.query(sql, function (err, rows) {
            connection.release();
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(null, rows[0].project_id);
            }
        });
    });
};


module.exports.getMemberUnitID = function (flightplanID, userID, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "SELECT unit_id, role_id FROM flightplan_team_users WHERE user_id = " + userID + " AND team_id = (SELECT id FROM flightplan_teams WHERE flightplan_id = " + flightplanID + ")";

        connection.query(sql, function (err, rows) {
            connection.release();
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(null, rows[0]);
            }
        });
    });
}

module.exports.getVehicleUnitID = function (flightplanID, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "SELECT vehicle_id, unit_id FROM flightplan_vehicles WHERE flightplan_id = " + flightplanID;
        connection.query(sql, function (err, rows) {
            connection.release();
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(null, rows[0]);
            }
        });
    });
}

module.exports.getAllFlightplanInfo = function (userID, projectID, callback) {
    var result = {};

    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sqlProject = "SELECT name FROM projects WHERE id = '" + projectID + "';";

        connection.query(sqlProject, function (err, rowsProject) {
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                result.projectName = rowsProject[0].name;

                var sqlFlightplans = "SELECT flightplans.*, locations.name AS location_name, locations.path AS snapshot FROM flightplans LEFT JOIN locations ON flightplans.location_id = locations.id WHERE flightplans.project_id = '" + projectID + "' AND flightplans.id IN(SELECT flightplan_id FROM flightplan_teams WHERE id IN (SELECT team_id FROM flightplan_team_users WHERE user_id = " + connection.escape(userID) + ")) ORDER BY start_time ASC";               

                connection.query(sqlFlightplans, function (err, rowsFlightplans) {
                    if (err) {
                        console.log(err);
                        callback(err, null);
                    } else {

                        result.flightplans = rowsFlightplans;

                        var sqlDocumentTypes = "SELECT id, name FROM document_types;";

                        connection.query(sqlDocumentTypes, function (err, rowsDocumentTypes) {
                            connection.release();
                            if (err) {
                                console.log(err);
                                callback(err, null);
                            } else {
                                result.documentTypes = rowsDocumentTypes;
                                callback(null, result);
                            }
                        });
                    }
                });
            }
        });
    });
}

module.exports.getAllInfoAdd = function (projectID, companyID, callback) {
    var result = {};

    Project.getProjectName(projectID, function (err, projectName) {
        if (err) {
            callback(err, null);
        } else {
            result.projectName = projectName;

            User.getAllUsersFromCompany(companyID, function (err, members) {
                if (err) {
                    callback(err, null);
                } else {
                    result.members = members;
                    db.getConnection(function (err, connection) {
                        connectionError(err, connection);
                        var sqlLocations = "SELECT COUNT(id) AS num FROM locations WHERE project_id = " + projectID;
                        connection.query(sqlLocations, function (err, rowsLocations) {
                            if (err) {
                                console.log(err);
                                callback(err, null);
                            } else {
                                result.locations = rowsLocations[0].num;

                                User.getAllRoles(function (err, roles) {
                                    if (err) {
                                        callback(err, null);
                                    } else {
                                        result.roles = roles;

                                        Battery.getAllBatteriesForCompany(companyID, function (err, batteries) {
                                            if (err) {
                                                callback(err, null);
                                            } else {
                                                result.batteries = batteries;

                                                Vehicle.getAllVehiclesForCompany(companyID, function (err, vehicles) {
                                                    if (err) {
                                                        callback(err, null);
                                                    } else {
                                                        result.vehicles = vehicles;

                                                        Document.getDocumentTypes(function (err, documentTypes) {
                                                            if (err) {
                                                                callback(err, null);
                                                            } else {
                                                                result.documentTypes = documentTypes;

                                                                Document.getAllDocumentsForCompany(companyID, function (err, documents) {
                                                                    if (err) {
                                                                        callback(err, null);
                                                                    } else {
                                                                        for (var i = 0; i < documents.length; i++) {
                                                                            if (documents[i].user_name != '') {
                                                                                documents[i].user = documents[i].user_name;
                                                                            } else if (documents[i].user_google_name != '') {
                                                                                documents[i].user = documents[i].user_google_name;
                                                                            }

                                                                            documents[i].size = general.humanFileSize(documents[i].size);
                                                                            documents[i].modified = moment(documents[i].modified).format(config.timeFormat);
                                                                        }

                                                                        result.documents = documents;
                                                                        callback(null, result);
                                                                    }
                                                                })
                                                            }
                                                        })
                                                    }
                                                })
                                            }
                                        })
                                    }
                                });
                            }
                        });
                    });
                }
            })
        }
    });
}

module.exports.getAllInfoEdit = function (flightplanID, companyID, callback) {
    var result = {};

    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "SELECT * FROM flightplans WHERE id = '" + flightplanID + "' LIMIT 1;";

        connection.query(sql, function (err, rows) {
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                result.info = rows[0];
                var projectID = rows[0].project_id;

                var sqlLocations = "SELECT id, name, path FROM locations WHERE project_id = " + connection.escape(projectID);
                connection.query(sqlLocations, function (err, locations) {
                    if (err) {
                        console.log(err);
                        callback(err, null);
                    } else {
                        result.locations = locations;

                        var sqlSelectedLocationCoordinates = "SELECT latitude, longitude FROM location_coordinates WHERE location_id = " + connection.escape(rows[0].location_id);
                        connection.query(sqlSelectedLocationCoordinates, function (err, locationCoordinates) {
                            if (err) {
                                console.log(err);
                                callback(err, null);
                            } else {
                                result.selectedLocationCoordinates = locationCoordinates;
                                Project.getProjectName(projectID, function (err, projectName) {
                                    if (err) {
                                        callback(err, null);
                                    } else {
                                        result.projectName = projectName;

                                        User.getAllUsersFromCompany(companyID, function (err, members) {
                                            if (err) {
                                                callback(err, null);
                                            } else {
                                                result.members = members;

                                                User.getAllUsersForFlightplan(flightplanID, function (err, flightplanMembers) {
                                                    if (err) {
                                                        callback(err, null);
                                                    } else {
                                                        result.flightplanMembers = flightplanMembers;

                                                        User.getAllRoles(function (err, roles) {
                                                            if (err) {
                                                                callback(err, null);
                                                            } else {
                                                                result.roles = roles;

                                                                Battery.getAllBatteriesForCompany(companyID, function (err, batteries) {
                                                                    if (err) {
                                                                        callback(err, null);
                                                                    } else {
                                                                        result.batteries = batteries;

                                                                        Battery.getAllBatteriesForFlightplan(flightplanID, function (err, flightplanBatteries) {
                                                                            if (err) {
                                                                                callback(err, null);
                                                                            } else {

                                                                                result.flightplanBatteries = flightplanBatteries;

                                                                                Vehicle.getAllVehiclesForCompany(companyID, function (err, vehicles) {
                                                                                    if (err) {
                                                                                        callback(err, null);
                                                                                    } else {
                                                                                        result.vehicles = vehicles;

                                                                                        Vehicle.getAllVehiclesForFlightplan(flightplanID, function (err, flightplanVehicles) {
                                                                                            if (err) {
                                                                                                callback(err, null);
                                                                                            } else {

                                                                                                result.flightplanVehicles = flightplanVehicles;

                                                                                                Document.getDocumentTypes(function (err, documentTypes) {
                                                                                                    if (err) {
                                                                                                        callback(err, null);
                                                                                                    } else {
                                                                                                        result.documentTypes = documentTypes;

                                                                                                        Document.getAllDocumentsForCompany(companyID, function (err, documents) {
                                                                                                            if (err) {
                                                                                                                callback(err, null);
                                                                                                            } else {
                                                                                                                for (var i = 0; i < documents.length; i++) {
                                                                                                                    if (documents[i].user_name !== '') {
                                                                                                                        documents[i].user = documents[i].user_name;
                                                                                                                    } else if (documents[i].user_google_name !== '') {
                                                                                                                        documents[i].user = documents[i].user_google_name;
                                                                                                                    }

                                                                                                                    documents[i].size = general.humanFileSize(documents[i].size); documents[i].modified = moment(documents[i].modified).format(config.timeFormat);
                                                                                                                }

                                                                                                                result.documents = documents;

                                                                                                                Document.getAllDocumentsForFlightplan(flightplanID, function (err, flightplanDocuments) {
                                                                                                                    if (err) {
                                                                                                                        callback(err, null);
                                                                                                                    } else {
                                                                                                                        result.flightplanDocuments = flightplanDocuments;
                                                                                                                        callback(null, result);
                                                                                                                    }
                                                                                                                });
                                                                                                            }
                                                                                                        })
                                                                                                    }
                                                                                                });
                                                                                            }
                                                                                        });
                                                                                    }
                                                                                });
                                                                            }
                                                                        });
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        })
                    }
                });
            }
        });
    });
}

function modifyFlightplan(action, projectID, flightplanID, locationID, userID, data, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        // flightplans        
        var startTime = moment(new Date(data.start_time)).format(config.mysqlTimeFormat);
        var endTime = moment(new Date(data.end_time)).format(config.mysqlTimeFormat);

        if (action == 'add') {
            var sqlFlightplans = "INSERT INTO flightplans(project_id, location_id, name, group_name, start_time, end_time, notes, created_by) VALUES (" + connection.escape(projectID) + ", " + connection.escape(locationID) + ", " + connection.escape(data.name) + ", " + connection.escape(data.group_name) + ", " + connection.escape(startTime) + ", " + connection.escape(endTime) + ", " + connection.escape(data.notes) + ", " + connection.escape(userID) + ");";
        } else if (action == 'edit') {
            var sqlFlightplans = "UPDATE flightplans SET location_id = " + connection.escape(locationID) + ", name = " + connection.escape(data.name) + ", group_name = " + connection.escape(data.group_name) + ", start_time = " + connection.escape(startTime) + ", end_time = " + connection.escape(endTime) + ", notes = " + connection.escape(data.notes) + " WHERE id = " + connection.escape(flightplanID);
        }


        connection.query(sqlFlightplans, function (err, rowsFlightplans) {
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                if (action == 'add') {
                    flightplanID = rowsFlightplans.insertId;
                }

                // batteries
                var sqlBatteries = '';
                if (data.battery != undefined && data.battery.length > 0) {
                    sqlBatteries = 'INSERT INTO flightplan_batteries(flightplan_id, battery_id) VALUES ';
                    for (var i = 0; i < data.battery.length; i++) {
                        sqlBatteries += '(' + flightplanID + ', ' + data.battery[i] + '),'
                    }
                    sqlBatteries = sqlBatteries.substr(0, sqlBatteries.length - 1);
                } else {
                    sqlBatteries = 'SELECT * FROM batteries';
                }

                connection.query(sqlBatteries, function (err, rowsBatteries) {
                    if (err) {
                        console.log(err);
                        callback(err, null);
                    } else {
                        // vehicle
                        var unitID = 'd_' + (general.randomString(lengths.vehicleUnitID)).toLowerCase();

                        var sqlVehicle = "INSERT INTO flightplan_vehicles (flightplan_id, vehicle_id, unit_id) VALUES (" + flightplanID + ", " + data.vehicle + ", '" + unitID + "');"

                        connection.query(sqlVehicle, function (err, rowsVehicle) {
                            if (err) {
                                console.log(err);
                                callback(err, null);
                            } else {
                                var sqlTeam = "INSERT INTO flightplan_teams (flightplan_id) VALUES (" + flightplanID + ");";
                                connection.query(sqlTeam, function (err, rowsTeam) {
                                    if (err) {
                                        console.log(err);
                                        callback(err, null);
                                    } else {
                                        var teamID = rowsTeam.insertId;
                                        var sqlOK = false;

                                        var sqlTeamUsers = "INSERT INTO flightplan_team_users (team_id, user_id, role_id, unit_id) VALUES ";
                                        if (data.manager != '') {
                                            var managerUnitID = 't_' + (general.randomString(lengths.teamUserUnitID)).toLowerCase();
                                            sqlTeamUsers += '(' + teamID + ', ' + data.manager + ', ' + '1, ' + "'" + managerUnitID + "'" + '),';
                                            sqlOK = true;
                                        }

                                        if (data.pilot != '') {
                                            var pilotUnitID = 't_' + (general.randomString(lengths.teamUserUnitID)).toLowerCase();
                                            sqlTeamUsers += '(' + teamID + ', ' + data.pilot + ', ' + '2, ' + "'" + pilotUnitID + "'" + '),';
                                            sqlOK = true;
                                        }
                                        if (data.observer != '') {
                                            var observerUnitID = 't_' + (general.randomString(lengths.teamUserUnitID)).toLowerCase();
                                            sqlTeamUsers += '(' + teamID + ', ' + data.observer + ', ' + '3, ' + "'" + observerUnitID + "'" + '),';
                                            sqlOK = true;
                                        }
                                        if (data.payload_operator != '') {
                                            var payloadOperatorUnitID = 't_' + (general.randomString(lengths.teamUserUnitID)).toLowerCase();
                                            sqlTeamUsers += '(' + teamID + ', ' + data.payload_operator + ', ' + '4, ' + "'" + payloadOperatorUnitID + "'" + '),';
                                            sqlOK = true;
                                        }
                                        if (data.remote_observer != '') {
                                            var remoteObserverUnitID = 't_' + (general.randomString(lengths.teamUserUnitID)).toLowerCase();
                                            sqlTeamUsers += '(' + teamID + ', ' + data.remote_observer + ', ' + '5, ' + "'" + remoteObserverUnitID + "'" + '),';
                                            sqlOK = true;
                                        }

                                        if (sqlOK) {
                                            sqlTeamUsers = sqlTeamUsers.substr(0, sqlTeamUsers.length - 1);
                                            connection.query(sqlTeamUsers, function (err, rowsTeamUsers) {
                                                if (err) {
                                                    console.log(err);
                                                    callback(err, null);
                                                } else {

                                                    // documents
                                                    if (data.documents != undefined && data.documents.length > 0) {
                                                        var sqlDocuments = 'INSERT INTO flightplan_documents(flightplan_id, document_id) VALUES ';
                                                        for (var i = 0; i < data.documents.length; i++) {
                                                            sqlDocuments += '(' + flightplanID + ', ' + data.documents[i] + '),'
                                                        }
                                                        sqlDocuments = sqlDocuments.substr(0, sqlDocuments.length - 1);

                                                        connection.query(sqlDocuments, function (err, rowsDocuments) {
                                                            if (err) {
                                                                console.log(err);
                                                                callback(err, null);
                                                            } else {
                                                                connection.release();
                                                                callback(null, 'ok');
                                                            }
                                                        });

                                                    } else {
                                                        connection.release();
                                                        callback(null, 'ok');
                                                    }
                                                }
                                            });
                                        } else {
                                            // documents
                                            if (data.documents != undefined && data.documents.length > 0) {
                                                var sqlDocuments = 'INSERT INTO flightplan_documents(flightplan_id, document_id) VALUES ';
                                                for (var i = 0; i < data.documents.length; i++) {
                                                    sqlDocuments += '(' + flightplanID + ', ' + data.documents[i] + '),'
                                                }
                                                sqlDocuments = sqlDocuments.substr(0, sqlDocuments.length - 1);

                                                connection.query(sqlDocuments, function (err, rowsDocuments) {
                                                    if (err) {
                                                        console.log(err);
                                                        callback(err, null);
                                                    } else {
                                                        connection.release();
                                                        callback(null, 'ok');
                                                    }
                                                });

                                            } else {
                                                connection.release();
                                                callback(null, 'ok');
                                            }
                                        }
                                    }
                                })
                            }
                        });
                    }
                });
            }
        });
    });
}

module.exports.add = function (projectID, locationID, userID, data, callback) {
    modifyFlightplan('add', projectID, '', locationID, userID, data, function (err, done) {
        if (err) {
            callback(err, null);
        } else {
            callback(null, done);
        }
    });
}


module.exports.edit = function (projectID, flightplanID, locationID, userID, data, callback) {
    deleteFlightplanData(flightplanID, function (err, done) {
        if (err) {
            callback(err, null);
        } else {
            if (done) {
                modifyFlightplan('edit', projectID, flightplanID, locationID, userID, data, function (err, done) {
                    if (err) {
                        callback(err, null);
                    } else {
                        if (done) {
                            callback(null, done);
                        }
                    }
                });
            }
        }
    });
}

// DELETE
function deleteFlightplanData(id, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sqlBatteries = "DELETE FROM flightplan_batteries WHERE flightplan_id = " + id + ";";
        connection.query(sqlBatteries, function (err, rowsBatteries) {
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                var sqlVehicles = "DELETE FROM flightplan_vehicles WHERE flightplan_id = " + id + ";";
                connection.query(sqlVehicles, function (err, rowsVehicles) {
                    if (err) {
                        console.log(err);
                        callback(err, null);
                    } else {
                        var sqlDocuments = "DELETE FROM flightplan_documents WHERE flightplan_id = " + id + ";";
                        connection.query(sqlDocuments, function (err, rowsDocuments) {
                            if (err) {
                                console.log(err);
                                callback(err, null);
                            } else {
                                var sqlComments = "DELETE FROM flightplan_comments WHERE flightplan_id = " + id + ";";
                                connection.query(sqlComments, function (err, rowsComments) {
                                    if (err) {
                                        console.log(err);
                                        callback(err, null);
                                    } else {
                                        var sqlTeamUsers = "DELETE FROM flightplan_team_users WHERE team_id = (SELECT id FROM flightplan_teams WHERE flightplan_id = " + id + ")";
                                        connection.query(sqlTeamUsers, function (err, rowsTeamUsers) {
                                            if (err) {
                                                console.log(err);
                                                callback(err, null);
                                            } else {
                                                var sqlTeams = "DELETE FROM flightplan_teams WHERE flightplan_id = " + id + ";";
                                                connection.query(sqlTeams, function (err, rowsTeams) {
                                                    if (err) {
                                                        console.log(err);
                                                        callback(err, null);
                                                    } else {
                                                        callback(null, true);
                                                    }
                                                });
                                            }
                                        })
                                    }
                                })
                            }
                        })
                    }
                })
            }
        })
    })
}

module.exports.delete = function (id, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);
        var sqlFlightplans = "DELETE FROM flightplans WHERE id = " + id + ";";
        connection.query(sqlFlightplans, function (err, rowsFlightplans) {
            connection.release();
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(null, rowsFlightplans.affectedRows);
            }
        })
    });
    
}





// PROJECT COMMENTS
module.exports.getFlightplanComments = function (id, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "SELECT flightplan_comments.content, flightplan_comments.created AS timestamp, users.name, users.photo, users.google_photo, users.google_display_name, user_roles.name AS role, user_roles.class AS role_class FROM flightplan_comments LEFT JOIN users ON users.id = flightplan_comments.user_id LEFT JOIN user_roles ON user_roles.id = flightplan_comments.role_id WHERE flightplan_comments.flightplan_id = " + id;

        connection.query(sql, function (err, rows) {
            connection.release();
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(null, JSON.stringify(rows));
            }
        });
    });
};

module.exports.addComment = function (data, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);
        data.created = moment(new Date()).format("YYYY-MM-DD HH:mm:ss");

        var sql = "INSERT INTO flightplan_comments(flightplan_id, user_id, content, created, role_id) VALUES('" + data.flightplanID + "', '" + data.userID + "', '" + data.comment + "', '" + data.created + "', '" + data.roleID + "')";


        connection.query(sql, function (err, rows) {
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                var sqlRole = "SELECT name, class FROM user_roles WHERE id = " + data.roleID;
                connection.query(sqlRole, function (err, rowsRole) {
                    connection.release();
                    if (err) {
                        console.log(err);
                        callback(err, null);
                    } else {
                        data.roleName = rowsRole[0].name;
                        data.roleClass = rowsRole[0].class;
                        data.created = moment(data.created).format(config.timeFormat);
                        callback(null, data);
                    }
                })

            }
        });
    });
};

// LOCATIONS
module.exports.addLocation = function (companyID, projectID, name, coordinates, base64Data, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var locationID = 0;

        var imageTypeRegExp = /\/(.*?)$/;
        var crypto = require('crypto');
        var seed = crypto.randomBytes(20);
        var randomString = crypto.createHash('sha1').update(seed).digest('hex');
        var imageBuffer = general.decodeBase64Image(base64Data);
        var uploadFolder = __dirname + '/../public/uploads/locations/';
        var imageName = 'snapshot_' + randomString;
        var thumbName = 'snapshot_' + randomString + '_thumb';
        var imageTypeDetected = imageBuffer.type.match(imageTypeRegExp);
        var extension = imageTypeDetected[1];
        var imagePath = uploadFolder + imageName + '.' + extension;
        var thumbPath = uploadFolder + thumbName + '.' + extension;

        fs.writeFile(imagePath, imageBuffer.data, function (err) {
            if (err) {
                console.log(err);
            } else {

                var s3 = new aws.S3();
                var fileBuffer = fs.readFileSync(imagePath);
                var metaData = general.getImageContentType(imagePath);
                var key = companyID + '/locations/' + imageName + '.' + extension;
                var thumbKey = companyID + '/locations/' + thumbName + '.' + extension;

                s3.putObject({
                    ACL: 'public-read',
                    Bucket: configAWS.bucketName,
                    Key: key,
                    Body: fileBuffer,
                    ContentType: metaData
                }, function (err, response) {
                    if (err) {
                        console.log(err);
                        callback(err, null);
                    } else {
                        gm(uploadFolder + imageName + '.' + extension)
                        .resize(config.thumbW, config.thumbH)
                        .write(uploadFolder + thumbName + '.' + extension, function (err) {
                            if (err) {
                                console.log(err);
                                callback(err, null);
                            } else {
                                fs.readFile(thumbPath, function (err, thumbBuffer) {
                                    s3.putObject({
                                        ACL: 'public-read',
                                        Bucket: configAWS.bucketName,
                                        Key: thumbKey,
                                        Body: thumbBuffer,
                                        ContentType: metaData
                                    }, function (err, response) {
                                        if (err) {
                                            console.log(err);
                                            callback(err, null);
                                        } else {
                                            fs.exists(imagePath, function (exists) {
                                            if (exists) {
                                                fs.unlink(imagePath);
                                            }
                                            });
                                            fs.exists(thumbPath, function (exists) {
                                                if (exists) {
                                                    fs.unlink(thumbPath);
                                                }
                                            });

                                            var centerLocation = general.getMapCenter(coordinates);

                                            var sqlLocation = "INSERT INTO locations (project_id, name, image, thumb, path, path_thumb, center_latitude, center_longitude) VALUES (" + connection.escape(projectID) + ", " + connection.escape(name) + ", " + connection.escape(imageName + '.' + extension) + ", " + connection.escape(thumbName + '.' + extension) + ", " + connection.escape(sample.s3Path + key) + ", " + connection.escape(sample.s3Path + thumbKey) + ", " + connection.escape(centerLocation[0]) + ", " + connection.escape(centerLocation[1]) + ")";

                                            connection.query(sqlLocation, function (err, rowsLocation) {
                                                if (err) {
                                                    console.log(err);
                                                    callback(err, null);
                                                } else {
                                                    locationID = rowsLocation.insertId;
                                                    var sqlLocationCoordinates = "INSERT INTO location_coordinates (location_id, latitude, longitude) VALUES ";
                                                    for (var i = 0; i < coordinates.length; i++) {
                                                        sqlLocationCoordinates += "(" + connection.escape(locationID) + ", " + connection.escape(coordinates[i][0]) + ", " + connection.escape(coordinates[i][1]) + "), ";
                                                    }
                                                    sqlLocationCoordinates = sqlLocationCoordinates.substr(0, sqlLocationCoordinates.length - 2);

                                                    connection.query(sqlLocationCoordinates, function (err, rowsLocationCoordinates) {
                                                        connection.release();
                                                        if (err) {
                                                            console.log(err);
                                                            callback(err, null);
                                                        } else {
                                                            callback(null, locationID);
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                });    
                            }
                        });
                    }
                });
            }
        })
    });
}

module.exports.editLocation = function (companyID, locationID, coordinates, base64Data, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sqlOldLocation = "SELECT image, thumb FROM locations WHERE id = " + connection.escape(locationID);
        connection.query(sqlOldLocation, function (err, oldImage) {
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                var s3 = new aws.S3();
                var oldKey = companyID + '/locations/' + oldImage[0].image;
                var oldThumbKey = companyID + '/locations/' + oldImage[0].thumb;
                
                var deleteParams = {
                    Bucket: configAWS.bucketName,
                    Delete: {
                        Objects: [
                            { Key: oldKey },
                            { Key: oldThumbKey }
                        ]
                    }
                };

                s3.deleteObjects(deleteParams, function (err, data) {
                    if (err) {
                        console.log(err);
                        callback(err, null);
                    } else {
                        var imageTypeRegExp = /\/(.*?)$/;
                        var crypto = require('crypto');
                        var seed = crypto.randomBytes(20);
                        var randomString = crypto.createHash('sha1').update(seed).digest('hex');
                        var imageBuffer = general.decodeBase64Image(base64Data);
                        var uploadFolder = __dirname + '/../public/uploads/locations/';
                        var imageName = 'snapshot_' + randomString;
                        var thumbName = 'snapshot_' + randomString + '_thumb';
                        var imageTypeDetected = imageBuffer.type.match(imageTypeRegExp);
                        var extension = imageTypeDetected[1];
                        var imagePath = uploadFolder + imageName + '.' + extension;
                        var thumbPath = uploadFolder + thumbName + '.' + extension;

                        fs.writeFile(imagePath, imageBuffer.data, function (err) {
                            if (err) {
                                console.log(err);
                                callback(err, null);
                            } else {
                                var fileBuffer = fs.readFileSync(imagePath);
                                var metaData = general.getImageContentType(imagePath);
                                var key = companyID + '/locations/' + imageName + '.' + extension;
                                var thumbKey = companyID + '/locations/' + thumbName + '.' + extension;


                                s3.putObject({
                                    ACL: 'public-read',
                                    Bucket: configAWS.bucketName,
                                    Key: key,
                                    Body: fileBuffer,
                                    ContentType: metaData
                                }, function (err, response) {
                                    if (err) {
                                        console.log(err);
                                        callback(err, null);
                                    } else {
                                        gm(uploadFolder + imageName + '.' + extension)
                                        .resize(config.thumbW, config.thumbH)
                                        .write(uploadFolder + thumbName + '.' + extension, function (err) {
                                            if (err) {
                                                console.log(err);
                                                callback(err, null);
                                            } else {
                                                fs.readFile(thumbPath, function (err, thumbBuffer) {
                                                    s3.putObject({
                                                        ACL: 'public-read',
                                                        Bucket: configAWS.bucketName,
                                                        Key: thumbKey,
                                                        Body: thumbBuffer,
                                                        ContentType: metaData
                                                    }, function (err, response) {
                                                        if (err) {
                                                            console.log(err);
                                                            callback(err, null);
                                                        } else {
                                                            fs.exists(imagePath, function (exists) {
                                                                if (exists) {
                                                                    fs.unlink(imagePath);
                                                                }
                                                            });
                                                            fs.exists(thumbPath, function (exists) {
                                                                if (exists) {
                                                                    fs.unlink(thumbPath);
                                                                }
                                                            });




                                                            var sqlLocation = "UPDATE locations SET image = " + connection.escape(imageName + '.' + extension) + ", path = " + connection.escape(sample.s3Path + key) + ", thumb = " + connection.escape(thumbName + '.' + extension) + ", path_thumb = " + connection.escape(sample.s3Path + thumbKey) + " WHERE id = " + connection.escape(locationID);

                                                            connection.query(sqlLocation, function (err, rowsLocation) {
                                                                if (err) {
                                                                    console.log(err);
                                                                    callback(err, null);
                                                                } else {
                                                                    var sqlDeleteOldCoordinates = "DELETE FROM location_coordinates WHERE location_id = " + connection.escape(locationID);
                                                                    connection.query(sqlDeleteOldCoordinates, function (err, rowsDeleteCoordinates) {
                                                                        if (err) {
                                                                            console.log(err);
                                                                            callback(err, null);
                                                                        } else {
                                                                            var sqlLocationCoordinates = "INSERT INTO location_coordinates (location_id, latitude, longitude) VALUES ";
                                                                            for (var i = 0; i < coordinates.length; i++) {
                                                                                sqlLocationCoordinates += "(" + connection.escape(locationID) + ", " + connection.escape(coordinates[i][0]) + ", " + connection.escape(coordinates[i][1]) + "), ";
                                                                            }
                                                                            sqlLocationCoordinates = sqlLocationCoordinates.substr(0, sqlLocationCoordinates.length - 2);

                                                                            connection.query(sqlLocationCoordinates, function (err, rowsLocationCoordinates) {
                                                                                connection.release();
                                                                                if (err) {
                                                                                    console.log(err);
                                                                                    callback(err, null);
                                                                                } else {
                                                                                    callback(null, locationID);
                                                                                }
                                                                            });
                                                                        }
                                                                    })
                                                                }
                                                            });
                                                        }
                                                    });
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        })
                    }
                });
            }
        })
    });
}

module.exports.getAllLocations = function (projectID, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "SELECT id, name, path FROM locations WHERE project_id = " + connection.escape(projectID);

        connection.query(sql, function (err, rows) {
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(null, rows);
            }
        });
    });
}

module.exports.getLocationCoordinates = function (locationID, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "SELECT latitude, longitude FROM location_coordinates WHERE location_id = " + connection.escape(locationID);
        connection.query(sql, function (err, rows) {
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(null, rows);
            }
        })
    });
}


module.exports.changeVehicleID = function (flightplanID, vehicleID, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);
        var sql = "SELECT vehicle_id FROM flightplan_vehicles WHERE flightplan_id = " + connection.escape(flightplanID);
        connection.query(sql, function (err, rows) {
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                var sqlUpdate = "UPDATE flightplan_vehicles SET unit_id = " + connection.escape(vehicleID) + " WHERE flightplan_id = " + connection.escape(flightplanID) + " AND vehicle_id = " + connection.escape(rows[0].vehicle_id);
                connection.query(sqlUpdate, function (err, rowsUpdate) {
                    if (err) {
                        console.log(err);
                        callback(err, null);
                    } else {
                        connection.release();
                        callback(null, rowsUpdate.affectedRows);
                    }
                })
            }
        })
    });
}

module.exports.changeMemberID = function (flightplanID, newMemberID, userID, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);
        var sqlTeam = "SELECT id FROM flightplan_teams WHERE flightplan_id = " + connection.escape(flightplanID) + " LIMIT 1";
        connection.query(sqlTeam, function (err, rows) {
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                var teamID = rows[0].id;
                var sqlSearchDuplicates = "SELECT COUNT(id) AS num FROM flightplan_team_users WHERE team_id = " + connection.escape(teamID) + " AND unit_id = " + connection.escape(newMemberID) + " AND user_id != " + connection.escape(userID);
                connection.query(sqlSearchDuplicates, function (err, rowsDuplicates) {
                    if (err) {
                        console.log(err);
                        callback(err, null);
                    } else {
                        if (rowsDuplicates[0].num > 0) {
                            callback(err, 'duplicate');
                        } else {
                            var sqlUpdate = "UPDATE flightplan_team_users SET unit_id = " + connection.escape(newMemberID) + " WHERE team_id = " + connection.escape(teamID) + " AND user_id = " + connection.escape(userID);
                            connection.query(sqlUpdate, function (err, rowsUpdate) {
                                if (err) {
                                    console.log(err);
                                    callback(err, null);
                                } else {
                                    connection.release();
                                    callback(null, rowsUpdate.affectedRows);
                                }
                            })
                        }
                    }
                })
            }
        });
    });
}

module.exports.checkFlightplanForReport = function (flightplanID, callback) {
    var vehicle = false;
    var pilot = false;

    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sqlVehicle = "SELECT vehicles.id, vehicles.faa_aircraft_id FROM flightplan_vehicles JOIN vehicles ON flightplan_vehicles.vehicle_id = vehicles.id WHERE flightplan_vehicles.flightplan_id = " + connection.escape(flightplanID);

        connection.query(sqlVehicle, function (err, rowsVehicle) {
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                if (rowsVehicle[0].faa_aircraft_id != '') {
                    vehicle = true;
                } 

                var sqlTeam = "SELECT id FROM flightplan_teams WHERE flightplan_id = " + connection.escape(flightplanID);
                connection.query(sqlTeam, function (err, rowsTeam) {
                    if (err) {
                        console.log(err);
                        callback(err, null);
                    } else {
                        if (rowsTeam.length > 0) {
                            var sqlPilot = "SELECT user_id FROM flightplan_team_users WHERE team_id = " + connection.escape(rowsTeam[0].id) + " AND role_id = 2";

                            connection.query(sqlPilot, function (err, rowsPilot) {
                                if (err) {
                                    console.log(err);
                                    callback(err, null);
                                } else {
                                    if (rowsPilot.length > 0) {
                                        pilot = true;
                                    }

                                    connection.release();
                                    
                                    if (vehicle && pilot) {
                                        callback(null, { message: 'ok' });
                                    } else if (vehicle && !pilot) {
                                        callback(null, { message: 'pilot' });
                                    } else if (!vehicle && pilot) {
                                        callback(null, { message: 'vehicle', id: rowsVehicle[0].id });
                                    } else {
                                        callback(null, { message: 'all', id: rowsVehicle[0].id });
                                    }
                                }
                            });
                        } else {
                            connection.release();
                            if (vehicle) {
                                callback(null, { message: 'pilot' });
                            } else {
                                callback(null, { message: 'all', id: rowsVehicle[0].id });
                            }
                        }
                    }
                });
            }
        })
    });
}

module.exports.getFlightplanInfoForReport = function (flightplanID, callback) {
    var result = {};
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sqlVehicle = "SELECT vehicles.faa_aircraft_id FROM flightplan_vehicles JOIN vehicles ON flightplan_vehicles.vehicle_id = vehicles.id WHERE flightplan_vehicles.flightplan_id = " + connection.escape(flightplanID);

        connection.query(sqlVehicle, function (err, rowsVehicle) {
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                result.faa_aircraft_id = rowsVehicle[0].faa_aircraft_id;

                var sqlFlightplan = "SELECT flightplans.name, flightplans.start_time, flightplans.end_time, flightplans.location_id, locations.path FROM flightplans LEFT JOIN locations ON flightplans.location_id = locations.id WHERE flightplans.id = " + connection.escape(flightplanID);

                connection.query(sqlFlightplan, function (err, rowsFlightplan) {
                    if (err) {
                        console.log(err);
                        callback(err, null);
                    } else {
                        result.date = moment().format(config.usTimeFormat);
                        result.flightplan_name = rowsFlightplan[0].name;
                        result.flightplan_start = moment(rowsFlightplan[0].start_time).format(config.usTimeFormat);
                        result.flightplan_end = moment(rowsFlightplan[0].end_time).format(config.usTimeFormat);
                        result.path = rowsFlightplan[0].path;

                        var sqlLocationPoints = "SELECT latitude, longitude FROM location_coordinates WHERE location_id = " + connection.escape(rowsFlightplan[0].location_id);

                        connection.query(sqlLocationPoints, function (err, rowsLocationPoints) {
                            if (err) {
                                console.log(err);
                                callback(err, null);
                            } else {
                                var lpString = '';
                                for (var i = 0; i < rowsLocationPoints.length; i++){
                                    var point = general.ddToDms(rowsLocationPoints[i].longitude, rowsLocationPoints[i].latitude);
                                    point = point.replace(/d/g, '');
                                    point = point.replace(/m/g, '');
                                    point = point.replace(/s/g, '');
                                    lpString += point + ' ';
                                }
                                lpString = lpString.substr(0, lpString.length - 1);
                                result.locationPoints = lpString;

                                var sqlPilotID = "SELECT flightplan_team_users.user_id FROM flightplan_team_users WHERE flightplan_team_users.team_id = (SELECT flightplan_teams.id FROM flightplan_teams WHERE flightplan_id = " + connection.escape(flightplanID) + ")";

                                connection.query(sqlPilotID, function (err, rowsPilotID) {
                                    if (err) {
                                        console.log(err);
                                        callback(err, null);
                                    } else {
                                        var sqlContact = "SELECT id, provider, name, google_display_name, mobile, email, google_email FROM users WHERE id = " + connection.escape(rowsPilotID[0].user_id);

                                        connection.query(sqlContact, function (err, rowsContact) {
                                            connection.release();
                                            if (err) {
                                                console.log(err);
                                                callback(err, null);
                                            } else {
                                                var provider = rowsContact[0].provider;
                                                
                                                var n = '';
                                                var e = '';
                                                var p = 'N/A';

                                                if (provider == 'local') {
                                                    n = rowsContact[0].name;
                                                    e = rowsContact[0].email;
                                                } else if (provider == 'google') {
                                                    n = rowsContact[0].google_display_name;
                                                    e = rowsContact[0].google_email;
                                                }
                                                
                                                if (rowsContact[0].mobile != null && rowsContact[0].mobile != undefined && rowsContact[0].mobile != '') {
                                                    p = rowsContact[0].mobile;
                                                }
                                                                                           
                                                result.contact = {
                                                    id: rowsContact[0].id,
                                                    name: n,
                                                    phone: p,
                                                    email: e
                                                }
                                                callback(null, result);
                                            }
                                        })
                                    }
                                })
                            }
                        })
                    }
                });
            }
        });
    });
}
