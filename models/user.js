var db = require('../config/db').pool;
var connectionError = require('../config/db').connectionError;
var authentication = require('../helpers/authentication');
var general = require('../helpers/general');
var config = require('../config/config');
var providers = require('../config/config').providers;
var lengths = require('../config/config').lengths;
var timeFormat = require('../config/config').config.timeFormat;
var mysqlTimeFormat = require('../config/config').config.mysqlTimeFormat;
var moment = require('moment');
var sample = require('../config/config').sampleData;
var fs = require('fs');
var aws = require('aws-sdk');
var configAWS = require('../config/config').aws;
var uuid = require('node-uuid');
var plans = require('../config/config').plans;
var bucket = configAWS.bucketName;

var downloader = require('s3-download')(new aws.S3({
    accessKeyId: configAWS.key,
    secretAccessKey: configAWS.secret
}));

module.exports.getUserByID = function (id, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "SELECT * FROM users WHERE id = '" + id + "';";

        connection.query(sql, function (err, rows) {
            connection.release();
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(null, rows[0]);
            }
        });
    });
};

module.exports.getUserFromSession = function (id, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "SELECT users.*, companies.name AS company_name, companies.email AS company_email, companies.access_code AS access_code, companies.data_set AS company_set, companies.plan AS plan, companies.deleted AS deleted, companies.created AS company_created, user_permissions.name AS permission FROM users JOIN companies ON companies.id = users.company_id LEFT JOIN user_permissions ON user_permissions.id = users.permission_id WHERE users.id = '" + id + "';";

        connection.query(sql, function (err, rows) {
            connection.release();
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(null, rows[0]);
            }
        });
    });
};

module.exports.getUserByEmail = function (email, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "SELECT users.*, companies.deleted AS deleted FROM users LEFT JOIN companies ON users.company_id = companies.id WHERE users.email = '" + email + "';";

        connection.query(sql, function (err, rows) {
            connection.release();
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(null, rows[0]);
            }
        });
    });
};

module.exports.getUserNameByID = function (id, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "SELECT name, google_display_name FROM users WHERE id = '" + id + "';";

        connection.query(sql, function (err, rows) {
            connection.release();

            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                if (rows[0].name != '' && rows[0].name != undefined && rows[0].name != 'null') {
                    callback(null, rows[0].name);
                } else if (rows[0].google_display_name != '' && rows[0].google_display_name != undefined && rows[0].google_display_name != 'null') {
                    callback(null, rows[0].google_display_name);
                }
            }
        });
    });
};


module.exports.isEmailAvailable = function (email, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "SELECT COUNT(*) AS num FROM users WHERE email ='" + email + "';";

        connection.query(sql, function (err, rows) {
            connection.release();
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(null, rows[0].num);
            }
        });
    });
};

module.exports.isCompanyNameAvailable = function (name, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "SELECT COUNT(*) AS num FROM companies WHERE name ='" + name + "';";

        connection.query(sql, function (err, rows) {
            connection.release();
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(null, rows[0].num);
            }
        });
    });
};

module.exports.createUser = function (accessCode, email, password, name, company, plan, customerID, subscriptionID, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var set = 1;
        if (plan == 'professional') {

            var sqlSubscription = "SELECT id FROM subscriptions WHERE customer_id = " + connection.escape('cus_' + customerID) + " AND subscription_id = " + connection.escape('sub_' + subscriptionID) + " AND subscription_plan = 'professional' LIMIT 1";

            connection.query(sqlSubscription, function (err, rowsSubscription) {
                if (err) {
                    console.log(err);
                } else {
                    var subscriptionID = rowsSubscription[0].id;

                    var sqlCompany = "INSERT INTO companies(name, access_code, data_set, email, plan, subscription_id) VALUES (" + connection.escape(company) + ", " + connection.escape(accessCode) + ", " + connection.escape(set) + ", " + connection.escape(email) + ", " + connection.escape(plan.toUpperCase()) + ", " + connection.escape(subscriptionID) + ");";

                    connection.query(sqlCompany, function (err, rowsCompany) {
                        if (err) {
                            console.log(err);
                            callback(err, null, null);
                        } else {
                            // create user                
                            var companyID = rowsCompany.insertId;
                            var activationKey = general.randomString(40);

                            var sqlUser = "INSERT INTO users(company_id, permission_id, provider, email, password, name, activation_key) VALUES ('" + companyID + "', 1, 'local', '" + email + "', '" + password + "', '" + name + "', '" + activationKey + "');";

                            connection.query(sqlUser, function (err, rowsUser) {
                                connection.release();
                                if (err) {
                                    console.log(err);
                                    callback(err, null, null);
                                } else {
                                    var userID = rowsUser.insertId;
                                    // TESTING
                                    //callback(null, userID, activationKey);



                                    createSampleData(companyID, userID, name, function (err, done) {
                                        if (err) {
                                            console.log(err);
                                            callback(err, null, null);
                                        } else {
                                            if (done) {
                                                callback(null, userID, activationKey);
                                            }
                                        }
                                    });

                                }
                            });
                        }
                    });

                }
            });
        } else if (plan == 'free') {
            var sqlCompany = "INSERT INTO companies(name, access_code, data_set, email, plan) VALUES (" + connection.escape(company) + ", " + connection.escape(accessCode) + ", " + connection.escape(set) + ", " + connection.escape(email) + ", " + connection.escape(plan.toUpperCase()) + ");";

            connection.query(sqlCompany, function (err, rowsCompany) {
                if (err) {
                    console.log(err);
                    callback(err, null, null);
                } else {
                    // create user                
                    var companyID = rowsCompany.insertId;
                    var activationKey = general.randomString(40);

                    var sqlUser = "INSERT INTO users(company_id, permission_id, provider, email, password, name, activation_key) VALUES ('" + companyID + "', 1, 'local', '" + email + "', '" + password + "', '" + name + "', '" + activationKey + "');";

                    connection.query(sqlUser, function (err, rowsUser) {
                        connection.release();
                        if (err) {
                            console.log(err);
                            callback(err, null, null);
                        } else {
                            var userID = rowsUser.insertId;

                            // TESTING
                            //callback(null, userID, activationKey);

                            createSampleData(companyID, userID, name, function (err, done) {
                                if (err) {
                                    console.log(err);
                                    callback(err, null, null);
                                } else {
                                    if (done) {
                                        callback(null, userID, activationKey);
                                    }
                                }
                            });

                        }
                    });
                }
            });
        }
    });
};

module.exports.inviteUser = function (data, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        // insert company data
        var invitationKey = general.randomString(40);
        var sql = "INSERT INTO users(company_id, permission_id, provider, email, name, photo, invited, invitation_key, invited_by, help, sample) VALUES (" + data.company_id + ", " + data.permission_id + ", 'local', '" + data.email + "', '" + data.name + "', '" + data.photo + "', 1, '" + invitationKey + "', '" + data.invited_by + "', " + data.help + ", " + data.sample + ");";

        connection.query(sql, function (err, rows) {
            connection.release();
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                var userID = rows.insertId;
                callback(null, invitationKey);
                /*createSampleData(data.company_id, userID, data.name, function (err, done) {
                    if (err) {
                        console.log(err);
                        callback(err, null, null);
                    } else {
                        if (done) {
                            callback(null, invitationKey);
                        }
                    }
                });*/
            }
        });
    });
};

module.exports.updateUser = function (user, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "UPDATE users SET " +
            "company_id = " + user.company_id + ", " +
            "provider = '" + setValue(user.provider) + "', " +
            "name = '" + setValue(user.name) + "', " +
            "email = '" + setValue(user.email) + "', " +
            "password = '" + setValue(user.password) + "', " +
            "mobile = '" + setValue(user.mobile) + "', " +
            "photo = '" + setValue(user.photo) + "', " +
            "active = " + user.active + ", " +
            "activation_key = '" + setValue(user.activation_key) + "', " +
            "reset_key = '" + setValue(user.reset_key) + "', " +
            "google_id = '" + setValue(user.google_id) + "', " +
            "google_token = '" + setValue(user.google_token) + "', " +
            "google_display_name = '" + setValue(user.google_display_name) + "', " +
            "google_family_name = '" + setValue(user.google_family_name) + "', " +
            "google_given_name = '" + setValue(user.google_given_name) + "', " +
            "google_middle_name = '" + setValue(user.google_middle_name) + "', " +
            "google_email = '" + setValue(user.google_email) + "', " +
            "google_email_type = '" + setValue(user.google_email_type) + "', " +
            "google_photo = '" + setValue(user.google_photo) + "'" +
            " WHERE id = '" + user.id + "'; ";

        connection.query(sql, function (err, rows) {
            connection.release();
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(null, rows.affectedRows);
            }
        });
    });
};

function setValue(value) {
    var result = '';
    if (value !== '') {
        result = value;
    }
    return result;
}

module.exports.activateUser = function (key, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "SELECT id, active FROM users WHERE activation_key = " + connection.escape(key);

        connection.query(sql, function (err, rows) {
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                // already activated account
                if (rows[0].active === 1) {
                    callback(null, 'active');
                } else {
                    var sqlActivateUser = "UPDATE users SET active = 1 WHERE id = '" + rows[0].id + "';";
                    connection.query(sqlActivateUser, function (err, rowsActivateUser) {
                        connection.release();
                        if (err) {
                            console.log(err);
                            callback(err, null);
                        } else {
                            callback(null, 'ok');
                        }
                    });
                }
            }
        });
    });
};

module.exports.activateInvitedUser = function (key, password, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "SELECT * FROM users WHERE invitation_key = '" + key + "';";

        connection.query(sql, function (err, rows) {
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                // already activated account
                if (rows[0].invited === 0) {
                    callback(null, 'active');
                } else {
                    var sqlActivateInvitedUser = "UPDATE users SET invited = '0', active = '1', password = '" + password + "' WHERE invitation_key = '" + rows[0].invitation_key + "';";

                    connection.query(sqlActivateInvitedUser, function (err, rows) {
                        connection.release();
                        if (err) {
                            console.log(err);
                            callback(err, null);
                        } else {
                            callback(null, 'ok');
                        }
                    });
                }
            }
        });
    });
}

module.exports.reActivateUser = function (email, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var activationKey = general.randomString(40);
        var sql = "UPDATE users SET activation_key = '" + activationKey + "', active = '0' WHERE email = '" + email + "';";

        connection.query(sql, function (err, rows) {
            connection.release();
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(null, activationKey);
            }
        });
    });
};

module.exports.setResetKey = function (email, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var resetKey = general.randomString(40);
        var sql = "UPDATE users SET reset_key = '" + resetKey + "' WHERE email = '" + email + "';";

        connection.query(sql, function (err, rows) {
            connection.release();
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(null, resetKey);
            }
        });
    });
};

module.exports.resetPassword = function (key, password, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "UPDATE users SET reset_key = '', password = '" + password + "' WHERE reset_key = '" + key + "';";

        connection.query(sql, function (err, rows) {
            connection.release();
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(null, 'ok');
            }

        });
    });
};


module.exports.getUserByProviderID = function (provider, id, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "SELECT * FROM users WHERE provider = '" + provider + "' AND " + provider + "_id = '" + id + "';";

        connection.query(sql, function (err, rows) {
            connection.release();
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(null, rows[0]);
            }
        });
    });
};

module.exports.getUserByUnknownProviderEmail = function (email, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var where = '';
        for (var i = 0; i < providers.length; i++) {
            where += "(provider = '" + providers[i] + "' AND " + providers[i] + "_email = '" + email + "')";
            if (i < providers.length - 1) {
                where += ' OR ';
            }
        }

        var sql = "SELECT * FROM users WHERE " + where;

        connection.query(sql, function (err, rows) {
            connection.release();
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(null, rows[0]);
            }
        });
    });
};

module.exports.createUserByProvider = function (provider, accessCode, profile, token, plan, customerID, subscriptionID, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        if (plan == 'professional') {

            var sqlSubscription = "SELECT id FROM subscriptions WHERE customer_id = " + connection.escape('cus_' + customerID) + " AND subscription_id = " + connection.escape('sub_' + subscriptionID) + " AND subscription_plan = 'professional' LIMIT 1";

            connection.query(sqlSubscription, function (err, rowsSubscription) {
                if (err) {
                    console.log(err);
                } else {
                    var subscriptionID = rowsSubscription[0].id;
                    var company = general.randomString(20); // temporary company name (for users registered non-local provider)
                    var set = 0;

                    var sqlCompany = "INSERT INTO companies(name, access_code, data_set, email, plan, subscription_id) VALUES (" + connection.escape(company) + ", " + connection.escape(accessCode) + ", " + connection.escape(set) + ", " + connection.escape(profile.emails[0].value) + ", " + connection.escape(plan.toUpperCase()) + ", " + connection.escape(subscriptionID) + ");";

                    connection.query(sqlCompany, function (err, rowsCompany) {
                        if (err) {
                            console.log(err);
                            callback(err, null, null);
                        } else {
                            // create user                
                            var companyID = rowsCompany.insertId;
                            var roleID = 1;
                            var active = 1;
                            var permissionID = 1;
                            var id = profile.id || '';
                            var displayName = profile.displayName || '';
                            var familyName = profile.name.familyName || '';
                            var givenName = profile.name.givenName || '';
                            var middleName = profile.name.middleName || '';
                            var email = profile.emails[0].value || '';
                            var emailType = profile.emails[0].type || '';
                            var photoFull = profile.photos[0].value || '';
                            var photo = photoFull.substring(0, photoFull.indexOf('?')) || '';

                            var sqlUser = "INSERT INTO users(provider, company_id, permission_id, active, " + provider + "_id, " + provider + "_token, " + provider + "_display_name, " + provider + "_family_name, " + provider + "_given_name, " + provider + "_middle_name, " + provider + "_email, " + provider + "_email_type, " + provider + "_photo) VALUES ('" + provider + "', '" + companyID + "', '" + permissionID + "', '" + active + "', '" + id + "', '" + token + "', '" + displayName + "', '" + familyName + "', '" + givenName + "', '" + middleName + "', '" + email + "', '" + emailType + "', '" + photo + "');";

                            connection.query(sqlUser, function (err, rows) {
                                connection.release();
                                if (err) {
                                    console.log(err);
                                    callback(err, null);
                                } else {
                                    //TESTING
                                    //callback(null, rows.insertId);
                                    createSampleData(companyID, rows.insertId, displayName, function (err, done) {
                                        if (err) {
                                            console.log(err);
                                            callback(err, null, null);
                                        } else {
                                            if (done) {
                                                callback(null, rows.insertId);
                                            }
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        } else if (plan == 'free') {
            var company = general.randomString(20); // temporary company name (for users registered non-local provider)
            var set = 0;

            var sqlCompany = "INSERT INTO companies(name, access_code, data_set, email, plan) VALUES (" + connection.escape(company) + ", " + connection.escape(accessCode) + ", " + connection.escape(set) + ", " + connection.escape(profile.emails[0].value) + ", " + connection.escape(plan.toUpperCase()) + ");";

            connection.query(sqlCompany, function (err, rowsCompany) {
                if (err) {
                    console.log(err);
                    callback(err, null, null);
                } else {
                    // create user                
                    var companyID = rowsCompany.insertId;
                    var roleID = 1;
                    var active = 1;
                    var permissionID = 1;
                    var id = profile.id || '';
                    var displayName = profile.displayName || '';
                    var familyName = profile.name.familyName || '';
                    var givenName = profile.name.givenName || '';
                    var middleName = profile.name.middleName || '';
                    var email = profile.emails[0].value || '';
                    var emailType = profile.emails[0].type || '';
                    var photoFull = profile.photos[0].value || '';
                    var photo = photoFull.substring(0, photoFull.indexOf('?')) || '';

                    var sqlUser = "INSERT INTO users(provider, company_id, permission_id, active, " + provider + "_id, " + provider + "_token, " + provider + "_display_name, " + provider + "_family_name, " + provider + "_given_name, " + provider + "_middle_name, " + provider + "_email, " + provider + "_email_type, " + provider + "_photo) VALUES ('" + provider + "', '" + companyID + "', '" + permissionID + "', '" + active + "', '" + id + "', '" + token + "', '" + displayName + "', '" + familyName + "', '" + givenName + "', '" + middleName + "', '" + email + "', '" + emailType + "', '" + photo + "');";

                    connection.query(sqlUser, function (err, rows) {
                        connection.release();
                        if (err) {
                            console.log(err);
                            callback(err, null);
                        } else {
                            // TESTING
                            //callback(null, rows.insertId);
                            createSampleData(companyID, rows.insertId, displayName, function (err, done) {
                                if (err) {
                                    console.log(err);
                                    callback(err, null, null);
                                } else {
                                    if (done) {
                                        callback(null, rows.insertId);
                                    }
                                }
                            });
                        }
                    });
                }
            });
        }
    });
};

module.exports.updateUserProviderData = function (userID, provider, profile, token, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var id = profile.id || '';
        var displayName = profile.displayName || '';
        var familyName = profile.name.familyName || '';
        var givenName = profile.name.givenName || '';
        var middleName = profile.name.middleName || '';
        var email = profile.emails[0].value || '';
        var emailType = profile.emails[0].type || '';
        var photoFull = profile.photos[0].value || '';
        var photo = photoFull.substring(0, photoFull.indexOf('?')) || '';

        var sql = "UPDATE users SET provider = '" + provider + "', " + provider + "_id = '" + id + "', " + provider + "_token = '" + token + "', " + provider + "_display_name = '" + displayName + "', " + provider + "_family_name = '" + familyName + "', " + provider + "_given_name = '" + givenName + "', " + provider + "_middle_name = '" + middleName + "', " + provider + "_email = '" + email + "', " + provider + "_email_type = '" + emailType + "', " + provider + "_photo = '" + photo + "' WHERE id = '" + userID + "'";

        connection.query(sql, function (err) {
            connection.release();
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(null, userID);
            }
        });
    });
};

module.exports.updateProvider = function (userID, provider, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "UPDATE users SET provider = '" + provider + "' WHERE id = '" + userID + "'";

        connection.query(sql, function (err) {
            connection.release();
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(null, userID);
            }
        });
    });
};

module.exports.changeCompanyInfo = function (id, name, accessCode, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "UPDATE companies SET name = '" + name + "', access_code = '" + accessCode + "', data_set = '1' WHERE id = '" + id + "';";

        connection.query(sql, function (err, rows) {
            connection.release();
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(null, 'ok');
            }
        });
    });
};

module.exports.getMenus = function (id, companyID, callback) {
    'use strict';
    var result = [];

    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "SELECT projects.id, projects.name, projects.notes, projects.color, projects.sample, (SELECT COUNT(flightplans.id) FROM flightplans WHERE projects.id = flightplans.project_id) AS numOfFlightplans FROM projects WHERE (id IN (SELECT project_id FROM flightplans WHERE id IN (SELECT flightplan_id FROM flightplan_teams WHERE id IN (SELECT team_id FROM flightplan_team_users WHERE user_id = " + connection.escape(id) + ")))) OR (projects.created_by = " + connection.escape(id) + ")";

        connection.query(sql, function (err, rows) {
            if (err) {
                console.log(err);
                callback(err, null, null);
            } else {
                if (rows.length > 0) {
                    for (let i = 0; i < rows.length; i++) {
                        var sqlFlightplans = "SELECT flightplans.id, flightplans.name, flightplans.start_time, flightplans.end_time, flightplans.sample, locations.center_latitude, locations.center_longitude, locations.name AS location_name, locations.path_thumb AS snapshot FROM flightplans LEFT JOIN locations ON flightplans.location_id = locations.id WHERE flightplans.project_id = " + rows[i].id + " AND flightplans.id IN (SELECT flightplan_id FROM flightplan_teams WHERE id IN (SELECT team_id FROM flightplan_team_users WHERE user_id = " + connection.escape(id) + "))";


                        connection.query(sqlFlightplans, function (err, rowsFlightplans) {
                            if (err) {
                                console.log(err);
                                callback(err, null, null);
                            } else {
                                rows[i].flightplans = {};
                                rows[i].flightplans = rowsFlightplans;
                                if (i == rows.length - 1) {
                                    var sqlMembers = "SELECT id, name, google_display_name, archived FROM users WHERE company_id = '" + companyID + "';";
                                    connection.query(sqlMembers, function (err, rowsMembers) {
                                        connection.release();
                                        if (err) {
                                            console.log(err);
                                            callback(err, null, null);
                                        } else {
                                            callback(null, rows, rowsMembers);
                                        }
                                    });
                                }
                            }
                        })
                    }
                } else {
                    var sqlOnlyUserProjects = "SELECT projects.id, projects.name, projects.notes, projects.color FROM projects WHERE created_by = " + connection.escape(id);

                    connection.query(sqlOnlyUserProjects, function (err, rowsOnlyUserProjects) {
                        if (err) {
                            console.log(err);
                            callback(err, null, null);
                        } else {
                            for (var i = 0; i < rowsOnlyUserProjects.length; i++) {
                                rowsOnlyUserProjects[i].flightplans = {};
                            }
                            var sqlMembers = "SELECT id, name, google_display_name, archived  FROM users WHERE company_id = '" + companyID + "';";
                            connection.query(sqlMembers, function (err, rowsMembers) {
                                connection.release();
                                if (err) {
                                    console.log(err);
                                    callback(err, null, null);
                                } else {
                                    callback(null, rowsOnlyUserProjects, rowsMembers);
                                }
                            });
                        }
                    })
                }
            }
        });
    });
}

module.exports.getAllRoles = function (callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "SELECT id, name, class, form_name FROM user_roles;";
        connection.query(sql, function (err, rows) {
            connection.release();
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(null, rows);
            }
        });
    });
}

module.exports.getAllUsersFromCompany = function (companyID, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "SELECT id, provider, name, google_display_name, archived FROM users WHERE company_id = '" + companyID + "'";

        connection.query(sql, function (err, rows) {
            connection.release();
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(null, rows);
            }
        });
    });
}


module.exports.getAllUsersForFlightplan = function (flightplanID, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "SELECT flightplan_team_users.user_id, flightplan_team_users.role_id FROM flightplan_team_users WHERE team_id = (SELECT id FROM flightplan_teams WHERE flightplan_id = " + flightplanID + ")";

        connection.query(sql, function (err, rows) {
            connection.release();
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(null, rows);
            }
        });
    });
}

module.exports.createSampleDataExport = function (companyID, userID, userName, callback) {
    createSampleData(companyID, userID, userName, function (err, done) {
        if (err) {
            console.log(err);
            callback(err, null);
        } else {
            callback(null, done);
        }
    });
}

// SAMPLE DATA
function createSampleData(companyID, userID, userName, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var color = general.getRandomColor();
        var startTime = moment().format(mysqlTimeFormat);
        var endTime = moment(startTime).add(2, 'hours').format(mysqlTimeFormat);


        var sqlProject = "INSERT INTO projects (company_id, name, notes, color, sample, created_by) VALUES (" + connection.escape(companyID) + ", " + connection.escape(sample.projectName) + ", " + connection.escape(sample.projectNotes) + ", " + connection.escape(color) + ", 1, " + connection.escape(userID) + ");";

        connection.query(sqlProject, function (err, projectRows) {
            if (err) {
                console.log(err);
                callback(err, null);
            } else {

                var crypto = require('crypto');
                var seed = crypto.randomBytes(20);
                var randomString = crypto.createHash('sha1').update(seed).digest('hex');
                var imageName = 'snapshot_' + randomString;

                var s3 = new aws.S3();
                var fileBuffer = fs.readFileSync(__dirname + '/../public/sample/' + sample.locationImage);
                var thumbFileBuffer = fs.readFileSync(__dirname + '/../public/sample/' + sample.locationThumb);
                var metaData = general.getImageContentType(__dirname + '/../public/sample/' + sample.locationImage);
                var key = companyID + '/locations/' + imageName + '.png';
                var keyThumb = companyID + '/locations/' + imageName + '_thumb.png';

                s3.putObject({
                    ACL: 'public-read',
                    Bucket: configAWS.bucketName,
                    Key: key,
                    Body: fileBuffer,
                    ContentType: metaData
                }, function (err, response) {
                    if (err) {
                        console.log(err);
                        callback(err, null);
                    } else {

                        s3.putObject({
                            ACL: 'public-read',
                            Bucket: configAWS.bucketName,
                            Key: keyThumb,
                            Body: thumbFileBuffer,
                            ContentType: metaData
                        }, function (err, response) {
                            if (err) {
                                console.log(err);
                                callback(err, null);
                            } else {

                                var sqlLocation = "INSERT INTO locations (project_id, name, image, path, path_thumb, center_latitude, center_longitude) VALUES (" + connection.escape(projectRows.insertId) + ", " + connection.escape(sample.locationName) + ", " + connection.escape(sample.locationImage) + ", " + connection.escape(sample.s3Path + key) + ", " + connection.escape(sample.s3Path + keyThumb) + ", " + connection.escape(sample.locationCenterLatitude) + ", " + connection.escape(sample.locationCenterLongitude) + ")";

                                connection.query(sqlLocation, function (err, locationRows) {
                                    if (err) {
                                        console.log(err);
                                        callback(err, null);
                                    } else {
                                        var sqlLocationCoordinates = "INSERT INTO location_coordinates(location_id, latitude, longitude) VALUES ";
                                        for (var i = 0; i < sample.locationCoordinates.length; i++) {
                                            sqlLocationCoordinates += "(" + connection.escape(locationRows.insertId) + ", " + connection.escape(sample.locationCoordinates[i][0]) + ", " + connection.escape(sample.locationCoordinates[i][1]) + "), ";
                                        }
                                        sqlLocationCoordinates = sqlLocationCoordinates.substr(0, sqlLocationCoordinates.length - 2);

                                        connection.query(sqlLocationCoordinates, function (err, locationCoordinatesRows) {
                                            if (err) {
                                                console.log(err);
                                                callback(err, null);
                                            } else {
                                                var sqlBatteries = "INSERT INTO batteries (company_id, name, serial_number, charge, voltage, status, notes) VALUES (" + connection.escape(companyID) + ", " + connection.escape(sample.batteryName) + ", " + connection.escape(sample.batterySerialNumber) + ", " + connection.escape(sample.batteryCharge) + ", " + connection.escape(sample.batteryVoltage) + ", " + connection.escape(sample.batteryStatus) + ", " + connection.escape(sample.batteryNotes) + ")";

                                                connection.query(sqlBatteries, function (err, batteriesRows) {
                                                    if (err) {
                                                        console.log(err);
                                                        callback(err, null);
                                                    } else {
                                                        var sqlVehicles = "INSERT INTO vehicles (company_id, name, type, manufacturer, model, serial_number, status, notes) VALUES (" + connection.escape(companyID) + ", " + connection.escape(sample.vehicleName) + ", " + connection.escape(sample.vehicleType) + ", " + connection.escape(sample.vehicleManufacturer) + ", " + connection.escape(sample.vehicleModel) + ", " + connection.escape(sample.vehicleSerialNumber) + ", " + connection.escape(sample.vehicleStatus) + ", " + connection.escape(sample.vehicleNotes) + ")";

                                                        connection.query(sqlVehicles, function (err, vehiclesRows) {
                                                            if (err) {
                                                                console.log(err);
                                                                callback(err, null);
                                                            } else {
                                                                var startTime = moment().format(mysqlTimeFormat);
                                                                var endTime = moment(startTime).add(2, 'hours').format(mysqlTimeFormat);

                                                                var sqlFlightplans = "INSERT INTO flightplans (project_id, location_id, name, group_name, start_time, end_time, notes, sample, created_by) VALUES (" + connection.escape(projectRows.insertId) + ", " + connection.escape(locationRows.insertId) + ", " + connection.escape(sample.flightplanName) + ", " + connection.escape(sample.flightplanGroupName) + ", " + connection.escape(startTime) + ", " + connection.escape(endTime) + ", " + connection.escape(sample.flightplanNotes) + ", 1, " + connection.escape(userID) + ")";

                                                                connection.query(sqlFlightplans, function (err, flightplansRows) {
                                                                    if (err) {
                                                                        console.log(err);
                                                                        callback(err, null);
                                                                    } else {
                                                                        var sqlFlightplanVehicles = "INSERT INTO flightplan_vehicles (flightplan_id, vehicle_id, unit_id) VALUES (" + connection.escape(flightplansRows.insertId) + ", " + connection.escape(vehiclesRows.insertId) + ", " + connection.escape(sample.flightplanVehicleUnitID) + ")";

                                                                        connection.query(sqlFlightplanVehicles, function (err, flightplanVehiclesRows) {
                                                                            if (err) {
                                                                                console.log(err);
                                                                                callback(err, null);
                                                                            } else {
                                                                                var sqlFlightplanTeams = "INSERT INTO flightplan_teams (flightplan_id) VALUES (" + connection.escape(flightplansRows.insertId) + ")";

                                                                                connection.query(sqlFlightplanTeams, function (err, flightplanTeamsRows) {
                                                                                    if (err) {
                                                                                        console.log(err);
                                                                                        callback(err, null);
                                                                                    } else {
                                                                                        var sqlFlightplanTeamUsers = "INSERT INTO flightplan_team_users (team_id, user_id, role_id, unit_id) VALUES (" + connection.escape(flightplanTeamsRows.insertId) + ", " + connection.escape(userID) + ", " + connection.escape(sample.flightplanTeamUsersRoleID) + ", " + connection.escape(sample.flightplanTeamUsersUnitID) + ")";

                                                                                        connection.query(sqlFlightplanTeamUsers, function (err, flightplanTeamUsersRows) {
                                                                                            if (err) {
                                                                                                console.log(err);
                                                                                                callback(err, null);
                                                                                            } else {
                                                                                                var documentName = uuid.v4() + sample.documentFileExtension;
                                                                                                var documentPath = __dirname + '/../public/sample/' + sample.documentFileName;
                                                                                                var documentBuffer = fs.readFileSync(documentPath);
                                                                                                var metaData = general.getImageContentType(documentPath);
                                                                                                var key = companyID + '/documents/' + documentName;
                                                                                                var path = sample.s3Path + key;

                                                                                                s3.putObject({
                                                                                                    ACL: 'public-read',
                                                                                                    Bucket: configAWS.bucketName,
                                                                                                    Key: key,
                                                                                                    Body: documentBuffer,
                                                                                                    ContentType: metaData
                                                                                                }, function (err, response) {
                                                                                                    if (err) {
                                                                                                        console.log(err);
                                                                                                        callback(err, null);
                                                                                                    } else {

                                                                                                        var sqlDocuments = "INSERT INTO documents (company_id, user_id, user_name, name, file_name, file_type, file_path, file_ext, size, human_size, document_type, modified) VALUES (" + connection.escape(companyID) + ", " + connection.escape(userID) + ", " + connection.escape(userName) + ", " + connection.escape(sample.documentName) + ", " + connection.escape(key) + ", " + connection.escape(sample.documentFileType) + ", " + connection.escape(path) + ", " + connection.escape(sample.documentFileExtension) + ", " + connection.escape(sample.documentSize) + ", " + connection.escape(sample.documentHumanSize) + ", " + connection.escape(sample.documentType) + ", " + connection.escape(startTime) + ")";

                                                                                                        connection.query(sqlDocuments, function (err, documentsRows) {
                                                                                                            if (err) {
                                                                                                                console.log(err);
                                                                                                                callback(err, null);
                                                                                                            } else {
                                                                                                                var sqlFlightplanDocuments = "INSERT INTO flightplan_documents (flightplan_id, document_id) VALUES (" + connection.escape(flightplansRows.insertId) + ", " + connection.escape(documentsRows.insertId) + ")";

                                                                                                                connection.query(sqlFlightplanDocuments, function (err, flightplanDocumentsRows) {
                                                                                                                    if (err) {
                                                                                                                        console.log(err);
                                                                                                                        callback(err, null);
                                                                                                                    } else {
                                                                                                                        var sqlFlightplanComments = "INSERT INTO flightplan_comments (flightplan_id, user_id, content, created, role_id) VALUES (" + connection.escape(flightplansRows.insertId) + ", " + connection.escape(userID) + ", " + connection.escape(sample.commentContent) + ", " + connection.escape(startTime) + ", " + connection.escape(sample.commentRoleID) + ")";
                                                                                                                        connection.query(sqlFlightplanComments, function (err, flightplanCommentsRows) {
                                                                                                                            if (err) {
                                                                                                                                console.log(err);
                                                                                                                                callback(err, null);
                                                                                                                            } else {
                                                                                                                                var sqlFlightplanBatteries = "INSERT INTO flightplan_batteries (flightplan_id, battery_id) VALUES (" + connection.escape(flightplansRows.insertId) + ", " + connection.escape(batteriesRows.insertId) + ")";
                                                                                                                                connection.query(sqlFlightplanBatteries, function (err, flightplanBatteriesRows) {
                                                                                                                                    if (err) {
                                                                                                                                        console.log(err);
                                                                                                                                        callback(err, null);
                                                                                                                                    } else {
                                                                                                                                        connection.release();
                                                                                                                                        callback(null, true);
                                                                                                                                    }
                                                                                                                                });
                                                                                                                            }
                                                                                                                        });
                                                                                                                    }
                                                                                                                });
                                                                                                            }
                                                                                                        });
                                                                                                    }
                                                                                                });
                                                                                            }
                                                                                        });
                                                                                    }
                                                                                });
                                                                            }
                                                                        });
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    });
}

// AVATAR
module.exports.addUserAvatar = function (companyID, userID, image, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var s3 = new aws.S3();

        var sql = "SELECT photo FROM users WHERE id = " + connection.escape(userID);
        connection.query(sql, function (err, oldPhoto) {
            if (err) {
                console.log(err);
                callback(err, null);
            } else {

                var oldPhoto = oldPhoto[0].photo;

                if (oldPhoto != '/assets/images/avatar.png') {
                    var split = oldPhoto.split('https://s3.amazonaws.com/' + configAWS.bucketName + '/');
                    var oldKey = split[1];

                    var deleteParams = {
                        Bucket: configAWS.bucketName,
                        Delete: {
                            Objects: [
                                { Key: oldKey }
                            ]
                        }
                    };

                    s3.deleteObjects(deleteParams, function (err, data) {
                        if (err) {
                            console.log(err);
                            callback(err, null);
                        }
                    });
                }
                var imageTypeRegExp = /\/(.*?)$/;
                var crypto = require('crypto');
                var seed = crypto.randomBytes(20);
                var randomString = crypto.createHash('sha1').update(seed).digest('hex');
                var imageBuffer = general.decodeBase64Image(image);
                var uploadFolder = __dirname + '/../public/uploads/avatars/';
                var imageName = 'avatar_' + randomString;
                var imageTypeDetected = imageBuffer.type.match(imageTypeRegExp);
                var extension = imageTypeDetected[1];
                var imagePath = uploadFolder + imageName + '.' + extension;

                fs.writeFile(imagePath, imageBuffer.data, function (err) {
                    if (err) {
                        console.log(err);
                    } else {

                        var fileBuffer = fs.readFileSync(imagePath);
                        var metaData = general.getImageContentType(imagePath);
                        var key = companyID + '/avatars/' + imageName + '.' + extension;

                        s3.putObject({
                            ACL: 'public-read',
                            Bucket: configAWS.bucketName,
                            Key: key,
                            Body: fileBuffer,
                            ContentType: metaData
                        }, function (err, response) {
                            if (err) {
                                console.log(err);
                                callback(err, null);
                            } else {
                                var sql = "UPDATE users SET photo = 'https://s3.amazonaws.com/" + configAWS.bucketName + "/" + key + "' WHERE id = " + userID;

                                connection.query(sql, function (err, rows) {
                                    connection.release();
                                    if (err) {
                                        console.log(err);
                                        callback(err, null);
                                    } else {
                                        fs.exists(imagePath, function (exists) {
                                            if (exists) {
                                                fs.unlink(imagePath);
                                            }
                                        });

                                        callback(null, 'https://s3.amazonaws.com/' + configAWS.bucketName + '/' + key);
                                    }
                                });
                            }
                        });
                    }
                })
            }
        });
    });
}

module.exports.deleteUserAvatar = function (companyID, userID, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "SELECT photo FROM users WHERE id = " + connection.escape(userID);
        connection.query(sql, function (err, oldPhoto) {
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                var s3 = new aws.S3();
                var oldPhoto = oldPhoto[0].photo;
                var split = oldPhoto.split('https://s3.amazonaws.com/' + configAWS.bucketName + '/');
                var oldKey = split[1];

                var deleteParams = {
                    Bucket: configAWS.bucketName,
                    Delete: {
                        Objects: [
                            { Key: oldKey }
                        ]
                    }
                };

                s3.deleteObjects(deleteParams, function (err, data) {
                    if (err) {
                        console.log(err);
                        callback(err, null);
                    } else {
                        var sqlUpdate = "UPDATE users SET photo = '/assets/images/avatar.png' WHERE id = " + connection.escape(userID);
                        connection.query(sqlUpdate, function (err, rowsUpdate) {
                            connection.release();
                            if (err) {
                                console.log(err);
                                callback(err, null);
                            } else {
                                callback(null, rowsUpdate.affectedRows);
                            }
                        });
                    }
                });
            }
        });
    });
}


module.exports.hideWelcome = function (userID, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "UPDATE users SET help = 0 WHERE id = " + connection.escape(userID);
        connection.query(sql, function (err, rows) {
            connection.release();
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(null, rows.affectedRows);
            }
        });
    });
}


module.exports.checkPlanUsers = function (companyID, plan, callback) {
    if (plan == 'enterprise') {
        callback(null, 'yes');
    } else {
        db.getConnection(function (err, connection) {
            connectionError(err, connection);
            var sql = "SELECT COUNT(id) AS num FROM users WHERE company_id = " + connection.escape(companyID);
            connection.query(sql, function (err, rows) {
                connection.release();
                if (err) {
                    console.log(err);
                    callback(err, null);
                } else {
                    var num = rows[0].num;

                    if (plan == 'free') {
                        callback(null, 'no');
                    } else if (plan == 'professional') {
                        if (num < plans.professional.users) {
                            callback(null, 'yes');
                        } else {
                            callback(null, 'no');
                        }
                    }
                }
            });
        });
    }
}

module.exports.checkPlanProjects = function (companyID, plan, callback) {
    if (plan == 'enterprise') {
        callback(null, 'yes');
    } else {
        db.getConnection(function (err, connection) {
            connectionError(err, connection);

            var sql = "SELECT COUNT(id) AS num FROM projects WHERE company_id = " + connection.escape(companyID);
            connection.query(sql, function (err, rows) {
                connection.release();
                if (err) {
                    console.log(err);
                    callback(err, null);
                } else {
                    var num = rows[0].num;
                    if (plan == 'free') {
                        if (num < plans.free.projects) {
                            callback(null, 'yes');
                        } else {
                            callback(null, 'no');
                        }
                    } else if (plan == 'professional') {
                        if (num < plans.professional.projects) {
                            callback(null, 'yes');
                        } else {
                            callback(null, 'no');
                        }
                    }
                }
            });
        });
    }
}

module.exports.checkPlanFlightplans = function (userID, companyID, plan, callback) {
    'use strict';
    if (plan == 'enterprise') {
        callback(null, 'yes');
    } else {
        db.getConnection(function (err, connection) {
            connectionError(err, connection);

            var sqlProjects = "SELECT id FROM projects WHERE company_id = " + connection.escape(companyID);
            connection.query(sqlProjects, function (err, rowsProjects) {
                if (err) {
                    console.log(err);
                    callback(err, null);
                } else {
                    var total = 0;
                    for (let i = 0; i < rowsProjects.length; i++) {
                        var sqlFlightplans = "SELECT COUNT(id) AS num FROM flightplans WHERE created_by = " + connection.escape(userID) + " AND project_id = " + connection.escape(rowsProjects[i].id);
                        connection.query(sqlFlightplans, function (err, rowsFlightplans) {
                            if (err) {
                                console.log(err);
                                callback(err, null);
                            } else {
                                total += rowsFlightplans[0].num;

                                if (i == rowsProjects.length - 1) {
                                    connection.release();
                                    if (plan == 'free') {
                                        if (total < plans.free.flights) {
                                            callback(null, 'yes');
                                        } else {
                                            callback(null, 'no');
                                        }
                                    } else if (plan == 'professional') {
                                        if (total < plans.professional.flights) {
                                            callback(null, 'yes');
                                        } else {
                                            callback(null, 'no');
                                        }
                                    }
                                }
                            }
                        })
                    }
                }
            })
        });
    }
}

module.exports.checkPlanStorageSpace = function (companyID, plan, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);
        var sql = "SELECT SUM(size) AS num FROM documents WHERE company_id = " + connection.escape(companyID);
        connection.query(sql, function (err, rows) {
            connection.release();
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                var num = rows[0].num;

                if (plan == 'free') {
                    if (num < plans.free.storageInBytes) {
                        callback(null, 'yes');
                    } else {
                        callback(null, 'no');
                    }
                } else if (plan == 'professional') {
                    if (num < plans.professional.storageInBytes) {
                        callback(null, 'yes');
                    } else {
                        callback(null, 'no');
                    }
                } else if (plan == 'enterprise') {
                    if (num < plans.enterprise.storageInBytes) {
                        callback(null, 'yes');
                    } else {
                        callback(null, 'no');
                    }
                }
            }
        });
    });
}

module.exports.getUserPlan = function (email, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "SELECT companies.plan AS plan FROM users JOIN companies ON users.company_id = companies.id WHERE users.email = " + connection.escape(email) + " OR users.google_email = " + connection.escape(email);

        connection.query(sql, function (err, rows) {
            connection.release();
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(null, rows[0].plan);
            }
        });
    });
}

module.exports.getCompanyEmailAndAccessCode = function (companyID, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "SELECT email, access_code FROM companies WHERE id = " + connection.escape(companyID);

        connection.query(sql, function (err, rows) {
            connection.release();
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(null, rows[0]);
            }
        });
    });
}

module.exports.saveNewAccessCode = function (email, accessCode, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "UPDATE companies SET access_code = " + connection.escape(accessCode) + " WHERE email = " + connection.escape(email);

        connection.query(sql, function (err, rows) {
            connection.release();
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(null, rows.affectedRows);
            }
        });
    });
}

module.exports.subscribe = function (data, companyID, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "INSERT INTO subscriptions(customer_id, customer_email, subscription_id, subscription_start, subscription_end, subscription_plan) VALUES(" + connection.escape(data.customer_id) + ", " + connection.escape(data.customer_email) + ", " + connection.escape(data.subscription_id) + ", " + connection.escape(data.subscription_start) + ", " + connection.escape(data.subscription_end) + ", " + connection.escape(data.subscription_plan) + ")";

        connection.query(sql, function (err, rows) {
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                var sqlUpdateCompany = "UPDATE companies SET plan = " + connection.escape(data.subscription_plan) + ", subscription_id = " + connection.escape(rows.insertId) + ", subscription_end = " + connection.escape(data.subscription_end) + " WHERE id = " + connection.escape(companyID);

                connection.query(sqlUpdateCompany, function (err, rowsCompany) {
                    connection.release();
                    if (err) {
                        console.log(err);
                        callback(err, null);
                    } else {
                        callback(rowsCompany.affectedRows);
                    }
                })
            }
        });
    });
}




module.exports.subscribeNewCustomer = function (data, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "INSERT INTO subscriptions(customer_id, customer_email, subscription_id, subscription_start, subscription_end, subscription_plan) VALUES(" + connection.escape(data.customer_id) + ", " + connection.escape(data.customer_email) + ", " + connection.escape(data.subscription_id) + ", " + connection.escape(data.subscription_start) + ", " + connection.escape(data.subscription_end) + ", " + connection.escape(data.subscription_plan) + ")";

        connection.query(sql, function (err, rows) {
            connection.release();
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(null, rows.insertId);
            }
        });
    });
}


module.exports.getDowngradeData = function (userID, companyID, callback) {
    var result = {};

    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sqlProjects = "SELECT id, name FROM projects WHERE company_id = " + connection.escape(companyID);

        connection.query(sqlProjects, function (err, rowsProjects) {
            if (err) {
                console.log('a', err);
                callback(err, null);
            } else {
                result.projects = rowsProjects;

                var sqlUsers = "SELECT id, permission_id, name, google_display_name FROM users WHERE company_id = " + connection.escape(companyID);

                connection.query(sqlUsers, function (err, rowsUsers) {
                    if (err) {
                        console.log('b', err);
                        callback(err, null);
                    } else {
                        result.users = rowsUsers;

                        if(rowsProjects.length > 0){
                            for (var i = 0; i < rowsProjects.length; i++) {
                                (function (i) {
                                    var sqlFlightplans = "SELECT id, name FROM flightplans WHERE project_id = " + connection.escape(rowsProjects[i].id);
                                    connection.query(sqlFlightplans, function (err, rowsFlightplans) {
                                        if (err) {
                                            console.log('c', err);
                                            callback(err, null);
                                        } else {
                                            result.projects[i].flightplans = rowsFlightplans;
                                            if (i == rowsProjects.length - 1) {
                                                connection.release();
                                                callback(null, result);
                                            }
                                        }
                                    })
                                })(i);
                            }
                        }else{
                            connection.release();
                            callback(null, result);
                        }
                    }
                })

            }
        });
    });
}

module.exports.downgrade = function (userID, companyID, data, callback) {
    var stripe = require('stripe')(config.stripe.secretKey);

    var downgradeTo = data.plan;
    var projectsAndFlightplans = data.projectsAndFlightplans;

    var projectIds = [];
    var flightplanIds = [];
    var userIds = [];

    var projectsString = '';
    var flightplansString = '';
    var usersString = '';

    if (downgradeTo == 'free') {
        userIds.push(userID);
    } else if (downgradeTo == 'professional') {
        userIds = data.users;
    }

    for (var i = 0; i < projectsAndFlightplans.length; i++) {
        if (projectsAndFlightplans[i].indexOf('project') != -1) {
            var splitProject = projectsAndFlightplans[i].split('project_');
            projectIds.push(splitProject[1]);
        } else if (projectsAndFlightplans[i].indexOf('flightplan') != -1) {
            var splitFlightplan = projectsAndFlightplans[i].split('flightplan_');
            flightplanIds.push(splitFlightplan[1]);
        }
    }

    if (projectIds.length > 0) {
        projectsString = projectIds.join(',');
    }
    if (flightplanIds.length > 0) {
        flightplansString = flightplanIds.join(',');
    }
    if (userIds.length > 0) {
        usersString = userIds.join(',');
    }

    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        if (downgradeTo == 'free') {
            // GET SUBSCRIPTION DETAILS ----------------------------
            var sqlSubscription = "SELECT subscriptions.id, subscriptions.subscription_id FROM companies LEFT JOIN subscriptions ON companies.subscription_id = subscriptions.id WHERE companies.id = " + connection.escape(companyID);
            connection.query(sqlSubscription, function (err, rowsSubscription) {
                if (err) {
                    console.log('1', err);
                    callback(err, null);
                } else {
                    if (rowsSubscription.length > 0) {
                        stripe.subscriptions.del(rowsSubscription[0].subscription_id, function (err, confirmation) {
                            if (err) {
                                console.log('2', err);
                                callback(err, null);
                            } else {
                                if (confirmation.status == 'canceled') {
                                    // UPDATE SUBSCRIPTION ----------------------------
                                    var sqlUpdateSubscription = "UPDATE subscriptions SET archived = 1 WHERE id = " + connection.escape(rowsSubscription[0].id);
                                    connection.query(sqlUpdateSubscription, function (err, rowsUpdateSubscription) {
                                        if (err) {
                                            console.log('3', err);
                                            callback(err, null);
                                        } else {

                                            // UPDATE COMPANY ----------------------------
                                            var sqlCompany = "UPDATE companies SET plan = " + connection.escape(downgradeTo) + ", subscription_end = NULL";
                                            connection.query(sqlCompany, function (err, rowsCompany) {
                                                if (err) {
                                                    console.log('4', err);
                                                    callback(err, null);
                                                } else {

                                                    // UPDATE USERS ----------------------------
                                                    var sqlUsers = "UPDATE users SET archived = 1 WHERE company_id = " + connection.escape(companyID) + " AND id != " + connection.escape(userID);
                                                    connection.query(sqlUsers, function (err, rowsUsers) {
                                                        if (err) {
                                                            console.log('5', err);
                                                            callback(err, null);
                                                        } else {
                                                            // HAS PROJECTS TO SAVE ----------------------------
                                                            if (projectsString != '') {

                                                                // HAS FLIGHT PLANS TO SAVE ----------------------------
                                                                if (flightplansString != '') {
                                                                    // GET LOCATION IMAGES INFO ----------------------------

                                                                    var sqlSelectLocations = "SELECT location_id FROM flightplans WHERE id IN (" + flightplansString + ")";
                                                                    connection.query(sqlSelectLocations, function (err, rowsSelectLocations) {
                                                                        if (err) {
                                                                            console.log('6', err);
                                                                            callback(err, null);
                                                                        } else {
                                                                            // HAS LOCATION IMAGES ----------------------------         
                                                                            if (rowsSelectLocations.length > 0) {
                                                                                var locationsString = '';
                                                                                for (var i = 0; i < rowsSelectLocations.length; i++) {
                                                                                    locationsString += rowsSelectLocations[i].location_id + ',';
                                                                                }

                                                                                locationsString = locationsString.substr(0, locationsString.length - 1);

                                                                                // SELECT LOCATION IMAGES FOR DELETE  ----------------------------
                                                                                var sqlDeleteLocationImages = "SELECT thumb, image FROM locations WHERE id NOT IN(" + locationsString + ")";
                                                                                connection.query(sqlDeleteLocationImages, function (err, rowsDeleteLocationImages) {
                                                                                    if (err) {
                                                                                        console.log('7', err);
                                                                                        callback(err, null);
                                                                                    } else {
                                                                                        // DELETE LOCATION IMAGES FROM S3 ----------------------------
                                                                                        if (rowsDeleteLocationImages.length > 0) {
                                                                                            var s3 = new aws.S3();                                 
                                                                                            var deleteObjects = [];

                                                                                            for (var i = 0; i < rowsDeleteLocationImages.length; i++) {
                                                                                                deleteObjects.push({ Key: companyID + '/locations/' + rowsDeleteLocationImages[i].image });
                                                                                                deleteObjects.push({ Key: companyID + '/locations/' + rowsDeleteLocationImages[i].thumb })
                                                                                            }

                                                                                            var params = {
                                                                                                Bucket: bucket,
                                                                                                Delete: { Objects: deleteObjects }
                                                                                            };

                                                                                            s3.deleteObjects(params, function (err, data) {
                                                                                                if (err) {
                                                                                                    console.log('8', err);
                                                                                                    callback(err, null);
                                                                                                } else {
                                                                                                    // DELETE OTHER LOCATIONS FROM DB ----------------------------
                                                                                                    var sqlDeleteLocations = "DELETE FROM locations WHERE id NOT IN (" + locationsString + ")";
                                                                                                    connection.query(sqlDeleteLocations, function (err, rowsDeleteLocations) {
                                                                                                        if (err) {
                                                                                                            console.log('9', err);
                                                                                                            callback(err, null);
                                                                                                        } else {
                                                                                                            // DELETE PROJECTS FROM DB ----------------------------
                                                                                                            var sqlProjects = "DELETE FROM projects WHERE id NOT IN(" + projectsString + ")";
                                                                                                            connection.query(sqlProjects, function (err, rowsProjects) {
                                                                                                                if (err) {
                                                                                                                    console.log('10', err);
                                                                                                                    callback(err, null);
                                                                                                                } else {
                                                                                                                    // DELETE FLIGHTPLANS FROM DB ----------------------------
                                                                                                                    var sqlFlightplans = "DELETE FROM flightplans WHERE id NOT IN(" + flightplansString + ")";
                                                                                                                    connection.query(sqlFlightplans, function (err, rowsFlightplans) {
                                                                                                                        if (err) {
                                                                                                                            console.log('11', err);
                                                                                                                            callback(err, null);
                                                                                                                        } else {
                                                                                                                            // DELETE FLIGHTPLAN USERS FROM DB ----------------------------
                                                                                                                            var sqlFlightplanUsers = "DELETE FROM flightplan_team_users WHERE user_id NOT IN(" + usersString + ");"
                                                                                                                            connection.query(sqlFlightplanUsers, function (err, rowsFlightplanUsers) {
                                                                                                                                if (err) {
                                                                                                                                    console.log('12', err);
                                                                                                                                    callback(err, null);
                                                                                                                                } else {
                                                                                                                                    // DELETE DOCUMENTS
                                                                                                                                    var sqlSelectDocuments = "SELECT document_id FROM flightplan_documents WHERE flightplan_id IN(" + flightplansString + ")";
                                                                                                                                    connection.query(sqlSelectDocuments, function (err, rowsSelectDocuments) {
                                                                                                                                        if (err) {
                                                                                                                                            console.log('33a', err);
                                                                                                                                            callback(err, null);
                                                                                                                                        } else {
                                                                                                                                            var documentsString = '';
                                                                                                                                            for (var i = 0; i < rowsSelectDocuments.length; i++) {
                                                                                                                                                documentsString += rowsSelectDocuments[i].document_id + ',';
                                                                                                                                            }

                                                                                                                                            documentsString = documentsString.substr(0, documentsString.length - 1);

                                                                                                                                            // SELECT DOCUMENTS FOR DELETE  ----------------------------
                                                                                                                                            var sqlSelectDeleteDocuments = "SELECT file_name FROM documents WHERE company_id = " + connection.escape(companyID) + " AND id NOT IN(" + documentsString + ")";

                                                                                                                                            connection.query(sqlSelectDeleteDocuments, function (err, rowsSelectDeleteDocuments) {
                                                                                                                                                if (err) {
                                                                                                                                                    console.log('28a', err);
                                                                                                                                                    callback(err, null);
                                                                                                                                                } else {
                                                                                                                                                    // DELETE DOCUMENTS FROM S3 ----------------------------
                                                                                                                                                    if (rowsSelectDeleteDocuments.length > 0) {
                                                                                                                                                        var s3 = new aws.S3();
                                                                                                                                                        var deleteObjects = [];

                                                                                                                                                        for (var i = 0; i < rowsSelectDeleteDocuments.length; i++) {
                                                                                                                                                            deleteObjects.push({ Key: rowsSelectDeleteDocuments[i].file_name });
                                                                                                                                                        }

                                                                                                                                                        var params = {
                                                                                                                                                            Bucket: bucket,
                                                                                                                                                            Delete: { Objects: deleteObjects }
                                                                                                                                                        };

                                                                                                                                                        s3.deleteObjects(params, function (err, data) {
                                                                                                                                                            if (err) {
                                                                                                                                                                console.log('29a', err);
                                                                                                                                                                callback(err, null);
                                                                                                                                                            } else {
                                                                                                                                                                // DELETE DOCUMENTS FROM DB ----------------------------
                                                                                                                                                                var sqlDeleteDocuments = "DELETE FROM documents WHERE company_id = " + connection.escape(companyID) + " AND id NOT IN (" + documentsString + ")";
                                                                                                                                                                connection.query(sqlDeleteDocuments, function (err, rowsDeleteDocuments) {
                                                                                                                                                                    if (err) {
                                                                                                                                                                        console.log('30', err);
                                                                                                                                                                        callback(err, null);
                                                                                                                                                                    } else {
                                                                                                                                                                        connection.release();
                                                                                                                                                                        callback(null, 'ok');
                                                                                                                                                                    }
                                                                                                                                                                });
                                                                                                                                                            }
                                                                                                                                                        });
                                                                                                                                                    // NO DOCUMENTS
                                                                                                                                                    } else {
                                                                                                                                                        connection.release();
                                                                                                                                                        callback(null, 'ok');
                                                                                                                                                    }
                                                                                                                                                }
                                                                                                                                            });
                                                                                                                                        }
                                                                                                                                    });
                                                                                                                                }
                                                                                                                            });
                                                                                                                        }
                                                                                                                    });
                                                                                                                }
                                                                                                            });
                                                                                                        }

                                                                                                    });
                                                                                                }
                                                                                            });
                                                                                        }
                                                                                    }
                                                                                });
                                                                            }
                                                                        }
                                                                    });
                                                                    // NO FLIGHT PLANS TO SAVE
                                                                } else {
                                                                    // SELECT LOCATION IMAGES FOR DELETE  ----------------------------
                                                                    var sqlDeleteLocationImages = "SELECT locations.id, locations.thumb, locations.image FROM locations LEFT JOIN projects ON locations.project_id = projects.id WHERE projects.company_id = " + connection.escape(companyID) + " AND  locations.project_id NOT IN(" + projectsString + ")";
                                                                    connection.query(sqlDeleteLocationImages, function (err, rowsDeleteLocationImages) {
                                                                        if (err) {
                                                                            console.log('13', err);
                                                                            callback(err, null);
                                                                        } else {
                                                                            // DELETE LOCATION IMAGES FROM S3 ----------------------------
                                                                            if (rowsDeleteLocationImages.length > 0) {
                                                                                var s3 = new aws.S3();
                                                                                var deleteObjects = [];

                                                                                for (var i = 0; i < rowsDeleteLocationImages.length; i++) {
                                                                                    deleteObjects.push({ Key: companyID + '/locations/' + rowsDeleteLocationImages[i].image });
                                                                                    deleteObjects.push({ Key: companyID + '/locations/' + rowsDeleteLocationImages[i].thumb })
                                                                                }

                                                                                var params = {
                                                                                    Bucket: bucket,
                                                                                    Delete: { Objects: deleteObjects }
                                                                                };

                                                                                s3.deleteObjects(params, function (err, data) {
                                                                                    if (err) {
                                                                                        console.log('14', err);
                                                                                        callback(err, null);
                                                                                    } else {
                                                                                        var locationsString = '';
                                                                                        for (var i = 0; i < rowsDeleteLocationImages.length; i++) {
                                                                                            locationsString += rowsDeleteLocationImages[i].id + ',';
                                                                                        }

                                                                                        locationsString = locationsString.substr(0, locationsString.length - 1);

                                                                                        // DELETE OTHER LOCATIONS FROM DB ----------------------------
                                                                                        var sqlDeleteLocations = "DELETE FROM locations WHERE id IN (" + locationsString + ")";
                                                                                        connection.query(sqlDeleteLocations, function (err, rowsDeleteLocations) {
                                                                                            if (err) {
                                                                                                console.log('15', err);
                                                                                                callback(err, null);
                                                                                            } else {
                                                                                                // DELETE PROJECTS FROM DB ----------------------------
                                                                                                var sqlProjects = "DELETE FROM projects WHERE id NOT IN(" + projectsString + ")";

                                                                                                connection.query(sqlProjects, function (err, rowsProjects) {
                                                                                                    if (err) {
                                                                                                        console.log('16', err);
                                                                                                        callback(err, null);
                                                                                                    } else {
                                                                                                        // DELETE ALL FLIGHTPLANS FROM DB FOR COMPANY ----------------------------
                                                                                                        var sqlFlightplans = "DELETE FROM flightplans WHERE project_id IN(SELECT id FROM projects WHERE company_id = " + connection.escape(companyID) + ")";

                                                                                                        connection.query(sqlFlightplans, function (err, rowsFlightplans) {
                                                                                                            if (err) {
                                                                                                                console.log('17', err);
                                                                                                                callback(err, null);
                                                                                                            } else {
                                                                                                                // DELETE DOCUMENTS
                                                                                                                var sqlSelectDocuments = "SELECT id, file_name FROM documents WHERE company_id = " + connection.escape(companyID);
                                                                                                                connection.query(sqlSelectDocuments, function (err, rowsSelectDocuments) {
                                                                                                                    if (err) {
                                                                                                                        console.log('33b', err);
                                                                                                                        callback(err, null);
                                                                                                                    } else {
                                                                                                                        
                                                                                                                        // DELETE DOCUMENTS FROM S3 ----------------------------
                                                                                                                        if (rowsSelectDocuments.length > 0) {
                                                                                                                            var s3 = new aws.S3();
                                                                                                                            var deleteObjects = [];

                                                                                                                            for (var i = 0; i < rowsSelectDocuments.length; i++) {
                                                                                                                                deleteObjects.push({ Key: rowsSelectDocuments[i].file_name });
                                                                                                                            }

                                                                                                                            var params = {
                                                                                                                                Bucket: bucket,
                                                                                                                                Delete: { Objects: deleteObjects }
                                                                                                                            };

                                                                                                                            s3.deleteObjects(params, function (err, data) {
                                                                                                                                if (err) {
                                                                                                                                    console.log('29b', err);
                                                                                                                                    callback(err, null);
                                                                                                                                } else {
                                                                                                                                    // DELETE DOCUMENTS FROM DB ----------------------------
                                                                                                                                    var sqlDeleteDocuments = "DELETE FROM documents WHERE company_id = " + connection.escape(companyID);
                                                                                                                                    connection.query(sqlDeleteDocuments, function (err, rowsDeleteDocuments) {
                                                                                                                                        if (err) {
                                                                                                                                            console.log('30', err);
                                                                                                                                            callback(err, null);
                                                                                                                                        } else {
                                                                                                                                            connection.release();
                                                                                                                                            callback(null, 'ok');
                                                                                                                                        }
                                                                                                                                    });
                                                                                                                                }
                                                                                                                            });
                                                                                                                        // NO DOCUMENTS
                                                                                                                        } else {
                                                                                                                            connection.release();
                                                                                                                            callback(null, 'ok');
                                                                                                                        }
                                                                                                                    }
                                                                                                                });
                                                                                                            }
                                                                                                        });
                                                                                                    }
                                                                                                });
                                                                                            }
                                                                                        });
                                                                                    }
                                                                                });
                                                                            }
                                                                        }
                                                                    });
                                                                }
                                                                // NO PROJECTS TO SAVE
                                                            } else {
                                                                // SELECT LOCATION IMAGES FOR DELETE  ----------------------------
                                                                var sqlDeleteLocationImages = "SELECT locations.id, locations.thumb, locations.image FROM locations LEFT JOIN projects ON locations.project_id = projects.id WHERE projects.company_id = " + connection.escape(companyID);
                                                                connection.query(sqlDeleteLocationImages, function (err, rowsDeleteLocationImages) {
                                                                    if (err) {
                                                                        console.log('18', err);
                                                                        callback(err, null);
                                                                    } else {
                                                                        // DELETE LOCATION IMAGES FROM S3 ----------------------------
                                                                        if (rowsDeleteLocationImages.length > 0) {
                                                                            var s3 = new aws.S3();
                                                                            var deleteObjects = [];

                                                                            for (var i = 0; i < rowsDeleteLocationImages.length; i++) {
                                                                                deleteObjects.push({ Key: companyID + '/locations/' + rowsDeleteLocationImages[i].image });
                                                                                deleteObjects.push({ Key: companyID + '/locations/' + rowsDeleteLocationImages[i].thumb })
                                                                            }

                                                                            var params = {
                                                                                Bucket: bucket,
                                                                                Delete: { Objects: deleteObjects }
                                                                            };

                                                                            s3.deleteObjects(params, function (err, data) {
                                                                                if (err) {
                                                                                    console.log('19', err);
                                                                                    callback(err, null);
                                                                                } else {
                                                                                    var locationsString = '';
                                                                                    for (var i = 0; i < rowsDeleteLocationImages.length; i++) {
                                                                                        locationsString += rowsDeleteLocationImages[i].id + ',';
                                                                                    }

                                                                                    locationsString = locationsString.substr(0, locationsString.length - 1);

                                                                                    // DELETE OTHER LOCATIONS FROM DB ----------------------------
                                                                                    var sqlDeleteLocations = "DELETE FROM locations WHERE id IN (" + locationsString + ")";
                                                                                    connection.query(sqlDeleteLocations, function (err, rowsDeleteLocations) {
                                                                                        if (err) {
                                                                                            console.log('20', err);
                                                                                            callback(err, null);
                                                                                        } else {
                                                                                            // DELETE PROJECTS FROM DB ----------------------------
                                                                                            var sqlProjects = "DELETE FROM projects WHERE company_id = " + connection.escape(companyID);

                                                                                            connection.query(sqlProjects, function (err, rowsProjects) {
                                                                                                if (err) {
                                                                                                    console.log('21', err);
                                                                                                    callback(err, null);
                                                                                                } else {
                                                                                                    // DELETE DOCUMENTS
                                                                                                    var sqlSelectDocuments = "SELECT id, file_name FROM documents WHERE company_id = " + connection.escape(companyID);
                                                                                                    connection.query(sqlSelectDocuments, function (err, rowsSelectDocuments) {
                                                                                                        if (err) {
                                                                                                            console.log('33b', err);
                                                                                                            callback(err, null);
                                                                                                        } else {
                                                                                                            
                                                                                                            // DELETE DOCUMENTS FROM S3 ----------------------------
                                                                                                            if (rowsSelectDocuments.length > 0) {
                                                                                                                var s3 = new aws.S3();
                                                                                                                var deleteObjects = [];

                                                                                                                for (var i = 0; i < rowsSelectDocuments.length; i++) {
                                                                                                                    deleteObjects.push({ Key: rowsSelectDocuments[i].file_name });
                                                                                                                }

                                                                                                                var params = {
                                                                                                                    Bucket: bucket,
                                                                                                                    Delete: { Objects: deleteObjects }
                                                                                                                };

                                                                                                                s3.deleteObjects(params, function (err, data) {
                                                                                                                    if (err) {
                                                                                                                        console.log('29b', err);
                                                                                                                        callback(err, null);
                                                                                                                    } else {
                                                                                                                        // DELETE DOCUMENTS FROM DB ----------------------------
                                                                                                                        var sqlDeleteDocuments = "DELETE FROM documents WHERE company_id = " + connection.escape(companyID);
                                                                                                                        connection.query(sqlDeleteDocuments, function (err, rowsDeleteDocuments) {
                                                                                                                            if (err) {
                                                                                                                                console.log('30', err);
                                                                                                                                callback(err, null);
                                                                                                                            } else {
                                                                                                                                connection.release();
                                                                                                                                callback(null, 'ok');
                                                                                                                            }
                                                                                                                        });
                                                                                                                    }
                                                                                                                });
                                                                                                            // NO DOCUMENTS
                                                                                                            } else {
                                                                                                                connection.release();
                                                                                                                callback(null, 'ok');
                                                                                                            }
                                                                                                        }
                                                                                                    });
                                                                                                }
                                                                                            });
                                                                                        }
                                                                                    });
                                                                                }
                                                                            });
                                                                        }else{
                                                                            connection.release();
                                                                            callback(null, 'ok');
                                                                        }
                                                                    }
                                                                });
                                                            }
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                } else {
                                    console.log('22');
                                    callback(err, null);
                                }
                            }
                        });
                    } else {
                        console.log('23');
                        callback(err, null);
                    }
                }
            });
        } else if (downgradeTo == 'professional') {
            /* TO DO : enterprise to professional
            stripe.subscriptions.update("sub_3R3PlB2YlJe84a", {
                plan: config.plans.professional.id
            },function (err, subscription) {
                if (err) {
                    console.log('24', err);
                } else {
                    console.log(subscription);
                }
            });
            */

            // update company subscription
            var subscriptionEnd = moment(moment.now()).add(1, 'month').format(mysqlTimeFormat);

            var sqlCompany = "UPDATE companies SET plan = " + connection.escape(downgradeTo) + ", subscription_end = " + connection.escape(subscriptionEnd) + " WHERE companies.id = " + connection.escape(companyID);

            connection.query(sqlCompany, function (err, rowsCompany) {
                if (err) {
                    console.log('25', err);
                    callback(err, null);
                } else {
                    var sqlUsers = "UPDATE users SET archived = 1 WHERE company_id = " + connection.escape(companyID) + " AND id != " + connection.escape(userID) + " AND id NOT IN(" + usersString + ")";

                    connection.query(sqlUsers, function (err, rowsUsers) {
                        if (err) {
                            console.log('26', err);
                            callback(err, null);
                        } else {
                            // HAS PROJECTS TO SAVE ----------------------------
                            if (projectsString != '') {

                                // HAS FLIGHT PLANS TO SAVE ----------------------------
                                if (flightplansString != '') {
                                    // GET LOCATION IMAGES INFO ----------------------------

                                    var sqlSelectLocations = "SELECT location_id FROM flightplans WHERE id IN (" + flightplansString + ")";

                                    connection.query(sqlSelectLocations, function (err, rowsSelectLocations) {
                                        if (err) {
                                            console.log('27', err);
                                            callback(err, null);
                                        } else {
                                            // HAS LOCATION IMAGES ----------------------------         
                                            if (rowsSelectLocations.length > 0) {
                                                var locationsString = '';
                                                for (var i = 0; i < rowsSelectLocations.length; i++) {
                                                    locationsString += rowsSelectLocations[i].location_id + ',';
                                                }

                                                locationsString = locationsString.substr(0, locationsString.length - 1);

                                                // SELECT LOCATION IMAGES FOR DELETE  ----------------------------
                                                var sqlDeleteLocationImages = "SELECT thumb, image FROM locations WHERE id NOT IN(" + locationsString + ")";

                                                connection.query(sqlDeleteLocationImages, function (err, rowsDeleteLocationImages) {
                                                    if (err) {
                                                        console.log('28', err);
                                                        callback(err, null);
                                                    } else {
                                                        // DELETE LOCATION IMAGES FROM S3 ----------------------------
                                                        if (rowsDeleteLocationImages.length > 0) {
                                                            var s3 = new aws.S3();
                                                            var deleteObjects = [];

                                                            for (var i = 0; i < rowsDeleteLocationImages.length; i++) {
                                                                deleteObjects.push({ Key: companyID + '/locations/' + rowsDeleteLocationImages[i].image });
                                                                deleteObjects.push({ Key: companyID + '/locations/' + rowsDeleteLocationImages[i].thumb })
                                                            }

                                                            var params = {
                                                                Bucket: bucket,
                                                                Delete: { Objects: deleteObjects }
                                                            };

                                                            s3.deleteObjects(params, function (err, data) {
                                                                if (err) {
                                                                    console.log('29', err);
                                                                    callback(err, null);
                                                                } else {
                                                                    // DELETE OTHER LOCATIONS FROM DB ----------------------------
                                                                    var sqlDeleteLocations = "DELETE FROM locations WHERE id NOT IN (" + locationsString + ")";
                                                                    connection.query(sqlDeleteLocations, function (err, rowsDeleteLocations) {
                                                                        if (err) {
                                                                            console.log('30', err);
                                                                            callback(err, null);
                                                                        } else {
                                                                            // DELETE PROJECTS FROM DB ----------------------------
                                                                            var sqlProjects = "DELETE FROM projects WHERE id NOT IN(" + projectsString + ")";

                                                                            connection.query(sqlProjects, function (err, rowsProjects) {
                                                                                if (err) {
                                                                                    console.log('31', err);
                                                                                    callback(err, null);
                                                                                } else {
                                                                                    // DELETE FLIGHTPLANS FROM DB ----------------------------
                                                                                    var sqlFlightplans = "DELETE FROM flightplans WHERE id NOT IN(" + flightplansString + ")";

                                                                                    connection.query(sqlFlightplans, function (err, rowsFlightplans) {
                                                                                        if (err) {
                                                                                            console.log('32', err);
                                                                                            callback(err, null);
                                                                                        } else {
                                                                                            // DELETE FLIGHTPLAN USERS FROM DB ----------------------------
                                                                                            var sqlFlightplanUsers = "DELETE FROM flightplan_team_users WHERE user_id NOT IN(" + usersString + ");"

                                                                                            connection.query(sqlFlightplanUsers, function (err, rowsFlightplanUsers) {
                                                                                                if (err) {
                                                                                                    console.log('33', err);
                                                                                                    callback(err, null);
                                                                                                } else {
                                                                                                    // DELETE DOCUMENTS
                                                                                                    var sqlSelectDocuments = "SELECT document_id FROM flightplan_documents WHERE flightplan_id IN(" + flightplansString + ")";
                                                                                                    connection.query(sqlSelectDocuments, function (err, rowsSelectDocuments) {
                                                                                                        if (err) {
                                                                                                            console.log('33a', err);
                                                                                                            callback(err, null);
                                                                                                        } else {
                                                                                                            var documentsString = '';
                                                                                                            for (var i = 0; i < rowsSelectDocuments.length; i++) {
                                                                                                                documentsString += rowsSelectDocuments[i].document_id + ',';
                                                                                                            }

                                                                                                            documentsString = documentsString.substr(0, documentsString.length - 1);

                                                                                                            // SELECT DOCUMENTS FOR DELETE  ----------------------------
                                                                                                            var sqlSelectDeleteDocuments = "SELECT file_name FROM documents WHERE company_id = " + connection.escape(companyID) + " AND id NOT IN(" + documentsString + ")";

                                                                                                            connection.query(sqlSelectDeleteDocuments, function (err, rowsSelectDeleteDocuments) {
                                                                                                                if (err) {
                                                                                                                    console.log('28a', err);
                                                                                                                    callback(err, null);
                                                                                                                } else {
                                                                                                                    // DELETE DOCUMENTS FROM S3 ----------------------------
                                                                                                                    if (rowsSelectDeleteDocuments.length > 0) {
                                                                                                                        var s3 = new aws.S3();
                                                                                                                        var deleteObjects = [];

                                                                                                                        for (var i = 0; i < rowsSelectDeleteDocuments.length; i++) {
                                                                                                                            deleteObjects.push({ Key: rowsSelectDeleteDocuments[i].file_name });
                                                                                                                        }

                                                                                                                        var params = {
                                                                                                                            Bucket: bucket,
                                                                                                                            Delete: { Objects: deleteObjects }
                                                                                                                        };

                                                                                                                        s3.deleteObjects(params, function (err, data) {
                                                                                                                            if (err) {
                                                                                                                                console.log('29a', err);
                                                                                                                                callback(err, null);
                                                                                                                            } else {
                                                                                                                                // DELETE DOCUMENTS FROM DB ----------------------------
                                                                                                                                var sqlDeleteDocuments = "DELETE FROM documents WHERE company_id = " + connection.escape(companyID) + " AND id NOT IN (" + documentsString + ")";
                                                                                                                                connection.query(sqlDeleteDocuments, function (err, rowsDeleteDocuments) {
                                                                                                                                    if (err) {
                                                                                                                                        console.log('30', err);
                                                                                                                                        callback(err, null);
                                                                                                                                    } else {
                                                                                                                                        connection.release();
                                                                                                                                        callback(null, 'ok');
                                                                                                                                    }
                                                                                                                                });
                                                                                                                            }
                                                                                                                        });
                                                                                                                    // NO DOCUMENTS
                                                                                                                    } else {
                                                                                                                        connection.release();
                                                                                                                        callback(null, 'ok');
                                                                                                                    }
                                                                                                                }
                                                                                                            });
                                                                                                        }
                                                                                                    });
                                                                                                }
                                                                                            });
                                                                                        }
                                                                                    });
                                                                                }
                                                                            });
                                                                        }

                                                                    });
                                                                }
                                                            });
                                                        }
                                                    }
                                                });
                                            }
                                        }
                                    });
                                // NO FLIGHT PLANS TO SAVE
                                } else {
                                    // SELECT LOCATION IMAGES FOR DELETE  ----------------------------
                                    var sqlDeleteLocationImages = "SELECT locations.id, locations.thumb, locations.image FROM locations LEFT JOIN projects ON locations.project_id = projects.id WHERE projects.company_id = " + connection.escape(companyID) + " AND  locations.project_id NOT IN(" + projectsString + ")";

                                    connection.query(sqlDeleteLocationImages, function (err, rowsDeleteLocationImages) {
                                        if (err) {
                                            console.log('34', err);
                                            callback(err, null);
                                        } else {
                                            // DELETE LOCATION IMAGES FROM S3 ----------------------------
                                            if (rowsDeleteLocationImages.length > 0) {
                                                var s3 = new aws.S3();
                                                var deleteObjects = [];

                                                for (var i = 0; i < rowsDeleteLocationImages.length; i++) {
                                                    deleteObjects.push({ Key: companyID + '/locations/' + rowsDeleteLocationImages[i].image });
                                                    deleteObjects.push({ Key: companyID + '/locations/' + rowsDeleteLocationImages[i].thumb })
                                                }

                                                var params = {
                                                    Bucket: bucket,
                                                    Delete: { Objects: deleteObjects }
                                                };

                                                s3.deleteObjects(params, function (err, data) {
                                                    if (err) {
                                                        console.log('35', err);
                                                        callback(err, null);
                                                    } else {
                                                        var locationsString = '';
                                                        for (var i = 0; i < rowsDeleteLocationImages.length; i++) {
                                                            locationsString += rowsDeleteLocationImages[i].id + ',';
                                                        }

                                                        locationsString = locationsString.substr(0, locationsString.length - 1);

                                                        // DELETE OTHER LOCATIONS FROM DB ----------------------------
                                                        var sqlDeleteLocations = "DELETE FROM locations WHERE id IN (" + locationsString + ")";
                                                        connection.query(sqlDeleteLocations, function (err, rowsDeleteLocations) {
                                                            if (err) {
                                                                console.log('36', err);
                                                                callback(err, null);
                                                            } else {
                                                                // DELETE PROJECTS FROM DB ----------------------------
                                                                var sqlProjects = "DELETE FROM projects WHERE id NOT IN(" + projectsString + ")";

                                                                connection.query(sqlProjects, function (err, rowsProjects) {
                                                                    if (err) {
                                                                        console.log('37', err);
                                                                        callback(err, null);
                                                                    } else {
                                                                        // DELETE ALL FLIGHTPLANS FROM DB FOR COMPANY ----------------------------
                                                                        var sqlFlightplans = "DELETE FROM flightplans WHERE project_id IN(SELECT id FROM projects WHERE company_id = " + connection.escape(companyID) + ")";

                                                                        connection.query(sqlFlightplans, function (err, rowsFlightplans) {
                                                                            if (err) {
                                                                                console.log('38', err);
                                                                                callback(err, null);
                                                                            } else {
                                                                                // DELETE DOCUMENTS
                                                                                var sqlSelectDocuments = "SELECT id, file_name FROM documents WHERE company_id = " + connection.escape(companyID);
                                                                                connection.query(sqlSelectDocuments, function (err, rowsSelectDocuments) {
                                                                                    if (err) {
                                                                                        console.log('33b', err);
                                                                                        callback(err, null);
                                                                                    } else {
                                                                                        
                                                                                        // DELETE DOCUMENTS FROM S3 ----------------------------
                                                                                        if (rowsSelectDocuments.length > 0) {
                                                                                            var s3 = new aws.S3();
                                                                                            var deleteObjects = [];

                                                                                            for (var i = 0; i < rowsSelectDocuments.length; i++) {
                                                                                                deleteObjects.push({ Key: rowsSelectDocuments[i].file_name });
                                                                                            }

                                                                                            var params = {
                                                                                                Bucket: bucket,
                                                                                                Delete: { Objects: deleteObjects }
                                                                                            };

                                                                                            s3.deleteObjects(params, function (err, data) {
                                                                                                if (err) {
                                                                                                    console.log('29b', err);
                                                                                                    callback(err, null);
                                                                                                } else {
                                                                                                    // DELETE DOCUMENTS FROM DB ----------------------------
                                                                                                    var sqlDeleteDocuments = "DELETE FROM documents WHERE company_id = " + connection.escape(companyID);
                                                                                                    connection.query(sqlDeleteDocuments, function (err, rowsDeleteDocuments) {
                                                                                                        if (err) {
                                                                                                            console.log('30', err);
                                                                                                            callback(err, null);
                                                                                                        } else {
                                                                                                            connection.release();
                                                                                                            callback(null, 'ok');
                                                                                                        }
                                                                                                    });
                                                                                                }
                                                                                            });
                                                                                        // NO DOCUMENTS
                                                                                        } else {
                                                                                            connection.release();
                                                                                            callback(null, 'ok');
                                                                                        }
                                                                                    }
                                                                                });
                                                                            }
                                                                        });
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    }
                                                });
                                            // NO LOCATION IMAGES
                                            } else {
                                                // DELETE DOCUMENTS
                                                var sqlSelectDocuments = "SELECT id, file_name FROM documents WHERE company_id = " + connection.escape(companyID);
                                                connection.query(sqlSelectDocuments, function (err, rowsSelectDocuments) {
                                                    if (err) {
                                                        console.log('33b', err);
                                                        callback(err, null);
                                                    } else {
                                                        
                                                        // DELETE DOCUMENTS FROM S3 ----------------------------
                                                        if (rowsSelectDocuments.length > 0) {
                                                            var s3 = new aws.S3();
                                                            var deleteObjects = [];

                                                            for (var i = 0; i < rowsSelectDocuments.length; i++) {
                                                                deleteObjects.push({ Key: rowsSelectDocuments[i].file_name });
                                                            }

                                                            var params = {
                                                                Bucket: bucket,
                                                                Delete: { Objects: deleteObjects }
                                                            };

                                                            s3.deleteObjects(params, function (err, data) {
                                                                if (err) {
                                                                    console.log('29b', err);
                                                                    callback(err, null);
                                                                } else {
                                                                    // DELETE DOCUMENTS FROM DB ----------------------------
                                                                    var sqlDeleteDocuments = "DELETE FROM documents WHERE company_id = " + connection.escape(companyID);
                                                                    connection.query(sqlDeleteDocuments, function (err, rowsDeleteDocuments) {
                                                                        if (err) {
                                                                            console.log('30', err);
                                                                            callback(err, null);
                                                                        } else {
                                                                            connection.release();
                                                                            callback(null, 'ok');
                                                                        }
                                                                    });
                                                                }
                                                            });
                                                        // NO DOCUMENTS
                                                        } else {
                                                            connection.release();
                                                            callback(null, 'ok');
                                                        }
                                                    }
                                                });
                                            }
                                        }
                                    });
                                }
                            // NO PROJECTS TO SAVE
                            } else {
                                // SELECT LOCATION IMAGES FOR DELETE  ----------------------------
                                var sqlDeleteLocationImages = "SELECT locations.id, locations.thumb, locations.image FROM locations LEFT JOIN projects ON locations.project_id = projects.id WHERE projects.company_id = " + connection.escape(companyID);

                                connection.query(sqlDeleteLocationImages, function (err, rowsDeleteLocationImages) {
                                    if (err) {
                                        console.log('39', err);
                                        callback(err, null);
                                    } else {
                                        // DELETE LOCATION IMAGES FROM S3 ----------------------------
                                        if (rowsDeleteLocationImages.length > 0) {
                                            var s3 = new aws.S3();
                                            var deleteObjects = [];

                                            for (var i = 0; i < rowsDeleteLocationImages.length; i++) {
                                                deleteObjects.push({ Key: companyID + '/locations/' + rowsDeleteLocationImages[i].image });
                                                deleteObjects.push({ Key: companyID + '/locations/' + rowsDeleteLocationImages[i].thumb })
                                            }

                                            var params = {
                                                Bucket: bucket,
                                                Delete: { Objects: deleteObjects }
                                            };

                                            s3.deleteObjects(params, function (err, data) {
                                                if (err) {
                                                    console.log('40', err);
                                                    callback(err, null);
                                                } else {
                                                    var locationsString = '';
                                                    for (var i = 0; i < rowsDeleteLocationImages.length; i++) {
                                                        locationsString += rowsDeleteLocationImages[i].id + ',';
                                                    }

                                                    locationsString = locationsString.substr(0, locationsString.length - 1);

                                                    // DELETE OTHER LOCATIONS FROM DB ----------------------------
                                                    var sqlDeleteLocations = "DELETE FROM locations WHERE id IN (" + locationsString + ")";
                                                    connection.query(sqlDeleteLocations, function (err, rowsDeleteLocations) {
                                                        if (err) {
                                                            console.log('41', err);
                                                            callback(err, null);
                                                        } else {
                                                            // DELETE PROJECTS FROM DB ----------------------------
                                                            var sqlProjects = "DELETE FROM projects WHERE company_id = " + connection.escape(companyID);

                                                            connection.query(sqlProjects, function (err, rowsProjects) {
                                                                if (err) {
                                                                    console.log('42', err);
                                                                    callback(err, null);
                                                                } else {
                                                                    // DELETE DOCUMENTS
                                                                    var sqlSelectDocuments = "SELECT id, file_name FROM documents WHERE company_id = " + connection.escape(companyID);
                                                                    connection.query(sqlSelectDocuments, function (err, rowsSelectDocuments) {
                                                                        if (err) {
                                                                            console.log('33b', err);
                                                                            callback(err, null);
                                                                        } else {
                                                                            
                                                                            // DELETE DOCUMENTS FROM S3 ----------------------------
                                                                            if (rowsSelectDocuments.length > 0) {
                                                                                var s3 = new aws.S3();
                                                                                var deleteObjects = [];

                                                                                for (var i = 0; i < rowsSelectDocuments.length; i++) {
                                                                                    deleteObjects.push({ Key: rowsSelectDocuments[i].file_name });
                                                                                }

                                                                                var params = {
                                                                                    Bucket: bucket,
                                                                                    Delete: { Objects: deleteObjects }
                                                                                };

                                                                                s3.deleteObjects(params, function (err, data) {
                                                                                    if (err) {
                                                                                        console.log('29b', err);
                                                                                        callback(err, null);
                                                                                    } else {
                                                                                        // DELETE DOCUMENTS FROM DB ----------------------------
                                                                                        var sqlDeleteDocuments = "DELETE FROM documents WHERE company_id = " + connection.escape(companyID);
                                                                                        connection.query(sqlDeleteDocuments, function (err, rowsDeleteDocuments) {
                                                                                            if (err) {
                                                                                                console.log('30', err);
                                                                                                callback(err, null);
                                                                                            } else {
                                                                                                connection.release();
                                                                                                callback(null, 'ok');
                                                                                            }
                                                                                        });
                                                                                    }
                                                                                });
                                                                            // NO DOCUMENTS
                                                                            } else {
                                                                                connection.release();
                                                                                callback(null, 'ok');
                                                                            }
                                                                        }
                                                                    });
                                                                }
                                                            });
                                                        }
                                                    });
                                                }
                                            });
                                        // NO LOCATION IMAGES
                                        }else{
                                             // DELETE DOCUMENTS
                                            var sqlSelectDocuments = "SELECT id, file_name FROM documents WHERE company_id = " + connection.escape(companyID);
                                            connection.query(sqlSelectDocuments, function (err, rowsSelectDocuments) {
                                                if (err) {
                                                    console.log('33b', err);
                                                    callback(err, null);
                                                } else {
                                                    
                                                    // DELETE DOCUMENTS FROM S3 ----------------------------
                                                    if (rowsSelectDocuments.length > 0) {
                                                        var s3 = new aws.S3();
                                                        var deleteObjects = [];

                                                        for (var i = 0; i < rowsSelectDocuments.length; i++) {
                                                            deleteObjects.push({ Key: rowsSelectDocuments[i].file_name });
                                                        }

                                                        var params = {
                                                            Bucket: bucket,
                                                            Delete: { Objects: deleteObjects }
                                                        };

                                                        s3.deleteObjects(params, function (err, data) {
                                                            if (err) {
                                                                console.log('29b', err);
                                                                callback(err, null);
                                                            } else {
                                                                // DELETE DOCUMENTS FROM DB ----------------------------
                                                                var sqlDeleteDocuments = "DELETE FROM documents WHERE company_id = " + connection.escape(companyID);
                                                                connection.query(sqlDeleteDocuments, function (err, rowsDeleteDocuments) {
                                                                    if (err) {
                                                                        console.log('30', err);
                                                                        callback(err, null);
                                                                    } else {
                                                                        connection.release();
                                                                        callback(null, 'ok');
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    // NO DOCUMENTS
                                                    } else {
                                                        connection.release();
                                                        callback(null, 'ok');
                                                    }
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            });
        }
    });
}
    
module.exports.deleteAccount = function (plan, companyID, callback) {
    var stripe = require('stripe')(config.stripe.secretKey);

    general.emptyS3Bucket(bucket, companyID, function (err, result) {
        if (err) {
             console.log(err);
        } else {
            db.getConnection(function (err, connection) {
                connectionError(err, connection);

                if (plan == 'free') {
                    var sqlUpdateCompany = "UPDATE companies SET deleted = 1 WHERE id = " + connection.escape(companyID);
                    connection.query(sqlUpdateCompany, function (err, rowsUpdateCompany) {
                        connection.release();
                        if (err) {
                            console.log(err);
                            callback(err, null);
                        } else {
                            callback(null, 'ok');
                        }
                    });
                } else if (plan == 'professional') {
                    var sqlSubscription = "SELECT subscriptions.id, subscriptions.subscription_id FROM subscriptions LEFT JOIN companies ON companies.subscription_id = subscriptions.id WHERE companies.id = " + connection.escape(companyID);

                    connection.query(sqlSubscription, function (err, rowsSubscription) {
                        if (err) {
                            console.log(err);
                            callback(err, null);
                        } else {
                            // professional plan
                            if (rowsSubscription.length > 0) {
                                stripe.subscriptions.del(rowsSubscription[0].subscription_id, function (err, confirmation) {
                                    if (confirmation.status == 'canceled') {
                                        var sqlUpdateSubscription = "UPDATE subscriptions SET deleted = 1 WHERE id = " + connection.escape(rowsSubscription[0].id);

                                        connection.query(sqlUpdateSubscription, function (err, rowsUpdateSubscription) {
                                            if (err) {
                                                console.log(err);
                                                callback(err, null);
                                            } else {
                                                var sqlUpdateCompany = "UPDATE companies SET deleted = 1 WHERE id = " + connection.escape(companyID);

                                                connection.query(sqlUpdateCompany, function (err, rowsUpdateCompany) {
                                                    connection.release();

                                                    if (err) {
                                                        console.log(err);
                                                        callback(err, null);
                                                    } else {
                                                        callback(null, 'ok');
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    });
                }
            });
        }
    });
}


module.exports.getPlanNumbers = function (plan, companyID, callback) {
    var result = {};

    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sqlStorage = "SELECT SUM(size) AS size FROM documents WHERE company_id = " + connection.escape(companyID);

        connection.query(sqlStorage, function (err, rowsStorage) {
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                var size = rowsStorage[0].size;
                var humanSize = general.humanFileSize(size);

                result.storageSize = size;
                result.storageHumanSize = humanSize;


                if (plan == 'free') {
                    result.storagePercent = (100 * size / plans.free.storageInBytes).toFixed(2);
                    result.availableDocStorageSpace = plans.free.storageInBytes - size;
                } else if (plan == 'professional') {
                    result.storagePercent = (100 * size / plans.professional.storageInBytes).toFixed(2);
                    result.availableDocStorageSpace = plans.professional.storageInBytes - size;
                } else if (plan == 'enterprise') {
                    result.storagePercent = (100 * size / plans.enterprise.storageInBytes).toFixed(2);
                    result.availableDocStorageSpace = plans.enterprise.storageInBytes - size;
                }

                var sqlUsers = "SELECT COUNT(id) AS num FROM users WHERE company_id = " + connection.escape(companyID) + " AND archived = 0";

                connection.query(sqlUsers, function (err, rowsUsers) {
                    if (err) {
                        console.log(err);
                        callback(err, null);
                    } else {
                        result.numOfUsers = rowsUsers[0].num;
                        result.numOfUsersPercent = result.numOfUsers * 100 / plans[plan].users;

                        var sqlProjects = "SELECT COUNT(id) AS num FROM projects WHERE company_id = " + connection.escape(companyID);

                        connection.query(sqlProjects, function (err, rowsProjects) {
                            if (err) {
                                console.log(err);
                                callback(err, null);
                            } else {
                                result.numOfProjects = rowsProjects[0].num;
                                result.numOfProjectsPercent = result.numOfProjects * 100 / plans[plan].projects;

                                var sqlFlights = "SELECT COUNT(id) AS num FROM flightplans WHERE project_id IN (SELECT * FROM (SELECT id FROM projects WHERE company_id = " + connection.escape(companyID) + " ) AS derived_table)";

                                connection.query(sqlFlights, function (err, rowsFlights) {
                                    connection.release();
                                    if (err) {
                                        console.log(err);
                                        callback(err, null);
                                    } else {
                                        result.numOfFlights = rowsFlights[0].num;
                                        result.numOfFlightsPercent = result.numOfFlights * 100 / plans[plan].flights;

                                        callback(null, result);
                                    }
                                })
                            }
                        })
                    }
                })
            }
        });
    });
}



module.exports.getArchivedUsers = function (companyID, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "SELECT id, name, google_display_name FROM users WHERE archived = 1 AND company_id = " + connection.escape(companyID);

        connection.query(sql, function (err, rows) {
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                connection.release();
                callback(null, rows);
            }
        });
    });
}

module.exports.activateUsers = function (userIDs, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var ids = '';
        for (var i = 0; i < userIDs.length; i++) {
            ids += userIDs[i] + ',';
        }
        ids = ids.substr(0, ids.length - 1);

        var sql = "UPDATE users SET archived = 0 WHERE id IN (" + ids + ")";

        connection.query(sql, function (err, rows) {
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                connection.release();
                callback(null, rows);
            }
        });
    });
}

module.exports.activateUser = function (id, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "UPDATE users SET archived = 0 WHERE id = " + connection.escape(id);
        console.log(sql);

        connection.query(sql, function (err, rows) {
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                connection.release();
                callback(null, rows.affectedRows);
            }
        });
    });
}

module.exports.checkActiveUsers = function (companyID, plan, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "SELECT COUNT(id) AS num FROM users WHERE archived = 0 AND company_id = " + connection.escape(companyID);

        connection.query(sql, function (err, rows) {
            if (err) {
                callback(err, null);
            } else {
                connection.release();
                if (plan == 'free') {
                    if (rows[0].num < config.plans.free.users) {
                        callback(null, 'ok');
                    } else {
                        callback(null, 'limit');
                    }
                } else if (plan == 'professional') {
                    if (rows[0].num < config.plans.professional.users) {
                        callback(null, 'ok');
                    } else {
                        callback(null, 'limit');
                    }
                } else {
                    callback(null, 'ok');
                }
            }
        });
    });
}

module.exports.failedSubscription = function (companyID, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var date = moment();
        date = date.subtract(1, "days");
        date = date.format(config.config.mysqlTimeFormat);

        var sql = "UPDATE companies SET subscription_end = " + connection.escape(date) + " WHERE id = " + connection.escape(companyID);

        connection.query(sql, function (err, rows) {
            if (err) {
                callback(err, null);
            } else {
                callback(null, true);
            }
        });
    });
}


module.exports.checkSubscription = function(email, callback){
    db.getConnection(function(err, connection){
        connectionError(err, connection);

        var sql = "SELECT companies.subscription_end FROM companies WHERE id = (SELECT company_id FROM users WHERE email = " + connection.escape(email) + " OR google_email = " + connection.escape(email) + ")";

        connection.query(sql, function(err, rows){
            if(err){
                callback(err, null);
            }else{
                if(moment(rows[0].subscription_end) < moment()){
                    callback(null, 'no');
                }else{
                    callback(null, 'ok');
                }
            }
        })
    })
}

module.exports.getCompanyFromSubscriptionID = function (subscriptionID, customerID, callback) {
    db.getConnection(function(err, connection){
        connectionError(err, connection);

        var sql = "SELECT companies.id, companies.name, companies.email FROM companies LEFT JOIN subscriptions ON companies.subscription_id = subscriptions.id WHERE subscriptions.subscription_id = " + connection.escape(subscriptionID) + " AND subscriptions.customer_id = " + connection.escape(customerID);

        connection.query(sql, function(err, rows){
            if(err){
                callback(err, null);
            }else{
                callback(null, { id:rows[0].id, name: rows[0].name, email: rows[0].email });
            }
        })
    })
}

module.exports.updateSubscriptionDate = function (customer, newDate, callback) {
    db.getConnection(function(err, connection){
        connectionError(err, connection);

        var sqlSubscription = "SELECT id FROM subscriptions WHERE customer_id = " + connection.escape(customer);

        connection.query(sqlSubscription, function(err, rowsSubscription){
            if(err){
                callback(err, null);
            } else {
                var sqlUpdate = "UPDATE companies SET subscription_end = " + connection.escape(newDate) + " WHERE subscription_id = " + connection.escape(rowsSubscription[0].id);
                connection.query(sqlUpdate, function (err, rowsUpdate) {
                    if (err) {
                        callback(err, null);
                    } else {
                        if (rowsUpdate.affectedRows == 1) {
                            callback(null, 'ok');
                        } else {
                            callback(null, 'error');
                        }
                    }
                });
            }
        })
    })
}