var db = require('../config/db').pool;
var connectionError = require('../config/db').connectionError;

module.exports.getVehicleByID = function (id, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "SELECT * FROM vehicles WHERE id = '" + id + "' LIMIT 1;";

        connection.query(sql, function (err, rows) {
            connection.release();
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(null, rows[0]);
            }
        });
    });
};

module.exports.getAllVehiclesForCompany = function (id, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "SELECT vehicles.id, vehicles.name, vehicles.type, vehicles.manufacturer, vehicles.model, vehicles.serial_number, vehicles.faa_aircraft_id, vehicles.status, vehicles.notes, statuses.name AS status_name, statuses.class AS status_class FROM vehicles JOIN statuses ON statuses.id = vehicles.status WHERE company_id = '" + id + "';";

        connection.query(sql, function (err, rows) {
            connection.release();
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(null, rows);
            }
        });
    });
};

module.exports.getAllVehiclesForFlightplan = function (id, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "SELECT vehicle_id, unit_id FROM flightplan_vehicles WHERE flightplan_id = " + id + ";";

        connection.query(sql, function (err, rows) {
            connection.release();
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(null, rows);
            }
        });
    });
};

module.exports.add = function (data, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "INSERT INTO vehicles(company_id, name, type, manufacturer, model, serial_number, status, notes, faa_aircraft_id) VALUES ('" + data.company_id + "', '" + data.name + "', '" + data.type + "', '" + data.manufacturer + "', '" + data.model + "', '" + data.serial_number + "', '" + data.status + "', '" + data.notes + "', '" + data.faa_aircraft_id + "');";

        connection.query(sql, function (err, rows) {
            connection.release();
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(null, rows.insertId);
            }
        });
    });
}

module.exports.edit = function (id, data, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "UPDATE vehicles SET name = '" + data.name + "', type = '" + data.type + "', manufacturer = '" + data.manufacturer + "', model = '" + data.model + "', serial_number = '" + data.serial_number + "', status = '" + data.status + "', notes = '" + data.notes + "', faa_aircraft_id = '" + data.faa_aircraft_id + "' WHERE id = '" + id + "';";

        connection.query(sql, function (err, rows) {
            connection.release();
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(null, rows.affectedRows);
            }
        });
    });
}

module.exports.delete = function (id, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "DELETE FROM vehicles WHERE id = '" + id + "';";

        connection.query(sql, function (err, rows) {
            connection.release();
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(null, rows.affectedRows);
            }
        });
    });
}
