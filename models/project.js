var db = require('../config/db').pool;
var connectionError = require('../config/db').connectionError;
var moment = require('moment');
var config = require('../config/config').config;
var general = require('../helpers/general');

module.exports.getProjectByID = function (id, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "SELECT * FROM projects WHERE id = '" + id + "' LIMIT 1;";

        connection.query(sql, function (err, rows) {
            connection.release();
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(null, rows[0]);
            }
        });
    });
};

module.exports.getProjectName = function (id, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "SELECT name FROM projects WHERE id = '" + id + "'";

        connection.query(sql, function (err, rows) {
            connection.release();
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(null, rows[0].name);
            }
        });
    });
};

module.exports.getAllProjectsForCompany = function (id, callback) {
    'use strict';
    var result = [];

    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "SELECT projects.id, projects.name, projects.notes, projects.color, (SELECT COUNT(flightplans.id) FROM flightplans WHERE projects.id = flightplans.project_id) AS numOfFlightplans FROM projects  WHERE company_id = " + id;

        connection.query(sql, function (err, rows) {
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                if (rows.length > 0) {
                    for (let i = 0; i < rows.length; i++) {
                        var sqlFlightplans = "SELECT flightplans.name, flightplans.start_time, flightplans.end_time, locations.center_latitude, locations.center_longitude, locations.name AS location_name, locations.path_thumb AS snapshot FROM flightplans LEFT JOIN locations ON flightplans.location_id = locations.id WHERE flightplans.project_id = " + rows[i].id;
                        connection.query(sqlFlightplans, function (err, rowsFlightplans) {
                            if (err) {
                                console.log(err);
                                callback(err, null);
                            } else {
                                rows[i].flightplans = {};
                                rows[i].flightplans = rowsFlightplans;
                                if (i == rows.length - 1) {
                                    connection.release();
                                    callback(null, rows);
                                }

                            }
                        })
                    }
                } else {
                    connection.release();
                    callback(null, rows);
                }
            }
        });
    });
};


module.exports.getAllProjectsForUser = function (id, callback) {
    'use strict';
    var result = [];

    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "SELECT projects.id, projects.name, projects.notes, projects.color, (SELECT COUNT(flightplans.id) FROM flightplans WHERE projects.id = flightplans.project_id) AS numOfFlightplans FROM projects WHERE (id IN (SELECT project_id FROM flightplans WHERE id IN (SELECT flightplan_id FROM flightplan_teams WHERE id IN (SELECT team_id FROM flightplan_team_users WHERE user_id = " + connection.escape(id) + ")))) OR (projects.created_by = " + connection.escape(id) + ")";

        connection.query(sql, function (err, rows) {
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                if (rows.length > 0) {
                    for (let i = 0; i < rows.length; i++) {
                        var sqlFlightplans = "SELECT flightplans.name, flightplans.start_time, flightplans.end_time, locations.center_latitude, locations.center_longitude, locations.name AS location_name, locations.path_thumb AS snapshot FROM flightplans LEFT JOIN locations ON flightplans.location_id = locations.id WHERE flightplans.project_id = " + rows[i].id + " AND flightplans.id IN (SELECT flightplan_id FROM flightplan_teams WHERE id IN (SELECT team_id FROM flightplan_team_users WHERE user_id = " + connection.escape(id) + "))";
                        
                        connection.query(sqlFlightplans, function (err, rowsFlightplans) {
                            if (err) {
                                console.log(err);
                                callback(err, null);
                            } else {
                                rows[i].flightplans = {};
                                rows[i].flightplans = rowsFlightplans;
                                if (i == rows.length - 1) {
                                    connection.release();
                                    callback(null, rows);
                                }

                            }
                        })
                    }
                } else {
                    var sqlOnlyUserProjects = "SELECT projects.id, projects.name, projects.notes, projects.color FROM projects WHERE created_by = " + connection.escape(id);
                    
                    connection.query(sqlOnlyUserProjects, function (err, rowsOnlyUserProjects) {
                        if (err) {
                            console.log(err);
                            callback(err, null);
                        } else {
                            for (var i = 0; i < rowsOnlyUserProjects.length; i++){
                                rowsOnlyUserProjects[i].flightplans = {};
                            }
                            
                            connection.release();
                            callback(null, rowsOnlyUserProjects);

                        }
                    })
                }
            }
        });
    });
};


module.exports.add = function (userID, data, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var color = general.getRandomColor();
        var sql = "INSERT INTO projects(company_id, name, notes, color, created_by) VALUES (" + connection.escape(data.company_id) + ", " + connection.escape(data.name) + ", " + connection.escape(data.notes) + ", " + connection.escape(color)  + ", " + connection.escape(userID) + ")";

        connection.query(sql, function (err, rows) {
            connection.release();
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(null, rows.insertId);
            }
        });
    });
}

module.exports.edit = function (id, data, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "UPDATE projects SET name = '" + data.name + "', notes = '" + data.notes + "' WHERE id = '" + id + "';";

        connection.query(sql, function (err, rows) {
            connection.release();
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(null, rows.affectedRows);
            }
        });
    });
}

module.exports.delete = function (id, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "DELETE FROM projects WHERE id = '" + id + "';";

        connection.query(sql, function (err, rows) {
            connection.release();
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(null, rows.affectedRows);
            }
        });
    });
}

module.exports.getLocations = function (projectID, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "SELECT center_latitude, center_longitude FROM locations WHERE project_id = " + connection.escape(projectID);

        connection.query(sql, function (err, rows) {
            connection.release();
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(null, rows);
            }
        })
    })
}