var db = require('../config/db').pool;
var connectionError = require('../config/db').connectionError;
var moment = require('moment');
var config = require('../config/config').config;

module.exports.getAllTeamMembersForCompany = function (id, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "SELECT users.id, users.archived, users.provider, users.name, users.email, users.mobile, users.photo, users.active, users.invited, users.google_display_name, users.google_email, users.google_photo, users.permission_id, user_permissions.name AS permission, user_permissions.class AS permission_class FROM users LEFT JOIN user_permissions ON users.permission_id = user_permissions.id WHERE users.company_id = " + connection.escape(id) + "  ORDER BY users.permission_id ASC";
        
        connection.query(sql, function (err, rows) {
            connection.release();
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                callback(null, rows);
            }
        });
    });
}

module.exports.getTeamMember = function (id, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sql = "SELECT users.provider, users.name, users.email, users.mobile, users.photo, users.active, users.invited, users.google_display_name, users.google_email, users.google_photo, user_permissions.name AS permission, user_permissions.class AS permission_class, users.permission_id FROM users LEFT JOIN user_permissions ON users.permission_id = user_permissions.id WHERE users.id = " + id + " LIMIT 1";

        connection.query(sql, function (err, rows) {
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                var result = {};
                result.user = rows[0];

                var sqlFlightplans = "SELECT user_roles.name AS role, user_roles.class AS role_class, flightplan_team_users.unit_id, flightplan_teams.flightplan_id, flightplans.name, flightplans.start_time, flightplans.end_time FROM flightplan_team_users LEFT JOIN flightplan_teams ON flightplan_team_users.team_id = flightplan_teams.id LEFT JOIN flightplans ON flightplan_teams.flightplan_id = flightplans.id  LEFT JOIN user_roles ON flightplan_team_users.role_id = user_roles.id WHERE flightplan_team_users.user_id =" + connection.escape(id);
                
                connection.query(sqlFlightplans, function (err, rowsFlightplans) {
                    if (err) {
                        console.log(err);
                        callback(err, null);
                    } else {
                        connection.release();
                        result.flightplans = rowsFlightplans;
                        callback(null, result);
                    }
                })
                
            }
        });
    });
}

module.exports.delete = function (id, callback) {
    db.getConnection(function (err, connection) {
        connectionError(err, connection);

        var sqlDeleteProjects = "DELETE FROM projects WHERE created_by = " + connection.escape(id);
        connection.query(sqlDeleteProjects, function (err, rowsProjects) {
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                var sqlUser = "DELETE FROM users WHERE id =" + connection.escape(id);
                
                connection.query(sqlUser, function (err, rowsUser) {
                    if (err) {
                        console.log(err);
                        callback(err, null);
                    } else {
                        connection.release();
                        callback(null, rowsUser.affectedRows);
                    }
                })
            }
        });
    });
}